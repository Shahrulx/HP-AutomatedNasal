﻿using HardwareInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace MachineUserControl
{
    /// <summary>
    /// Interaction logic for CylinderControl.xaml
    /// </summary>
    public partial class CylinderControl : UserControl
    {
        private Dictionary<string, ICylinder> cylinders;
        private List<Tuple<Button, Button, Button, TextBox, TextBox, ICylinder>> controlList;
        bool stop = false;
        DispatcherTimer timer1 = null;

        public bool Initialize { set; get; }
        public CylinderControl()
        {
            Initialize = false;

            InitializeComponent();


            timer1 = new DispatcherTimer();
            timer1.Tick += new EventHandler(timer1_Tick);
        }

        public void Init(Dictionary<string, ICylinder> cylinders)
        {
            Initialize = true;
            int offset = 0;
            this.cylinders = cylinders;
            controlList = new List<Tuple<Button, Button, Button, TextBox, TextBox, ICylinder>>();
            foreach (var cylinderPair in cylinders)
            {
                var name = cylinderPair.Key;
                var cylinder = cylinderPair.Value;

                var extendButton = CreateButton(cylinder.ExtendName);
                extendButton.Tag = cylinder;
                extendButton.Click += btnExtend_Click;//handle the handler for each icons);

                var retractButton = CreateButton(cylinder.RetractName);
                retractButton.Tag = cylinder;
                retractButton.Click += btnRetract_Click;//this.btnRetract_Click);

                var InfoButton = CreateButton("Info  ");
                InfoButton.Tag = cylinder;
                InfoButton.Click += btnInfo_Click;//btnInfo_Click);
                //InfoButton.Click += btnOutput_Click;//handle the handler for each icons


                var StatustextBox = CreateTextBox("                        ");
                var nameTextBox = CreateTextBox(name);

                StackPanel stackPanelHori = new StackPanel();
                stackPanelHori.Orientation = Orientation.Horizontal;
                stackPanelHori.Children.Add(extendButton);
                stackPanelHori.Children.Add(retractButton);
                stackPanelHori.Children.Add(InfoButton);
                stackPanelHori.Children.Add(StatustextBox);
                stackPanelHori.Children.Add(nameTextBox);
                stackPanelVerti.Children.Add(stackPanelHori);
                offset += 1;
                //this.tableLayoutPanel1.RowCount += 1;
                controlList.Add(Tuple.Create(extendButton, retractButton, InfoButton,
                    StatustextBox, nameTextBox, cylinder));
            }
        }


        private void SetMaxSize()
        {
            double extendMaxWidth = 0;
            double retractMaxWidth = 0;
            double statusMaxWidth = 0;
            double nameMaxWidth = 0;
            foreach (var items in controlList)
            {
                var extendButtons = items.Item1;
                var retractButtons = items.Item2;
                var infoButtons = items.Item3;
                var statusTextBox = items.Item4;
                var nameTextBox = items.Item5;
                if (extendButtons.ActualWidth > extendMaxWidth)
                    extendMaxWidth = extendButtons.ActualWidth;

                if (retractButtons.ActualWidth > retractMaxWidth)
                    retractMaxWidth = retractButtons.ActualWidth;

                if (statusTextBox.ActualWidth > statusMaxWidth)
                    statusMaxWidth = statusTextBox.ActualWidth;

                if (nameTextBox.ActualWidth > nameMaxWidth)
                    nameMaxWidth = nameTextBox.ActualWidth;
            }

            foreach (var items in controlList)
            {
                var extendButtons = items.Item1;
                var retractButtons = items.Item2;
                var infoButtons = items.Item3;
                var statusTextBox = items.Item4;
                var nameTextBox = items.Item5;
                extendButtons.Height = extendButtons.Height + 10;
                extendButtons.Width = extendMaxWidth + 15;

                retractButtons.Height = retractButtons.Height + 10;
                retractButtons.Width = retractMaxWidth + 15;

                infoButtons.Height = infoButtons.Height + 10;
                infoButtons.Width = infoButtons.Width + 15;

                statusTextBox.Height = statusTextBox.Height + 10;
                statusTextBox.Width = statusTextBox.Width + 15;

                nameTextBox.Height = nameTextBox.Height + 10;
                nameTextBox.Width = nameTextBox.Width + 15;
            }
        }

        void Size(double width,double height,UserControl uIElement)
        {
            uIElement.Height = height;
            uIElement.Width = width;
        }


        private TextBox CreateTextBox(string text)
        {
            var textBox = new TextBox
            {
                //var textSize = TextRenderer.MeasureText(text, this.Font);
                //textBox.Name = text;
                //textBox.Size = new Size(textSize.Width + 15, textSize.Height + 10);
                //textBox.ReadOnly = true;
                Text = text
            };
            //textBox.BorderStyle = 0;
            //textBox.BackColor = Color.White;
            //textBox.TabStop = false;
            return textBox;
        }

        private Button CreateButton(string label)
        {
            var button = new Button
            {
                //var textSize = TextRenderer.MeasureText(label, this.Font);
                //button.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
                //button.Name = label;
                Content = label
            };
            //button.Size = textSize;
            //button.UseVisualStyleBackColor = true;
            //button.BackColor = Color.White;
            return button;
        }

        private void btnRetract_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            var cylinder = btn.Tag as ICylinder;
            cylinder.Retract(wait: false);
        }

        private void btnExtend_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            var cylinder = btn.Tag as ICylinder;
            cylinder.Extend(wait: false);
        }
        private void btnInfo_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            var cylinder = btn.Tag as ICylinder;
            string info = "";
            info += cylinder.OutputInformation + Environment.NewLine;
            info += cylinder.SensorInformation;
            MessageBox.Show(info);
        }

        public void UpdateCylinder()
        {
            foreach (var items in controlList)
            {
                var extendButtons = items.Item1;
                var retractButtons = items.Item2;
                var statusTextBox = items.Item4;
                var cylinder = items.Item6;

                extendButtons.Background = cylinder.CurrentState == extendButtons.Content.ToString() ? Brushes.Green : Brushes.White;
                retractButtons.Background = cylinder.CurrentState == retractButtons.Content.ToString() ? Brushes.Green : Brushes.White;
                statusTextBox.Text = cylinder.SensorStatus;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.IsEnabled = false;
            UpdateCylinder();
            timer1.IsEnabled = true;
            if (stop)
                timer1.IsEnabled = false;
        }

        public void Start(int interval = 200)
        {
            stop = false;
            timer1.Interval = new TimeSpan(0, 0, 0, 0, interval);
            timer1.IsEnabled = true;

        }

        public void Stop()
        {
            stop = true;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            SetMaxSize();
        }
    }
}

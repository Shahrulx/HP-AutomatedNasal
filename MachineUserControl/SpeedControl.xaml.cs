﻿using HardwareInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MachineUserControl
{

    public enum MotorSpeedType
    {
        X,
        XY,
        XYZ
    }
    [System.ComponentModel.DefaultBindingProperty("Speed")]

    public partial class SpeedControl : UserControl
    {

        IMotor motor;
        MotorSpeedType SpeedType;
        public SpeedControl()
        {
            InitializeComponent();
        }

        public void Init(IMotor motor, object dataSource, string key, MotorSpeedType speedType)
        {
            groupBox.Content = key;
            SpeedType = speedType;
            this.motor = motor;
            //this.DataBindings.Add("Speed", dataSource, key);

            //if (motor.UnitInmm)
            //{
            //    unit4.Text = "mm";
            //    unit5.Text = "mm";
            //    unit2.Text = "mm";
            //    unit3.Text = "mm";
            //}

            //if (motor.MotorType == MotorType.AMONET)
            //{
            //    unit4.Text = "sec";
            //    unit5.Text = "sec";
            //    label13.Text = "RampUp";
            //    label17.Text = "RampDwn";
            //}
            //      this.SetBinding(
        }

        public Tuple<double, double, double, double> Speed
        {
            get
            {
                string item1, item2, item3, item4;
                item1 = txtStartVelocity.Text.Trim();
                item2 = txtMaxVelocity.Text.Trim();
                item3 = txtRampUpTime.Text.Trim();
                item4 = txtRampDownTime.Text.Trim();
                //    if (item1 == "") { item1 = "0"; }
                //    if (item2 == "") { item2 = "0"; }
                //    if (item3 == "") { item3 = "0"; }
                //    if (item4 == "") { item4 = "0"; }
                Tuple<double, double, double, double> value = null;
                if (motor != null && motor.MotorType == MotorType.COMMON_MOTION)
                {
                    value = new Tuple<double, double, double, double>(GetPulseValueDbl(item1),
                                         GetPulseValueDbl(item2),
                                         GetPulseValueDbl(item3),
                                         GetPulseValueDbl(item4));
                }
                else if (motor != null && motor.MotorType == MotorType.AMONET)
                {
                    value = new Tuple<double, double, double, double>(GetPulseValueDbl(item1),
                                         GetPulseValueDbl(item2),
                                         double.Parse(item3),
                                         double.Parse(item4));
                }
                else //motor is null or other type
                {
                    value = new Tuple<double, double, double, double>(GetPulseValueDbl(item1),
                                         GetPulseValueDbl(item2),
                                         double.Parse(item3),
                                         double.Parse(item4));
                }
                if (motor != null)
                {
                    if (SpeedType == MotorSpeedType.X)
                        motor.RunSpeedCfg = new ISpeedCfg(value.Item1, value.Item2, value.Item3, value.Item4);
                    if (SpeedType == MotorSpeedType.XY)
                        motor.XYRunSpeedCfg = new ISpeedCfg(value.Item1, value.Item2, value.Item3, value.Item4);
                    if (SpeedType == MotorSpeedType.XYZ)
                        motor.XYZRunSpeedCfg = new ISpeedCfg(value.Item1, value.Item2, value.Item3, value.Item4);
                }
                return value;
            }
            set
            {
                if (value == null)
                {
                    return;
                }
                if (motor != null && motor.MotorType == MotorType.COMMON_MOTION)
                {
                    txtStartVelocity.Text = GetPulseText(value.Item1);
                    txtMaxVelocity.Text = GetPulseText(value.Item2);
                    txtRampUpTime.Text = GetPulseText(value.Item3);
                    txtRampDownTime.Text = GetPulseText(value.Item4);
                }
                else if (motor != null && motor.MotorType == MotorType.AMONET)
                {
                    txtStartVelocity.Text = GetPulseText(value.Item1);
                    txtMaxVelocity.Text = GetPulseText(value.Item2);
                    txtRampUpTime.Text = value.Item3.ToString();
                    txtRampDownTime.Text = value.Item4.ToString();
                }
                else
                {
                    txtStartVelocity.Text = GetPulseText(value.Item1);
                    txtMaxVelocity.Text = GetPulseText(value.Item2);
                    txtRampUpTime.Text = value.Item3.ToString();
                    txtRampDownTime.Text = value.Item4.ToString();
                }
                if (motor != null)
                {
                    if (SpeedType == MotorSpeedType.X)
                        motor.RunSpeedCfg = new ISpeedCfg(value.Item1, value.Item2, value.Item3, value.Item4);
                    if (SpeedType == MotorSpeedType.XY)
                        motor.XYRunSpeedCfg = new ISpeedCfg(value.Item1, value.Item2, value.Item3, value.Item4);
                    if (SpeedType == MotorSpeedType.XYZ)
                        motor.XYZRunSpeedCfg = new ISpeedCfg(value.Item1, value.Item2, value.Item3, value.Item4);
                }
            }
        }

        private int GetPulseValue(string text)
        {
            if (motor != null && motor.UnitInmm)
                return (int)(double.Parse(text) * motor.PulsePermm);
            else
                return int.Parse(text);
        }

        private string GetPulseText(int pulse)
        {
            if (motor != null && motor.UnitInmm)
                return (pulse / motor.PulsePermm).ToString();
            else
                return pulse.ToString();
        }

        private string GetPulseText(double pulse)
        {
            if (motor != null && motor.UnitInmm)
                return (pulse / motor.PulsePermm).ToString();
            else
                return pulse.ToString();
        }

        private double GetPulseValueDbl(string text)
        {
            if (motor != null && motor.UnitInmm)
                return (double.Parse(text) * motor.PulsePermm);
            else
                return double.Parse(text);
        }

        private void SetSpeed_Click(object sender, EventArgs e)
        {
            if (motor != null)
            {
                if (SpeedType == MotorSpeedType.X)
                    motor.SetSpeed(motor.RunSpeedCfg.StartVelocity, motor.RunSpeedCfg.MaxVelocity, motor.RunSpeedCfg.RampUpTime, motor.RunSpeedCfg.RampDownTime);
                //no need to set for other speed
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace MachineUserControl
{
    /// <summary>
    /// Interaction logic for UC_Tray.xaml
    /// </summary>
    public partial class UC_Tray : UserControl
    {
        public static readonly DependencyProperty RowProperty = DependencyProperty.Register("Row", typeof(string), typeof(UC_Tray)),
             ColumnProperty = DependencyProperty.Register("Column", typeof(string), typeof(UC_Tray)),
             ColorProperty = DependencyProperty.Register("TrayColor", typeof(Brush), typeof(UC_Tray)),
             UnitWidthProperty = DependencyProperty.Register("UnitWidth", typeof(double), typeof(UC_Tray)),
             UnitHeightProperty = DependencyProperty.Register("UnitHeight", typeof(double), typeof(UC_Tray)),
             TrayTitleProperty = DependencyProperty.Register("TrayTitle", typeof(string), typeof(UC_Tray));
        public string TrayTitle { get { return this.GetValue(TrayTitleProperty) as string; } set { this.SetValue(TrayTitleProperty, value); } }
        public string Row { get { return this.GetValue(RowProperty) as string; } set { this.SetValue(RowProperty, value); } }
        public string Column { get { return this.GetValue(ColumnProperty) as string; } set { this.SetValue(ColumnProperty, value); } }
        public Brush TrayColor { get { return this.GetValue(ColorProperty) as Brush; } set { this.SetValue(ColorProperty, value); } }
        public double? UnitWidth { get { return this.GetValue(UnitWidthProperty) as double?; } set { this.SetValue(UnitWidthProperty, value); } }
        public double? UnitHeight { get { return this.GetValue(UnitHeightProperty) as double?; } set { this.SetValue(UnitHeightProperty, value); } }
        List<Rectangle> listOfUnit = new List<Rectangle>();
        int counter = 0;

        bool Init = false;
        public UC_Tray()
        {
            InitializeComponent();
        }
        public void Reset()
        {
            counter = 0;

            foreach (var rect in listOfUnit)
            {
                rect.Fill = Brushes.White;
            }
        }

        public void SkipTray() { counter++; }
        public void NextTray()
        {
            if (counter > listOfUnit.Count - 1)
            {
                MessageBox.Show($"The {TrayTitle} is full");
                return;
            }
            else
                listOfUnit[counter].Fill = TrayColor;

            counter++;
        }

        public void GenerateTray()
        {
            var row = Convert.ToInt32(Row);
            var column = Convert.ToInt32(Column);
            for (int r = 0; r < row; r++)
            {
                StackPanel stackPanel = new StackPanel();
                stackPanel.Orientation = Orientation.Horizontal;
                for (int c = 0; c < column; c++)
                {
                    var rect = new Rectangle();
                    rect.Fill = Brushes.White;
                    rect.Stroke = Brushes.Black;
                    rect.Width = UnitWidth as double? ?? 20;
                    rect.Height = UnitWidth as double? ?? 20;
                    stackPanel.Children.Add(rect);
                    listOfUnit.Add(rect);
                }
                mainStack.Children.Add(stackPanel);
            }
        }

        private void mainStack_Loaded(object sender, RoutedEventArgs e)
        {
            if (Init)
                return;
            GenerateTray();
            lblTitle.Content = TrayTitle;

            Init = true;
        }
    }
}

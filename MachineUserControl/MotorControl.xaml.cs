﻿using HardwareInterfaces;
using MachineControl;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;

namespace MachineUserControl
{
    /// <summary>
    /// Interaction logic for MotorControl.xaml
    /// </summary>
    /// 
    [System.ComponentModel.DefaultBindingProperty("Speed")]

    public partial class MotorControl : UserControl
    {
        public IMotor motor;
        public delegate void CallBackDelegate();
        public CallBackDelegate BeforeHomeCallBack;
        public CallBackDelegate AfterHomeCallBack;
        bool stop = false;
        MachineUserControl.MotorSpeedType SpeedType;
        DispatcherTimer timer1 = null;
        object dataSource;
        string key;
        List<string> OtherParam;
        public bool isRotator { set; get; }

        public bool Initialize { set; get; }

        public MotorControl()
        {
            InitializeComponent();
            Initialize = false;
        }


        public void Init(IMotor motor, object dataSource, string key,List<string> OtherParam=null)
        {
            Initialize = true;
            this.motor = motor;
            groupBox.Header = motor.Name;
            SpeedType = MachineUserControl.MotorSpeedType.X;
            this.dataSource = dataSource;
            this.key = key;
            this.OtherParam = OtherParam;

            if (motor.UnitInmm)
            {
                string unit = "mm";
                if (isRotator == true)
                {
                    unit = "deg";
                }
                unit1.Content = unit;
                unit2.Content = unit;
                unit3.Content = unit;
                unit4.Content = unit;
                unit5.Content = unit;
                unit6.Content = unit;
                unit7.Content = unit;
                unit8.Content = unit;
                unit9.Content = unit;
            }
            if (motor.MotorType == MotorType.AMONET)
            {
                //unit4.Text = "sec";
                //unit5.Text = "sec";
                //label13.Text = "RampUp";
                //label17.Text = "RampDown";
            }

            double startVel = 0, maxVel = 0, rampUp = 0, rampDown = 0;
            startVel = double.Parse(motor.RunSpeedCfg.StartVelocity.ToString());
            maxVel = double.Parse(motor.RunSpeedCfg.MaxVelocity.ToString());
            rampUp = double.Parse(motor.RunSpeedCfg.RampUpTime.ToString());
            rampDown = double.Parse(motor.RunSpeedCfg.RampDownTime.ToString());
            Speed = new Tuple<double, double, double, double>(startVel, maxVel, rampUp, rampDown);

            timer1 = new DispatcherTimer();
            timer1.Tick += new EventHandler(timer1_Tick);
            timer1.Interval = new TimeSpan(0, 0, 0, 0, 300);

            //this.DataBindings.Add("Speed", dataSource, key);
            //timer1.Start();

        }
        public System.Tuple<double, double, double, double> Speed
        {
            get
            {
                string item1, item2, item3, item4;
                item1 = txtStartVelocity.Text.Trim();
                item2 = txtMaxVelocity.Text.Trim();
                item3 = txtRampUpTime.Text.Trim();
                item4 = txtRampDownTime.Text.Trim();
                if (item1 == "") { item1 = "0"; }
                if (item2 == "") { item2 = "0"; }
                if (item3 == "") { item3 = "0"; }
                if (item4 == "") { item4 = "0"; }
                System.Tuple<double, double, double, double> value;
                if (motor != null && motor.MotorType == MotorType.COMMON_MOTION)
                {
                    value = new System.Tuple<double, double, double, double>(GetPulseValueDbl(item1),
                                         GetPulseValueDbl(item2),
                                         GetPulseValueDbl(item3),
                                         GetPulseValueDbl(item4));
                }
                else if (motor != null && motor.MotorType == MotorType.AMONET)
                {
                    value = new System.Tuple<double, double, double, double>(GetPulseValueDbl(item1),
                                         GetPulseValueDbl(item2),
                                         double.Parse(item3),
                                         double.Parse(item4));
                }
                else //motor is null or other type
                {
                    value = new System.Tuple<double, double, double, double>(GetPulseValueDbl(item1),
                                         GetPulseValueDbl(item2),
                                         double.Parse(item3),
                                         double.Parse(item4));
                }
                if (motor != null)
                {
                    if (SpeedType == MachineUserControl.MotorSpeedType.X)
                        motor.RunSpeedCfg = new ISpeedCfg(value.Item1, value.Item2, value.Item3, value.Item4);
                    if (SpeedType == MachineUserControl.MotorSpeedType.XY)
                        motor.XYRunSpeedCfg = new ISpeedCfg(value.Item1, value.Item2, value.Item3, value.Item4);
                    if (SpeedType == MachineUserControl.MotorSpeedType.XYZ)
                        motor.XYZRunSpeedCfg = new ISpeedCfg(value.Item1, value.Item2, value.Item3, value.Item4);
                }
                return value;
            }
            set
            {
                if (value == null)
                {
                    return;
                }
                if (motor != null && motor.MotorType == MotorType.COMMON_MOTION)
                {
                    txtStartVelocity.Text = GetPulseText(value.Item1);
                    txtMaxVelocity.Text = GetPulseText(value.Item2);
                    txtRampUpTime.Text = GetPulseText(value.Item3);
                    txtRampDownTime.Text = GetPulseText(value.Item4);
                }
                else if (motor != null && motor.MotorType == MotorType.AMONET)
                {
                    txtStartVelocity.Text = GetPulseText(value.Item1);
                    txtMaxVelocity.Text = GetPulseText(value.Item2);
                    txtRampUpTime.Text = value.Item3.ToString();
                    txtRampDownTime.Text = value.Item4.ToString();
                }
                else
                {
                    txtStartVelocity.Text = GetPulseText(value.Item1);
                    txtMaxVelocity.Text = GetPulseText(value.Item2);
                    txtRampUpTime.Text = value.Item3.ToString();
                    txtRampDownTime.Text = value.Item4.ToString();
                }
                if (motor != null)
                {
                    if (SpeedType == MachineUserControl.MotorSpeedType.X)
                        motor.RunSpeedCfg = new ISpeedCfg(value.Item1, value.Item2, value.Item3, value.Item4);
                    if (SpeedType == MachineUserControl.MotorSpeedType.XY)
                        motor.XYRunSpeedCfg = new ISpeedCfg(value.Item1, value.Item2, value.Item3, value.Item4);
                    if (SpeedType == MachineUserControl.MotorSpeedType.XYZ)
                        motor.XYZRunSpeedCfg = new ISpeedCfg(value.Item1, value.Item2, value.Item3, value.Item4);
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Stop();
            UpdateStatus();
            if (!stop)
                timer1.Start();
        }

        public void Start(int interval = 200)
        {
            stop = false;
            timer1.Start();
        }

        public void Stop()
        {
            stop = true;
            timer1.Stop();
        }
        public System.Tuple<double, double, double, double> SpeedOld
        {
            get
            {
                string item1, item2, item3, item4;
                item1 = txtStartVelocity.Text.Trim();
                item2 = txtMaxVelocity.Text.Trim();
                item3 = txtRampUpTime.Text.Trim();
                item4 = txtRampDownTime.Text.Trim();
                if (item1 == "") { item1 = "0"; }
                if (item2 == "") { item2 = "0"; }
                if (item3 == "") { item3 = "0"; }
                if (item4 == "") { item4 = "0"; }
                var value = new System.Tuple<double, double, double, double>(GetPulseValueDbl(item1),
                                     GetPulseValueDbl(item2),
                                     GetPulseValueDbl(item3),
                                     GetPulseValueDbl(item4));
                if (motor != null) motor.RunSpeedCfg = new ISpeedCfg(value.Item1, value.Item2, value.Item3, value.Item4);
                return value;
            }
            set
            {
                txtStartVelocity.Text = GetPulseText(value.Item1);
                txtMaxVelocity.Text = GetPulseText(value.Item2);
                txtRampUpTime.Text = GetPulseText(value.Item3);
                txtRampDownTime.Text = GetPulseText(value.Item4);
                if (motor != null) motor.RunSpeedCfg = new ISpeedCfg(value.Item1, value.Item2, value.Item3, value.Item4);
            }
        }

        public ISpeedCfg GetSpeedCfg()
        {
            return new ISpeedCfg(GetPulseValueDbl(txtStartVelocity.Text),
                                 GetPulseValueDbl(txtMaxVelocity.Text),
                                 GetPulseValueDbl(txtRampUpTime.Text),
                                 GetPulseValueDbl(txtRampDownTime.Text));
        }

        public string Position
        {
            get;
            set;
        }

        public string CMD
        {
            get;
            set;
        }

        public string GetMotorPosition()
        {
            string Position = GetPulseText(motor.GetPosition());
            return Position;
        }

        public void UpdateStatus()
        {
            var IOStatus = motor.GetIOStatus();
            Position = GetPulseText(motor.GetPosition());
            CMD = GetPulseText(motor.GetCommand());
            var errorCounter = GetPulseText(motor.GetErrorCounter());
            lblPosition.Content = "Position:" + Position;
            lblErrorCounter.Content = "ErrorCnt:" + errorCounter;
            lblCommand.Content = "Cmd:" + CMD;
            lblALM.Content = IOStatus.alarm ? "Alarm" : "NA";
            lblALM.Background = IOStatus.alarm ? Brushes.Red : Brushes.LightGreen;
            lblEMG.Content = "EMG: " + IOStatus.emergencySignalInput.ToString();
            lblINP.Content = IOStatus.inPosition ? "INP: 1" : "INP: 0";
            lblORG.Content = "ORG: " + IOStatus.origin.ToString();
            lblRALM.Content = "RALM: " + IOStatus.alarmResetOutputStatus.ToString();
            lblRDY.Content = IOStatus.ready ? "RDY: 1" : "RDY: 0";
            lblSVON.Content = IOStatus.servoOn ? "ON" : "OFF";
            lblSVON.Background = IOStatus.servoOn ? Brushes.LightGreen : Brushes.Red;
            lblPosLimit.Content = "+EL: " + IOStatus.positiveLimit.ToString();
            lblNegLimit.Content = "-EL: " + IOStatus.negativeLimit.ToString();
            lblDIR.Content = "DIR: " + IOStatus.dirOutput.ToString();
            lblSD.Content = "SD: " + IOStatus.slowDownSignalInput.ToString();
            lblERC.Content = "ERC: " + IOStatus.ERCPinOutput.ToString();
            lblErrSt.Content = "ErrSt: " + (motor.GetErrorStatus()).ToString();
            //lblErrSt.Text = (uint.Parse(lblErrSt.Text) | motor.GetErrorStatus()).ToString();
            //main

            if (IOStatus.alarm)
            {
                lblALM.Foreground = Brushes.Red;
            }
            else
            {
                lblALM.Foreground = Brushes.Black;
            }
            if (!IOStatus.ready)
            {
                lblRDY.Foreground = Brushes.Red;
            }
            else
            {
                lblRDY.Foreground = Brushes.Black;
            }
            if (!IOStatus.servoOn)
            {
                lblSVON.Foreground = Brushes.Red;
            }
            else
            {
                lblSVON.Foreground = Brushes.Black;
            }
        }

        private void btnHome_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                var beforeHomeHandler = BeforeHomeCallBack;
                var afterHomeHandler = AfterHomeCallBack;
                if (beforeHomeHandler != null)
                {
                    beforeHomeHandler();
                }
                motor.HomeNoWait();
                if (afterHomeHandler != null)
                {
                    afterHomeHandler();
                }
            }
            catch (System.Exception ex)
            {

                System.Windows.MessageBox.Show(ex.Message);
            }
        }

        public string GetPulseText(double pulse)
        {
            if (motor != null && motor.UnitInmm)
            {
                double value = (pulse / motor.PulsePermm);
                return System.String.Format("{0:0.######}", value);
            }
            else
                return pulse.ToString();
        }

        private double GetPulseValueDbl(string text)
        {
            if (motor != null && motor.UnitInmm)
                return (double.Parse(text) * motor.PulsePermm);
            else
                return double.Parse(text);
        }

        private void btnSVON_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (motor.GetIOStatus().servoOn)
            {
                motor.SetSVOn(0);
            }
            else
            {
                motor.SetSVOn(1);
            }
        }


        private void btnSetSpeed_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            //motor.RunSpeedCfg.MaxVelocity = 1;
            try
            {
                Tuple<double, double, double, double> mySpeed;
                double startVel = 0, maxVel = 0, rampUp = 0, rampDown = 0;
                if (motor.MotorType == MotorType.COMMON_MOTION)
                {
                    motor.SetSpeed(GetPulseValueDbl(txtStartVelocity.Text), GetPulseValueDbl(txtMaxVelocity.Text),
                    GetPulseValueDbl(txtRampUpTime.Text), GetPulseValueDbl(txtRampDownTime.Text));
                    mySpeed = new Tuple<double, double, double, double>(GetPulseValueDbl(txtStartVelocity.Text), GetPulseValueDbl(txtMaxVelocity.Text),
                        GetPulseValueDbl(txtRampUpTime.Text), GetPulseValueDbl(txtRampDownTime.Text));

                }
                else //amonet type
                {
                    motor.SetSpeed(GetPulseValueDbl(txtStartVelocity.Text), GetPulseValueDbl(txtMaxVelocity.Text),
                    double.Parse(txtRampUpTime.Text), double.Parse(txtRampDownTime.Text));
                    rampUp = double.Parse(txtRampUpTime.Text);
                    rampDown = double.Parse(txtRampDownTime.Text);
                    mySpeed = new Tuple<double, double, double, double>(GetPulseValueDbl(txtStartVelocity.Text), GetPulseValueDbl(txtMaxVelocity.Text), rampUp, rampDown);
                }
                startVel = double.Parse(txtStartVelocity.Text);
                maxVel = double.Parse(txtMaxVelocity.Text);
                rampUp = double.Parse(txtRampUpTime.Text);
                rampDown = double.Parse(txtRampDownTime.Text);

                PropertyInfo prop = this.dataSource.GetType().GetProperty(this.key, BindingFlags.Public | BindingFlags.Instance);
                if (null != prop && prop.CanWrite)
                {
                    prop.SetValue(this.dataSource, mySpeed, null);
                }
                if (motor.Name == "Motor1X")
                {
                    Config.Cfg.Motor1XSpeed = new Tuple<double, double, double, double>(GetPulseValueDbl(txtStartVelocity.Text), GetPulseValueDbl(txtMaxVelocity.Text), rampUp, rampDown);
                }
                else if (motor.Name == "PPArm_Z")
                {
                    //Config.Cfg.Z_AxisSpeed = new Tuple<double, double, double, double>(GetPulseValueDbl(txtStartVelocity.Text), GetPulseValueDbl(txtMaxVelocity.Text), rampUp, rampDown);
                }
                // motor.ru

            }
            catch (System.Exception ex)
            {

                System.Windows.MessageBox.Show(ex.Message);
            }

            //Config.Cfg.Save();

        }

        private void btnMoveAbs_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                motor.MoveAbsoluteNoWait(GetPulseValue(txtMoveAbsPosition.Text));
            }
            catch (System.Exception ex)
            {

                System.Windows.MessageBox.Show(ex.Message);
            }
        }

        private int GetPulseValue(string text)
        {
            if (motor != null && motor.UnitInmm)
                return (int)(double.Parse(text) * motor.PulsePermm);
            else
                return int.Parse(text);
        }

        private void btnStop_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                motor.Stop(true);
            }
            catch (System.Exception ex)
            {

                System.Windows.MessageBox.Show(ex.Message);
            }
        }

        public void JogPos(string distance)
        {
            try
            {
                motor.MoveRelativeNoWait(GetPulseValue(distance));
            }
            catch (System.Exception ex)
            {

                System.Windows.MessageBox.Show(ex.Message);
            }
        }

        public void JogNeg(string distance)
        {
            try
            {
                motor.MoveRelativeNoWait(-GetPulseValue(distance));
            }
            catch (System.Exception ex)
            {

                System.Windows.MessageBox.Show(ex.Message);
            }
        }


        public void MovePos(string distance)
        {
            try
            {
                motor.MoveAbsoluteNoWait(GetPulseValue(distance));
            }
            catch (System.Exception ex)
            {

                System.Windows.MessageBox.Show(ex.Message);
            }
        }

        public void MoveNeg(string distance)
        {
            try
            {
                motor.MoveAbsoluteNoWait(-GetPulseValue(distance));
            }
            catch (System.Exception ex)
            {

                System.Windows.MessageBox.Show(ex.Message);
            }
        }

        public void ManualJogPlus(string Distance)
        {
            try
            {
                motor.MoveRelativeNoWait(GetPulseValue(Distance));
            }
            catch (System.Exception ex)
            {

                System.Windows.MessageBox.Show(ex.Message);
            }
        }

        public void ManualJogNeg(string Distance)
        {
            try
            {
                motor.MoveRelativeNoWait(-GetPulseValue(Distance));
            }
            catch (System.Exception ex)
            {

                System.Windows.MessageBox.Show(ex.Message);
            }
        }




        public void btnJogPos_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                motor.MoveRelativeNoWait(GetPulseValue(txtJogDistance.Text));
            }
            catch (System.Exception ex)
            {

                System.Windows.MessageBox.Show(ex.Message);
            }
        }
        private void btnJogNeg_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                motor.MoveRelativeNoWait(-GetPulseValue(txtJogDistance.Text));
            }
            catch (System.Exception ex)
            {

                System.Windows.MessageBox.Show(ex.Message);
            }
        }

        private void btnResetAlarm_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                motor.ResetAlarm();
            }
            catch (System.Exception ex)
            {

                System.Windows.MessageBox.Show(ex.Message);
            }
        }

        private void btnResetErrorCnt_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                motor.ResetErrorCounter();
            }
            catch (System.Exception ex)
            {

                System.Windows.MessageBox.Show(ex.Message);
            }
        }

        private void btnResetPosition_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                motor.Reset();
            }
            catch (System.Exception ex)
            {

                System.Windows.MessageBox.Show(ex.Message);
            }
        }

        private void Speed_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void UserControl_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {

        }

        private void button_Copy11_Click(object sender, System.Windows.RoutedEventArgs e)
        {

        }

        private void txtMaxVelocity_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {

        }
    }
}

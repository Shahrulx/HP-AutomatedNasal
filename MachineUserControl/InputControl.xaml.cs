﻿using HardwareInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace MachineUserControl
{
    /// <summary>
    /// Interaction logic for InputControl.xaml
    /// </summary>
    public partial class InputControl : UserControl
    {
        private List<ushort> inputCards;
        private Dictionary<string, IDigitalInput> inputs;
        private List<Tuple<Rectangle, IDigitalInput, TextBox>> pictureBoxInputPairList;
        bool stop = true;

        DispatcherTimer timer1 = null;
        public bool Initialize { set; get; }
        public InputControl()
        {
            if (System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                return;
            }

            InitializeComponent();
           // groupBoxName.Header = "Input";
            gboxName.Text = "Input";
            timer1 = new DispatcherTimer();
            timer1.Tick += new EventHandler(timer1_Tick);
        }

        public void Init(List<ushort> inputCards, Dictionary<string, IDigitalInput> inputs)
        {
            Initialize = true;
            pictureBoxInputPairList = new List<Tuple<Rectangle, IDigitalInput, TextBox>>();
            int offset = 0;
            this.inputCards = inputCards;
            this.inputs = inputs;
            StackPanel subPanel_Hori = null;
            Grid mgrid = null;
            StackPanel PanelVertical = new StackPanel();
            int countIO = 0;
            foreach (var cardID in inputCards)
            {
                var sortedList = inputs.Values.Where(vk => vk.CardID == cardID).OrderBy(vk => vk.Lsb).ToList();
                foreach (var input in sortedList)
                {
                    var text = input.Name;

                    Border myborder = new Border()
                    {
                        BorderThickness = new Thickness()
                        {
                            Bottom = 1,
                            Left = 1,
                            Right = 1,
                            Top = 1
                        },
                        BorderBrush = new SolidColorBrush(Colors.Black)
                    };

                    var pictureBox = new Rectangle();
                    var label = new TextBox();
                    mgrid = new Grid();
                    mgrid.Background = new SolidColorBrush(Colors.WhiteSmoke);
                    mgrid.Margin = new Thickness(3, 3, 3, 3);
                   
                    subPanel_Hori = new StackPanel();
                    subPanel_Hori.Orientation = Orientation.Horizontal;

                    pictureBox.Fill = Brushes.White;
                    pictureBox.Stroke = Brushes.Black;
                    pictureBox.Width = 25;
                    pictureBox.Height = 23;
                    pictureBox.Margin = new Thickness(5, 5, 5, 7); ;


                    
                    label.Name = "label" + offset.ToString();
                    label.Text = text;
                    label.Margin = new Thickness(3, 5, 5, 0);
                    label.Background = Brushes.Transparent;


                    label.IsHitTestVisible = false;
                    label.BorderThickness = new Thickness(0);
                    subPanel_Hori.Children.Add(pictureBox);
                    subPanel_Hori.Children.Add(label);
                   
                    subPanel_Hori.Margin = new Thickness(0, 0, 0, 0);

                    countIO++;
                    offset += 1;
                    pictureBoxInputPairList.Add(new Tuple<Rectangle, IDigitalInput, TextBox>(pictureBox, input, label));

                    mgrid.Children.Add(myborder);
                    
                    mgrid.Children.Add(subPanel_Hori);
                    //if (countIO > 40)
                    //{
                    //    mainPanel_Hori.Children.Add(PanelVertical);
                    //    PanelVertical = new StackPanel();
                    //    countIO = 0;
                    //}
                    //else
                    //{
                    PanelVertical.Children.Add(mgrid);
                    //}

                }
                mainPanel_Hori.Children.Add(PanelVertical);
                PanelVertical = new StackPanel();


            }

        }

        public void AdjustPosition()
        {
            double maxWidth = 0;
            foreach (var pair in pictureBoxInputPairList)
            {
                var input = pair.Item3;
                if (input.ActualWidth > maxWidth)
                    maxWidth = input.ActualWidth;
            }

            foreach (var pair in pictureBoxInputPairList)
            {
                var input = pair.Item3;
                input.Width = maxWidth;
            }
        }

        public void UpdateInput()
        {
            foreach (var pair in pictureBoxInputPairList)
            {
                var pictureBox = pair.Item1;
                var input = pair.Item2;
                if (input.Read() == 1)
                {
                    pictureBox.Fill = Brushes.Green;
                }
                else
                {
                    pictureBox.Fill = Brushes.White;
                }
            }
        }

        public void Start(int interval = 200)
        {
            stop = false;
            timer1.Interval = new TimeSpan(0, 0, 0, 0, interval);
            timer1.IsEnabled = true;
        }

        public void Stop()
        {
            stop = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.IsEnabled = false;
            UpdateInput();
            timer1.IsEnabled = true;
            if (stop)
                timer1.IsEnabled = false;
        }

        private void UserControl_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            AdjustPosition();
            Start(250);
        }
    }
}

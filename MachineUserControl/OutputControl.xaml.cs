﻿using HardwareInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace MachineUserControl
{
    /// <summary>
    /// Interaction logic for OutputControl.xaml
    /// </summary>
    public partial class OutputControl : UserControl
    {
        private List<ushort> outputCards;
        private Dictionary<string, IDigitalOutput> outputs;
        private List<Tuple<Rectangle, IDigitalOutput, TextBox>> buttonOutputPairList;
        DispatcherTimer timer1 = null;

        bool stop = false;
        public bool Initialize { set; get; }
        public OutputControl()
        {
            if (System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                return;
            }

            Initialize = false;
            InitializeComponent();
            //groupBoxName.Header = "Output";
            gboxName.Text = "Output";
        }
        public void Init(List<ushort> outputCards, Dictionary<string, IDigitalOutput> outputs)
        {
            Initialize = true;
            int offset = 0;
            this.outputCards = outputCards;
            this.outputs = outputs;
            StackPanel subPanel_Hori = null;
            Grid mgrid = null;
            int countIO = 0;

            buttonOutputPairList = new List<Tuple<Rectangle, IDigitalOutput, TextBox>>();
            foreach (var cardID in outputCards)
            {
                StackPanel PanelVertical = new StackPanel();

                var sortedList = outputs.Values.Where(vk => vk.CardID == cardID).OrderBy(vk => vk.Lsb).ToList();
                foreach (var output in sortedList)
                {
                    Border myborder = new Border()
                    {
                        BorderThickness = new Thickness()
                        {
                            Bottom = 1,
                            Left = 1,
                            Right = 1,
                            Top = 1
                        },
                        BorderBrush = new SolidColorBrush(Colors.Black)
                    };

                    var button = new Rectangle();
                    var rec = new Rectangle();
                    var textBox = new TextBox();
                    var text = output.Name;

                    mgrid = new Grid();
                    mgrid.Background = new SolidColorBrush(Colors.WhiteSmoke);
                    mgrid.Margin = new Thickness(5, 5, 5, 5);

                    subPanel_Hori = new StackPanel();
                    subPanel_Hori.Orientation = Orientation.Horizontal;
                   // subPanel_Hori.Background = new SolidColorBrush(Colors.WhiteSmoke);
                    button.Name = "button" + offset.ToString();
                    button.Fill = Brushes.White;
                    button.Stroke = Brushes.Black;
                    button.Width = 25;
                    button.Height = 23;
                    button.Tag = output;
                    button.Cursor = Cursors.Hand;
                    button.MouseDown += new MouseButtonEventHandler((sender, e) => btnOutput_Click(sender, e));//handle the handler for each icons
                    button.Margin = new Thickness(5, 5, 5, 7); ;


                   
                    textBox.Name = "textBox" + offset.ToString();
                    textBox.Text = text;
                    textBox.Background = Brushes.Transparent;
                    textBox.IsHitTestVisible = false;
                    textBox.BorderThickness = new Thickness(0);

                    offset += 1;
                    buttonOutputPairList.Add(new Tuple<Rectangle, IDigitalOutput, TextBox>(button, output, textBox));

                    subPanel_Hori.Children.Add(button);
                    subPanel_Hori.Children.Add(textBox);
                    
                    subPanel_Hori.Margin = new System.Windows.Thickness(0, 0, 0, 0);
                    countIO++;

                    //if (countIO > 40)
                    //{
                    //    mainPanel_Hori.Children.Add(PanelVertical);
                    //    PanelVertical = new StackPanel();
                    //    countIO = 0;
                    //}
                    //else
                    mgrid.Children.Add(myborder);

                    mgrid.Children.Add(subPanel_Hori);

                    PanelVertical.Children.Add(mgrid);

                }
                mainPanel_Hori.Children.Add(PanelVertical);

                PanelVertical = new StackPanel();

            }

            timer1 = new DispatcherTimer();
            timer1.Tick += new EventHandler(timer1_Tick);
            //Start();
        }

        public void btnOutput_Click(object sender, MouseButtonEventArgs e)
        {
            Rectangle btn = (Rectangle)sender;
            IDigitalOutput output = (IDigitalOutput)btn.Tag;
            if (output.Get() == 0)
            {
                output.Write(1);
                btn.Fill = Brushes.Green;
            }
            else
            {
                output.Write(0);
                btn.Fill = Brushes.White;
            }
        }

        public void UpdateOutput()
        {
            foreach (var pair in buttonOutputPairList)
            {
                var button = pair.Item1;
                var output = pair.Item2;
                if (output.Get() == 1)
                {
                    button.Fill = Brushes.Green;
                }
                else
                {
                    button.Fill = Brushes.White;
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.IsEnabled = false;
            UpdateOutput();
            timer1.IsEnabled = true;
            if (stop)
                timer1.IsEnabled = false;
        }

        public void Start(int interval = 200)
        {
            stop = false;
            timer1.Interval = new TimeSpan(0, 0, 0, 0, interval);
            timer1.IsEnabled = true;
        }

        public void Stop()
        {
            stop = true;
        }

        public void AdjustPosition()
        {
            double maxWidth = 0;
            foreach (var pair in buttonOutputPairList)
            {
                var input = pair.Item3;
                if (input.ActualWidth > maxWidth)
                    maxWidth = input.ActualWidth;
            }

            foreach (var pair in buttonOutputPairList)
            {
                var input = pair.Item3;
                input.Width = maxWidth;
            }
        }

        private void UserControl_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            AdjustPosition();
            Start(250);
        }
    }
}

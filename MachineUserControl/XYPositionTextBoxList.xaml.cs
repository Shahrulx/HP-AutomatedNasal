﻿using HardwareInterfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace MachineUserControl
{
    /// <summary>
    /// Interaction logic for XYPositionTextBoxList.xaml
    /// </summary>
    public partial class XYPositionTextBoxList : UserControl
    {
        IMotor motorX;
        IMotor motorY;
        List<Button> Values = new List<Button>();
        List<string> Names = new List<string>();
        object dataSource;
        public bool GroupMove { get; set; }
        public bool SingleMotor { get; set; }

        public XYPositionTextBoxList()
        {
            InitializeComponent();
        }

        private Tuple<TextBox, TextBox> MakeItem(string name, int ypos, int count)
        {
            StatusBar statusBar = new StatusBar();
            System.Windows.Controls.Label positionLabel;
            TextBox XValue;
            Button Move;
            Button Load;
            positionLabel = new System.Windows.Controls.Label();
            XValue = new TextBox();

           
                TextBox YValue;
                YValue = new TextBox();
            
        
            Move = new Button();
            Load = new Button();
            statusBar.Background = System.Windows.Media.Brushes.Transparent;

            positionLabel.Name = "lbl" + name;
            positionLabel.Content = name;
            positionLabel.Width = 140;

            XValue.HorizontalAlignment = HorizontalAlignment.Center;
            XValue.VerticalAlignment = VerticalAlignment.Center;
            XValue.Name = name;
            XValue.TextWrapping = TextWrapping.NoWrap;
            XValue.Width = 50;

            YValue.HorizontalAlignment = HorizontalAlignment.Center;
            YValue.VerticalAlignment = VerticalAlignment.Center;
            YValue.Name = name;
            YValue.TextWrapping = TextWrapping.NoWrap;
            YValue.Width = 50;

            Move.HorizontalAlignment = HorizontalAlignment.Center;
            Move.VerticalAlignment = VerticalAlignment.Center;
            Move.Name = "Move" + name;
            Move.Content = "Move";
            Move.Tag = new Tuple<TextBox, TextBox>(XValue, YValue);
            Move.Click += new RoutedEventHandler(this.Move_Click);

            Load.HorizontalAlignment = HorizontalAlignment.Center;
            Load.VerticalAlignment = VerticalAlignment.Center;
            Load.Name = "Load" + name;
            Load.Content = "Load";
            Load.Tag = new Tuple<TextBox, TextBox>(XValue, YValue);
            Load.Click += new RoutedEventHandler(this.Load_Click);

            statusBar.Items.Add(positionLabel);
            statusBar.Items.Add(XValue);
            if (!SingleMotor)
            {
                statusBar.Items.Add(YValue);
            }
            else
            {
                YValue.Text ="0";
            }
            statusBar.Items.Add(Move);
            statusBar.Items.Add(Load);
            stackPanel.Children.Add(statusBar);
            return new Tuple<TextBox, TextBox>(XValue, YValue);
        }

        ///// <summary>
        ///// generate position XY textbox list base on category and custom name list
        ///// 
        ///// </summary>
        ///// <param name="category">the category attribute name in the datasource</param>
        ///// <param name="customNames">list of property to generate which is not in the category</param>
        ///// <param name="dataSource">datasource which store all the property</param>
        ///// <param name="motorX"></param>
        ///// <param name="motorY"></param>
        public void Init(string category, List<string> customNames, object dataSource, IMotor motorX, IMotor motorY, bool GroupMove = true,bool SingleMotor= false)
        {
            this.groupBox.Header = category;
            this.motorX = motorX;
            this.motorY = motorY;
            this.GroupMove = GroupMove;
            this.SingleMotor = SingleMotor;
            //      SuspendLayout();
            int ypos = 0;
            int count = 0;
            this.dataSource = dataSource;
            Names = GetPropertyNameFromCategory(category);
            Names.AddRange(customNames);


            // System.Drawing.Size maxSize = FindMaxWidthForText();

            foreach (var name in Names)
            {
                var xyTextBox = MakeItem(name, ypos, count);
                var xyValue = (Tuple<int, int>)GetPropValue(dataSource, name);
                xyTextBox.Item1.Text = GetPulseText(xyValue.Item1, motorX);
                xyTextBox.Item2.Text = GetPulseText(xyValue.Item2, motorY);

                xyTextBox.Item1.TextChanged += (sender, e) =>
                {
                    var XValue = xyTextBox.Item1;
                    var YValue = xyTextBox.Item2;
                    if (XValue.Text.Trim() != "")
                    {
                        double result1;
                        double result2;
                        if (double.TryParse(XValue.Text, out result1) && double.TryParse(YValue.Text, out result2))
                        {
                            int xPulseValue = GetPulseValue(XValue.Text, motorX);
                            int yPulseValue = GetPulseValue(YValue.Text, motorY);
                            SetPropValue(dataSource, name, new Tuple<int, int>(xPulseValue, yPulseValue));
                        }
                    }
                };
                xyTextBox.Item2.TextChanged += (sender, e) =>
                {
                    var XValue = xyTextBox.Item1;
                    var YValue = xyTextBox.Item2;
                    if (XValue.Text.Trim() != "")
                    {
                        double result1;
                        double result2;
                        if (double.TryParse(XValue.Text, out result1) && double.TryParse(YValue.Text, out result2))
                        {
                            int xPulseValue = GetPulseValue(XValue.Text, motorX);
                            int yPulseValue = GetPulseValue(YValue.Text, motorY);
                            SetPropValue(dataSource, name, new Tuple<int, int>(xPulseValue, yPulseValue));
                        }
                    }
                };

                count += 6;
                ypos += 25;
            }
            this.Name = "XYPositionTextBoxList";
            //this.Size = new System.Drawing.Size(338, 136);
            //   this.ResumeLayout(false);
            //    this.PerformLayout();
        }

        private List<string> GetPropertyNameFromCategory(string category)
        {
            //user reflection to set the cfg
            List<string> names = new List<string>();
            Type dataSourceType = dataSource.GetType();
            var properties = dataSourceType.GetProperties();
            foreach (PropertyInfo pInfo in properties)
            {
                var qq = GetCategory(pInfo);
                if (pInfo.PropertyType == typeof(Tuple<int, int>) && GetCategory(pInfo) == category)
                {

                    names.Add(pInfo.Name);
                }
            }
            return names;
        }

        private string GetCategory(PropertyInfo propInfo)
        {
            var categoryAtrributes = propInfo.GetCustomAttributes(typeof(CategoryAttribute), true);
            if (categoryAtrributes.Length == 0)
            {
                return "";
            }
            var attribute = categoryAtrributes[0];
            var category = (CategoryAttribute)attribute;
            return category.Category;
        }

        //private Size FindMaxWidthForText()
        //{
        //    Size maxSize = TextRenderer.MeasureText("", this.Font);
        //    foreach (var name in Names)
        //    {
        //        var size = TextRenderer.MeasureText(name, this.Font);
        //        if (size.Width > maxSize.Width)
        //            maxSize = size;
        //    }
        //    return maxSize;
        //}

        private void Move_Click(object sender, EventArgs e)
        {
            try
            {
                var xyValue = (sender as Button).Tag as Tuple<TextBox, TextBox>;
                TextBox xValue = xyValue.Item1;
                TextBox yValue = xyValue.Item2;
                if (this.GroupMove)
                {
                    motorX.MoveAbsoluteXYNoWait(GetPulseValue(xValue.Text, motorX), GetPulseValue(yValue.Text, motorY));
                }
                if (this.SingleMotor)
                {
                    motorX.MoveAbsoluteNoWait(GetPulseValue(xValue.Text, motorX));
                }
                else
                {
                    motorX.MoveAbsoluteNoWait(GetPulseValue(xValue.Text, motorX));
                    motorY.MoveAbsoluteNoWait(GetPulseValue(yValue.Text, motorY));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Cannot move " + motorX.Name + ": " + ex.ToString());
                return;
            }
        }

        private void Load_Click(object sender, EventArgs e)
        {
            try
            {
                var xyValue = (sender as Button).Tag as Tuple<TextBox, TextBox>;
                TextBox xValue = xyValue.Item1;
                TextBox yValue = xyValue.Item2;
                xValue.Text = GetPulseText(motorX.GetPosition(), motorX);
                yValue.Text = GetPulseText(motorY.GetPosition(), motorY);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Cannot load position for " + motorX.Name + ": " + ex.ToString());
                return;
            }
        }

        private int GetPulseValue(string text, IMotor motor)
        {
            if (motor != null && motor.UnitInmm)
                return (int)(double.Parse(text) * motor.PulsePermm);
            else
                return int.Parse(text);
        }

        private string GetPulseText(int pulse, IMotor motor)
        {
            if (motor != null && motor.UnitInmm)
            {
                double value = (pulse / motor.PulsePermm);
                return String.Format("{0:0.######}", value);
            }
            else
                return pulse.ToString();
        }

        public static object GetPropValue(object src, string propName)
        {
            return src.GetType().GetProperty(propName).GetValue(src, null);
        }

        public static void SetPropValue(object src, string propName, object value)
        {
            src.GetType().GetProperty(propName).SetValue(src, value, null);
        }
    }
}

﻿using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;

namespace AutomationLib
{
    public partial class Model : INotifyPropertyChanged
    {
        private string myDataProperty;
        //  public static Logger logger = LogManager.GetCurrentClassLogger();
        public static Logger logger = LogManager.GetLogger("ProcessLog");
        public static Logger Alertlogger = LogManager.GetLogger("AlertLog");
        public static Logger PowerSupplyLog = LogManager.GetLogger("PowerSupplyLog");
        public static Logger PowerSupplyLogBySet = LogManager.GetLogger("PowerSupplyLogBySet");

        public static Logger PS1 = LogManager.GetLogger("PS1");
        public static Logger PS2 = LogManager.GetLogger("PS2");
        public static Logger PS3 = LogManager.GetLogger("PS3");
        public static Logger PS4 = LogManager.GetLogger("PS4");
        public static Logger PS5 = LogManager.GetLogger("PS5");
        public static Logger PS6 = LogManager.GetLogger("PS6");
        public static Logger PS7 = LogManager.GetLogger("PS7");
        public static Logger PS8 = LogManager.GetLogger("PS8");
        public static Logger PS9 = LogManager.GetLogger("PS9");
        public static Logger PS10 = LogManager.GetLogger("PS10");
        public static Logger PS11 = LogManager.GetLogger("PS11");
        public static Logger PS12 = LogManager.GetLogger("PS12");


        public static Logger AmpMinLog = LogManager.GetLogger("AmpMinLog");

        public Model() { }

        public Model(DateTime dateTime)
        {
            myDataProperty = "Last bound time was " + dateTime.ToLongTimeString();
        }

        public void SetPowerSupplyLog(string Type,string Message,int PSNum=0)
        {
            switch (Type)
            {
                case "Info":
                    PowerSupplyLog.Info(Message);
                    break;
                case "SetLog":
                    if(PSNum==1)
                        PS1.Info(Message);
                    if (PSNum == 2)
                        PS2.Info(Message);
                    if (PSNum == 3)
                        PS3.Info(Message);
                    if (PSNum == 4)
                        PS4.Info(Message);
                    if (PSNum == 5)
                        PS5.Info(Message);
                    if (PSNum == 6)
                        PS6.Info(Message);
                    if (PSNum == 7)
                        PS7.Info(Message);
                    if (PSNum == 8)
                        PS8.Info(Message);
                    if (PSNum == 9)
                        PS9.Info(Message);
                    if (PSNum == 10)
                        PS10.Info(Message);
                    if (PSNum == 11)
                        PS11.Info(Message);
                    if (PSNum == 12)
                        PS12.Info(Message);

                    break;
                case "Error":
                    PowerSupplyLog.Error(Message);
                    break;
                default:
                    break;
            }
        }

        public void SetAmpMinLog(string Type, string Message)
        {
            switch (Type)
            {
                case "Info":
                    AmpMinLog.Info(Message);
                    break;
                case "Error":
                    AmpMinLog.Error(Message);
                    break;
                default:
                    break;
            }
        }

        public String MyDataProperty
        {
            get { return myDataProperty; }
            set
            {
                myDataProperty = value;
                OnPropertyChanged("MyDataProperty");
            }
        }

        public HardwareLib.Hardware hw;
        
        public TankModule tankModule;
        public CellModule cellModule;
        public RobotControl robotControl;
        public PowerSupplyModule powerSupply;


        #region LOGS
        private Tuple<DateTime, string> _MessageLogsTuple;
        public Tuple<DateTime, string> MessageLogsTuple
        {
            get { return _MessageLogsTuple; }
            set
            {
                _MessageLogsTuple = value;
                OnPropertyChanged("MessageLogsTuple");
              
                String Message = "";

                if (value.Item2.Contains(","))
                {
                    var val = value.Item2.Split(',');
                    Message = val[1];
                }
                else
                {
                    Message = value.Item2;
                }

                if(value.Item2.Contains("Alert"))
                {
                    //logger.Error(Message);
                    Alertlogger.Error(Message);
                }
                else
                {
                    logger.Info(Message);
                }
               
            }
        }

        public class Item
        {
            public string Date { get; set; }
            public string Message { get; set; }
        }

        private string _StatusBar;
        public String StatusBar
        {
            get { return _StatusBar; }
            set
            {
                _StatusBar = value;
                OnPropertyChanged("StatusBar");
            }
        }

        private string _ErrorCode;
        public string ErrorCode
        {
            get { return _ErrorCode; }
            set
            {
                _ErrorCode = value;

                OnPropertyChanged2(this, _ErrorCode);
            }
        }
        #endregion



        
        private int _TimerExpireEndLot;
        public int TimerExpireEndLot
        {
            get { return _TimerExpireEndLot; }
            set
            {
                _TimerExpireEndLot = value;
                OnPropertyChanged("TimerExpireEndLot");
            }
        }

        private string _ErrorLabel;
        public string ErrorLabel
        {
            get { return _ErrorLabel; }
            set
            {
                _ErrorLabel = value;
                OnPropertyChanged("ErrorLabel");
            }
        }
        
        private int _EndLotCountDown;
        public int EndLotCountDown
        {
            get { return _EndLotCountDown; }
            set
            {
                _EndLotCountDown = value;
                OnPropertyChanged("EndLotCountDown");
            }
        }


        //Software datetime
        private string _TimeNow;
        public string TimeNow
        {
            get { return _TimeNow; }
            set
            {
                _TimeNow = value;
                OnPropertyChanged("TimeNow");
            }
        }

        private string _UPTime;
        public string UPTime
        {
            get { return _UPTime; }
            set
            {
                _UPTime = value;
                OnPropertyChanged("UPTime");
            }
        }
        
        #region TimerCell
        private string _TimeCell1;
        public string TimeCell1
        {
            get { return _TimeCell1; }
            set
            {
                _TimeCell1 = value;
                OnPropertyChanged("TimeCell1");
            }
        }

        private string _TimeCell2;
        public string TimeCell2
        {
            get { return _TimeCell2; }
            set
            {
                _TimeCell2 = value;
                OnPropertyChanged("TimeCell2");
            }
        }

        private string _TimeCell3;
        public string TimeCell3
        {
            get { return _TimeCell3; }
            set
            {
                _TimeCell3 = value;
                OnPropertyChanged("TimeCell3");
            }
        }

        private string _TimeCell4;
        public string TimeCell4
        {
            get { return _TimeCell4; }
            set
            {
                _TimeCell4 = value;
                OnPropertyChanged("TimeCell4");
            }
        }

        private string _TimeCell5;
        public string TimeCell5
        {
            get { return _TimeCell5; }
            set
            {
                _TimeCell5 = value;
                OnPropertyChanged("TimeCell5");
            }
        }

        private string _TimeCell6;
        public string TimeCell6
        {
            get { return _TimeCell6; }
            set
            {
                _TimeCell6 = value;
                OnPropertyChanged("TimeCell6");
            }
        }

        private string _TimeCell7;
        public string TimeCell7
        {
            get { return _TimeCell7; }
            set
            {
                _TimeCell7 = value;
                OnPropertyChanged("TimeCell7");
            }
        }

        private string _TimeCell8;
        public string TimeCell8
        {
            get { return _TimeCell8; }
            set
            {
                _TimeCell8 = value;
                OnPropertyChanged("TimeCell8");
            }
        }

        private string _TimeCell9;
        public string TimeCell9
        {
            get { return _TimeCell9; }
            set
            {
                _TimeCell9 = value;
                OnPropertyChanged("TimeCell9");
            }
        }

        private string _TimeCell10;
        public string TimeCell10
        {
            get { return _TimeCell10; }
            set
            {
                _TimeCell10 = value;
                OnPropertyChanged("TimeCell10");
            }
        }

        private string _TimeCell11;
        public string TimeCell11
        {
            get { return _TimeCell11; }
            set
            {
                _TimeCell11 = value;
                OnPropertyChanged("TimeCell11");
            }
        }

        private string _TimeCell12;
        public string TimeCell12
        {
            get { return _TimeCell12; }
            set
            {
                _TimeCell12 = value;
                OnPropertyChanged("TimeCell12");
            }
        }

        #endregion

        //bath level
        private string _CurrentSetBank1="A";
        public string CurrentSetBank1
        {
            get { return _CurrentSetBank1; }
            set
            {
                _CurrentSetBank1 = value;
                OnPropertyChanged("CurrentSetBank1");
            }
        }
        private string _CurrentSetBank2="B";
        public string CurrentSetBank2
        {
            get { return _CurrentSetBank2; }
            set
            {
                _CurrentSetBank2 = value;
                OnPropertyChanged("CurrentSetBank2");
            }
        }

        #region BATH LEVEL
                //bath level
                private string _BathLevelLabel;
                public string BathLevelLabel
                {
                    get { return _BathLevelLabel; }
                    set
                    {
                        _BathLevelLabel = value;
                        OnPropertyChanged("BathLevelLabel");
                    }
                }

                private bool _BathLevelLL;
                public bool BathLevelLL
                {
                    get { return _BathLevelLL; }
                    set
                    {
                        _BathLevelLL = value;
                        OnPropertyChanged("BathLevelLL");
                    }
                }

                private bool _BathLevelL;
                public bool BathLevelL
                {
                    get { return _BathLevelL; }
                    set
                    {
                        _BathLevelL = value;
                        OnPropertyChanged("BathLevelL");
                    }
                }

                private bool _BathLevelF;
                public bool BathLevelF
                {
                    get { return _BathLevelF; }
                    set
                    {
                        _BathLevelF = value;
                        OnPropertyChanged("BathLevelF");
                    }
                }

                private bool _BathLevelH;
                public bool BathLevelH
                {
                    get { return _BathLevelH; }
                    set
                    {
                        _BathLevelH = value;
                        OnPropertyChanged("BathLevelH");
                    }
                }

                private bool _BathLevelHH;
                public bool BathLevelHH
                {
                    get { return _BathLevelHH; }
                    set
                    {
                        _BathLevelHH = value;
                        OnPropertyChanged("BathLevelHH");
                    }
                }
        #endregion

        private string _RobotCurrentStat;
        public string RobotCurrentStat
        {
            get { return _RobotCurrentStat; }
            set
            {
                _RobotCurrentStat = value;
                OnPropertyChanged("RobotCurrentStat");
            }
        }


        private string _RobotStat;
        public string RobotStat
        {
            get { return _RobotStat; }
            set
            {
                _RobotStat = value;
                OnPropertyChanged("RobotStat");
            }
        }


        private string _EMO = "OFF";
        public string EMO
        {
            get { return _EMO; }
            set
            {
                _EMO = value;
                OnPropertyChanged("EMO");
            }
        }

        private string _CurtainSensor = "ON";
        public string CurtainSensor
        {
            get { return _CurtainSensor; }
            set
            {
                _CurtainSensor = value;
                OnPropertyChanged("CurtainSensor");
            }
        }

        #region IOA sensor List
        private bool _cboxIOA1;
        public bool cboxIOA1
        {
            get { return _cboxIOA1; }
            set
            {
                _cboxIOA1 = value;
                OnPropertyChanged("cboxIOA1");
            }
        }

        private bool _cboxIOA2;
        public bool cboxIOA2
        {
            get { return _cboxIOA2; }
            set
            {
                _cboxIOA2 = value;
                OnPropertyChanged("cboxIOA2");
            }
        }

        private bool _cboxIOA3;
        public bool cboxIOA3
        {
            get { return _cboxIOA3; }
            set
            {
                _cboxIOA3 = value;
                OnPropertyChanged("cboxIOA3");
            }
        }

        private bool _cboxIOA4;
        public bool cboxIOA4
        {
            get { return _cboxIOA4; }
            set
            {
                _cboxIOA4 = value;
                OnPropertyChanged("cboxIOA4");
            }
        }

        private bool _cboxIOA5;
        public bool cboxIOA5
        {
            get { return _cboxIOA5; }
            set
            {
                _cboxIOA5 = value;
                OnPropertyChanged("cboxIOA5");
            }
        }

        private bool _cboxIOA6;
        public bool cboxIOA6
        {
            get { return _cboxIOA6; }
            set
            {
                _cboxIOA6 = value;
                OnPropertyChanged("cboxIOA6");
            }
        }


        private bool _cboxIOA7;
        public bool cboxIOA7
        {
            get { return _cboxIOA7; }
            set
            {
                _cboxIOA7 = value;
                OnPropertyChanged("cboxIOA7");
            }
        }
        #endregion

        #region IOAB sensor List
        private bool _cboxIOB1;
        public bool cboxIOB1
        {
            get { return _cboxIOB1; }
            set
            {
                _cboxIOB1 = value;
                OnPropertyChanged("cboxIOB1");
            }
        }
        private bool _cboxIOB2;
        public bool cboxIOB2
        {
            get { return _cboxIOB2; }
            set
            {
                _cboxIOB2 = value;
                OnPropertyChanged("cboxIOB2");
            }
        }
        private bool _cboxIOB3;
        public bool cboxIOB3
        {
            get { return _cboxIOB3; }
            set
            {
                _cboxIOB3 = value;
                OnPropertyChanged("cboxIOB3");
            }
        }
        private bool _cboxIOB4;
        public bool cboxIOB4
        {
            get { return _cboxIOB4; }
            set
            {
                _cboxIOB4 = value;
                OnPropertyChanged("cboxIOB4");
            }
        }
        private bool _cboxIOB5;
        public bool cboxIOB5
        {
            get { return _cboxIOB5; }
            set
            {
                _cboxIOB5 = value;
                OnPropertyChanged("cboxIOB5");
            }
        }
        private bool _cboxIOB6;
        public bool cboxIOB6
        {
            get { return _cboxIOB6; }
            set
            {
                _cboxIOB6 = value;
                OnPropertyChanged("cboxIOB6");
            }
        }
        private bool _cboxIOB7;
        public bool cboxIOB7
        {
            get { return _cboxIOB7; }
            set
            {
                _cboxIOB7 = value;
                OnPropertyChanged("cboxIOB7");
            }
        }
        #endregion

        #region cell sensor
        private bool _cboxCellSensor1;
        public bool cboxCellSensor1
        {
            get { return _cboxCellSensor1; }
            set
            {
                _cboxCellSensor1 = value;
                OnPropertyChanged("cboxCellSensor1");
            }
        }
        private bool _cboxCellSensor2;
        public bool cboxCellSensor2
        {
            get { return _cboxCellSensor2; }
            set
            {
                _cboxCellSensor2 = value;
                OnPropertyChanged("cboxCellSensor2");
            }
        }
        private bool _cboxCellSensor3;
        public bool cboxCellSensor3
        {
            get { return _cboxCellSensor3; }
            set
            {
                _cboxCellSensor3 = value;
                OnPropertyChanged("cboxCellSensor3");
            }
        }
        private bool _cboxCellSensor4;
        public bool cboxCellSensor4
        {
            get { return _cboxCellSensor4; }
            set
            {
                _cboxCellSensor4 = value;
                OnPropertyChanged("cboxCellSensor4");
            }
        }
        private bool _cboxCellSensor5;
        public bool cboxCellSensor5
        {
            get { return _cboxCellSensor5; }
            set
            {
                _cboxCellSensor5 = value;
                OnPropertyChanged("cboxCellSensor5");
            }
        }
        private bool _cboxCellSensor6;
        public bool cboxCellSensor6
        {
            get { return _cboxCellSensor6; }
            set
            {
                _cboxCellSensor6 = value;
                OnPropertyChanged("cboxCellSensor6");
            }
        }
        private bool _cboxCellSensor7;
        public bool cboxCellSensor7
        {
            get { return _cboxCellSensor7; }
            set
            {
                _cboxCellSensor7 = value;
                OnPropertyChanged("cboxCellSensor7");
            }
        }
        private bool _cboxCellSensor8;
        public bool cboxCellSensor8
        {
            get { return _cboxCellSensor8; }
            set
            {
                _cboxCellSensor8 = value;
                OnPropertyChanged("cboxCellSensor8");
            }
        }
        private bool _cboxCellSensor9;
        public bool cboxCellSensor9
        {
            get { return _cboxCellSensor9; }
            set
            {
                _cboxCellSensor9 = value;
                OnPropertyChanged("cboxCellSensor9");
            }
        }
        private bool _cboxCellSensor10;
        public bool cboxCellSensor10
        {
            get { return _cboxCellSensor10; }
            set
            {
                _cboxCellSensor10 = value;
                OnPropertyChanged("cboxCellSensor10");
            }
        }
        private bool _cboxCellSensor11;
        public bool cboxCellSensor11
        {
            get { return _cboxCellSensor11; }
            set
            {
                _cboxCellSensor11 = value;
                OnPropertyChanged("cboxCellSensor11");
            }
        }
        private bool _cboxCellSensor12;
        public bool cboxCellSensor12
        {
            get { return _cboxCellSensor12; }
            set
            {
                _cboxCellSensor12 = value;
                OnPropertyChanged("cboxCellSensor12");
            }
        }
        #endregion

        #region bladdercell
        private string _BladderCell_1 = "IDLE";
        public string BladderCell_1
        {
            get { return _BladderCell_1; }
            set
            {
                _BladderCell_1 = value;
                OnPropertyChanged("BladderCell_1");
            }
        }

        private string _BladderCell_2 = "IDLE";
        public string BladderCell_2
        {
            get { return _BladderCell_2; }
            set
            {
                _BladderCell_2 = value;
                OnPropertyChanged("BladderCell_2");
            }
        }

        private string _BladderCell_3 = "IDLE";
        public string BladderCell_3
        {
            get { return _BladderCell_3; }
            set
            {
                _BladderCell_3 = value;
                OnPropertyChanged("BladderCell_3");
            }
        }

        private string _BladderCell_4 = "IDLE";
        public string BladderCell_4
        {
            get { return _BladderCell_4; }
            set
            {
                _BladderCell_4 = value;
                OnPropertyChanged("BladderCell_4");
            }
        }

        private string _BladderCell_5= "IDLE";
        public string BladderCell_5
        {
            get { return _BladderCell_5; }
            set
            {
                _BladderCell_5 = value;
                OnPropertyChanged("BladderCell_5");
            }
        }

        private string _BladderCell_6 = "IDLE";
        public string BladderCell_6
        {
            get { return _BladderCell_6; }
            set
            {
                _BladderCell_6 = value;
                OnPropertyChanged("BladderCell_6");
            }
        }

        private string _BladderCell_7 = "IDLE";
        public string BladderCell_7
        {
            get { return _BladderCell_7; }
            set
            {
                _BladderCell_7 = value;
                OnPropertyChanged("BladderCell_7");
            }
        }

        private string _BladderCell_8 = "IDLE";
        public string BladderCell_8
        {
            get { return _BladderCell_8; }
            set
            {
                _BladderCell_8 = value;
                OnPropertyChanged("BladderCell_8");
            }
        }

        private string _BladderCell_9 = "IDLE";
        public string BladderCell_9
        {
            get { return _BladderCell_9; }
            set
            {
                _BladderCell_9 = value;
                OnPropertyChanged("BladderCell_9");
            }
        }

        private string _BladderCell_10 = "IDLE";
        public string BladderCell_10
        {
            get { return _BladderCell_10; }
            set
            {
                _BladderCell_10 = value;
                OnPropertyChanged("BladderCell_10");
            }
        }

        private string _BladderCell_11 = "IDLE";
        public string BladderCell_11
        {
            get { return _BladderCell_11; }
            set
            {
                _BladderCell_11 = value;
                OnPropertyChanged("BladderCell_11");
            }
        }

        private string _BladderCell_12 = "IDLE";
        public string BladderCell_12
        {
            get { return _BladderCell_12; }
            set
            {
                _BladderCell_12 = value;
                OnPropertyChanged("BladderCell_12");
            }
        }

        #endregion

        #region powersupply stat
        private string _PSStat_1 = "IDLE";
        public string PSStat_1
        {
            get { return _PSStat_1; }
            set
            {
                _PSStat_1 = value;
                OnPropertyChanged("PSStat_1");
            }
        }

        private string _PSStat_2 = "IDLE";
        public string PSStat_2
        {
            get { return _PSStat_2; }
            set
            {
                _PSStat_2 = value;
                OnPropertyChanged("PSStat_2");
            }
        }

        private string _PSStat_3 = "IDLE";
        public string PSStat_3
        {
            get { return _PSStat_3; }
            set
            {
                _PSStat_3 = value;
                OnPropertyChanged("PSStat_3");
            }
        }

        private string _PSStat_4 = "IDLE";
        public string PSStat_4
        {
            get { return _PSStat_4; }
            set
            {
                _PSStat_4 = value;
                OnPropertyChanged("PSStat_4");
            }
        }

        private string _PSStat_5 = "IDLE";
        public string PSStat_5
        {
            get { return _PSStat_5; }
            set
            {
                _PSStat_5 = value;
                OnPropertyChanged("PSStat_5");
            }
        }

        private string _PSStat_6 = "IDLE";
        public string PSStat_6
        {
            get { return _PSStat_6; }
            set
            {
                _PSStat_6 = value;
                OnPropertyChanged("PSStat_6");
            }
        }

        private string _PSStat_7 = "IDLE";
        public string PSStat_7
        {
            get { return _PSStat_7; }
            set
            {
                _PSStat_7 = value;
                OnPropertyChanged("PSStat_7");
            }
        }

        private string _PSStat_8 = "IDLE";
        public string PSStat_8
        {
            get { return _PSStat_8; }
            set
            {
                _PSStat_8 = value;
                OnPropertyChanged("PSStat_8");
            }
        }

        private string _PSStat_9 = "IDLE";
        public string PSStat_9
        {
            get { return _PSStat_9; }
            set
            {
                _PSStat_9 = value;
                OnPropertyChanged("PSStat_9");
            }
        }

        private string _PSStat_10 = "IDLE";
        public string PSStat_10
        {
            get { return _PSStat_10; }
            set
            {
                _PSStat_10 = value;
                OnPropertyChanged("PSStat_10");
            }
        }

        private string _PSStat_11 = "IDLE";
        public string PSStat_11
        {
            get { return _PSStat_11; }
            set
            {
                _PSStat_11 = value;
                OnPropertyChanged("PSStat_11");
            }
        }

        private string _PSStat_12 = "IDLE";
        public string PSStat_12
        {
            get { return _PSStat_12; }
            set
            {
                _PSStat_12 = value;
                OnPropertyChanged("PSStat_12");
            }
        }
        #endregion

        #region power supply voltage current

        private string _CellVoltage1 = "0";
        public string CellVoltage1
        {
            get { return _CellVoltage1; }
            set
            {
                _CellVoltage1 = value;
                OnPropertyChanged("CellVoltage1");
            }
        }

        private string _CellVoltage2 = "0";
        public string CellVoltage2
        {
            get { return _CellVoltage2; }
            set
            {
                _CellVoltage2 = value;
                OnPropertyChanged("CellVoltage2");
            }
        }

        private string _CellVoltage3 = "0";
        public string CellVoltage3
        {
            get { return _CellVoltage3; }
            set
            {
                _CellVoltage3 = value;
                OnPropertyChanged("CellVoltage3");
            }
        }

        private string _CellVoltage4= "0";
        public string CellVoltage4
        {
            get { return _CellVoltage4; }
            set
            {
                _CellVoltage4 = value;
                OnPropertyChanged("CellVoltage4");
            }
        }

        private string _CellVoltage5 = "0";
        public string CellVoltage5
        {
            get { return _CellVoltage5; }
            set
            {
                _CellVoltage5 = value;
                OnPropertyChanged("CellVoltage5");
            }
        }

        private string _CellVoltage6 = "0";
        public string CellVoltage6
        {
            get { return _CellVoltage6; }
            set
            {
                _CellVoltage6 = value;
                OnPropertyChanged("CellVoltage6");
            }
        }

        private string _CellVoltage7 = "0";
        public string CellVoltage7
        {
            get { return _CellVoltage7; }
            set
            {
                _CellVoltage7 = value;
                OnPropertyChanged("CellVoltage7");
            }
        }

        private string _CellVoltage8 = "0";
        public string CellVoltage8
        {
            get { return _CellVoltage8; }
            set
            {
                _CellVoltage8 = value;
                OnPropertyChanged("CellVoltage8");
            }
        }

        private string _CellVoltage9 = "0";
        public string CellVoltage9
        {
            get { return _CellVoltage9; }
            set
            {
                _CellVoltage9 = value;
                OnPropertyChanged("CellVoltage9");
            }
        }

        private string _CellVoltage10 = "0";
        public string CellVoltage10
        {
            get { return _CellVoltage10; }
            set
            {
                _CellVoltage10 = value;
                OnPropertyChanged("CellVoltage10");
            }
        }

        private string _CellVoltage11 = "0";
        public string CellVoltage11
        {
            get { return _CellVoltage11; }
            set
            {
                _CellVoltage11 = value;
                OnPropertyChanged("CellVoltage11");
            }
        }

        private string _CellVoltage12 = "0";
        public string CellVoltage12
        {
            get { return _CellVoltage12; }
            set
            {
                _CellVoltage12 = value;
                OnPropertyChanged("CellVoltage12");
            }
        }

        private string _CellCurent1 = "0";
        public string CellCurent1
        {
            get { return _CellCurent1; }
            set
            {
                _CellCurent1 = value;
                OnPropertyChanged("CellCurent1");
            }
        }

        private string _CellCurent2 = "0";
        public string CellCurent2
        {
            get { return _CellCurent2; }
            set
            {
                _CellCurent2 = value;
                OnPropertyChanged("CellCurent2");
            }
        }

        private string _CellCurent3 = "0";
        public string CellCurent3
        {
            get { return _CellCurent3; }
            set
            {
                _CellCurent3 = value;
                OnPropertyChanged("CellCurent3");
            }
        }

        private string _CellCurent4 = "0";
        public string CellCurent4
        {
            get { return _CellCurent4; }
            set
            {
                _CellCurent4 = value;
                OnPropertyChanged("CellCurent4");
            }
        }

        private string _CellCurent5 = "0";
        public string CellCurent5
        {
            get { return _CellCurent5; }
            set
            {
                _CellCurent5 = value;
                OnPropertyChanged("CellCurent5");
            }
        }

        private string _CellCurent6 = "0";
        public string CellCurent6
        {
            get { return _CellCurent6; }
            set
            {
                _CellCurent6 = value;
                OnPropertyChanged("CellCurent6");
            }
        }

        private string _CellCurent7= "0";
        public string CellCurent7
        {
            get { return _CellCurent7; }
            set
            {
                _CellCurent7 = value;
                OnPropertyChanged("CellCurent7");
            }
        }

        private string _CellCurent8 = "0";
        public string CellCurent8
        {
            get { return _CellCurent8; }
            set
            {
                _CellCurent8 = value;
                OnPropertyChanged("CellCurent8");
            }
        }

        private string _CellCurent9 = "0";
        public string CellCurent9
        {
            get { return _CellCurent9; }
            set
            {
                _CellCurent9 = value;
                OnPropertyChanged("CellCurent9");
            }
        }

        private string _CellCurent10 = "0";
        public string CellCurent10
        {
            get { return _CellCurent10; }
            set
            {
                _CellCurent10 = value;
                OnPropertyChanged("CellCurent10");
            }
        }

        private string _CellCurent11 = "0";
        public string CellCurent11
        {
            get { return _CellCurent11; }
            set
            {
                _CellCurent11 = value;
                OnPropertyChanged("CellCurent11");
            }
        }

        private string _CellCurent12 = "0";
        public string CellCurent12
        {
            get { return _CellCurent12; }
            set
            {
                _CellCurent12 = value;
                OnPropertyChanged("CellCurent12");
            }
        }

        #endregion

        #region ManualMode
        private string _MandrelStat;
        public string MandrelStat
        {
            get { return _MandrelStat; }
            set
            {
                _MandrelStat = value;
                OnPropertyChanged("MandrelStat");
            }
        }

        private string _BackerStat;
        public string BackerStat
        {
            get { return _BackerStat; }
            set
            {
                _BackerStat = value;
                OnPropertyChanged("BackerStat");
            }
        }

        private string _IOQBStat;
        public string IOQBStat
        {
            get { return _IOQBStat; }
            set
            {
                _IOQBStat = value;
                OnPropertyChanged("IOQBStat");
            }
        }

        private string _IOQAStat;
        public string IOQAStat
        {
            get { return _IOQAStat; }
            set
            {
                _IOQAStat = value;
                OnPropertyChanged("IOQAStat");
            }
        }

        private string _BRTPumpStat;
        public string BRTPumpStat
        {
            get { return _BRTPumpStat; }
            set
            {
                _BRTPumpStat = value;
                OnPropertyChanged("BRTPumpStat");
            }
        }

        private string _MainPumpStat;
        public string MainPumpStat
        {
            get { return _MainPumpStat; }
            set
            {
                _MainPumpStat = value;
                OnPropertyChanged("MainPumpStat");
            }
        }

        private string _TempStat = "0";
        public string TempStat
        {
            get { return _TempStat; }
            set
            {
                _TempStat = value;
                OnPropertyChanged("TempStat");
            }
        }

        private string _HeaterStat;
        public string HeaterStat
        {
            get { return _HeaterStat; }
            set
            {
                _HeaterStat = value;
                OnPropertyChanged("HeaterStat");
            }
        }

        private string _PlenumQBStat;
        public string PlenumQBStat
        {
            get { return _PlenumQBStat; }
            set
            {
                _PlenumQBStat = value;
                OnPropertyChanged("PlenumQBStat");
            }
        }

        private string _PlenumQAStat;
        public string PlenumQAStat
        {
            get { return _PlenumQAStat; }
            set
            {
                _PlenumQAStat = value;
                OnPropertyChanged("PlenumQAStat");
            }
        }

        private string _PlenumDBLStat;
        public string PlenumDBLStat
        {
            get { return _PlenumDBLStat; }
            set
            {
                _PlenumDBLStat = value;
                OnPropertyChanged("PlenumDBLStat");
            }
        }

        private string _PlenumPMPStat;
        public string PlenumPMPStat
        {
            get { return _PlenumPMPStat; }
            set
            {
                _PlenumPMPStat = value;
                OnPropertyChanged("PlenumPMPStat");
            }
        }

        private string _PlenumSRDStat;
        public string PlenumSRDStat
        {
            get { return _PlenumSRDStat; }
            set
            {
                _PlenumSRDStat = value;
                OnPropertyChanged("PlenumSRDStat");
            }
        }

        private string _PlenumRNStat;
        public string PlenumRNStat
        {
            get { return _PlenumRNStat; }
            set
            {
                _PlenumRNStat = value;
                OnPropertyChanged("PlenumRNStat");
            }
        }

        private string _DiWaterStat;
        public string DiWaterStat
        {
            get { return _DiWaterStat; }
            set
            {
                _DiWaterStat = value;
                OnPropertyChanged("DiWaterStat");
            }
        }

        private string _DoorLockStat;
        public string DoorLockStat
        {
            get { return _DoorLockStat; }
            set
            {
                _DoorLockStat = value;
                OnPropertyChanged("DoorLockStat");
            }
        }

        private string _BypassStat;
        public string BypassStat
        {
            get { return _BypassStat; }
            set
            {
                _BypassStat = value;
                OnPropertyChanged("BypassStat");
            }
        }

        private string _AlarmStat;
        public string AlarmStat
        {
            get { return _AlarmStat; }
            set
            {
                _AlarmStat = value;
                OnPropertyChanged("AlarmStat");
            }
        }

        private string _EstopStat;
        public string EstopStat
        {
            get { return _EstopStat; }
            set
            {
                _EstopStat = value;
                OnPropertyChanged("EstopStat");
            }
        }

        private string _PowerSupplyStat;
        public string PowerSupplyStat
        {
            get { return _PowerSupplyStat; }
            set
            {
                _PowerSupplyStat = value;
                OnPropertyChanged("PowerSupplyStat");
            }
        }

        private string _BKAgitStat;
        public string BKAgitStat
        {
            get { return _BKAgitStat; }
            set
            {
                _BKAgitStat = value;
                OnPropertyChanged("BKAgitStat");
            }
        }

        private string _FTAgitStat;
        public string FTAgitStat
        {
            get { return _FTAgitStat; }
            set
            {
                _FTAgitStat = value;
                OnPropertyChanged("FTAgitStat");
            }
        }

        private string _CTPumpStat;
        public string CTPumpStat
        {
            get { return _CTPumpStat; }
            set
            {
                _CTPumpStat = value;
                OnPropertyChanged("CTPumpStat");
            }
        }

        private string _PHPumpStat;
        public string PHPumpStat
        {
            get { return _PHPumpStat; }
            set
            {
                _PHPumpStat = value;
                OnPropertyChanged("PHPumpStat");
            }
        }
        #endregion

        
        private bool _cboxEn_1;
        public bool cboxEn_1
        {
            get { return _cboxEn_1; }
            set
            {
                _cboxEn_1 = value;
                OnPropertyChanged("cboxEn_1");
            }
        }

        private bool _cboxEn_2;
        public bool cboxEn_2
        {
            get { return _cboxEn_2; }
            set
            {
                _cboxEn_2 = value;
                OnPropertyChanged("cboxEn_2");
            }
        }

        private bool _cboxEn_3;
        public bool cboxEn_3
        {
            get { return _cboxEn_3; }
            set
            {
                _cboxEn_3 = value;
                OnPropertyChanged("cboxEn_3");
            }
        }

        private bool _cboxEn_4;
        public bool cboxEn_4
        {
            get { return _cboxEn_4; }
            set
            {
                _cboxEn_4 = value;
                OnPropertyChanged("cboxEn_4");
            }
        }

        private bool _cboxEn_5;
        public bool cboxEn_5
        {
            get { return _cboxEn_5; }
            set
            {
                _cboxEn_5 = value;
                OnPropertyChanged("cboxEn_5");
            }
        }

        private bool _cboxEn_6;
        public bool cboxEn_6
        {
            get { return _cboxEn_6; }
            set
            {
                _cboxEn_6 = value;
                OnPropertyChanged("cboxEn_6");
            }
        }


        private bool _cboxEn_7;
        public bool cboxEn_7
        {
            get { return _cboxEn_7; }
            set
            {
                _cboxEn_7 = value;
                OnPropertyChanged("cboxEn_7");
            }
        }

        private bool _cboxEn_8;
        public bool cboxEn_8
        {
            get { return _cboxEn_8; }
            set
            {
                _cboxEn_8 = value;
                OnPropertyChanged("cboxEn_8");
            }
        }


        private bool _cboxEn_9;
        public bool cboxEn_9
        {
            get { return _cboxEn_9; }
            set
            {
                _cboxEn_9 = value;
                OnPropertyChanged("cboxEn_9");
            }
        }

        private bool _cboxEn_10;
        public bool cboxEn_10
        {
            get { return _cboxEn_10; }
            set
            {
                _cboxEn_10 = value;
                OnPropertyChanged("cboxEn_10");
            }
        }

        private bool _cboxEn_11;
        public bool cboxEn_11
        {
            get { return _cboxEn_11; }
            set
            {
                _cboxEn_11 = value;
                OnPropertyChanged("cboxEn_11");
            }
        }

        private bool _cboxEn_12;
        public bool cboxEn_12
        {
            get { return _cboxEn_12; }
            set
            {
                _cboxEn_12 = value;
                OnPropertyChanged("cboxEn_12");
            }
        }

        private string _BatchNo="0";
        public string BatchNo
        {
            get { return _BatchNo; }
            set
            {
                _BatchNo = value;
                OnPropertyChanged("BatchNo");
            }
        }

        private string _NextSet = "A";
        public string NextSet
        {
            get { return _NextSet; }
            set
            {
                _NextSet = value;
                OnPropertyChanged("NextSet");
            }
        }

        

      

        private bool _IOASensorEnable = true;
        public bool IOASensorEnable
        {
            get { return _IOASensorEnable; }
            set
            {
                _IOASensorEnable = value;
                OnPropertyChanged("IOASensorEnable");
            }
        }

        private bool _IOBSensorEnable = true;
        public bool IOBSensorEnable
        {
            get { return _IOBSensorEnable; }
            set
            {
                _IOBSensorEnable = value;
                OnPropertyChanged("IOBSensorEnable");
            }
        }

        private bool _TemperatureCheck = true;
        public bool TemperatureCheck
        {
            get { return _TemperatureCheck; }
            set
            {
                _TemperatureCheck = value;
                OnPropertyChanged("TemperatureCheck");
            }
        }

        private bool _CellSensorCheck = true;
        public bool CellSensorCheck
        {
            get { return _CellSensorCheck; }
            set
            {
                _CellSensorCheck = value;
                OnPropertyChanged("CellSensorCheck");
            }
        }
        


        private bool _PowerSupplyEnable=  true;
        public bool PowerSupplyEnable
        {
            get { return _PowerSupplyEnable; }
            set
            {
                _PowerSupplyEnable = value;
                OnPropertyChanged("PowerSupplyEnable");
            }
        }

        private bool _CurtainSensorCheck = true;
        public bool CurtainSensorCheck
        {
            get { return _CurtainSensorCheck; }
            set
            {
                _CurtainSensorCheck = value;
                OnPropertyChanged("CurtainSensorCheck");
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string info)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(info));
            }
        }

        public void SilenceAlarm()
        {
            ErrorCode = "Silence Alarm";
        }

        public void ResetAlarm()
        {
            ErrorCode = "Reset Alarm!";
        }



        private void OnPropertyChanged2(object sender, string property)
        {
            if (property == "")
            {

            }
            else if (property.Contains("Alert,"))
            {
                var msg = property.Split(',');
                MessageLogsTuple = Tuple.Create(DateTime.Now, " " + property);
                ErrorCode = "";
                AlarmStat = "ON";
                MyConfig.Cfg.TowerLight("Red");
                hw.outputs["Y3_15_SONAALERT_(OWA)"].ON();
                MyConfig.Cfg.ShowCustomDialog("Alert");
                //System.Windows.MessageBox.Show(msg[1], "ERROR", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK, System.Windows.MessageBoxOptions.DefaultDesktopOnly);

            }
            else if (property.Contains("Info,"))
            { 
                var msg = property.Split(',');
                MessageLogsTuple = Tuple.Create(DateTime.Now, " " + property);
                ErrorCode = "";
                //hw.outputs["Y3_15_SONAALERT_(OWA)"].ON();
                BuzzerInfo();
                //System.Windows.MessageBox.Show(msg[1], "INFO", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK, System.Windows.MessageBoxOptions.DefaultDesktopOnly);
                MyConfig.Cfg.ShowCustomDialog("Info");

            }
            else if (property.Contains("Silence Alarm"))
            {
                MessageLogsTuple = Tuple.Create(DateTime.Now, " " + property);
                //MessageBox.Show(_property2[1]);
                ErrorCode = "";
                AlarmStat = "Silence";
                hw.outputs["Y3_15_SONAALERT_(OWA)"].OFF();
            }
            else if (property.Contains("Reset Alarm!"))
            {
                MessageLogsTuple = Tuple.Create(DateTime.Now, " " + property);
                //MessageBox.Show(_property2[1]);
                ErrorCode = "";
                AlarmStat = "OFF";
                MyConfig.Cfg.TowerLight("Green");
                hw.outputs["Y3_15_SONAALERT_(OWA)"].OFF();
            }
            else
            {
                //logger.Error(property);
                MessageLogsTuple = Tuple.Create(DateTime.Now, " " + property);
            }

        }
        
        private void BuzzerInfo()
        {
            Task.Run(() =>
            {
                hw.outputs["Y3_15_SONAALERT_(OWA)"].ON();
                Thread.Sleep(300);
                hw.outputs["Y3_15_SONAALERT_(OWA)"].OFF();
                hw.outputs["Y3_15_SONAALERT_(OWA)"].ON();
                Thread.Sleep(300);
                hw.outputs["Y3_15_SONAALERT_(OWA)"].OFF();
            });
        }
    }

}

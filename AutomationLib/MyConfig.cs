﻿using Advantech.Adam;
using AutomationLib;
using MachineControl;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace AutomationLib
{

    public  sealed partial class MyConfig 
    {
        private static volatile MyConfig instance;
        private static object syncRoot = new Object();
        public string ConfigPath = @"C:\HP\";
        public Sqlcrud Sql = new Sqlcrud();
        public Model Model;
        Dictionary<string, RootObject> Recepidict;
        public RootObject RecepiList = new RootObject();
        public UserManagement userManagement = new UserManagement();
        public ChangeLogList changeLogList = new ChangeLogList();
        public List<Tuple<string, DispatcherTimer,int>> TimerList = new List<Tuple<string, DispatcherTimer, int>>();
        public Dictionary<string, PowerSupply> PSList = new Dictionary<string, PowerSupply>();
        public List<Tuple<string, int>> PowerSupplyAddress = new List<Tuple<string, int>>();
        public bool CheckUndip = false;
        public Task EndLotTask = null;
        string ProcessName = "EPSON RC+ 7.0 (32 BIT).exe";

        public List<string> UndetectedCellBank1 = new List<string>();
        public List<string> UndetectedCellBank2 = new List<string>();

        public Action<string> TowerLight;
        public Action<string, string , string> UpdateUI;
        public Action<string> ShowCustomDialog;

        public Task FrontAgittask, BackAgitTask;

        //adam
        private AdamSocket adamModbus, adamUDP;
        private Adam6000Type m_Adam6000Type;

        public ConcurrentQueue<Tuple<int, string>> UnloadQueA;
        public ConcurrentQueue<Tuple<int, string>> UnloadQueB;

        /// <summary>
        /// ez mode value return to MyConfig.Cfg.PSList["cellnumber"]
        /// <param name="Option"> (cellnumber,monitor|OFF|ON)</param>
        /// <param name="CellNumber"></param>
        /// </summary>
        //public Action<string, string> PScmd;
        private MyConfig()
        {
            //scriptPath = ConfigPath + @"script\";
            //path = ConfigPath + @"Configuration\";
            //profilePath = ConfigPath + @"Configuration\Profiles\";
            //globalINI = new INIFile(path + "Global.ini");
            //currentProfile = globalINI.GetValue("General", "Profile", "Default");
            //profileINI = new INIFile(profilePath + currentProfile + ".ini");
        }

        public static MyConfig Cfg
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new MyConfig();
                    }
                }

                return instance;
            }
        }

        internal MainFlow.SequenceState currentState { get; set; }

        public void InitializeConfig(string CurrentRecepi)
        {
            Model = new Model();
            //load Recepi
            this.CurrentRecepi = CurrentRecepi;
            LoadRecepi(CurrentRecepi);

            for (int i = 1; i < 12; i++)
            {
                PowerSupplyAddress.Add(Tuple.Create($"PS{i}", 5));
            }
            Sql.Initialize("Sqlite", "", "", "", "");
    
        }


        public void LoadRecepi(string CurrentRecepi)
        {

            string json = File.ReadAllText(ConfigPath + $@"Recepi\{CurrentRecepi}.json");
            try
            {
                JToken.Parse(json);
            }
            catch (Exception ex)
            {
                Model.ErrorCode = "Alert,Error Reading Recepi";
                return;
            }
         
            JsonConvert.PopulateObject(json, RecepiList);
        }

        public void LoadUser()
        {
            string json = File.ReadAllText(ConfigPath + $@"Recepi\{UserFile}.json");
            try
            {
                JToken.Parse(json);
            }
            catch (Exception ex)
            {
                Model.ErrorCode = "Alert,Error Reading Recepi";
                return;
            }

            JsonConvert.PopulateObject(json, userManagement);

        }

        public void LoadChangeLog()
        {
            string json = File.ReadAllText(ConfigPath + $@"Recepi\{ChangeLog}.json");
            try
            {
                JToken.Parse(json);
            }
            catch (Exception ex)
            {
                Model.ErrorCode = "Alert,Error Reading Recepi";
                return;
            }

            JsonConvert.PopulateObject(json, changeLogList);
        }
        public void SaveChangeLog()
        {
            File.WriteAllText(ConfigPath + $@"Recepi\{ChangeLog}.json", JsonConvert.SerializeObject(changeLogList, Formatting.Indented));
        }

        public void SaveRecepi()
        {
            File.WriteAllText(ConfigPath + $@"Recepi\{CurrentRecepi}.json", JsonConvert.SerializeObject(RecepiList, Formatting.Indented));
        }


        public string ProcessState { get; set; } = "Idle";

        public bool SimulationMode { get; set; }
   

        public Dictionary<string,string> CellList { get; set; }
        public Dictionary<string, Tuple<string,int>> Bank1 { get; set; }
        public Dictionary<string, Tuple<string, int>> Bank2 { get; set; }

        public bool EndLotButtonPressed { get; set; } = false;
        public bool StartEndlotTimer { get; set; } = false;
        public bool SoftEmoRecover { get; set; } = false;
        public bool SoftEmo { get; set; } = false;
        public bool RobotRequestStop { get; set; } = false;

        public bool RobotSoftEMO { get; set; } = false;

        public int BankUsed { get; set; } = 1;
        public string Bank1Stat { get; set; } = "Empty";
        public string Bank2Stat { get; set; } = "Empty";

       
        public int NumberOfUnitInsideBank1{get;set;} = 0;
        public int NumberOfUnitInsideBank2 { get; set; } = 0;
        public int NumberOfUnitToPick { get; set; } = 0;
        public int EnabledCellinBank1 { get; set; } = 0;
        public int EnabledCellinBank2 { get; set; } = 0;

        public string CurrentSet { get; set; } = "A";

        public string Username { get; set; } = "";
        public string UserType { get; set; } = "";

        public string CurrentRecepi { get; set; } = "Recepi";
        public string UserFile { get; set; } = "User";
        public string ChangeLog { get; set; } = "ChangeLog";
        public string SelectedRecepi { get; set; }
   
        public bool RequestEndLot { get; set; }
        public bool EndLotTimer { get; set; }
        public bool RequestEndLotToIDLE { get; set; }
        public bool EndProcess { get; set; }

        public bool RequestPause { get; internal set; }
        public string TempValue { get; set; }
        public bool RequestNextSet { get; set; }


        //plating configuration

        public int CellExpireTime { get; set; }
        public double PlatingCurrent { get; set; } = 20;
        public double PlatingVoltage { get; set; } = 12.50;
        public double TrickleVoltage { get; set; }=0.04;
        public double TrickleCurrent { get; set; } = 0.10;
        public double CutoffVoltage { get; set; } = 12.50;
        public double LowResistance { get; set; } = 0.30;
        public double HighResistance { get; set; } = 0.80;
        public double OpenCircuit { get; set; } = 2.00;
        public double RampSteps { get; set; } = 3;
        public double RampDuration { get; set; } = 3;
        public bool FrontAgitIsOn { get; set; }
        public bool BackAgitIsOn { get; set; }

        public double TemperatureOffset { get; set; } = 0.0;//40 sec

        public bool TankFillCancel { get; set; } = false;
  
        public double FillDelayIOA { get; set; } = 70;//40 sec
        public double SecondFillDelayIOA { get; set; } = 30;//40 sec
        public double DrainDelayIOA { get; set; } = 30;//40 sec
        public double ReFillDelayIOA { get; set; } = 70;//40 sec

        public double FillDelayIOB { get; set; } = 70;//40 sec
        public double SecondFillDelayIOB { get; set; } = 20;//40 sec
        public double DrainDelayIOB { get; set; } = 30;//40 sec
        public double ReFillDelayIOB { get; set; } = 70;//40 sec

        public double FillDelayMandrel { get; set; } = 210;//40 sec
        public double SecondFillDelayMandrel { get; set; } = 30;//40 sec
        public double DrainDelayMandrel { get; set; } = 30;//40 sec
        public double FillDelayBacker { get; set; } = 210;//40 sec
        public double SecondFillDelayBacker { get; set; } = 210;//40 sec
        public double DrainDelayBacker { get; set; } = 30;//40 sec

        public double TimeAgitMoveBk { get; set; } = 30;//40 sec
        public double TimeAgitMoveFt { get; set; } = 30;//40 sec

        public double TimeSignalFt { get; set; } = 30;//40 sec
        public double TimeSignalBk { get; set; } = 30;//40 sec

        public double TempSetPoint { get; set; } = 34;
        public double TempTolerance { get; set; } = 34;

        public double BrtCycleDelay { get; set; } = 18;


        public double TotalAmpMin
        {

            get
            {
                return (double)(Config.Cfg.GetSettings("SavedValue", "TotalAmpMin"));
            }
            set
            {
                Config.Cfg.SaveSetting("SavedValue", "TotalAmpMin", value);
     
            }

        }

        public double UpTimeCount
        {
            get
            {
                return (double)(Config.Cfg.GetSettings("SavedValue", "UpTimeCount"));
            }
            set
            {
                Config.Cfg.SaveSetting("SavedValue", "UpTimeCount", value);

            }
        }

        public void SaveINI(string Key="",string Section="",String Value="")
        {
            if (Key != "")
            {
                Config.Cfg.SaveSetting(Key, Section, Value);
            }

            Config.Cfg.Save();
        }

        public int FillToL
        {
            get
            {
                return (int)(Config.Cfg.GetSettings("Water Level", "FillToL"));
            }
            set
            {
                Config.Cfg.SaveSetting("Water Level", "FillToL", value);
            }
        }

        public int FillToF
        {
            get
            {
                return (int)(Config.Cfg.GetSettings("Water Level", "FillToF"));
            }
            set
            {
                Config.Cfg.SaveSetting("Water Level", "FillToF", value);
            }
        }

        public int FillToH
        {
            get
            {
                return (int)(Config.Cfg.GetSettings("Water Level", "FillToH"));
            }
            set
            {
                Config.Cfg.SaveSetting("Water Level", "FillToH", value);
            }
        }

        public int FillToHH
        {
            get
            {
                return (int)(Config.Cfg.GetSettings("Water Level", "FillToHH"));
            }
            set
            {
                Config.Cfg.SaveSetting("Water Level", "FillToHH", value);
            }
        }

        public string UIValue { get; set; } = "";//40 sec

        public int BrtStrokeCount
        {
            get
            {
                return (int)(Config.Cfg.GetSettings("BRT", "Count"));
            }
            set
            {
                Config.Cfg.SaveSetting("BRT", "Count", value);
            }
        }

        public bool EMOStat { get; set; } = false;
        public bool MainTankStop { get; internal set; }
        public bool IOQBStatus { get; set; } = false;
        public bool JustOneCell { get;  set; }

        [DllImport("User32.dll")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);
        [DllImport("User32.dll")]
        public static extern bool ShowWindow(IntPtr hWnd,int nCmdShow);

        public void OpenRobotProgram()
        {
           
            try
            {
                ProcessName = @"C:\EpsonRC70\exe\erc70.exe";
                var PNAME = Process.GetProcessesByName("erc70");
                IntPtr WIN = PNAME[0].MainWindowHandle;
                ShowWindow(WIN, 9);
                SetForegroundWindow(WIN);
                //Process process = Process.Start(ProcessName);
                //WindowHelper.BringProcessToFront(process);
            }
            catch (Exception ex)
            {

                MyConfig.Cfg.Model.ErrorCode = "Info,Could not open Robot Program please open robot program";
            }
           
        }

        private static class WindowHelper
        {

            public static void BringProcessToFront(Process process)
            {
                IntPtr handle = process.MainWindowHandle;
                if (IsIconic(handle))
                {
                    ShowWindow(handle, SW_RESTORE);
                }

                SetForegroundWindow(handle);
            }

            const int SW_RESTORE = 9;

            [System.Runtime.InteropServices.DllImport("User32.dll")]
            private static extern bool SetForegroundWindow(IntPtr handle);
            [System.Runtime.InteropServices.DllImport("User32.dll")]
            private static extern bool ShowWindow(IntPtr handle, int nCmdShow);
            [System.Runtime.InteropServices.DllImport("User32.dll")]
            private static extern bool IsIconic(IntPtr handle);
        }

        public class Var
        {
            public const string WaitingSet = "WaitingSet";
            public const string Loading = "Loading";
            public const string Idle = "Idle";
            public const string Undip = "Undip";
            public const string Dip = "Dip";
            public const string LotEnd = "LotEnd";
            public const string RobotStop = "Stop";
            public const string RobotResume = "Resume";
            public const string IOAFront = "IOAFront";
            public const string IOBFront = "IOBFront";
        }
    }
    

}

﻿using HardwareLib;
using MachineControl;
using MachineControlBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AutomationLib
{
    public class MainFlow
    {
        public Hardware hw;
        public Events ev;
        public TowerLight towerLight;
        public IOListener ioListener;
        public PowerSupplyModule PS;
        public RobotControl Robot;
        public CellModule cellModule;
        public TankModule tankModule;
        public SensorModule sensorModule;
        public TemperatureControl temperatureControl;
        Task taskChangeState;
        Task taskInitialize;

        public bool StartLotPressed = false;
        public bool WarningPressed = false;
        public bool EndLotPressed = false;
        bool SequenceExit = false;
        bool InitializeDone = false;
        bool SensorError = false;
        bool CellInitialized = false;
        public bool AlarmStat = false;
        bool EndLotTimerStarted = false;
        DateTime timecycleStart;

        DateTime EndLotEndTime = new DateTime();
        DateTime EndLotCountTime = new DateTime();
        bool isEndLotTimerStarted = false;

        SequenceState PreviousState;

        public List<Tuple<string, int>> SensorListenerList = new List<Tuple<string, int>>();

        public bool InitializePressed { get; set; } = false;

        String PreviosStatus = "";

        public MainFlow(Hardware hw)
        {
            this.hw = hw;
            this.ev = new Events();
            PS = new PowerSupplyModule();
            InitIoListener();
            towerLight = new TowerLight(hw.outputs, 1000, "Y3_13_LTOWER_&_OWA_POWER_ALARM_LAMP_(RED)", "Y3_12_LTOWER_&_OWA_POWER_DANDER_(YELLOW)", "Y3_11_LTOWER_&_OWA_POWER_READY_(GREEN)", "Y3_15_SONAALERT_(OWA)");
            tankModule = new TankModule(hw, ev);
            MyConfig.Cfg.Model.tankModule = tankModule;
            MyConfig.Cfg.Model.powerSupply = PS;
            temperatureControl = new TemperatureControl();
            Robot = new RobotControl(hw, ev, tankModule, this);
            MyConfig.Cfg.Model.robotControl = Robot;
            cellModule = new CellModule(hw, ev);
            cellModule.Initialize();
            MyConfig.Cfg.Model.cellModule = cellModule;
            //StartAgitator();
            temperatureControl.Init();

            sensorModule = new SensorModule(this, hw, ev);
            sensorModule.Initialize();
            sensorModule.StartSensorReading();
            //init sensor to stop/pause machine
            //InitEmgSensor(); custom sensor not used

            Task.Run(() => { WaitInitRobot(60); });

            StartSequenceTask();
            temperatureControl.ReadOn();
            //hw.inputs["X5_04_I_O_OUEUE_LID_DOWN"].CurrentValue = 1;
            //var ss = hw.inputs["X5_04_I_O_OUEUE_LID_DOWN"].Read();
            towerLight.Start();
            MyConfig.Cfg.TowerLight = SetTowerLight;
            ////start emg sensor/stop listener
            Task.Run(() => { SensorListener(); });

        }

        private void InitRobot(int resettime)
        {
            hw.outputs["Y5_14_ROBOT_RESET"].ON();
            Thread.Sleep(500);
            hw.outputs["Y5_14_ROBOT_RESET"].OFF();
        }

        //reset robot
        private int WaitInitRobot(int resettime)
        {
            if (MyConfig.Cfg.SimulationMode)
                return 0;

            //reset robot
            DateTime desired = DateTime.Now.AddSeconds(resettime);
            hw.outputs["Y5_14_ROBOT_RESET"].ON();
            while (hw.inputs["X6_14_ROBOT_POSITION_RECEIVED"].Read() != 1)
            {
                if (DateTime.Now > desired)
                {
                    desired = DateTime.Now.AddSeconds(resettime);
                    MyConfig.Cfg.Model.ErrorCode = "Alert,TimeOut: Robot Initialize Reset";
                    return 1;
                }
            }

            hw.outputs["Y5_14_ROBOT_RESET"].OFF();
            return 0;
        }


        public void InitPowerSupply(bool On)
        {
            if (On)
            {
                Thread.Sleep(3000);
                PS.StartMonitor = true;
                PS.Init(hw, MyConfig.Cfg.PowerSupplyAddress);
            }
            else
            {
                Thread.Sleep(1000);
                PS.StartMonitor = false;
                Thread.Sleep(500);
                PS.CloseAll();
            }
        }

        #region SensorListener
        public void SensorListener()
        {
            while (true)
            {
                if (ioListener.StopPressed)
                {
                    Robot.Cmd("Stop", "");
                    Robot.Cmd("StopActuator", "");
                    ioListener.StopPressed = false;
                    MyConfig.Cfg.Model.ErrorCode = "Alert, Stop Pressed";
                    MyConfig.Cfg.Model.StatusBar = "Stop Pressed-WAITING USER TO RESET";
                    MyConfig.Cfg.RequestPause = true;
                    this.currentState = SequenceState.STOP_REQUEST;
                }
                //if (AlarmStat == true)
                //{
                //    towerLight.SetAlarm();
                //    AlarmStat = false;
                //}

                if (MyConfig.Cfg.EMOStat)
                {

                }
                //read door sensor
                Thread.Sleep(300);
            }
        }

        private void InitEmgSensor()
        {
            //get sensor list
            foreach (var item in MyConfig.Cfg.RecepiList.SensorList)
            {
                var check = hw.inputs.FirstOrDefault(x => x.Key == item.SensorName);
                if (check.Key == null)
                {
                    MyConfig.Cfg.Model.ErrorCode = $"InitError: Sensor {item.SensorName} not found";
                }
                else
                    SensorListenerList.Add(Tuple.Create(item.SensorName, item.Value));
            }
        }
        #endregion


        public int CheckIOStat(string option, string option2 = "")
        {
            int stat = 0;

            switch (option)
            {
                case "Lid":
                    if (MyConfig.Cfg.SimulationMode)
                        return 0;

                    if (hw.inputs["X5_04_I_O_OUEUE_LID_DOWN"].Read() != 1)
                    {
                        stat = 1;
                        MyConfig.Cfg.Model.ErrorCode = "Alert, IOA Lid Not Closed-Please Press Reset and Start";
                        return stat;
                    }
                    if (hw.inputs["X5_05_I_O_OUEUE_LID_DOWN"].Read() != 1 && option2 == "")
                    {
                        stat = 1;
                        MyConfig.Cfg.Model.ErrorCode = "Alert, IOB Lid Not Closed-Please Press Reset and Start";
                        return stat;
                    }
                    break;

                case "IOASensor":
                    int count = 0;

                    if (!MyConfig.Cfg.Model.IOASensorEnable)
                        return 0;


                    for (int i = 2; i <= 7; i++)
                    {

                        var ioaval = GetModelValue($"cboxIOA{i}");
                        String Enable = "";

                        int banknumr = int.Parse(option2);

                        if (banknumr == 1)
                        {
                            Enable = GetModelValue($"cboxEn_{i - 1}");
                        }
                        else
                        {
                            Enable = GetModelValue($"cboxEn_{i + 5}");
                        }

                        if (Enable == "True")
                        {
                            if (ioaval != Enable)
                                count++;
                        }
                    }

                    if (count > 0)
                        stat = 1;

                    break;

                case "IOBSensor":
                    count = 0;

                    if (MyConfig.Cfg.SimulationMode || !MyConfig.Cfg.Model.IOBSensorEnable)
                        return 0;

                    for (int i = 1; i <= 6; i++)
                    {
                        var val = GetModelValue($"cboxIOB{i}");
                        if (val == "True")
                            count++;
                    }

                    if (count > 0)
                        stat = 1;
                    break;

                default:
                    break;
            }



            return stat;
        }

        //need to change
        public void StartAgitator()
        {
            MyConfig.Cfg.Model.FTAgitStat = "ON";
            MyConfig.Cfg.FrontAgitIsOn = true;
            MyConfig.Cfg.FrontAgittask = Task.Run(() =>
            {
                hw.outputs["Y1_11_SVON"].ON();
                Thread.Sleep(100);
                hw.outputs["Y1_10_RESET"].ON();
                Thread.Sleep(100);
                hw.outputs["Y1_10_RESET"].OFF();
                while (hw.inputs["X1_15_SVRE"].Read() != 1)
                {
                    if (MyConfig.Cfg.SimulationMode)
                        break;
                    //Thread.Sleep(1);
                    //wait svre
                }
                hw.outputs["Y1_07_SETUP"].ON();
                while (hw.inputs["X1_11_BUSY"].Read() == 1)
                {
                    if (MyConfig.Cfg.SimulationMode)
                        break;
                    //Thread.Sleep(1);
                    //wait buzy
                }
                while (hw.inputs["X1_12_SETON"].Read() != 1)
                {
                    if (MyConfig.Cfg.SimulationMode)
                        break;
                    //Thread.Sleep(1);
                    //wait home pos
                }
                hw.outputs["Y1_07_SETUP"].OFF();
                while (MyConfig.Cfg.FrontAgitIsOn)
                {
                    hw.outputs["Y1_09_DRIVE"].ON();
                    Thread.Sleep(50);
                    while (hw.inputs["X1_11_BUSY"].Read() == 1)
                    {
                        if (MyConfig.Cfg.SimulationMode)
                            break;
                        Thread.Sleep(1);
                    }

                    hw.outputs["Y1_09_DRIVE"].OFF();
                    hw.outputs["Y1_06_IN0"].ON();
                    Thread.Sleep(20);
                    hw.outputs["Y1_09_DRIVE"].ON();
                    Thread.Sleep(50);
                    while (hw.inputs["X1_11_BUSY"].Read() == 1)
                    {
                        if (MyConfig.Cfg.SimulationMode)
                            break;
                        Thread.Sleep(1);
                    }
                    //Thread.Sleep(800);
                    hw.outputs["Y1_06_IN0"].OFF();
                    hw.outputs["Y1_09_DRIVE"].OFF();
                    //Thread.Sleep(130);
                    Thread.Sleep(20);
                }
            });
        }

        public void StopAgitator()
        {
            hw.outputs["Y1_06_IN0"].OFF();
            hw.outputs["Y1_09_DRIVE"].OFF();
            MyConfig.Cfg.Model.FTAgitStat = "OFF";
            MyConfig.Cfg.FrontAgitIsOn = false;
            MyConfig.Cfg.FrontAgittask = null;
        }


        #region Tower Light
        private void InitIoListener()
        {

            ioListener = new IOListener(hw.inputs["X5_07_SPARE"], hw.inputs["X3_15_SPARE12"], hw.inputs["X5_01_ALARM_RESET_(OWA)"], hw.inputs["X5_03_E_STOP_LOOP_OK"]);
            ioListener.StartActiveHigh = false;
            ioListener.ResetActiveHigh = false;
            ioListener.EmergencyActiveHigh = false;
            ioListener.OnEmergencyOccur += () =>
            {
                EmoFunc();
            };
            ioListener.Start();
        }

        public void EmoFunc()
        {
            hw.outputs["Y5_03_ROBOT_PAUSE"].ON();
            hw.outputs["Y4_06_ROBOT_EMO_ON"].ON();
            hw.outputs["Y2_13_MAIN_WATER"].OFF();
            hw.outputs["Y3_00_MAIN_TANK_PUMP_(RUN)"].OFF();
            hw.outputs["Y3_01_HEATER_ENABLE"].OFF();
            hw.outputs["Y4_00_IWAKI_(_CARBON_TREAT_)"].ON();
            StopAgitator();
            //back agitator off
            hw.outputs["Y6_00_IN0"].OFF();
            hw.outputs["Y6_03_DRIVE"].OFF();
            MyConfig.Cfg.Model.BKAgitStat = "OFF";
            MyConfig.Cfg.BackAgitIsOn = false;
            MyConfig.Cfg.BackAgitTask = null;

            hw.outputs["Y3_02_POWER_SUPPLIES_ENABLE_CELL_1_6"].OFF();
            hw.outputs["Y3_03_POWER_SUPPLIES_ENABLE_CELL_7_12"].OFF();

            hw.motors["Motor1X"].Stop();
            hw.motors["Motor1X"].SetSVOn(0);
            MyConfig.Cfg.Model.StatusBar = "EMERGENCY ACTIVATED - Please release emergency Button";
            currentState = SequenceState.EMERGENCY;
        }

        //custom tower light
        public void SetTowerLight(string LightName)
        {
            if (LightName == "Red")
            {
                towerLight.SetAlarm();
            }
            else if (LightName == "Green")
            {
                towerLight.SetRunning();
            }
            else if (LightName == "Yellow")
            {
                towerLight.SetIdle();
            }
        }

        public void SetButton(string name, string param = "")
        {
            switch (name)
            {
                case "Start":
                    if (param == "StartLot")
                    {
                        //cellModule.Initialize();
                    }
                    ioListener.StartPressed = true;
                    break;
                case "Stop":
                    ioListener.StopPressed = true;
                    break;
                case "Reset":
                    ioListener.ResetPressed = true;
                    break;
                case "Emergency":
                    ioListener.EmergencyPressed = true;
                    break;
                case "EmergencyReleased":
                    ioListener.EmergencyPressed = false;
                    break;
                default:
                    throw new Exception("Invalid button name " + name);
            }
        }
        #endregion

        private void Initialize()
        {
            if (MyConfig.Cfg.SimulationMode)
            {
                InitializeDone = true;
                return;
            }
            MyConfig.Cfg.ProcessState = "Initializing";

            InitializeDone = false;
            //reset robot
            MyConfig.Cfg.RobotRequestStop = false;
            Robot.ResetRobotIO(true);

            var stat = WaitInitRobot(60);
            if (stat == 1)
            {
                MyConfig.Cfg.UpdateUI("btnInitialize", "Enable", "");
                MyConfig.Cfg.Model.StatusBar = "Error Initialize.";
                currentState = SequenceState.UNINITIALIZE;
                return;
            }

            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Homing Robot");
            Robot.Cmd("RobotHome", "");
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Homing Done");

            if (MyConfig.Cfg.RobotRequestStop)
            {
                MyConfig.Cfg.UpdateUI("btnInitialize", "Enable", "");
                MyConfig.Cfg.Model.StatusBar = "Error Initialize.";
                currentState = SequenceState.UNINITIALIZE;
                return;
            }

            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Homing MotorX");
            //home motor
            hw.motors["Motor1X"].SetSVOn(1);
            hw.motors["Motor1X"].Home(100);
            hw.motors["Motor1X"].WaitMotionDone(500);
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Homing MotorX Done");

            // RetractAllCells();

            //move ioa iob
            //tankModule.MoveIOAB("IOABack"); //ck 20Mar18
            //tankModule.MoveIOAB("IOBBack"); //ck 20Mar18
            tankModule.IOQBMove("IOABack");
            tankModule.IOQBMove("IOBBack");

            MyConfig.Cfg.Model.ErrorCode = "";

            //fill
            Task.Run(() =>
            {
                tankModule.IOQA("Fill");
            });

            Task.Run(() =>
            {
                tankModule.IOQB("Fill");
            });

            for (int i = 1; i <= 12; i++)
            {
                MyConfig.Cfg.UpdateUI($"cboxEn_{i}", "Enable", "CheckBox");
            }

            for (int i = 1; i <= 12; i++)
            {
                MyConfig.Cfg.UpdateUI($"tboxTimeCell{i}", "SetValue", "00:00");
            }

            InitializeDone = true;
            MyConfig.Cfg.ProcessState = "Initialize Done";
        }

        private void RetractAllCells()
        {
            hw.outputs["Y0_00_INFLATE_VALVE_CELL____1"].OFF();
            hw.outputs["Y0_01_INFLATE_VALVE_CELL____2"].OFF();
            hw.outputs["Y0_02_INFLATE_VALVE_CELL____3"].OFF();
            hw.outputs["Y0_03_INFLATE_VALVE_CELL____4"].OFF();
            hw.outputs["Y0_04_INFLATE_VALVE_CELL____5"].OFF();
            hw.outputs["Y0_05_INFLATE_VALVE_CELL____6"].OFF();
            hw.outputs["Y0_06_INFLATE_VALVE_CELL____7"].OFF();
            hw.outputs["Y0_07_INFLATE_VALVE_CELL____8"].OFF();
            hw.outputs["Y0_08_INFLATE_VALVE_CELL____9"].OFF();
            hw.outputs["Y0_09_INFLATE_VALVE_CELL____10"].OFF();
            hw.outputs["Y0_10_INFLATE_VALVE_CELL____11"].OFF();
            hw.outputs["Y0_11_INFLATE_VALVE_CELL____12"].OFF();
        }

        private void StartInitialize()
        {
            taskInitialize = Task.Factory.StartNew(() => Initialize());
        }

        private void InitCell()
        {
            cellModule.InitializeEnabledCells(1);
            cellModule.InitializeEnabledCells(2);
        }

        private void StartRun(int BankNumber)
        {
            //get 
            MyConfig.Cfg.UpdateUI("btnStart", "Disable", "");
            MyConfig.Cfg.UpdateUI("btnRobotActHome", "Disable", "");
            //MyConfig.Cfg.UpdateUI("btnRequestEndLot", "Disable", "");

            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Loading Started: " + MyConfig.Cfg.CurrentSet);
            //disable cell click
            for (int i = 1; i <= 12; i++)
            {
                MyConfig.Cfg.UpdateUI($"cboxEn_{i}", "Disable", "CheckBox");
            }
            //tankModule.MoveIOAB(MyConfig.Var.IOAFront);
            //tankModule.MoveIOAB(MyConfig.Var.IOBFront);
            //ev.Reset();
            MyConfig.Cfg.Model.TaskDip = "Dip Process Running";
            Robot.Run(BankNumber);
            //cellModule.Run(BankNumber);
        }

        private void StartSequenceTask()
        {
            taskChangeState = Task.Factory.StartNew(() =>
            {
                SequenceExit = false;
                while (!SequenceExit)
                {
                    MachineState();
                    Thread.Sleep(100);
                    MyConfig.Cfg.Model.TaskMainFlowState = currentState.ToString();
                }
            });
        }

        public void MachineState()
        {
            switch (currentState)
            {
                case SequenceState.UNINITIALIZE:
                    if (InitializePressed)
                    {
                        InitializePressed = false;
                        MyConfig.Cfg.UpdateUI("btnInitialize", "Disable", "");
                        towerLight.SetIdle();
                        StartInitialize();
                        this.currentState = SequenceState.RUNNING_INITIALIZING;
                        MyConfig.Cfg.Model.StatusBar = "Initializing....";
                        MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Started Initialize");
                    }
                    break;

                case SequenceState.RUNNING_INITIALIZING:
                    if (InitializeDone)
                    {
                        ioListener.StartPressed = false;
                        towerLight.SetRunning();
                        this.currentState = SequenceState.IDLE;
                        MyConfig.Cfg.Model.StatusBar = "Initialized";

                        MyConfig.Cfg.ProcessState = "Initialized";
                        MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Initialized");

                        MyConfig.Cfg.UpdateUI("btnClear", "Enable", "");
                        MyConfig.Cfg.UpdateUI("btnAutoMode", "Enable", "");
                        MyConfig.Cfg.UpdateUI("btnManual", "Enable", "");

                        MyConfig.Cfg.NumberOfUnitInsideBank2 = 0;
                        MyConfig.Cfg.NumberOfUnitInsideBank1 = 0;
                        Robot.FirstTime = true;

                        cellModule.InitializeEnabledCells(1, true);
                        cellModule.InitializeEnabledCells(2, true);
                    }
                    break;

                case SequenceState.IDLE:
                    if (ioListener.StartPressed)
                    {
                        int stat = CheckIOStat("Lid");
                        ioListener.StartPressed = false;
                        if (stat == 1)
                        {
                            MyConfig.Cfg.Model.StatusBar = "Error -Lid Not Closed-Please Press Reset and Start";
                            PreviousState = SequenceState.IDLE;
                            MyConfig.Cfg.UpdateUI("btnManual", "Enable", "");
                            //this.currentState = SequenceState.ALARM;
                            return;
                        }

                        InitCell(); //init cells

                        ///move tank
                        //tankModule.MoveIOAB(MyConfig.Var.IOAFront); //ck 20Mar18                        //tankModule.MoveIOAB(MyConfig.Var.IOBFront); //ck 20Mar18
                        tankModule.IOQBMove(MyConfig.Var.IOAFront);
                        tankModule.IOQBMove(MyConfig.Var.IOBFront);

                        //check if unit match with enabled
                        Thread.Sleep(700);
                        stat = CheckIOStat("IOASensor", GetBankNo().ToString());
                        if (stat == 1)
                        {
                            //tankModule.MoveIOAB("IOABack"); //ck 20Mar18
                            tankModule.IOQBMove("IOABack");
                            MyConfig.Cfg.Model.StatusBar = "Error -Enabled Cell Does Not Match";
                            MyConfig.Cfg.UpdateUI("btnManual", "Enable", "");
                            MyConfig.Cfg.Model.ErrorCode = "Alert,Enabled Cell Does Not Match";
                            PreviousState = SequenceState.IDLE;
                            // this.currentState = SequenceState.ALARM;
                            return;
                        }

                        Thread.Sleep(500);
                        stat = CheckIOStat("IOBSensor");
                        if (stat == 1)
                        {
                            //tankModule.MoveIOAB("IOBBack"); //ck 20Mar18
                            tankModule.IOQBMove("IOBBack");
                            MyConfig.Cfg.Model.StatusBar = "Error-Please Remove Item in Unloading - Press Reset And Start";
                            PreviousState = SequenceState.IDLE;
                            MyConfig.Cfg.Model.ErrorCode = "Alert,Please Remove Item in Unloading";
                            MyConfig.Cfg.UpdateUI("btnManual", "Enable", "");
                            // this.currentState = SequenceState.ALARM;
                            ioListener.StartPressed = false;
                            return;
                        }

                        //fill
                        tankModule.IOQA("Fill");
                        tankModule.IOQB("Fill");

                        MyConfig.Cfg.Model.StatusBar = "PROCESS STARTED";
                        MyConfig.Cfg.Model.ErrorCode = "";
                        MyConfig.Cfg.RequestEndLot = false;
                        ioListener.StartPressed = false;

                        if (MyConfig.Cfg.CurrentSet == "A" && MyConfig.Cfg.Model.NextSet == "A")
                        {
                            MyConfig.Cfg.Model.StatusBar = $"SET: {MyConfig.Cfg.CurrentSet} Started";
                            timecycleStart = DateTime.Now;
                            if (MyConfig.Cfg.EnabledCellinBank1 != 0)
                                StartRun(1);
                            else
                                StartRun(2);
                            MyConfig.Cfg.Model.NextSet = "B";
                        }
                        else
                        {
                            int BankNumber = 0;
                            int cellnum = cellModule.CheckEnabledCells(MyConfig.Cfg.CurrentSet);
                            switch (MyConfig.Cfg.CurrentSet)
                            {
                                case "A":
                                    BankNumber = 2;
                                    MyConfig.Cfg.CurrentSet = "B";
                                    MyConfig.Cfg.Model.NextSet = "C";
                                    MyConfig.Cfg.Model.CurrentSetBank2 = "B";
                                    break;
                                case "B":
                                    BankNumber = 1;
                                    MyConfig.Cfg.CurrentSet = "C";
                                    MyConfig.Cfg.Model.NextSet = "D";
                                    MyConfig.Cfg.Model.CurrentSetBank1 = "C";
                                    break;
                                case "C":
                                    BankNumber = 2;
                                    MyConfig.Cfg.CurrentSet = "D";
                                    MyConfig.Cfg.Model.NextSet = "A";
                                    MyConfig.Cfg.Model.CurrentSetBank2 = "D";
                                    break;
                                case "D":
                                    BankNumber = 1;
                                    MyConfig.Cfg.CurrentSet = "A";
                                    MyConfig.Cfg.Model.NextSet = "B";
                                    MyConfig.Cfg.Model.CurrentSetBank1 = "A";
                                    break;
                                default:
                                    break;
                            }
                            cellModule.InitializeEnabledCells(BankNumber);
                            MyConfig.Cfg.Model.StatusBar = $"SET: {MyConfig.Cfg.CurrentSet} Started";
                            timecycleStart = DateTime.Now;
                            StartRun(BankNumber);
                        }

                        this.currentState = SequenceState.RUNNING;
                        MyConfig.Cfg.Model.StatusBar = $"SET: {MyConfig.Cfg.CurrentSet} STARTED LOADING";
                        MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "StartPressed");
                    }
                    break;
                case SequenceState.LOT_START:
                    break;
                case SequenceState.RUNNING:

                    if (MyConfig.Cfg.RequestNextSet)
                    {
                        MyConfig.Cfg.RequestNextSet = false;
                        MyConfig.Cfg.Model.StatusBar = $"Set Loading Complete";


                        if (!MyConfig.Cfg.RequestEndLot)
                        {
                            MyConfig.Cfg.Model.ErrorCode = "Info, Load Next Set";
                            if (MyConfig.Cfg.BankUsed > 1)
                            {

                            }
                            else
                            {
                                //MyConfig.Cfg.StartEndlotTimer = true;
                                //MyConfig.Cfg.Model.ErrorCode = "Info, CK Test";
                                // EndLotTimerStart(); //CK_19Mar18
                            }

                        }

                        //MyConfig.Cfg.UpdateUI("btnRequestEndLot", "Enable", "");
                        MyConfig.Cfg.Model.TaskDip = "IDLE";
                        this.currentState = SequenceState.WAITING_NEXT_SET;
                    }

                    if (MyConfig.Cfg.Model.ErrorCode != "" && !MyConfig.Cfg.Model.ErrorCode.Contains("ROBOT") && !MyConfig.Cfg.Model.ErrorCode.Contains("Robot"))
                    {
                        PreviosStatus = MyConfig.Cfg.Model.StatusBar;
                        MyConfig.Cfg.Model.StatusBar = "ALARM -- WAITING USER TO CLEAR";
                        MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, MyConfig.Cfg.Model.ErrorCode);
                        this.currentState = SequenceState.STOP_REQUEST;
                    }

                    break;


                case SequenceState.WAITING_NEXT_SET:

                    if (MyConfig.Cfg.EndLotTimer || MyConfig.Cfg.RequestEndLot)
                    {
                        //ev.RobotIdle.WaitOne();
                        while (Robot.count > 0)
                        {
                            Thread.Sleep(100);
                        }
                        DoIfEndLot();
                        MyConfig.Cfg.EndLotTimer = false;
                        MyConfig.Cfg.RequestEndLot = false;
                        MyConfig.Cfg.StartEndlotTimer = false;
                    }

                    if (MyConfig.Cfg.StartEndlotTimer == true)
                        StartEndLotCount(false);
                    else
                        StartEndLotCount(true);



                    if (((MyConfig.Cfg.CurrentSet == "A" || MyConfig.Cfg.CurrentSet == "C") && MyConfig.Cfg.NumberOfUnitInsideBank2 == 0) ||
                            ((MyConfig.Cfg.CurrentSet == "B" || MyConfig.Cfg.CurrentSet == "D") && MyConfig.Cfg.NumberOfUnitInsideBank1 == 0))
                    {
                        MyConfig.Cfg.UpdateUI("btnStart", "Enable", "");
                        MyConfig.Cfg.UpdateUI("btnRobotActHome", "Enable", "");
                        MyConfig.Cfg.UpdateUI("btnRequestEndLot", "Enable", "");
                        //S----- When waiting for next lot, timer start -----19Mar18_1000
                        if (!EndLotTimerStarted)
                            EndLotTimerStart();
                        //E----- When waiting for next lot, timer start -----

                        if (MyConfig.Cfg.CurrentSet == "A" || MyConfig.Cfg.CurrentSet == "C")
                        {
                            for (int i = 7; i <= 12; i++)
                            {
                                MyConfig.Cfg.UpdateUI($"cboxEn_{i}", "Enable", "CheckBox");
                            }
                        }
                        else if (MyConfig.Cfg.CurrentSet == "B" || MyConfig.Cfg.CurrentSet == "D")
                        {
                            for (int i = 1; i <= 6; i++)
                            {
                                MyConfig.Cfg.UpdateUI($"cboxEn_{i}", "Enable", "CheckBox");
                            }
                        }
                    }

                    if (ioListener.StartPressed)
                    {
                        MyConfig.Cfg.StartEndlotTimer = false;

                        int stat = CheckIOStat("Lid", "IOA");

                        if (stat == 1)
                        {
                            MyConfig.Cfg.Model.StatusBar = "Error-Lid Not Closed- Press Reset And Start";
                            PreviousState = SequenceState.WAITING_NEXT_SET;
                            MyConfig.Cfg.UpdateUI("btnManual", "Enable", "");
                            // this.currentState = SequenceState.ALARM;
                            ioListener.StartPressed = false;
                            return;
                        }
                        MyConfig.Cfg.CheckUndip = false;
                        ioListener.StartPressed = false;

                        //tankModule.MoveIOAB(MyConfig.Var.IOAFront); //ck 20Mar18
                        tankModule.IOQBMove(MyConfig.Var.IOAFront);

                        Thread.Sleep(500);
                        stat = CheckIOStat("IOASensor", GetBankNo().ToString());
                        if (stat == 1)
                        {
                            //tankModule.MoveIOAB("IOABack"); //ck 20Mar18
                            tankModule.IOQBMove("IOABack");
                            MyConfig.Cfg.Model.StatusBar = "Error-Enabled Cell Does Not Match - Press Reset And Start";
                            MyConfig.Cfg.Model.ErrorCode = "Alert,Enabled Cell Does Not Match";
                            PreviousState = SequenceState.WAITING_NEXT_SET;
                            MyConfig.Cfg.UpdateUI("btnManual", "Enable", "");
                            // this.currentState = SequenceState.ALARM;
                            ioListener.StartPressed = false;
                            return;
                        }

                        MyConfig.Cfg.UpdateUI("btnManual", "Disable", "");
                        int BankNumber = 0;

                        int cellnum = cellModule.CheckEnabledCells(MyConfig.Cfg.CurrentSet);

                        switch (MyConfig.Cfg.CurrentSet)
                        {

                            case "A":
                                BankNumber = 2;
                                MyConfig.Cfg.CurrentSet = "B";
                                MyConfig.Cfg.Model.NextSet = "C";
                                MyConfig.Cfg.Model.CurrentSetBank2 = "B";
                                break;
                            case "B":
                                BankNumber = 1;
                                MyConfig.Cfg.CurrentSet = "C";
                                MyConfig.Cfg.Model.NextSet = "D";
                                MyConfig.Cfg.Model.CurrentSetBank1 = "C";
                                break;
                            case "C":
                                BankNumber = 2;
                                MyConfig.Cfg.CurrentSet = "D";
                                MyConfig.Cfg.Model.NextSet = "A";
                                MyConfig.Cfg.Model.CurrentSetBank2 = "D";
                                break;
                            case "D":
                                BankNumber = 1;
                                MyConfig.Cfg.CurrentSet = "A";
                                MyConfig.Cfg.Model.NextSet = "B";
                                MyConfig.Cfg.Model.CurrentSetBank1 = "A";
                                break;
                            default:
                                break;
                        }

                        MyConfig.Cfg.BankUsed++;

                        cellModule.InitializeEnabledCells(BankNumber);

                        //save batchnumber
                        MyConfig.Cfg.SaveINI("SavedValue", "CurrentSet", MyConfig.Cfg.CurrentSet);
                        MyConfig.Cfg.SaveINI("SavedValue", "NextSet", MyConfig.Cfg.Model.NextSet);
                        MyConfig.Cfg.SaveINI("SavedValue", "Bank1Set", MyConfig.Cfg.Model.CurrentSetBank1);
                        MyConfig.Cfg.SaveINI("SavedValue", "Bank2Set", MyConfig.Cfg.Model.CurrentSetBank2);

                        MyConfig.Cfg.Model.StatusBar = $"SET: {MyConfig.Cfg.CurrentSet} Started";
                        MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, $"SET: {MyConfig.Cfg.CurrentSet} Started");

                        StartRun(BankNumber);
                        this.currentState = SequenceState.RUNNING;

                    }

                  

                    break;

                case SequenceState.STOP_REQUEST:

                    if (MyConfig.Cfg.Model.ErrorCode != "")
                    {

                    }

                    if (ioListener.ResetPressed)
                    {
                        MyConfig.Cfg.Model.ErrorCode = "";
                        // MyConfig.Cfg.Model.RobotStat = "Free";
                        MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "ALARM CLEARED");
                        this.currentState = SequenceState.RUNNING;
                        ioListener.ResetPressed = false;
                        hw.outputs["Y5_03_ROBOT_PAUSE"].OFF();
                        hw.motors["Motor1X"].Start();

                        MyConfig.Cfg.Model.StatusBar = PreviosStatus;
                    }



                    break;
                case SequenceState.ALARM_CLEARED:
                    break;
                case SequenceState.ALARM:

                    if (AlarmStat == false)
                    {
                        AlarmStat = true;
                        towerLight.SetAlarm();
                    }

                    if (AlarmStat == false)
                    {
                        towerLight.SetRunning();
                    }

                    if (ioListener.ResetPressed)
                    {
                        this.currentState = PreviousState;
                        ioListener.ResetPressed = false;
                        towerLight.SetRunning();
                    }

                    break;

                case SequenceState.REQUEST_END_LOT:
                    break;

                case SequenceState.EMERGENCY:
                    MyConfig.Cfg.EMOStat = true;
                    MyConfig.Cfg.RequestEndLot = true;
                    MyConfig.Cfg.UpdateUI("btnManual", "Enable", "");
                    MyConfig.Cfg.UpdateUI("btnInitialize", "Enable", "");
                    MyConfig.Cfg.UpdateUI("btnStop", "Disable", "");
                    Robot.ResetRobotIO(true);
                    hw.outputs["Y5_14_ROBOT_RESET"].ON();

                    if (MyConfig.Cfg.SoftEmo)
                    {
                        MyConfig.Cfg.Model.StatusBar = "MACHINE STOPPED-PLEASE RESET ROBOT PROGRAM";

                        MyConfig.Cfg.UpdateUI("btnStop", "Enable", "");

                        while (MyConfig.Cfg.SoftEmo != false)
                        {
                            if (MyConfig.Cfg.ProcessState == "Idle")
                                break;

                            Thread.Sleep(260);
                        }

                        while (hw.inputs["X6_14_ROBOT_POSITION_RECEIVED"].Read() != 1)
                        {
                            if (MyConfig.Cfg.ProcessState == "Idle")
                                break;

                            if (MyConfig.Cfg.SimulationMode)
                                break;

                            Thread.Sleep(260);
                        }

                        hw.outputs["Y5_14_ROBOT_RESET"].OFF();
                        MyConfig.Cfg.SoftEmo = false;
                        MyConfig.Cfg.EMOStat = false;
                        MyConfig.Cfg.Model.StatusBar = "MACHINE RESET- PLEASE RE-INITIALIZE";
                        MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "MACHINE RESET-Waiting  RE-INITIALIZE");
                    }

                    else
                    {
                        while (hw.inputs["X5_03_E_STOP_LOOP_OK"].Read() != 1)
                        {
                            Thread.Sleep(200);
                        }
                        MyConfig.Cfg.Model.StatusBar = "EMO RELEASED-PLEASE RESET ROBOT PROGRAM";
                        MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "EMO RELEASED--Waiting Other Process To Stop");
                        //while (MyConfig.Cfg.ProcessState != "EMO" || 
                        while (hw.inputs["X6_14_ROBOT_POSITION_RECEIVED"].Read() != 1)
                        {
                            if (MyConfig.Cfg.ProcessState == "Idle")
                                break;

                            if (MyConfig.Cfg.ProcessState == "Wait Emo Robot Release")
                                break;

                            Thread.Sleep(200);

                        }
                    }
                    MyConfig.Cfg.Model.StatusBar = "ROBOT PROGRAM RESET - PLEASE SYSTEM INITIALIZE";
                    hw.outputs["Y4_06_ROBOT_EMO_ON"].OFF();
                    hw.outputs["Y5_14_ROBOT_RESET"].OFF();
                    MyConfig.Cfg.EMOStat = false;
                    MyConfig.Cfg.UpdateUI("btnStop", "Enable", "");
                    MyConfig.Cfg.UpdateUI("btnStopRobot", "Enable", "");
                    currentState = SequenceState.UNINITIALIZE;

                    break;

                default:
                    break;

            }
        }

        public int GetBankNo()
        {
            if (MyConfig.Cfg.CurrentSet == "A" && MyConfig.Cfg.Model.NextSet == "A")
            {
                return 1;
            }
            else
            {
                int BankNumber = 0;

                switch (MyConfig.Cfg.CurrentSet)
                {
                    case "A":
                        BankNumber = 2;
                        break;
                    case "B":
                        BankNumber = 1;
                        break;
                    case "C":
                        BankNumber = 2;
                        break;
                    case "D":
                        BankNumber = 1;
                        break;
                    default:
                        break;
                }

                return BankNumber;
            }


        }

        public void DoIfEndLot()
        {
            if (MyConfig.Cfg.RequestEndLot)
            {
                MyConfig.Cfg.StartEndlotTimer = false;
                //MyConfig.Cfg.RequestEndLotToIDLE = true;
                int BankNumber = 0;
                MyConfig.Cfg.Model.StatusBar = $"Ending Lot";
                MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, $"Ending Lot");

                MyConfig.Cfg.UpdateUI("btnStart", "Disable", "");
                MyConfig.Cfg.UpdateUI("btnRobotActHome", "Disable", "");
                MyConfig.Cfg.UpdateUI("btnRequestEndLot", "Disable", "");

                //wait lot end
                switch (MyConfig.Cfg.CurrentSet)
                {
                    case "A": BankNumber = 1; break;
                    case "B": BankNumber = 2; break;
                    case "C": BankNumber = 1; break;
                    case "D": BankNumber = 2; break;
                    default:
                        break;
                }

                if (BankNumber == 1)
                {
                    var bank2stat = MyConfig.Cfg.EnabledCellinBank2;
                    if (bank2stat == 0)
                        MyConfig.Cfg.JustOneCell = true;
                }
                else
                {
                    var bank1stat = MyConfig.Cfg.EnabledCellinBank1;
                    if (bank1stat == 0)
                        MyConfig.Cfg.JustOneCell = true;
                }

                Robot.StartUndipProcess(BankNumber);
                Robot.waitUndip = false;
                ev.UndipDone.Set();
                //ev.UndipDone.Reset();
                while ((MyConfig.Cfg.ProcessState != MyConfig.Var.WaitingSet))
                {
                    if (MyConfig.Cfg.ProcessState == MyConfig.Cfg.ProcessState)
                        break;
                }

                Robot.FirstTime = true;
                MyConfig.Cfg.RequestEndLot = false;
                this.currentState = SequenceState.IDLE;
                //MyConfig.Cfg.CurrentSet = "A";
                //MyConfig.Cfg.Model.NextSet = "A";
                MyConfig.Cfg.ProcessState = MyConfig.Cfg.ProcessState;
                MyConfig.Cfg.UpdateUI("btnManual", "Enable", "");
                MyConfig.Cfg.UpdateUI("btnStart", "Disable", "");
                MyConfig.Cfg.UpdateUI("btnRobotActHome", "Disable", "");
                MyConfig.Cfg.UpdateUI("btnRequestEndLot", "Disable", "");


                MyConfig.Cfg.Model.StatusBar = $"Lot Ended";
                MyConfig.Cfg.StartEndlotTimer = false;
                MyConfig.Cfg.Model.ErrorCode = "Info,Lot Ended";

                if (MyConfig.Cfg.EndLotButtonPressed == true)
                    MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Lot Ended (Button Pressed)");
                else
                    MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Lot Ended (Timer)");

                //enable cells
                for (int i = 1; i <= 12; i++)
                {
                    MyConfig.Cfg.UpdateUI($"cboxEn_{i}", "Enable", "CheckBox");
                }
                MyConfig.Cfg.EndProcess = false;
                CellInitialized = false;
                //tankModule.MoveIOAB("IOABack"); //ck 20Mar18
                //tankModule.MoveIOAB("IOBBack"); //ck 20Mar18
                tankModule.IOQBMove("IOABack");
                tankModule.IOQBMove("IOBBack");

                var timecycleEnd = DateTime.Now - timecycleStart;
                MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Total Process Time(m): " + timecycleEnd.TotalMinutes.ToString("N2"));

                TimeSpan span = TimeSpan.FromMinutes(timecycleEnd.TotalMinutes);
                TimeSpan UpTimeNow = TimeSpan.FromMinutes(MyConfig.Cfg.UpTimeCount) + span;
                MyConfig.Cfg.UpTimeCount = UpTimeNow.TotalMinutes;
                String Uptime = UpTimeNow.Hours + "H " + UpTimeNow.Minutes + "m ";
                MyConfig.Cfg.Model.UPTime = Uptime;
                ioListener.StartPressed = false;

                //reset task stat
                MyConfig.Cfg.Model.TaskDip = "IDLE";
            }
        }

        public SequenceState currentState
        {
            get
            {
                return MyConfig.Cfg.currentState;
            }
            set
            {
                MyConfig.Cfg.currentState = value;

            }
        }

        public enum SequenceState
        {
            UNINITIALIZE,
            RUNNING_INITIALIZING,
            SCRIPT_ALARM_INITIALIZING,
            ALARM_INITIALIZING_CLEARED,
            IDLE,
            LOT_START,
            RUNNING,
            STOP_REQUEST,
            WAITING_NEXT_BATCH,
            WAITING_NEXT_SET,
            ALARM,
            ALARM_CLEARED,
            SCRIPT_WARNING,
            REQUEST_END_LOT,
            EMERGENCY
        }

        private void StartEndLotCount(bool Reset)
        {
            if (Reset)
            {
                MyConfig.Cfg.Model.EndLotCountDown = MyConfig.Cfg.Model.TimerExpireEndLot;
                isEndLotTimerStarted = false;
                return;
            }

            if (isEndLotTimerStarted == false)
            {
                isEndLotTimerStarted = true;
                MyConfig.Cfg.StartEndlotTimer = true;
                EndLotEndTime = DateTime.Now.AddSeconds(MyConfig.Cfg.Model.TimerExpireEndLot);
                MyConfig.Cfg.Model.EndLotCountDown = MyConfig.Cfg.Model.TimerExpireEndLot;
                EndLotCountTime = DateTime.Now.AddSeconds(1);

                Task.Run(() =>
                {
                    while (isEndLotTimerStarted)
                    {
                        Thread.Sleep(1000);
                        MyConfig.Cfg.Model.EndLotCountDown--;
                        if (DateTime.Now > EndLotEndTime)
                        {
                            isEndLotTimerStarted = false;
                            MyConfig.Cfg.RequestEndLot = true;
                            MyConfig.Cfg.EndLotTimer = true;
                            MyConfig.Cfg.StartEndlotTimer = false;
                        }

                        if(MyConfig.Cfg.StartEndlotTimer == false)
                        {
                            isEndLotTimerStarted = false;
                            break;
                        }
                    }

                });
            }

            
            //if (DateTime.Now > EndLotCountTime)
            //{
            //    EndLotCountTime = DateTime.Now.AddSeconds(1);
            //    MyConfig.Cfg.Model.EndLotCountDown--;
            //}

            //if (DateTime.Now > EndLotEndTime)
            //{
            //    MyConfig.Cfg.Model.EndLotCountDown--;
            //    isEndLotTimerStarted = false;
            //    MyConfig.Cfg.RequestEndLot = true;
            //    MyConfig.Cfg.EndLotTimer = true;
            //    MyConfig.Cfg.StartEndlotTimer = false;
            //}
        }

        public void EndLotTimerStart()
        {
            //var time = DateTime.Now.AddSeconds(MyConfig.Cfg.Model.TimerExpireEndLot);
            //MyConfig.Cfg.Model.EndLotCountDown = MyConfig.Cfg.Model.TimerExpireEndLot;

            //if (EndLotTimerStarted)
            //    return;

            //MyConfig.Cfg.EndLotTask = null;
            //MyConfig.Cfg.StartEndlotTimer = true; //CK_19Mar18
            //MyConfig.Cfg.EndLotTask = Task.Run(() =>
            // {
            //     EndLotTimerStarted = true;
            //     while (MyConfig.Cfg.StartEndlotTimer)
            //     {
            //         Thread.Sleep(1000);
            //        //if (!MyConfig.Cfg.StartEndlotTimer)
            //        //{
            //        //    EndLotTimerStarted =false;
            //        //    break;
            //        //}


            //        MyConfig.Cfg.Model.EndLotCountDown--;
            //         if (DateTime.Now > time)
            //         {
            //             MyConfig.Cfg.RequestEndLot = true;
            //             MyConfig.Cfg.EndLotTimer = true;
            //             MyConfig.Cfg.StartEndlotTimer = false;
            //             break;
            //         }

            //     }
            //     EndLotTimerStarted = false;

            // });
        }

        public bool ClearUnit(int banknumber)
        {
            int count = 0;
            int UnitNumber = 0;
            List<int> bank1 = new List<int>();
            List<int> bank2 = new List<int>();

            var ss = MyConfig.Cfg.RobotRequestStop;

            if (banknumber == 1)
            {
                for (int i = 1; i <= 6; i++)
                {
                    switch (i)
                    {
                        case 1:
                            if (hw.inputs["X2_00_CLAMSHELL_PRESENT_CELL___1"].Read() == 0)
                            {
                                bank1.Add(i);
                            }
                            break;
                        case 2:
                            if (hw.inputs["X2_01_CLAMSHELL_PRESENT_CELL___2"].Read() == 0)
                            {
                                bank1.Add(i);
                            }
                            break;
                        case 3:
                            if (hw.inputs["X2_02_CLAMSHELL_PRESENT_CELL___3"].Read() == 0)
                            {
                                bank1.Add(i);
                            }
                            break;
                        case 4:
                            if (hw.inputs["X2_03_CLAMSHELL_PRESENT_CELL___4"].Read() == 0)
                            {
                                bank1.Add(i);
                            }
                            break;
                        case 5:
                            if (hw.inputs["X2_04_CLAMSHELL_PRESENT_CELL___5"].Read() == 0)
                            {
                                bank1.Add(i);
                            }
                            break;
                        case 6:
                            if (hw.inputs["X2_05_CLAMSHELL_PRESENT_CELL___6"].Read() == 0)
                            {
                                bank1.Add(i);
                            }
                            break;
                        default:
                            break;
                    }

                }

                //clear bank1
                if (bank1.Count() > 0)
                {
                    //tankModule.MoveIOAB("IOBFront");//ck 20MAr18
                    tankModule.IOQBMove("IOBFront");
                    var stat = MyConfig.Cfg.Model.tankModule.IOQBMove("IOBFront");
                    if (stat == 0)
                    {
                        Thread.Sleep(500);
                        stat = MyConfig.Cfg.Model.tankModule.IOQBMove("IOBSensor");
                        if (stat == 1)
                        {
                            if (CheckIOStat("IOBSensor") == 1)
                            {
                                //MyConfig.Cfg.Model.tankModule.MoveIOAB("IOBBack"); //ck 20Mar18
                                MyConfig.Cfg.Model.tankModule.IOQBMove("IOBBack");
                                MyConfig.Cfg.Model.ErrorCode = "Alert,Please remove all fixture from IOQB tank And Recover Again";
                                return false;
                            }

                            MyConfig.Cfg.IOQBStatus = true;
                            //btnMoveIOQB.Visibility = Visibility.Hidden;
                        }
                        MyConfig.Cfg.IOQBStatus = true;
                    }


                    MyConfig.Cfg.Model.StatusBar = $"Start Unit Clear";
                    foreach (var item in bank1)
                    {
                        Robot.Cmd("MoveMotor", "PosCell", item.ToString());
                        Robot.Cmd("UnDip", item.ToString());
                        Robot.Cmd("MoveMotor", "PosTank");
                        Robot.Cmd("UnLoad", item.ToString());
                        Thread.Sleep(200);
                        UnitNumber++;

                        if (UnitNumber == 2)
                        {
                            tankModule.IOQB("ReFill");
                            UnitNumber = 0;
                        }

                    }
                    Thread.Sleep(100);
                    //tankModule.MoveIOAB("IOBBack"); //ck 20Mar18
                    tankModule.IOQBMove("IOBBack");
                    MyConfig.Cfg.Model.StatusBar = $"Unit Cleared";
                    MyConfig.Cfg.UpdateUI("btnClear", "Enable", "");
                }
            }
            else
            {
                for (int i = 7; i <= 12; i++)
                {
                    switch (i)
                    {
                        case 7:
                            if (hw.inputs["X2_06_CLAMSHELL_PRESENT_CELL___7"].Read() == 0)
                            {
                                bank2.Add(i);
                            }
                            break;
                        case 8:
                            if (hw.inputs["X2_07_CLAMSHELL_PRESENT_CELL___8"].Read() == 0)
                            {
                                bank2.Add(i);
                            }
                            break;
                        case 9:
                            if (hw.inputs["X2_08_CLAMSHELL_PRESENT_CELL___9"].Read() == 0)
                            {
                                bank2.Add(i);
                            }
                            break;
                        case 10:
                            if (hw.inputs["X2_09_CLAMSHELL_PRESENT_CELL___10"].Read() == 0)
                            {
                                bank2.Add(i);
                            }
                            break;
                        case 11:
                            if (hw.inputs["X2_10_CLAMSHELL_PRESENT_CELL___11"].Read() == 0)
                            {
                                bank2.Add(i);
                            }
                            break;
                        case 12:
                            if (hw.inputs["X2_11_CLAMSHELL_PRESENT_CELL___12"].Read() == 0)
                            {
                                bank2.Add(i);
                            }
                            break;
                        default:
                            break;
                    }
                }

                if (bank2.Count() > 0)
                {
                    //tankModule.MoveIOAB("IOBFront"); //ck 20Mar18
                    tankModule.IOQBMove("IOBFront");
                    var stat = MyConfig.Cfg.Model.tankModule.IOQBMove("IOBFront");
                    if (stat == 0)
                    {
                        Thread.Sleep(500);
                        stat = MyConfig.Cfg.Model.tankModule.IOQBMove("IOBSensor");
                        if (stat == 1)
                        {
                            if (CheckIOStat("IOBSensor") == 1)
                            {
                                //MyConfig.Cfg.Model.tankModule.MoveIOAB("IOBBack"); //ck 20Mar18
                                MyConfig.Cfg.Model.tankModule.IOQBMove("IOBBack");
                                MyConfig.Cfg.Model.ErrorCode = "Alert,Please remove all fixture from IOQB tank And Recover Again";
                                return false;
                            }

                            MyConfig.Cfg.IOQBStatus = true;
                            //btnMoveIOQB.Visibility = Visibility.Hidden;
                        }
                        MyConfig.Cfg.IOQBStatus = true;
                    }

                    foreach (var item in bank2)
                    {

                        Robot.Cmd("MoveMotor", "PosCell", item.ToString());
                        Robot.Cmd("UnDip", item.ToString());
                        Robot.Cmd("MoveMotor", "PosTank");
                        Robot.Cmd("UnLoad", (item - 6).ToString());
                        UnitNumber++;
                        if (UnitNumber == 2)
                        {
                            tankModule.IOQB("ReFill");
                            UnitNumber = 0;
                        }

                        Thread.Sleep(200);
                    }
                }
                Thread.Sleep(100);
                //tankModule.MoveIOAB("IOBBack"); //ck 20Mar18
                tankModule.IOQBMove("IOBBack");
                MyConfig.Cfg.Model.StatusBar = $"Unit Cleared";
                MyConfig.Cfg.UpdateUI("btnClear", "Enable", "");
            }



            return true;




        }

        private string GetModelValue(string UiName)
        {
            var propInfog = MyConfig.Cfg.Model.GetType().GetProperty(UiName);
            if (propInfog != null)
            {

                var val = propInfog.GetValue(MyConfig.Cfg.Model);
                return val.ToString();
            }

            return "False";
        }
    }
}

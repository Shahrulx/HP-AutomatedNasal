﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AutomationLib
{
    public class Events
    {
        //benda2 penting 

        public ManualResetEvent DippingProcess = new ManualResetEvent(false);
        public ManualResetEvent DippingDone = new ManualResetEvent(false);
        public AutoResetEvent DippingStart = new AutoResetEvent(true);
        public AutoResetEvent RobotPick = new AutoResetEvent(true);
        public AutoResetEvent RobotPlace = new AutoResetEvent(true);
        public AutoResetEvent RobotModule = new AutoResetEvent(true);
        public AutoResetEvent RobotMove = new AutoResetEvent(true);
        public ManualResetEvent EndLotTimerStart = new ManualResetEvent(false);
        public AutoResetEvent UndipDone = new AutoResetEvent(false);
        
        public AutoResetEvent RobotIdle = new AutoResetEvent(false);
        public void Reset()
        {
            DippingStart.Reset();
            RobotPick.Reset();
            RobotPlace.Reset();
            RobotModule.Reset();
        }

        public void WaitEvent(EventWaitHandle handler, int timeOutSeconds, bool quitOnRequestEndLot, string message, int errorCode = -1)
        {
            int counter = 0;
            while (true)
            {
                if (MyConfig.Cfg.RequestPause) WaitForResume();

                if (counter > timeOutSeconds * 10 && timeOutSeconds > 0)
                {
                    MyConfig.Cfg.Model.ErrorCode = $"Alert,Timeout on waiting for event: {message}";
                    MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, $"Timeout on waiting for event: {message}");
                    //SetError("Timeout on waiting for event in " + moduleName + " : " + message, errorCode);
                }
                if (handler.WaitOne(100))
                {
                    break;
                }
                
                counter++;
            }
        }

        protected void WaitForResume()
        {
            while (MyConfig.Cfg.RequestPause)
            {
                Thread.Sleep(300);
            }
        }
    }
}

﻿using HardwareLib;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace AutomationLib
{
    public class CellModule
    {
        private DispatcherTimer[] dtClockTime;
        private Dictionary<string, CustomTimer> CustomTimerList = new Dictionary<string, CustomTimer>();
        //List<Tuple<string, DispatcherTimer>> TimerList = new List<Tuple<string, DispatcherTimer>>();
        Hardware hw;
        Events ev;
        public static string CurrentCell = "";
        static readonly object _object = new object();
        bool Initialized = false;
        bool isManualCellInitialized = false;

        public CellModule(Hardware hw, Events ev)
        {
            this.hw = hw;
            this.ev = ev;
        }

        //create timer for each cell
        public void Initialize()
        {
            MyConfig.Cfg.TimerList.Clear();
            CustomTimerList.Clear();
            foreach (var item in MyConfig.Cfg.RecepiList.Cells)
            {
                DispatcherTimer dtimer = new DispatcherTimer();
                var cval = CalculateAmpMin(item.AmpMin);
                var tt = new CustomTimer(item.Cell, ev, (int)cval.TotalSeconds);
                //var tt = new CustomTimer(item.Cell,ev,item.Timer);
                CustomTimerList.Add(item.Cell, tt);
                dtimer.Interval = new TimeSpan(0, 0, 1);
                EventHandler timerTick = tt.PollUpdates;
                dtimer.Tick += timerTick;

                MyConfig.Cfg.TimerList.Add(Tuple.Create(item.Cell, dtimer, (int)cval.TotalSeconds));

                //TimeSpan time = TimeSpan.FromSeconds(item.Timer);
                // UpdateTimerUI("TimeCell"+item.Cell, time.ToString(@"mm\:ss"));
            }
        }

        //public void InitializeEnabledCells(int BankNumber)
        //{
        //    _InitializeEnabledCells(1);
        //    _InitializeEnabledCells(2);
        //}

        public void InitializeEnabledCells(int BankNumber, bool InitializeAllCells = false)
        {
            if (BankNumber == 1)
            {
                int count = 0;
                List<Cells> EnabledCells;

                MyConfig.Cfg.EnabledCellinBank1 = count;
                if(InitializeAllCells)
                    EnabledCells = MyConfig.Cfg.RecepiList.Cells.FindAll(x => int.Parse(x.Cell) < 7);
                else
                    EnabledCells = MyConfig.Cfg.RecepiList.Cells.FindAll(x => x.Enabled == true && int.Parse(x.Cell) < 7);

                int PickUnitnum = 0;
                MyConfig.Cfg.Bank1 = new Dictionary<string, Tuple<string, int>>();

                foreach (var item in EnabledCells)
                {
                    if (item.Cell == "1")
                        PickUnitnum = 2;
                    else if (item.Cell == "2")
                        PickUnitnum = 3;
                    else if (item.Cell == "3")
                        PickUnitnum = 4;
                    else if (item.Cell == "4")
                        PickUnitnum = 5;
                    else if (item.Cell == "5")
                        PickUnitnum = 6;
                    else if (item.Cell == "6")
                        PickUnitnum = 7;

                    MyConfig.Cfg.Bank1.Add(item.Cell, Tuple.Create("Idle", PickUnitnum));
                    var idx = MyConfig.Cfg.TimerList.FindIndex(x => x.Item1 == item.Cell);

                    if (idx != -1)
                    {
                        TimeSpan time = TimeSpan.FromSeconds(item.Timer);
                        UpdateTimerUI("TimeCell" + item.Cell, time.ToString(@"mm\:ss"));
                    }
                    count++;
                }
                MyConfig.Cfg.EnabledCellinBank1 = count;

            }

            else if (BankNumber == 2)
            {
                int count = 0;
                int PickUnitnum = 0;
                MyConfig.Cfg.EnabledCellinBank2 = count;
                List<Cells> EnabledCells;
                if(InitializeAllCells)
                    EnabledCells = MyConfig.Cfg.RecepiList.Cells.FindAll(x => int.Parse(x.Cell) > 6);
                else
                    EnabledCells = MyConfig.Cfg.RecepiList.Cells.FindAll(x => x.Enabled == true && int.Parse(x.Cell) > 6);

                MyConfig.Cfg.Bank2 = new Dictionary<string, Tuple<string, int>>();
                foreach (var item in EnabledCells)
                {
                    if (item.Cell == "7")
                        PickUnitnum = 2;
                    else if (item.Cell == "8")
                        PickUnitnum = 3;
                    else if (item.Cell == "9")
                        PickUnitnum = 4;
                    else if (item.Cell == "10")
                        PickUnitnum = 5;
                    else if (item.Cell == "11")
                        PickUnitnum = 6;
                    else if (item.Cell == "12")
                        PickUnitnum = 7;

                    MyConfig.Cfg.Bank2.Add(item.Cell, Tuple.Create("Idle", PickUnitnum));
                    var idx = MyConfig.Cfg.TimerList.FindIndex(x => x.Item1 == item.Cell);

                    if (idx != -1)
                    {
                        TimeSpan time = TimeSpan.FromSeconds(item.Timer);
                        UpdateTimerUI("TimeCell" + item.Cell, time.ToString(@"mm\:ss"));
                    }
                    count++;
                }
                MyConfig.Cfg.EnabledCellinBank2 = count;

            }

        }

        public int CheckEnabledCells(string Set)
        {
            int count = 0;
            int BankNumber = 0;
            
            switch (MyConfig.Cfg.CurrentSet)
            {
                case "A": BankNumber = 2; break;
                case "B": BankNumber = 1; break;
                case "C": BankNumber = 2; break;
                case "D": BankNumber = 1; break;
                default:
                    break;
            }

            if (BankNumber == 1)
            {
                var EnabledCells = MyConfig.Cfg.RecepiList.Cells.FindAll(x => x.Enabled == true && int.Parse(x.Cell) > 6);
                count = EnabledCells.Count();
                if (count == 0)
                {
                    switch (MyConfig.Cfg.CurrentSet)
                    {
                        case "A": MyConfig.Cfg.CurrentSet = "B"; break;
                        case "C": MyConfig.Cfg.CurrentSet = "D"; break;
                        default:
                            break;
                    }
                }
            }
            else
            {
                var EnabledCells = MyConfig.Cfg.RecepiList.Cells.FindAll(x => x.Enabled == true && int.Parse(x.Cell) > 6);
                count = EnabledCells.Count();
                if (count == 0)
                {
                    switch (MyConfig.Cfg.CurrentSet)
                    {
                        case "A": MyConfig.Cfg.CurrentSet = "B"; break;
                        case "C": MyConfig.Cfg.CurrentSet = "D"; break;
                        default:
                            break;
                    }
                }
            }
            return count;
        }

        public TimeSpan CalculateAmpMin(double AmpMin)
        {
            double result = 0;
            result = (AmpMin * 60) / MyConfig.Cfg.PlatingCurrent;
            TimeSpan time = TimeSpan.FromSeconds(result);
            return time;
        }

        public void ClearAllTimer()
        {
            for (int i = 1; i <= 12; i++)
            {
                var Cell = 1.ToString();
                var CellNum = MyConfig.Cfg.TimerList.Find(x => x.Item1 == Cell);
                CustomTimerList[Cell].Counter = CustomTimerList[Cell].timer;
                CellNum.Item2.Stop();
            }
        }

        public void ChangeTimer(string CellName, int TimerSecond)
        {
            var idx = MyConfig.Cfg.TimerList.FindIndex(x => x.Item1 == CellName);
            if (idx != -1)
            {
                var val = MyConfig.Cfg.TimerList[idx];
                val.Item2.IsEnabled = false;
                var timer = CreateTimer(TimerSecond, CellName);

                MyConfig.Cfg.TimerList[idx] = Tuple.Create(val.Item1, timer, val.Item3);

                var idx2 = MyConfig.Cfg.RecepiList.Cells.FindIndex(x => x.Cell == CellName);
                MyConfig.Cfg.RecepiList.Cells[idx].Timer = TimerSecond;

                UpdateTimerUI("TimerStatCell" + CellName, "TimerChanged");

            }
        }

        public void ResetTimerAndStart(String Cell)
        {
            UpdateTimerUI("TimerStatCell" + Cell, "TimerReset");
            var CellNum = MyConfig.Cfg.TimerList.Find(x => x.Item1 == Cell);
            CustomTimerList[Cell].Counter = CustomTimerList[Cell].timer;
            CellNum.Item2.Start();
        }



        public void SetTimerTo0()
        {
            for (int i = 1; i <= 12; i++)
            {
                CustomTimerList[i.ToString()].Counter = 0;
            }
        }

        public DispatcherTimer CreateTimer(int TotalSeconds, string Cell)
        {
            DispatcherTimer dtimer = new DispatcherTimer();
            var tt = new CustomTimer(Cell, ev, TotalSeconds);
            CustomTimerList[Cell] = tt;
            //Timelist.Add(tt);
            //var tt = new CustomTimer(item.Cell,ev,item.Timer);
            dtimer.Interval = new TimeSpan(0, 0, 1);
            EventHandler timerTick = tt.PollUpdates;
            dtimer.Tick += timerTick;

            return dtimer;
        }

        private void UpdateTimerUI(string UIName, string Values)
        {
            var propInfo = MyConfig.Cfg.Model.GetType().GetProperty(UIName);
            if (propInfo != null)
            {
                propInfo.SetValue(MyConfig.Cfg.Model, Values, null);
            }
        }

        public void StopTimer(string CellName)
        {
            var CellNum = MyConfig.Cfg.TimerList.Find(x => x.Item1 == CellName);
            int banknum = 0;
            CellNum.Item2.Stop();

            var bank = int.Parse(CellName);

            if (bank == 1 || bank < 70)
            {
                banknum = 1;
                MyConfig.Cfg.Bank1[CellName] = Tuple.Create("TimerDone", MyConfig.Cfg.Bank1[CellName].Item2);
            }
            else
            {
                banknum = 2;
                MyConfig.Cfg.Bank2[CellName] = Tuple.Create("TimerDone", MyConfig.Cfg.Bank2[CellName].Item2);
            }
            MyConfig.Cfg.Model.robotControl.AddToUnloadQue(banknum, CellName);
            UpdateTimerUI("TimeCell" + CellName, "00:00");

        }

        public void StartTimer(string CellName, string State = "")
        {
            var CellNum = MyConfig.Cfg.TimerList.Find(x => x.Item1 == CellName);
            if (State == "MANUAL PLATE")
            {
                //int _celnum = int.Parse(CellName);
                //var _celltimer = MyConfig.Cfg.RecepiList.Cells[_celnum - 1].Timer;
                //ChangeTimer(CellName, _celltimer);
            }
       
            UpdateTimerUI("TimerStatCell" + CellName, "TimerStarted");
            CellNum.Item2.Start();
           
            int _CellName = int.Parse(CellName);
            if (_CellName < 7)
            {
                var idx = MyConfig.Cfg.Bank1.ContainsKey(CellName);
                if (!idx)
                {
                    MyConfig.Cfg.Bank1.Add(CellName, Tuple.Create("Idle", _CellName));
                }
                MyConfig.Cfg.Bank1[CellName] = Tuple.Create("TimerStart", MyConfig.Cfg.Bank1[CellName].Item2);
            }
            else
            {
                var idx = MyConfig.Cfg.Bank2.ContainsKey(CellName);
                if (!idx)
                {
                    MyConfig.Cfg.Bank2.Add(CellName, Tuple.Create("Idle", _CellName));
                }
                MyConfig.Cfg.Bank2[CellName] = Tuple.Create("TimerStart", MyConfig.Cfg.Bank2[CellName].Item2);
            }
        }

        public void ManualStartTimer(string CellName)
        {
            if (!isManualCellInitialized)
            {
                isManualCellInitialized = true;
            }
            var CellNum = MyConfig.Cfg.TimerList.Find(x => x.Item1 == CellName);
            UpdateTimerUI("TimerStatCell" + CellName, "TimerStarted");
            CellNum.Item2.Start();
        }

        public void StartDip(int BankNumber, string CellNumber)
        {
            string CellName = "";
            if (BankNumber == 1)
            {
                CellName = MyConfig.Cfg.Bank1.FirstOrDefault(x => x.Value.Item1 == "Loaded").Key;
            }
            else
            {
                CellName = MyConfig.Cfg.Bank2.FirstOrDefault(x => x.Value.Item1 == "Loaded").Key;
            }

            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Start Dipping Process on Cell: " + CellName);
            Console.WriteLine("Dipping: " + CellName);
            StartTimer(CellName);
            ev.DippingStart.Set();
        }

        //S---------------- Custom Timer Class ------------------
        class CustomTimer
        {
            Events ev;
            public String CellName = "";
            public int timer = 0;
            public int Counter = 0;

            public CustomTimer(string CellName, Events ev, int timer)
            {
                this.CellName = CellName;
                this.ev = ev;
                this.timer = timer;
                this.Counter = timer;
            }

            public void PollUpdates(object sender, EventArgs e)
            {
                Counter--;
                TimeSpan time = TimeSpan.FromSeconds(Counter);
                switch (CellName)
                {
                    case "1": MyConfig.Cfg.Model.TimeCell1 = time.ToString(@"mm\:ss"); break;
                    case "2": MyConfig.Cfg.Model.TimeCell2 = time.ToString(@"mm\:ss"); break;
                    case "3": MyConfig.Cfg.Model.TimeCell3 = time.ToString(@"mm\:ss"); break;
                    case "4": MyConfig.Cfg.Model.TimeCell4 = time.ToString(@"mm\:ss"); break;
                    case "5": MyConfig.Cfg.Model.TimeCell5 = time.ToString(@"mm\:ss"); break;
                    case "6": MyConfig.Cfg.Model.TimeCell6 = time.ToString(@"mm\:ss"); break;
                    case "7": MyConfig.Cfg.Model.TimeCell7 = time.ToString(@"mm\:ss"); break;
                    case "8": MyConfig.Cfg.Model.TimeCell8 = time.ToString(@"mm\:ss"); break;
                    case "9": MyConfig.Cfg.Model.TimeCell9 = time.ToString(@"mm\:ss"); break;
                    case "10": MyConfig.Cfg.Model.TimeCell10 = time.ToString(@"mm\:ss"); break;
                    case "11": MyConfig.Cfg.Model.TimeCell11 = time.ToString(@"mm\:ss"); break;
                    case "12": MyConfig.Cfg.Model.TimeCell12 = time.ToString(@"mm\:ss"); break;

                    default:
                        break;
                }

                if (MyConfig.Cfg.EMOStat == true)
                {
                    var tm1 = MyConfig.Cfg.TimerList.Find(x => x.Item1 == CellName);
                    SetVal("TimerStatCell" + CellName, "TimerStop-Forced");
                    tm1.Item2.Stop();
                    ResetTimerBox(CellName);
                }
                
                if (Counter == 0)
                {
                    int banknum = 0;
                    var tm = MyConfig.Cfg.TimerList.Find(x => x.Item1 == CellName);
                    tm.Item2.Stop();
                    SetVal("TimerStatCell" + CellName, "TimerStop-Complete");
                    Console.WriteLine("DippingDone: " + CellName);
                    MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Dipping Done on Cell: " + CellName);

                    var find = MyConfig.Cfg.Bank1.FirstOrDefault(x => x.Key == CellName);
                    if (find.Key != null)
                        banknum = 1;
                    else
                        banknum = 2;

                    if (banknum == 1)
                        MyConfig.Cfg.Bank1[CellName] = Tuple.Create("TimerDone", MyConfig.Cfg.Bank1[CellName].Item2);
                    else
                        MyConfig.Cfg.Bank2[CellName] = Tuple.Create("TimerDone", MyConfig.Cfg.Bank2[CellName].Item2);

                    MyConfig.Cfg.Model.powerSupply.PSCmd(CellName, "OFF","TIMER DONE");

                    if(MyConfig.Cfg.PSList[CellName].Status== "MANUAL PLATE DONE")
                    {
                        CurrentCell = CellName;
                        Counter = timer;
                        return;
                    }
                    var CellOn = GetUIVal("cboxCellSensor" + CellName);

                    if (CellOn == "True" || !MyConfig.Cfg.Model.CellSensorCheck)
                    {
                        MyConfig.Cfg.Model.robotControl.AddToUnloadQue(banknum, CellName);
                        CheckForExpired(banknum, CellName);
                    }
                    else if (CellOn == "False" || !MyConfig.Cfg.Model.CellSensorCheck)
                    {
                        MyConfig.Cfg.Model.ErrorCode = "Alert,Fixture not detected in cell" + CellName;
                        MyConfig.Cfg.Model.robotControl.AddToUnloadQue(banknum, CellName);
                        CheckForExpired(banknum, CellName);

                        if (banknum == 1)
                            MyConfig.Cfg.UndetectedCellBank1.Add(CellName);
                        else
                            MyConfig.Cfg.UndetectedCellBank2.Add(CellName);
                    }

                    CurrentCell = CellName;
                    Counter = timer;
                }
            }

            private void CheckForExpired(int banknum, string CellName)
            {
                SetVal("TimerStatCell" + CellName, "TimerComplete-Cheking For Expire");
                Task.Run(() =>
                {
                    var startTime = DateTime.Now;
                    bool timerExpired = true;
                    var queue = banknum == 1 ? MyConfig.Cfg.UnloadQueA : MyConfig.Cfg.UnloadQueB;
                    var item = Tuple.Create(banknum, CellName);
                    while ((DateTime.Now - startTime).Minutes <= MyConfig.Cfg.CellExpireTime)
                    {
                        if (!queue.Contains(item))
                        {
                            timerExpired = false;
                            SetVal("TimerStatCell" + CellName, "TimerDone");
                            break;
                        }
                        if (MyConfig.Cfg.EMOStat == true)
                        {
                            timerExpired = false;
                            break;
                        }
                        Thread.Sleep(250);
                    }
                    if (timerExpired)
                    {
                        MyConfig.Cfg.Model.ErrorCode = $"Alert, Bank {banknum} : Cell {CellName} oversoak for pickup";
                        SetVal("TimerStatCell" + CellName, "TimerDone");
                    }
                });
            }

            private void ResetTimerBox(string CellName)
            {
                lock (_object)
                {
                    TimeSpan time = TimeSpan.FromSeconds(0);
                    SetVal("TimerStatCell" + CellName, "TimerReset");
                    switch (CellName)
                    {
                        case "1": MyConfig.Cfg.Model.TimeCell1 = time.ToString(@"mm\:ss"); break;
                        case "2": MyConfig.Cfg.Model.TimeCell2 = time.ToString(@"mm\:ss"); break;
                        case "3": MyConfig.Cfg.Model.TimeCell3 = time.ToString(@"mm\:ss"); break;
                        case "4": MyConfig.Cfg.Model.TimeCell4 = time.ToString(@"mm\:ss"); break;
                        case "5": MyConfig.Cfg.Model.TimeCell5 = time.ToString(@"mm\:ss"); break;
                        case "6": MyConfig.Cfg.Model.TimeCell6 = time.ToString(@"mm\:ss"); break;
                        case "7": MyConfig.Cfg.Model.TimeCell7 = time.ToString(@"mm\:ss"); break;
                        case "8": MyConfig.Cfg.Model.TimeCell8 = time.ToString(@"mm\:ss"); break;
                        case "9": MyConfig.Cfg.Model.TimeCell9 = time.ToString(@"mm\:ss"); break;
                        case "10": MyConfig.Cfg.Model.TimeCell10 = time.ToString(@"mm\:ss"); break;
                        case "11": MyConfig.Cfg.Model.TimeCell11 = time.ToString(@"mm\:ss"); break;
                        case "12": MyConfig.Cfg.Model.TimeCell12 = time.ToString(@"mm\:ss"); break;

                        default:
                            break;
                    }
                }
            }

            private string GetUIVal(string UiName)
            {
                string value = "";

                var propInfog = MyConfig.Cfg.Model.GetType().GetProperty(UiName);
                if (propInfog != null)
                {
                    var val = propInfog.GetValue(MyConfig.Cfg.Model);
                    value = val.ToString();
                }
                return value;
            }

            private void SetVal(string ModelName, string Value)
            {
                var propInfo = MyConfig.Cfg.Model.GetType().GetProperty(ModelName);
                if (propInfo != null)
                {
                    propInfo.SetValue(MyConfig.Cfg.Model, Value, null);
                }
            }
        }
        //E---------------- Custom Timer Class ------------------
    }
}

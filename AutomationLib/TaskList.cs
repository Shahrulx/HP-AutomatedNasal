﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLib
{
    public partial class Model : INotifyPropertyChanged
    {
        private string _TaskMainFlowState;
        public string TaskMainFlowState
        {
            get { return _TaskMainFlowState; }
            set
            {
                _TaskMainFlowState = value;
                OnPropertyChanged("TaskMainFlowState");

            }

        }

        private string _TaskPowerSupplyMonitor = "IDLE";
        public string TaskPowerSupplyMonitor
        {
            get { return _TaskPowerSupplyMonitor; }
            set
            {
                _TaskPowerSupplyMonitor = value;
                OnPropertyChanged("TaskPowerSupplyMonitor");

            }

        }

        private string _TaskUndip = "IDLE";
        public string TaskUndip
        {
            get { return _TaskUndip; }
            set
            {
                _TaskUndip = value;
                OnPropertyChanged("TaskUndip");

            }

        }

        private string _TaskDip = "IDLE";
        public string TaskDip
        {
            get { return _TaskDip; }
            set
            {
                _TaskDip = value;
                OnPropertyChanged("TaskDip");

            }

        }

        private string _TimerStatCell1 = "IDLE";
        public string TimerStatCell1
        {
            get { return _TimerStatCell1; }
            set
            {
                _TimerStatCell1 = value;
                OnPropertyChanged("TimerStatCell1");

            }

        }

        private string _TimerStatCell2 = "IDLE";
        public string TimerStatCell2
        {
            get { return _TimerStatCell2; }
            set
            {
                _TimerStatCell2 = value;
                OnPropertyChanged("TimerStatCell2");

            }

        }

        private string _TimerStatCell3 = "IDLE";
        public string TimerStatCell3
        {
            get { return _TimerStatCell3; }
            set
            {
                _TimerStatCell3 = value;
                OnPropertyChanged("TimerStatCell3");

            }

        }

        private string _TimerStatCell4 = "IDLE";
        public string TimerStatCell4
        {
            get { return _TimerStatCell4; }
            set
            {
                _TimerStatCell4 = value;
                OnPropertyChanged("TimerStatCell4");

            }

        }

        private string _TimerStatCell5 = "IDLE";
        public string TimerStatCell5
        {
            get { return _TimerStatCell5; }
            set
            {
                _TimerStatCell5 = value;
                OnPropertyChanged("TimerStatCell5");

            }

        }

        private string _TimerStatCell6 = "IDLE";
        public string TimerStatCell6
        {
            get { return _TimerStatCell6; }
            set
            {
                _TimerStatCell6 = value;
                OnPropertyChanged("TimerStatCell6");

            }

        }

        private string _TimerStatCell7 = "IDLE";
        public string TimerStatCell7
        {
            get { return _TimerStatCell7; }
            set
            {
                _TimerStatCell7 = value;
                OnPropertyChanged("TimerStatCell7");

            }

        }

        private string _TimerStatCell8 = "IDLE";
        public string TimerStatCell8
        {
            get { return _TimerStatCell8; }
            set
            {
                _TimerStatCell8 = value;
                OnPropertyChanged("TimerStatCell8");

            }

        }

        private string _TimerStatCell9 = "IDLE";
        public string TimerStatCell9
        {
            get { return _TimerStatCell9; }
            set
            {
                _TimerStatCell9 = value;
                OnPropertyChanged("TimerStatCell9");

            }

        }

        private string _TimerStatCell10 = "IDLE";
        public string TimerStatCell10
        {
            get { return _TimerStatCell10; }
            set
            {
                _TimerStatCell10 = value;
                OnPropertyChanged("TimerStatCell10");

            }

        }

        private string _TimerStatCell11 = "IDLE";
        public string TimerStatCell11
        {
            get { return _TimerStatCell11; }
            set
            {
                _TimerStatCell11 = value;
                OnPropertyChanged("TimerStatCell11");

            }

        }

        private string _TimerStatCell12 = "IDLE";
        public string TimerStatCell12
        {
            get { return _TimerStatCell12; }
            set
            {
                _TimerStatCell12 = value;
                OnPropertyChanged("TimerStatCell12");

            }

        }
    }
}

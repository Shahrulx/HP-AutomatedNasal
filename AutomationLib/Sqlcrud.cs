﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLib
{
    public class Sqlcrud
    {

        public dynamic Conn = new SqlConnection("Data Source=MSI\\SQLEXPRESS;Initial Catalog=KTM;User ID=admin;Password=admin");

        string CurrentConnectionType = "";


        private static volatile Sqlcrud instance;
        private static object syncRoot = new Object();

        static SQLiteConnection Connection;
        static SQLiteCommand Command;
        static SQLiteDataReader dr;


        public void Initialize(string ConType, string Server, string DBName, string username, string password)
        {
            switch (ConType)
            {
                case "Oracle":

                    break;
                case "Sqlite":
                   
                    string conn = string.Format(@"Data Source=C:\HP\Recepi\HP.db;");
                    //string conn = @"Data Source=database.s3db;";
                    Connection = new SQLiteConnection(conn);
               

                    CurrentConnectionType = "Sqlite";
                    break;
                case "Mssql":
                    Conn = new SqlConnection("Data Source=" + Server + ";Initial Catalog=" + DBName + ";User ID=" + username + ";Password=" + password + ";");
                    CurrentConnectionType = "Mssql";
                    break;

                default:
                    break;
            }

        }
        public static Sqlcrud Cfg
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new Sqlcrud();
                    }
                }

                return instance;
            }
        }
        //public void read(object output)
        //{
        //    dynamic _output = output as dynamic;
        //    readdata("", "*", ref _output);
        //}
        public dynamic read(object output)
        {
            dynamic _output = output as dynamic;

            Type type = output.GetType();

            if (type.Name == "List`1")
            {
                type = output.GetType().GetGenericArguments()[0];
                var tname = type.FullName.Split('+');
                readdata(tname[1], "*", ref _output);
            }

            else
            {
                readdata(type.Name, "*", ref _output);
            }

            return _output;
        }
        public void read(string tablename, dynamic column, ref string output)
        {
            dynamic _output = output as dynamic;
            readdata(tablename, column, ref _output);
        }
        public void read(object tablename, dynamic column, ref DataTable output, string where = "")
        {
            Type type = tablename.GetType();
            object _output = output as object;
            dynamic reader = null; ;
            dynamic cmd = null;

            List<string> _where = new List<string>();
            string values = "";


            if (where != "")
            {
                values = "where ";
                _where = where.Split(',').ToList();
            }

            foreach (var item in _where)
            {
                var prop1 = tablename.GetType().GetProperties();
                var col = prop1.First(x => x.Name == item);
                values += col.Name + "=" + col.GetValue(tablename, null) + ",";
            }

            string columns = "";


            if (values != "")
                values = values.Substring(0, (values.Length - 1));




            readdata(type.Name, column, ref _output, values);
        }

        private void readdata(string tablename, dynamic column, ref dynamic output, string param = "")
        {
            dynamic reader = null;
            dynamic cmd = null;
            string sqlcomand = "";

            if (column != "*" || column != "")
            {
                sqlcomand = "select " + column + " from " + tablename;
            }
            else
            {
                sqlcomand = "select  * from " + tablename;
            }

            if (param != "")
            {
                sqlcomand = sqlcomand + " " + param;
            }

            Conn.Open();

            switch (CurrentConnectionType)
            {
                case "Oracle":
                   
                    break;
                case "Mssql":
                    SqlDataReader _reader = null;
                    SqlCommand _cmd = Conn.CreateCommand();
                    _cmd.CommandText = sqlcomand;

                    reader = _reader;
                    cmd = _cmd;
                    break;
                default:
                    break;
            }

            string columnname = "";

            try
            {
                // instantiate and open connection

                // don't ever do this
                // SqlCommand cmd = new SqlCommand(
                // "select * from Customers where city = '" + inputCity + "'";

                // 1. declare command object with parameter
                //cmd = new SqlCommand(
                //"select TOP 1 * from table1", Conn);


                // 2. define parameters used in command object

                // param.ParameterName = "@data1";
                //param.Value = "datavarchar";
                // 3. add new parameter to command object
                //cmd.Parameters.Add(param);

                Type type = output.GetType();

                switch (type.Name)
                {
                    case "DataTable":
                        output.Load(cmd.ExecuteReader());
                        // It's an int
                        break;

                    case "String":

                        break;

                    case "List`1":
                        reader = cmd.ExecuteReader();
                        dynamic f = null;
                        var dataTable = new DataTable();
                        dataTable.Load(reader);



                        foreach (DataRow item in dataTable.Rows)
                        {
                            f = Activator.CreateInstance(output.GetType().GetGenericArguments()[0]);
                            foreach (DataColumn item2 in dataTable.Columns)
                            {

                                Console.WriteLine("{0}={1}", item2, item[item2]);
                                var tablecol = f.GetType().GetProperty(item2.ToString());

                                if (tablecol != null)
                                {
                                    var vtype = item[item2].GetType();
                                    if (item[item2].ToString() != "")
                                    {
                                        if (vtype.Name == "DateTime")
                                        {
                                            tablecol.SetValue(f, item[item2].ToString());
                                        }

                                        else
                                        {
                                            tablecol.SetValue(f, item[item2]);
                                        }




                                    }

                                }


                            }
                            output.Add(f);
                        }

                        break;
                    default:
                        reader = cmd.ExecuteReader();
                        f = null;
                        dataTable = new DataTable();
                        dataTable.Load(reader);



                        foreach (DataRow item in dataTable.Rows)
                        {

                            foreach (DataColumn item2 in dataTable.Columns)
                            {

                                Console.WriteLine("{0}={1}", item2, item[item2]);
                                var tablecol = output.GetType().GetProperty(item2.ToString());

                                if (tablecol != null)
                                {
                                    var vtype = item[item2].GetType();
                                    if (item[item2].ToString() != "")
                                    {
                                        if (vtype.Name == "DateTime")
                                        {
                                            tablecol.SetValue(output, item[item2].ToString());
                                        }

                                        else
                                        {
                                            tablecol.SetValue(output, item[item2]);
                                        }




                                    }

                                }


                            }

                        }




                        break;
                }


            }
            finally
            {
                // close reader
                if (reader != null)
                {
                    reader.Close();
                }
                // close connection
                if (Conn != null)
                {
                    Conn.Close();
                }
            }
        }




        public void Query(string Query)
        {
            if (CurrentConnectionType == "Mssql")
            {

                SqlCommand cmd = new SqlCommand(Query, Conn);
                SqlTransaction trans;

                Conn.Open();
                trans = Conn.BeginTransaction();

                cmd.Transaction = trans;
                cmd.CommandText = Query;
                try
                {
                    cmd.ExecuteNonQuery(); //Exected first query
                    cmd.Transaction.Commit();

                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error but we are rollbacking");
                    cmd.Transaction.Rollback();
                }
                Conn.Close();

            }

            else if (CurrentConnectionType == "Sqlite")
            {

                //Connection.Open();
                try
                {
                    Connection.Open();
                    SQLiteCommand mCmd = new SQLiteCommand(Query, Connection);
                    //mCmd.ExecuteNonQuery();
                    dr = mCmd.ExecuteReader();
                    dr.Close();
                }
                catch (SQLiteException e)
                {

                }
                Connection.Close();

            }
        }

        public void Query(string Query, ref DataTable dt)
        {
            if (CurrentConnectionType == "Mssql")
            {
                SqlCommand cmd = new SqlCommand(Query, Conn);
                SqlDataReader reader = null;
                SqlTransaction trans;

                Conn.Open();
                trans = Conn.BeginTransaction();

                cmd.Transaction = trans;
                cmd.CommandText = Query;
                try
                {
                    cmd.ExecuteNonQuery(); //Exected first query
                    reader = cmd.ExecuteReader();
                    dt.Load(reader);
                    cmd.Transaction.Commit();

                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error but we are rollbacking");
                    cmd.Transaction.Rollback();
                }
                Conn.Close();
            }

            else if (CurrentConnectionType == "Sqlite")
            {

                //Connection.Open();
                try
                {
                    Connection.Open();
                    SQLiteCommand mCmd = new SQLiteCommand(Query, Connection);
                    //mCmd.ExecuteNonQuery();
                    dr = mCmd.ExecuteReader();
                    dt.Load(dr);
                    dr.Close();
                }
                catch (SQLiteException e)
                {

                }
                Connection.Close();

            }

        }

        public void insert(object output)
        {
            dynamic _output = output as dynamic;
            Type type = output.GetType();
            var tname = type.Name;
            string columns = "";
            string values = "";
            foreach (var prop in output.GetType().GetProperties())
            {

                columns += prop.Name + ",";
                values += prop.GetValue(output, null) + ",";
                Console.WriteLine("{0}={1}", prop.Name, prop.GetValue(output, null));
            }
            columns = columns.Remove(columns.Length - 1, 1);
            values = values.Remove(values.Length - 1, 1);
            insertdata(tname, columns, values);

        }

        public void insertdata(string tablename, dynamic column, dynamic values)
        {
            //string _value = "@val1,@val2";
            string vv = values.ToString();
            string content1 = column.Replace(",", ",@");
            content1 = "@" + content1;

            List<string> _content1 = content1.Split(',').ToList<string>();
            List<string> _values = vv.Split(',').ToList<string>();

            String Query = String.Format("INSERT INTO {0} ({1}) VALUES ({2})", tablename, column, content1);

            SqlCommand cmd = new SqlCommand(Query, Conn);
            SqlTransaction trans;
            SqlParameter param = new SqlParameter();
            param = new SqlParameter();

            Conn.Open();
            trans = Conn.BeginTransaction();

            cmd.Transaction = trans;

            int i = 0;
            foreach (var item in _content1)
            {
                if (item == "DateTimeNow")
                    cmd.Parameters.AddWithValue(item, DateTime.Now);
                else
                    cmd.Parameters.AddWithValue(item, _values[i]);

                i++;
            }

            //cmd.Parameters.AddWithValue("@val2", 14);

            try
            {
                cmd.ExecuteNonQuery(); //Exected first query
                cmd.Transaction.Commit();

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error but we are rollbacking");
                cmd.Transaction.Rollback();
            }
            Conn.Close();

            //string FirstQuery = "INSERT INTO Table1 VALUES('Vineeth',24)";
            //string SecondQuery = "INSERT INTO Table2 VALUES('HisAddress')";
            //int ErrorVar = 0;
            //using (SqlConnection con = new SqlConnection("Server=con;UID=harish;PWD=;database=pubs;"))
            //{
            //    SqlCommand ObjCommand = new SqlCommand(FirstQuery, con);
            //    SqlTransaction trans;
            //    con.Open();
            //    trans = con.BeginTransaction();
            //    ObjCommand.Transaction = trans;
            //    //Executing first query
            //    try
            //    {
            //        ObjCommand.ExecuteNonQuery();  //Exected first query
            //        int a = 10 / ErrorVar; //Generating error. This will stop executing next statement.
            //        ObjCommand.CommandText = SecondQuery;
            //        ObjCommand.ExecuteNonQuery();

            //        //Everything gone fine. So commiting
            //        ObjCommand.Transaction.Commit();
            //    }
            //    catch (Exception ex)
            //    {
            //        Console.WriteLine("Error but we are rollbacking");
            //        ObjCommand.Transaction.Rollback();
            //    }
            //    con.Close();
            //}


        }

        public void update(object tablename, string data, string condition)
        {
            dynamic _output = tablename as dynamic;
            Type type = tablename.GetType();
            var tname = type.Name;
            string columns = "";
            string query = "";
            string values = "";
            // List<string> _data = updatedata.Split(',').ToList<string>();
            List<string> _condition = condition.Split(',').ToList<string>();

            string dataquery = "";

            //foreach (var item in _data)
            //{
            //    var value = tablename.GetType().GetProperty(updatedata).GetValue(tablename, null);
            //    dataquery += item + "=@" + value.ToString() + ",";
            //}
            //query = query.Remove(query.Length - 1, 1);

            ////foreach (var prop in tablename.GetType().GetProperties())
            ////{
            ////    if (_content1.Contains(prop.Name))
            ////    {
            ////        _condition += prop.Name + "=@" + prop.Name + " AND ";
            ////    }

            ////    query += prop.Name + "=@" + prop.Name + ",";
            ////    columns += prop.Name + ",";
            ////    values += prop.GetValue(output, null) + ",";
            ////    Console.WriteLine("{0}={1}", prop.Name, prop.GetValue(output, null));
            ////}
            ////query = query.Remove(query.Length - 1, 1);
            ////_condition = _condition.Remove(_condition.Length - 4, 4);
            ////columns = columns.Remove(columns.Length - 1, 1);
            ////values = values.Remove(values.Length - 1, 1);
            //updatedata(tname, query, columns, values, _condition);
            updatedata("", "", "", "", "");
        }

        public void update(object output, string condition)
        {
            dynamic _output = output as dynamic;
            Type type = output.GetType();
            var tname = type.Name;
            string columns = "";
            string query = "";
            string values = "";
            List<string> _content1 = condition.Split(',').ToList<string>();
            string _condition = "";
            foreach (var prop in output.GetType().GetProperties())
            {
                if (_content1.Contains(prop.Name))
                {
                    _condition += prop.Name + "=@" + prop.Name + " AND ";
                }

                query += prop.Name + "=@" + prop.Name + ",";
                columns += prop.Name + ",";
                values += prop.GetValue(output, null) + ",";
                Console.WriteLine("{0}={1}", prop.Name, prop.GetValue(output, null));
            }
            query = query.Remove(query.Length - 1, 1);
            _condition = _condition.Remove(_condition.Length - 4, 4);
            columns = columns.Remove(columns.Length - 1, 1);
            values = values.Remove(values.Length - 1, 1);
            updatedata(tname, query, columns, values, _condition);

        }

        private void updatedata(string tablename, string query, dynamic column, dynamic values, string condition)
        {
            //string _value = "@val1,@val2";
            string qq = values.ToString();
            string content1 = column.Replace(",", ",@");
            content1 = "@" + content1;
            List<string> _content1 = content1.Split(',').ToList<string>();
            List<string> _values = qq.Split(',').ToList<string>();



            String Query = String.Format("UPDATE {0} set {1} where {2}", tablename, query, condition);

            SqlCommand cmd = new SqlCommand(Query, Conn);
            SqlTransaction trans;
            SqlParameter param = new SqlParameter();
            param = new SqlParameter();

            Conn.Open();
            trans = Conn.BeginTransaction();

            cmd.Transaction = trans;

            int i = 0;
            foreach (var item in _content1)
            {
                cmd.Parameters.AddWithValue(item, _values[i]);
                i++;
            }

            //cmd.Parameters.AddWithValue("@val2", 14);

            try
            {
                cmd.ExecuteNonQuery(); //Exected first query
                cmd.Transaction.Commit();

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error but we are rollbacking");
                cmd.Transaction.Rollback();
            }
            Conn.Close();

            //string FirstQuery = "INSERT INTO Table1 VALUES('Vineeth',24)";
            //string SecondQuery = "INSERT INTO Table2 VALUES('HisAddress')";
            //int ErrorVar = 0;
            //using (SqlConnection con = new SqlConnection("Server=con;UID=harish;PWD=;database=pubs;"))
            //{
            //    SqlCommand ObjCommand = new SqlCommand(FirstQuery, con);
            //    SqlTransaction trans;
            //    con.Open();
            //    trans = con.BeginTransaction();
            //    ObjCommand.Transaction = trans;
            //    //Executing first query
            //    try
            //    {
            //        ObjCommand.ExecuteNonQuery();  //Exected first query
            //        int a = 10 / ErrorVar; //Generating error. This will stop executing next statement.
            //        ObjCommand.CommandText = SecondQuery;
            //        ObjCommand.ExecuteNonQuery();

            //        //Everything gone fine. So commiting
            //        ObjCommand.Transaction.Commit();
            //    }
            //    catch (Exception ex)
            //    {
            //        Console.WriteLine("Error but we are rollbacking");
            //        ObjCommand.Transaction.Rollback();
            //    }
            //    con.Close();
            //}


        }

        public void getdatatype()
        {

        }



    }


}

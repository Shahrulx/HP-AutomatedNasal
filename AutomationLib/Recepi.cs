﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLib
{
    public class RootObject
    {
        public List<Cells> Cells { get; set; }
        public List<Product> Product { get; set; }
        public List<SensorList> SensorList { get; set; }
        public List<SetConfig> SetConfig { get; set; }
    }

    public class Cells
    {
        public string Cell { get; set; }
        public double Timer { get; set; }
        public bool Enabled { get; set; }
        public double AmpMin { get; set; }
        public List<string> PowerSupply { get; set; }
    }

    public class Product
    {
        public string Name { get; set; }
        public double Amp { get; set; }
        public bool Selected { get; set; }
    }
    public class SetConfig
    {
        public List<double> SetA { get; set; }
        public List<double> SetB { get; set; }
        public List<double> SetC { get; set; }
        public List<double> SetD { get; set; }
    }

    public class SensorList
    {
        public string SensorName { get; set; }
        public int Value { get; set; }
    }

    public class PowerSupply
    {
        public int deviceid { get; set; }
        public int address { get; set; }
        public int tmeout { get; set; }
        public string Status { get; set; }
        public double Current { get; set; }
        public double Voltage { get; set; }
        public double LastTimer { get; set; }
    }

    public class ChangeLog
    {
        public string Date { get; set; }
        public List<string> Changes { get; set; }
        public string Version { get; set; }
    }
    public class ChangeLogList
    {
        public List<ChangeLog> Changes { get; set; }
    }

    public class CellStatus
    {

    }

    public class UserManagement
    {
        public List<UserList> UserList { get; set; }
    }
    public class UserList
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Type { get; set; }
    }

}

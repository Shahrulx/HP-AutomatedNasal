﻿using HardwareLib;
using QISI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace AutomationLib
{
    public class PowerSupplyModule
    {
        static readonly object _object = new object();

        //key power supply num val={devicenum,pad,subad,timout,float voltnow,float curentnow}
        public bool StartMonitor = false;
        int DeviceId = 0;
        bool isCurrentSetRunning = false;
        // double TotalAmpMin = 0;
        Hardware hw;
        bool AmpRuning = false;
        bool isSetVolt = false;

        public void Init(Hardware hw, List<Tuple<string, int>> AddressList)
        {
            if (MyConfig.Cfg.Model.PowerSupplyEnable == false)
                return;

            this.hw = hw;
            MyConfig.Cfg.PSList.Clear();
            foreach (var add in MyConfig.Cfg.RecepiList.Cells)
            {
                int address = int.Parse(add.PowerSupply[1]);
                String _stat = "IDLE";
                bool stat = PSConnect(address);
                if (stat)
                {
                    _stat = "IDLE";
                }
                else if (MyConfig.Cfg.SimulationMode)
                {
                    _stat = "IDLE";
                }
                else
                {
                    _stat = "ERROR";
                }

                PowerSupply ps = new PowerSupply() { deviceid = DeviceId, address = address, tmeout = 16, Current = 0, Voltage = 0, Status = _stat,LastTimer=0.0 };
                MyConfig.Cfg.PSList.Add(add.PowerSupply[0], ps);
            }

            //if(!MyConfig.Cfg.SimulationMode)
            Task.Run(() => MonitorTask());
            Task.Run(() => MonitorTask2());
            Task.Run(() => MonitorTask3());
            Task.Run(() => LogPowerSupplyValue());
        }
        
        public bool PSConnect(int add)
        {
            bool stat = false;

            DeviceId = Gpib488.ibdev(0, add, 0, 16, 1, 0);
            Gpib488.ibwrt(DeviceId, "ID?", 3);
            if ((Gpib488.Ibsta() & Gpib488Consts.ERR) != 0)
                stat = false;
            else
                stat = true;

            return stat;
        }

        public void CloseAll()
        {
            foreach (var item in MyConfig.Cfg.PSList)
            {
                Close(item.Value.deviceid);
            }
        }

        public void Close(int DeviceId)
        {

            Gpib488.ibonl(DeviceId, 0);
        }


        public string PSManual(int add, string Option, string value = "0")
        {
            string result = "";
            StringBuilder RES = new StringBuilder();
            var DeviceId = Gpib488.ibdev(0, add, 0, 16, 1, 0);
            switch (Option)
            {
                case "ReadVolt":
                    Gpib488.ibwrt(DeviceId, "VOUT?", 5);
                    Gpib488.ibrd(DeviceId, RES, 32);

                    result = RES.ToString();
                    result = Resultparse(result).ToString();
                    break;

                case "ReadCurrent":
                    Gpib488.ibwrt(DeviceId, "IOUT?", 5);
                    Gpib488.ibrd(DeviceId, RES, 32);
                    result = RES.ToString();

                    result = Resultparse(result).ToString();
                    break;

                case "SetVolt":
                    Gpib488.ibwrt(DeviceId, $"VSET {value}", 5 + value.Length);

                    break;
                case "SetCurrent":
                    Gpib488.ibwrt(DeviceId, $"ISET {value}", 5 + value.Length);

                    break;
                case "Open":
                    DeviceId = Gpib488.ibdev(0, add, 0, 16, 1, 0);
                    break;

                case "Close":
                    Gpib488.ibonl(DeviceId, 0);
                    break;

                default:
                    break;
            }
            if ((Gpib488.Ibsta() & Gpib488Consts.ERR) != 0)
                MyConfig.Cfg.Model.ErrorCode = $"Alert,Power Supply Error: {Option} ";

            return result;
        }

        private double Resultparse(string result)
        {
            Regex regex = new Regex(@"\+?\-?\d+\.\d+");
            Match match = regex.Match(result);

            if (match.Success)
                return double.Parse(match.Value);
            else
                return 0.123;

        }

        private void _PSGpibCmd(string PSName, string Option, string value = "0")
        {
            StringBuilder RES = new StringBuilder();
            string result = "";

            lock (_object)
            {
                switch (Option)
                {
                    case "ReadVolt":

                        try
                        {
                            Gpib488.ibwrt(MyConfig.Cfg.PSList[PSName].deviceid, "VOUT?", 5);
                            Gpib488.ibrd(MyConfig.Cfg.PSList[PSName].deviceid, RES, 32);
                            if (MyConfig.Cfg.SimulationMode)
                                RES = new StringBuilder("2");

                            result = RES.ToString();
                            result = Resultparse(result).ToString();
                            result = String.Format("{0:0.00}", double.Parse(result));
                            MyConfig.Cfg.PSList[PSName].Voltage = double.Parse(result);
                        }
                        catch (Exception)
                        {
                            MyConfig.Cfg.Model.ErrorCode = $"Alert,Power Suppy Error {Option}";
                            return;
                        }

                        break;

                    case "ReadCurrent":
                        try
                        {
                            Gpib488.ibwrt(MyConfig.Cfg.PSList[PSName].deviceid, "IOUT?", 5);
                            Gpib488.ibrd(MyConfig.Cfg.PSList[PSName].deviceid, RES, 32);
                            if (MyConfig.Cfg.SimulationMode)
                                RES = new StringBuilder("1");

                            result = RES.ToString();

                            result = Resultparse(result).ToString();
                            result = String.Format("{0:0.00}", double.Parse(result));
                            MyConfig.Cfg.PSList[PSName].Current = double.Parse(result);
                        }
                        catch (Exception)
                        {

                            MyConfig.Cfg.Model.ErrorCode = $"Alert,Power Suppy Error {Option}";
                            return;
                        }


                        break;

                    case "SetVolt":
                        try
                        {
                            isSetVolt = true;
                            Gpib488.ibwrt(MyConfig.Cfg.PSList[PSName].deviceid, $"VSET {value}", 5 + value.Length);
                            MyConfig.Cfg.PSList[PSName].Voltage = double.Parse(value.ToString());
                            MyConfig.Cfg.Model.SetPowerSupplyLog("Info", $"Set Voltage For Cell {PSName}-Value:" + value);
                        }
                        catch (Exception EX)
                        {
                            isSetVolt = false;
                            MyConfig.Cfg.Model.ErrorCode = "Alert,Error PS SET Volt";
                            return;

                        }
                        isSetVolt = false;
                        break;
                    case "SetCurrent":
                        try
                        {
                            isSetVolt = true;
                            if (value == "0")
                            {
                                Gpib488.ibwrt(MyConfig.Cfg.PSList[PSName].deviceid, $"ISET {value}", 5 + value.Length);
                                Thread.Sleep(100);
                                MyConfig.Cfg.Model.SetPowerSupplyLog("Info", $"Set Current For Cell {PSName}-Value: {value}");
                            }
                            else
                            {
                                var delay = (MyConfig.Cfg.RampDuration * 1000) / MyConfig.Cfg.RampSteps;
                                var increament = MyConfig.Cfg.PlatingCurrent / MyConfig.Cfg.RampSteps;
                                //set current ramp

                                Task.Run(() =>
                                {
                                    while (AmpRuning)
                                    {

                                    }
                                    AmpRuning = true;
                                    for (var i = increament; i <= MyConfig.Cfg.PlatingCurrent; i += increament)
                                    {
                                        Gpib488.ibwrt(MyConfig.Cfg.PSList[PSName].deviceid, $"ISET {i}", 5 + value.Length);
                                        Thread.Sleep((int)(delay));
                                        MyConfig.Cfg.Model.SetPowerSupplyLog("Info", $"Set Current For Cell {PSName}-Value: {i.ToString("N3")}");
                                    }
                                    MyConfig.Cfg.PSList[PSName].Current = double.Parse(value.ToString());
                                    AmpRuning = false;
                                });
                            }


                        }
                        catch (Exception)
                        {
                            isSetVolt = false;
                            MyConfig.Cfg.Model.ErrorCode = "Alert,Error PS SET Current";
                        }
                        isSetVolt = false;
                        break;
                    default:
                        break;
                }

                //Close(MyConfig.Cfg.PSList[PSName].deviceid);

            }

        }

        public void PSCmd(string CellNumber, string Option, string Message = "")
        {
            if (MyConfig.Cfg.Model.PowerSupplyEnable == false)
                return;

            if (MyConfig.Cfg.currentState == MainFlow.SequenceState.UNINITIALIZE)
            {
                MyConfig.Cfg.Model.ErrorCode = "Alert,Unable To Turn On Power Supply. Please do System Initialize First";
            }

            lock (_object)
            {
                switch (Option)
                {
                    case "Monitor":
                        Monitor(CellNumber);
                        break;
                    case "OFF":

                        //check Power supply State
                        string State = MyConfig.Cfg.PSList[CellNumber].Status;

                        if(State == "IDLE")
                        {
                            //in idle state do nothing. just off power supply
                        }
                        else if (State == "RESET")
                        {
                            MyConfig.Cfg.PSList[CellNumber].Status = "RESET";
                            UpdateModel("PSStat_" + CellNumber, "CANCEL");
                        }
                        else if (State == "ERROR")
                        {
                            MyConfig.Cfg.PSList[CellNumber].Status = "RESET";
                            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Power Supply Over Voltagei-off Cell:" + CellNumber);
                        }
                        else if (State == "PLATE" && Message == "CANCEL")
                        {
                            //POWER SUPPLY TIMET DONE AUTO OFF
                            MyConfig.Cfg.PSList[CellNumber].Status = "RESET";
                            UpdateModel("PSStat_" + CellNumber, "CANCEL");
                            //MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Power Supply Auto-Off Cell:" + CellNumber);

                        }
                        else if (State == "PLATE" && Message != "CANCEL")
                        {
                            //POWER SUPPLY TIMET DONE AUTO OFF
                            MyConfig.Cfg.PSList[CellNumber].Status = "DONE";
                            UpdateModel("PSStat_" + CellNumber, "DONE");
                            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Power Supply Auto-Off Cell:" + CellNumber);

                        }
                        else if (State == "MANUAL PLATE" && Message=="TIMER DONE")
                        {
                            //STOP TIMER when manual mode done
                            MyConfig.Cfg.PSList[CellNumber].Status = "MANUAL PLATE DONE";
                            UpdateModel("PSStat_" + CellNumber, "DONE");
                            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Power Supply Manual Plate Auto-Off Cell:" + CellNumber);
                        }
                        else if (State == "MANUAL PLATE DONE")
                        {
                            //STOP TIMER STILL WHILIE RUNING IN MANUAL PLATE MODE
                            MyConfig.Cfg.PSList[CellNumber].Status = "IDLE";
                            UpdateModel("PSStat_" + CellNumber, "IDLE");
                            MyConfig.Cfg.Model.cellModule.StopTimer(CellNumber);
                        }
                        else if (State == "MANUAL PLATE")
                        {
                            //STOP TIMER STILL WHILIE RUNING IN MANUAL PLATE MODE
                            MyConfig.Cfg.PSList[CellNumber].Status = "IDLE";
                            UpdateModel("PSStat_" + CellNumber, "IDLE");
                            MyConfig.Cfg.Model.cellModule.StopTimer(CellNumber);
                        }

                        _PSGpibCmd(CellNumber, "SetVolt", "0");
                        _PSGpibCmd(CellNumber, "SetCurrent", "0");

                 

                        break;

                    case "ON":

                        //check Power supply State
                        State = MyConfig.Cfg.PSList[CellNumber].Status;

                        if (Message == "AUTO PLATE")
                        {
                            //POWER SUPPLY AUTO TURN ON IN AUTMODE
                            UpdateModel("PSStat_" + CellNumber, "PLATE");
                            MyConfig.Cfg.PSList[CellNumber].Status = "PLATE";
                            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Power Supply Auto-Plate Cell:" + CellNumber);

                            MyConfig.Cfg.Model.cellModule.ResetTimerAndStart(CellNumber);
                            UpdateModel("PSStat_" + CellNumber, "PLATE");
                            MyConfig.Cfg.PSList[CellNumber].Status = "PLATE";
                            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Power Supply Re-Plate Cell:" + CellNumber);
                        }
                        else if (State == "RESET")
                        {
                            //RESET TIMER AND TURN ON POWER SUPPLY
                            MyConfig.Cfg.Model.cellModule.ResetTimerAndStart(CellNumber);
                            UpdateModel("PSStat_" + CellNumber, "PLATE");
                            MyConfig.Cfg.PSList[CellNumber].Status = "PLATE";
                            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Power Supply Re-Plate Cell:" + CellNumber);
                        }
                        else if (Message == "MANUAL PLATE")
                        {
                            //POWER SUPPLY MANUAL TURN ON 
                          
                            if (MyConfig.Cfg.currentState == MainFlow.SequenceState.UNINITIALIZE)
                            {
                                MyConfig.Cfg.Model.ErrorCode = "Alert,Could not start timer.Please do System Initialize";

                            }
                            else if (MyConfig.Cfg.currentState != MainFlow.SequenceState.IDLE)
                            {
                                MyConfig.Cfg.Model.ErrorCode = "Alert,Could not On Power Supply in Auto Mode.Please Finish Auto Mode";
                            }
                            else
                            {
                                MyConfig.Cfg.Model.cellModule.StartTimer(CellNumber, "MANUAL PLATE");

                            }
                            UpdateModel("PSStat_" + CellNumber, Message);
                            MyConfig.Cfg.PSList[CellNumber].Status = "MANUAL PLATE";
                        }

                        _PSGpibCmd(CellNumber, "SetVolt", MyConfig.Cfg.PlatingVoltage.ToString());
                        _PSGpibCmd(CellNumber, "SetCurrent", MyConfig.Cfg.PlatingCurrent.ToString());

                        break;
                    default:
                        break;
                }
            }
        }

        public void Monitor(string CellNumber)
        {
            //_PSGpibCmd(CellNumber, "ReadCurrent");
            //_PSGpibCmd(CellNumber, "ReadVolt");

            while (MyConfig.Cfg.EMOStat)
            {
                Thread.Sleep(250);
                CloseAll();
                return;
            }

           // UpdateModel("CellVoltage" + CellNumber, MyConfig.Cfg.PSList[CellNumber].Voltage.ToString());
            //UpdateModel("CellCurent" + CellNumber, MyConfig.Cfg.PSList[CellNumber].Current.ToString());

            if (MyConfig.Cfg.PSList[CellNumber].Current > MyConfig.Cfg.TrickleCurrent + 0.1)
            {
                MyConfig.Cfg.TotalAmpMin += MyConfig.Cfg.PSList[CellNumber].Current * 0.0042;
                //MyConfig.Cfg.TotalAmpMin += MyConfig.Cfg.Model.TotalAmpMin;
                //TotalAmpMin += MyConfig.Cfg.PSList[CellNumber].Current * 0.0042;
            }

            if (MyConfig.Cfg.PSList[CellNumber].Voltage > MyConfig.Cfg.CutoffVoltage)
            {
                MyConfig.Cfg.Model.ErrorCode = "Alert,Over Voltage on Cell" + CellNumber;
                MyConfig.Cfg.PSList[CellNumber].Status = "ERROR";
                //var tt = MyConfig.Cfg.TimerList.Find(x => x.Item1 == CellNumber);
                //tt.Item2.Stop();

                UpdateModel("PSStat_" + CellNumber, "ERROR");
                PSCmd(CellNumber, "OFF");

            }



            if (MyConfig.Cfg.TotalAmpMin >= 500)
            {
                //hw.motors["Motor1X"].WaitMotionDone(500);
                MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Cycle Brt Pump");

                MyConfig.Cfg.Model.tankModule.CycleBRT();
                MyConfig.Cfg.TotalAmpMin -= 500;
            }

        }

        private void UpdateModel(string UiName, string val)
        {
            var propInfo = MyConfig.Cfg.Model.GetType().GetProperty(UiName);
            if (propInfo != null)
            {
                propInfo.SetValue(MyConfig.Cfg.Model, val, null);
            }
        }

        public void MonitorTask2()
        {
            Thread.Sleep(3000);
            while (StartMonitor)
            {
                foreach (var item in MyConfig.Cfg.PSList)
                {
                    UpdateModel("CellVoltage" + item.Key, MyConfig.Cfg.PSList[item.Key].Voltage.ToString());
                    UpdateModel("CellCurent" + item.Key, MyConfig.Cfg.PSList[item.Key].Current.ToString());
                }
                Thread.Sleep(500);
                MyConfig.Cfg.Model.TaskPowerSupplyMonitor = "Monitor Running";
                MyConfig.Cfg.SaveINI();
                MyConfig.Cfg.UpdateUI("tboxTotalAmpMin", "SetValue", MyConfig.Cfg.TotalAmpMin.ToString());
            }
            MyConfig.Cfg.Model.TaskPowerSupplyMonitor = "End Monitor";
        }

        public void MonitorTask3()
        {
            while (StartMonitor)
            {
                foreach (var item in MyConfig.Cfg.PSList)
                {
                    while (isSetVolt)
                    {
                        Thread.Sleep(50);
                    }
                    _PSGpibCmd(item.Key, "ReadCurrent");
                    _PSGpibCmd(item.Key, "ReadVolt");
                }
                Thread.Sleep(10);
            }
        }

        public void MonitorTask()
        {
            DateTime timeStart = DateTime.Now;
            while (StartMonitor)
            {
                TimeSpan timeSpan = new TimeSpan();
                timeSpan = DateTime.Now - timeStart;
                if (timeSpan.TotalMilliseconds >= 240)
                {
                    timeStart = DateTime.Now;
                    foreach (var item in MyConfig.Cfg.PSList)
                    {
                        Monitor(item.Key);

                        if (MyConfig.Cfg.EMOStat)
                        {
                            break;
                        }

                    }
                }
                Thread.Sleep(10);
            }
        }

        public void LogPowerSupplyValue()
        {
            while (StartMonitor)
            {
                foreach (var item in MyConfig.Cfg.PSList)
                {
                    if (item.Value.Status.Contains("PLATE"))
                    {
                        var cellnum = int.Parse(item.Key);
                        var curent = MyConfig.Cfg.PSList[cellnum.ToString()].Current;
                        var voltage = MyConfig.Cfg.PSList[cellnum.ToString()].Voltage;

                        if (cellnum < 7)
                        {
                            MyConfig.Cfg.Model.SetPowerSupplyLog("SetLog", $"Set-{MyConfig.Cfg.Model.CurrentSetBank1} Cell {cellnum}-Voltage:{voltage}-Current:{curent}",cellnum);
                            //MyConfig.Cfg.Model.SetPowerSupplyLog("SetLog", $"Set-{MyConfig.Cfg.Model.CurrentSetBank1} Cell {cellnum}-Current:{curent}", cellnum);
                        }
                        else
                        {
                            MyConfig.Cfg.Model.SetPowerSupplyLog("SetLog", $"Set-{MyConfig.Cfg.Model.CurrentSetBank2} Cell {cellnum}-Voltage:{voltage}-Current:{curent}", cellnum);
                            //MyConfig.Cfg.Model.SetPowerSupplyLog("SetLog", $"Set-{MyConfig.Cfg.Model.CurrentSetBank2} Cell {cellnum}-Current:{curent}", cellnum);
                        }
                    }

                }
                Thread.Sleep(3000);
            }
        }
    }

}

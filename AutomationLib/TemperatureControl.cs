﻿using Advantech.Adam;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace AutomationLib
{
   
    public class TemperatureControl
    {
        private bool m_bStart;
        private AdamSocket adamModbus, adamUDP;
        private Adam6000Type m_Adam6000Type;
        private string m_szIP;
        private string m_szFwVersion;
        const int m_Adam6000NewerFwVer = 5;
        private int m_DeviceFwVer = 4;
        private int m_iPort;
        private int m_iCount;
        private int m_iAiTotal, m_iDoTotal;
        private bool[] m_bChEnabled;
        private byte[] m_byRange;
        private ushort[] m_usRange; //for newer version
        private DispatcherTimer timer1;
        public Task TempTask;
        bool StartReading = false;
        public void Init()
        {
            m_bStart = false;           // the action stops at the beginning
                                        //m_szIP = "172.18.3.222";	// modbus slave IP address
            m_iPort = 502;				// modbus TCP port is 502
            m_szIP = "10.0.0.1";	// modbus slave IP address
            //m_iPort = 80;				// modbus TCP port is 502
            adamModbus = new AdamSocket();
            adamModbus.SetTimeout(1000, 1000, 1000); // set timeout for TCP
            adamUDP = new AdamSocket();
            adamUDP.SetTimeout(1000, 1000, 1000); // set timeout for UDP

            m_Adam6000Type = Adam6000Type.Adam6015; // the sample is for ADAM-6015
            //m_Adam6000Type = Adam6000Type.Adam6017; // the sample is for ADAM-6017
            //m_Adam6000Type = Adam6000Type.Adam6018; // the sample is for ADAM-6018

            adamUDP.Connect(AdamType.Adam6000, m_szIP, ProtocolType.Udp);
            if (adamUDP.Configuration().GetFirmwareVer(out m_szFwVersion))
                m_DeviceFwVer = int.Parse(m_szFwVersion.Trim().Substring(0, 1));
            adamUDP.Disconnect();

            m_iAiTotal = AnalogInput.GetChannelTotal(m_Adam6000Type);
            m_iDoTotal = DigitalOutput.GetChannelTotal(m_Adam6000Type);

            

            m_bChEnabled = new bool[m_iAiTotal];

            if (m_DeviceFwVer < m_Adam6000NewerFwVer)
            {
                m_byRange = new byte[m_iAiTotal];
            }
            else
            {
                //for newer version Adam6017
                m_usRange = new ushort[m_iAiTotal];
            }

            // arrange channel text box

            if (m_Adam6000Type == Adam6000Type.Adam6015)
            {
                // Channel
            
            }
            else if (m_Adam6000Type == Adam6000Type.Adam6017)
            {
                // DO
              
            }
            else  //Adam6018
            {
                ;
            }
        }

        public void ReadOff()
        {
            if (StartReading==true)
            {
                StartReading = false;
                Thread.Sleep(100);//wait 
                TempTask = null;
            }
            adamUDP.Disconnect();
            adamModbus.Disconnect(); // disconnect slave
            MyConfig.Cfg.Model.TempStat = "Stop Reading";
        }

        public void ReadOn()
        {
            if(StartReading == false)
            {
                StartReading = true;

                if (m_bStart) // was started
                {
                    m_bStart = false;       // starting flag
                    adamModbus.Disconnect(); // disconnect slave
                }
                else    // was stoped
                {
                    if (adamModbus.Connect(m_szIP, ProtocolType.Tcp, m_iPort))
                    {
                        m_iCount = 0; // reset the reading counter
                        m_bStart = true; // starting flag

                        if (m_DeviceFwVer < m_Adam6000NewerFwVer)
                        {
                            if (m_Adam6000Type == Adam6000Type.Adam6017 ||
                                m_Adam6000Type == Adam6000Type.Adam6018)
                                RefreshChannelRangeByteFormat(7);
                            RefreshChannelRangeByteFormat(6);
                            RefreshChannelRangeByteFormat(5);
                            RefreshChannelRangeByteFormat(4);
                            RefreshChannelRangeByteFormat(3);
                            RefreshChannelRangeByteFormat(2);
                            RefreshChannelRangeByteFormat(1);
                            RefreshChannelRangeByteFormat(0);
                        }
                        else
                        {
                            //for newer version
                            if (m_Adam6000Type == Adam6000Type.Adam6017 ||
                                m_Adam6000Type == Adam6000Type.Adam6018)
                                RefreshChannelRangeUshortFormat(7);
                            RefreshChannelRangeUshortFormat(6);
                            RefreshChannelRangeUshortFormat(5);
                            RefreshChannelRangeUshortFormat(4);
                            RefreshChannelRangeUshortFormat(3);
                            RefreshChannelRangeUshortFormat(2);
                            RefreshChannelRangeUshortFormat(1);
                            RefreshChannelRangeUshortFormat(0);
                        }

                        RefreshChannelEnabled();
                    }

                }

                    TempTask = Task.Run(() =>
                    {
                        while (StartReading)
                        {
                            RefreshChannelValueUshortFormat();
                            Thread.Sleep(500);
                        }


                    });

            }

        }

        private void RefreshChannelRangeByteFormat(int i_iChannel)
        {
            byte byRange;
            if (adamModbus.AnalogInput().GetInputRange(i_iChannel, out byRange))
                m_byRange[i_iChannel] = byRange;
           
        }

        private void RefreshChannelRangeUshortFormat(int i_iChannel)
        {
            //for newer version
            ushort usRange;
            if (adamModbus.AnalogInput().GetInputRange(i_iChannel, out usRange))
                m_usRange[i_iChannel] = usRange;
            
                
        }

        private void RefreshChannelEnabled()
        {
            bool[] bEnabled;

            if (adamModbus.AnalogInput().GetChannelEnabled(m_iAiTotal, out bEnabled))
            {
                Array.Copy(bEnabled, 0, m_bChEnabled, 0, m_iAiTotal);
            }
          
        }


        

        private void RefreshChannelValueUshortFormat()
        {
            int iStart = 1, iAiStatusStart = 101;
            int iIdx;
            int[] iData, iAiStatus;
            float[] fValue = new float[m_iAiTotal];
            string val = "";
            if (adamModbus.Modbus().ReadInputRegs(iStart, m_iAiTotal, out iData))
            {
                for (iIdx = 0; iIdx < 1; iIdx++)
                {
                    fValue[iIdx] = AnalogInput.GetScaledValue(m_Adam6000Type, m_usRange[iIdx], (ushort)iData[iIdx]);
                }

                if (adamModbus.Modbus().ReadInputRegs(iAiStatusStart, (m_iAiTotal * 2), out iAiStatus))
                {
                    RefreshSingleChannelWithAiStatus(0, ref val, fValue[0], (ushort)iAiStatus[(0 * 2)]);

                    //var val2 = double.Parse(val) + MyConfig.Cfg.TemperatureOffset;
                    //MyConfig.Cfg.Model.TempStat = val2.ToString();

                    if ((m_Adam6000Type == Adam6000Type.Adam6017) || (m_Adam6000Type == Adam6000Type.Adam6018))
                    {
                        RefreshSingleChannelWithAiStatus(7, ref val, fValue[7], (ushort)iAiStatus[(7 * 2)]);
                        
                    }
                }


                MyConfig.Cfg.Model.TempStat = (fValue[0] + MyConfig.Cfg.TemperatureOffset).ToString("##.##");
            }
        }

        private void CheckTemperatureValue()
        {
            var temperature = double.Parse(MyConfig.Cfg.Model.TempStat);
            
            if (temperature == 0.0)
            {
                MyConfig.Cfg.UpdateUI("lblHeater", "ChangeColor", "Red");
            }
            else if (temperature > 56.0)
            {
                MyConfig.Cfg.Model.ErrorCode = "Alert,bath temperature is too high";
                MyConfig.Cfg.UpdateUI("lblHeater", "ChangeColor", "Red");
            }
            else if (temperature > 55.0)
            {
                MyConfig.Cfg.UpdateUI("lblHeater", "ChangeColor", "Yellow");
            }
            else if (temperature < 52.0)
            {
                MyConfig.Cfg.Model.ErrorCode = "Alert,bath temperature is too low";

            }
            else if (temperature < 53.0)
            {
                MyConfig.Cfg.UpdateUI("lblHeater", "ChangeColor", "Yellow");
            }
            else
            {
                MyConfig.Cfg.UpdateUI("lblHeater", "ChangeColor", "Green");
            }

        }

        private void RefreshSingleChannelWithAiStatus(int i_iIndex, ref string i_txtCh, float i_fValue, ushort i_usAiStatus)
        {
            string szFormat;
            string szRange;
            string szErrorMsg = string.Empty;
            string szComma = " , ";
            szRange = AnalogInput.GetRangeName(m_Adam6000Type, m_usRange[i_iIndex]);

            if (m_bChEnabled[i_iIndex])
            {
                if (i_usAiStatus == (ushort)Adam_AiStatus_LowAddress.No_Fault_Detected)
                {
                    szErrorMsg = string.Empty;
                    szFormat = AnalogInput.GetFloatFormat(m_Adam6000Type, m_usRange[i_iIndex]);
                    i_txtCh = i_fValue.ToString(szFormat) + " " + AnalogInput.GetUnitName(m_Adam6000Type, m_usRange[i_iIndex]);
                }
                else
                {
                    if ((i_usAiStatus & (ushort)Adam_AiStatus_LowAddress.FailToProvideAiValueUartTimeout) == (ushort)Adam_AiStatus_LowAddress.FailToProvideAiValueUartTimeout)
                    {
                        szErrorMsg = "Fail to provide AI value (UART timeout)";
                    }
                    else if ((i_usAiStatus & (ushort)Adam_AiStatus_LowAddress.Over_Range) == (ushort)Adam_AiStatus_LowAddress.Over_Range)
                    {
                        szErrorMsg = (szErrorMsg == string.Empty) ? ("Over Range") : (szErrorMsg + szComma + "Over Range");
                    }
                    else if ((i_usAiStatus & (ushort)Adam_AiStatus_LowAddress.Under_Range) == (ushort)Adam_AiStatus_LowAddress.Under_Range)
                    {
                        szErrorMsg = (szErrorMsg == string.Empty) ? ("Under Range") : (szErrorMsg + szComma + "Under Range");
                    }
                    else if ((i_usAiStatus & (ushort)Adam_AiStatus_LowAddress.Open_Circuit_Burnout) == (ushort)Adam_AiStatus_LowAddress.Open_Circuit_Burnout)
                    {
                        szErrorMsg = (szErrorMsg == string.Empty) ? ("Open Circuit (Burnout)") : (szErrorMsg + szComma + "Open Circuit (Burnout)");
                    }
                    else if ((i_usAiStatus & (ushort)Adam_AiStatus_LowAddress.Zero_Span_CalibrationError) == (ushort)Adam_AiStatus_LowAddress.Zero_Span_CalibrationError)
                    {
                        szErrorMsg = (szErrorMsg == string.Empty) ? ("Zero/Span Calibration Error") : (szErrorMsg + szComma + "Zero/Span Calibration Error");
                    }
                    i_txtCh = szErrorMsg;
                }
            }
        }

        private void RefreshChannelValueByteFormat()
        {
            int iStart = 1, iBurnStart = 121;
            int iIdx;
            int[] iData;
            float[] fValue = new float[m_iAiTotal];
            bool[] bBurn = new bool[m_iAiTotal];
            string val = "";
            if (adamModbus.Modbus().ReadInputRegs(iStart, m_iAiTotal, out iData))
            {
                for (iIdx = 0; iIdx < m_iAiTotal; iIdx++)
                    fValue[iIdx] = AnalogInput.GetScaledValue(m_Adam6000Type, m_byRange[iIdx], iData[iIdx]);

                if (m_Adam6000Type == Adam6000Type.Adam6015)
                {
                    if (adamModbus.Modbus().ReadCoilStatus(iBurnStart, m_iAiTotal, out bBurn)) // read burn out flag
                    {
                        RefreshSingleChannelBurn(0, ref val, fValue[0], bBurn[0]);
                        MyConfig.Cfg.Model.TempStat = val;
                    }
                }
                else
                {
                    RefreshSingleChannel(0, ref val, fValue[0]);
                    MyConfig.Cfg.Model.TempStat = val;
                }
            }
        }

        private void RefreshSingleChannel(int i_iIndex, ref string txtCh, float fValue)
        {
            string szFormat;

            if (m_bChEnabled[i_iIndex])
            {
                szFormat = AnalogInput.GetFloatFormat(m_Adam6000Type, m_byRange[i_iIndex]);
                txtCh = fValue.ToString(szFormat) + " " + AnalogInput.GetUnitName(m_Adam6000Type, m_byRange[i_iIndex]);
            }
        }

        private void RefreshSingleChannelBurn(int i_iIndex, ref string i_txtCh, float i_fValue, bool i_bBurn)
        {
            string szFormat;

            if (m_bChEnabled[i_iIndex])
            {
                if (i_bBurn)
                    i_txtCh = "Burn out";
                else
                {
                    szFormat = AnalogInput.GetFloatFormat(m_Adam6000Type, m_byRange[i_iIndex]);
                    i_txtCh = i_fValue.ToString(szFormat) + " " + AnalogInput.GetUnitName(m_Adam6000Type, m_byRange[i_iIndex]);
                }
            }
        }

     
    }
}

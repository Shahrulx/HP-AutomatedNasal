﻿using HardwareLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AutomationLib
{
    public class TankModule
    {
        Hardware hw;
        Events ev;
        public bool TankON = false;
        public bool refillIOQA = false;
        public bool refillIOQB = false;
        public bool IOQAFilling = false;
        public bool IOQBFilling = false;

        bool TaskIOQARuning = false;
        bool TaskIOQBRuning = false;
        bool TaskMandrelRuning = false;

        bool TaskMandrelFilling = false;
        bool TaskMandrelRefill = false;
        bool TaskMandrelDrain = false;


        bool TaskBackerFilling = false;
        bool TaskBackerRefill = false;
        bool TaskBackerDrain = false;

        bool TaskIOQBRefill = false;
        bool TaskIOQBFilling = false;
        bool TaskIOQBDrain = false;
        
        bool TaskIOQARefill = false;
        bool TaskIOQAFilling = false;
        bool TaskIOQADrain = false;

        public TankModule(Hardware hw, Events ev)
        {
            this.hw = hw;
            this.ev = ev;
            
        }


        public void MoveIOAB(string HwName, string param = "")
        {
            switch (HwName)
            {
                case "IOAFront":
                    if (MyConfig.Cfg.SimulationMode)
                        return ;

                    if (hw.inputs["X5_04_I_O_OUEUE_LID_DOWN"].Read() != 1)
                    {
                        MyConfig.Cfg.Model.ErrorCode = "Alert, IOA Lid Not Closed";
                        return;
                    }

                    hw.outputs["Y2_08_I_O_OUEUE_(A)_MOVE_FORWARD"].ON();
                    hw.outputs["Y2_09_I_O_OUEUE_(A)_MOVE_BACKWARD"].OFF();
                    Thread.Sleep(500);
                    MyConfig.Cfg.Model.IOQAStat = "REVERSE";
                    if (TimeOut(10, "X4_12_I_O_OUEUE_(A)_REV_POSITION", "IOA Move Forward Error") == 0)
                        MyConfig.Cfg.Model.IOQAStat = "REVERSE";
                    else
                        MyConfig.Cfg.Model.IOQAStat = "ERROR";

                    hw.outputs["Y2_08_I_O_OUEUE_(A)_MOVE_FORWARD"].OFF();
                    hw.outputs["Y2_09_I_O_OUEUE_(A)_MOVE_BACKWARD"].OFF();
                    hw.outputs["Y3_08_LOCK_I_O_OUEUE_(A)_LID"].ON();

                    break;


                case "IOBFront":
                    if (MyConfig.Cfg.SimulationMode)
                        return;

                    if (hw.inputs["X5_05_I_O_OUEUE_LID_DOWN"].Read() != 1)
                    {
                        MyConfig.Cfg.Model.ErrorCode = "Alert, IOB Lid Not Closed";
                        return;
                    }
                    if (hw.inputs["X3_14_ROBOT_Z_AXIS_UP"].Read() != 1)
                    {
                        MyConfig.Cfg.Model.ErrorCode = "Alert, IOBFront_Robot Z not in up position";
                        return;
                    }

                    hw.outputs["Y2_10_I_O_OUEUE_(B)_MOVE_FORWARD"].ON();
                    hw.outputs["Y2_11_I_O_OUEUE_(B)_MOVE_BACKWARD"].OFF();
                    Thread.Sleep(500);
                    MyConfig.Cfg.Model.IOQBStat = "REVERSE";
                    if (TimeOut(15, "X4_14_I_O_OUEUE_(B)_REV_POSITION", "IOB Move Forward Error") == 0)
                        MyConfig.Cfg.Model.IOQBStat = "REVERSE";
                    else
                        MyConfig.Cfg.Model.IOQBStat = "ERROR";

                    hw.outputs["Y2_10_I_O_OUEUE_(B)_MOVE_FORWARD"].OFF();
                    hw.outputs["Y2_11_I_O_OUEUE_(B)_MOVE_BACKWARD"].OFF();
                    hw.outputs["Y3_07_LOCK_I_O_OUEUE_(B)_LID"].ON();
                    break;

                case "IOABack":
                    hw.outputs["Y2_08_I_O_OUEUE_(A)_MOVE_FORWARD"].OFF();
                    hw.outputs["Y2_09_I_O_OUEUE_(A)_MOVE_BACKWARD"].ON();
                    Thread.Sleep(500);
                    MyConfig.Cfg.Model.IOQAStat = "FORWARD";
                    if (TimeOut(15, "X4_11_I_O_OUEUE_(A)_FWD_POSITION", "IOA Move Back Error") == 0)
                        MyConfig.Cfg.Model.IOQAStat = "FORWARD";
                    else
                        MyConfig.Cfg.Model.IOQAStat = "ERROR";

                    hw.outputs["Y2_08_I_O_OUEUE_(A)_MOVE_FORWARD"].OFF();
                    hw.outputs["Y2_09_I_O_OUEUE_(A)_MOVE_BACKWARD"].OFF();
                    hw.outputs["Y3_08_LOCK_I_O_OUEUE_(A)_LID"].OFF();
                    break;

                case "IOBBack":
                    //X3_14_ROBOT_Z_AXIS_UP
                    while(hw.inputs["X3_14_ROBOT_Z_AXIS_UP"].Read() == 0)
                    {
                        MyConfig.Cfg.Model.ErrorCode = "Alert, IOBBack_Robot Z not in up position";
                        Thread.Sleep(500);
                    }
                    hw.outputs["Y2_10_I_O_OUEUE_(B)_MOVE_FORWARD"].OFF();
                    hw.outputs["Y2_11_I_O_OUEUE_(B)_MOVE_BACKWARD"].ON();
                    Thread.Sleep(500);
                    MyConfig.Cfg.Model.IOQBStat = "FORWARD";
                    if (TimeOut(15, "X4_13_I_O_OUEUE_(B)_FWD_POSITION", "IOB Move Back Error") == 0)
                        MyConfig.Cfg.Model.IOQBStat = "FORWARD";
                    else
                        MyConfig.Cfg.Model.IOQBStat = "ERROR";

                    hw.outputs["Y2_10_I_O_OUEUE_(B)_MOVE_FORWARD"].OFF();
                    hw.outputs["Y2_11_I_O_OUEUE_(B)_MOVE_BACKWARD"].OFF();
                    hw.outputs["Y3_07_LOCK_I_O_OUEUE_(B)_LID"].OFF();
                    break;
                 
                default:
                    break;
            }

        }

      
        public int IOQBMove(string Option)
        {
            //1 error 0 no error
            int stat = 0;
            switch (Option)
            {
                case "CurrenPosition":
                    if (TimeOut(3, "X4_14_I_O_OUEUE_(B)_REV_POSITION", "Please Move IOQB") == 0)
                    {
                        stat = 0;
                    }
                    else
                    {
                        return 1;
                    }
                    break;

                case "IOBFront": //Move IOQB to robot unload position
                    if (MyConfig.Cfg.SimulationMode)
                        return 0;

                    if (hw.inputs["X5_05_I_O_OUEUE_LID_DOWN"].Read() != 1)
                    {
                        MyConfig.Cfg.Model.ErrorCode = "Alert, IOB Lid Not Closed";
                        return 1;
                    }
                    if (hw.inputs["X3_14_ROBOT_Z_AXIS_UP"].Read() != 1)
                    {
                        MyConfig.Cfg.Model.ErrorCode = "Alert, IOBFront_Robot Z not in up position";
                        return 1;
                    }
                    hw.outputs["Y2_10_I_O_OUEUE_(B)_MOVE_FORWARD"].ON();
                    hw.outputs["Y2_11_I_O_OUEUE_(B)_MOVE_BACKWARD"].OFF();
                    Thread.Sleep(500);
                    MyConfig.Cfg.Model.IOQBStat = "REVERSE";
                    if (TimeOut(10, "X4_14_I_O_OUEUE_(B)_REV_POSITION", "IOB Move Forward Error") == 0)
                    {
                        MyConfig.Cfg.Model.IOQBStat = "REVERSE";
                    }
                    else
                    {
                        MyConfig.Cfg.Model.IOQBStat = "ERROR";
                        return 1;
                    }
                       
                    hw.outputs["Y2_10_I_O_OUEUE_(B)_MOVE_FORWARD"].OFF();
                    hw.outputs["Y2_11_I_O_OUEUE_(B)_MOVE_BACKWARD"].OFF();
                    hw.outputs["Y3_07_LOCK_I_O_OUEUE_(B)_LID"].ON();
                    stat = 0;
                    break;

                case "IOBBack": //Move IOQB to user unload position
                    //X3_14_ROBOT_Z_AXIS_UP
                    while (hw.inputs["X3_14_ROBOT_Z_AXIS_UP"].Read() != 1)
                    {
                        MyConfig.Cfg.Model.ErrorCode = "Alert, IOBBack_Robot Z not in up position";
                        Thread.Sleep(500);
                    }
                    hw.outputs["Y2_10_I_O_OUEUE_(B)_MOVE_FORWARD"].OFF();
                    hw.outputs["Y2_11_I_O_OUEUE_(B)_MOVE_BACKWARD"].ON();
                    Thread.Sleep(500);
                    MyConfig.Cfg.Model.IOQBStat = "FORWARD";
                    if (TimeOut(15, "X4_13_I_O_OUEUE_(B)_FWD_POSITION", "IOB Move Back Error") == 0)
                        MyConfig.Cfg.Model.IOQBStat = "FORWARD";
                    else
                        MyConfig.Cfg.Model.IOQBStat = "ERROR";

                    hw.outputs["Y2_10_I_O_OUEUE_(B)_MOVE_FORWARD"].OFF();
                    hw.outputs["Y2_11_I_O_OUEUE_(B)_MOVE_BACKWARD"].OFF();
                    hw.outputs["Y3_07_LOCK_I_O_OUEUE_(B)_LID"].OFF();
                    break;

                case "IOBSensor":
                    int count = 0;
                    if (MyConfig.Cfg.SimulationMode || !MyConfig.Cfg.Model.IOBSensorEnable)
                        return 0;

                    for (int i = 1; i <= 6; i++)
                    {
                        MyConfig.Cfg.UpdateUI($"cboxIOB{i}", "GetValue", "");
                        var val = MyConfig.Cfg.UIValue;

                        if (val == "True")
                            count++;
                    }

                    if (MyConfig.Cfg.CurrentSet == "A" || MyConfig.Cfg.CurrentSet == "C")
                    {
                        if (count != MyConfig.Cfg.EnabledCellinBank1)
                        {
                            //MyConfig.Cfg.Model.ErrorCode = "Error -Enabled Cell Does Not Match";
                            stat = 1;
                        }
                    }
                    else
                    {
                        if (count != MyConfig.Cfg.EnabledCellinBank2)
                        {
                            //MyConfig.Cfg.Model.ErrorCode = "Error -Enabled Cell Does Not Match";
                            stat = 1;
                        }
                           
                    }
                    break;

                case "IOAFront":
                    if (MyConfig.Cfg.SimulationMode)
                        return 0;

                    if (hw.inputs["X5_04_I_O_OUEUE_LID_DOWN"].Read() != 1)
                    {
                        MyConfig.Cfg.Model.ErrorCode = "Alert, IOA Lid Not Closed";
                        return 1;
                    }

                    hw.outputs["Y2_08_I_O_OUEUE_(A)_MOVE_FORWARD"].ON();
                    hw.outputs["Y2_09_I_O_OUEUE_(A)_MOVE_BACKWARD"].OFF();
                    Thread.Sleep(500);
                    MyConfig.Cfg.Model.IOQAStat = "REVERSE";
                    if (TimeOut(10, "X4_12_I_O_OUEUE_(A)_REV_POSITION", "IOA Move Forward Error") == 0)
                        MyConfig.Cfg.Model.IOQAStat = "REVERSE";
                    else
                        MyConfig.Cfg.Model.IOQAStat = "ERROR";

                    hw.outputs["Y2_08_I_O_OUEUE_(A)_MOVE_FORWARD"].OFF();
                    hw.outputs["Y2_09_I_O_OUEUE_(A)_MOVE_BACKWARD"].OFF();
                    hw.outputs["Y3_08_LOCK_I_O_OUEUE_(A)_LID"].ON();
                    break;

                case "IOABack":
                    hw.outputs["Y2_08_I_O_OUEUE_(A)_MOVE_FORWARD"].OFF();
                    hw.outputs["Y2_09_I_O_OUEUE_(A)_MOVE_BACKWARD"].ON();
                    Thread.Sleep(500);
                    MyConfig.Cfg.Model.IOQAStat = "FORWARD";
                    if (TimeOut(15, "X4_11_I_O_OUEUE_(A)_FWD_POSITION", "IOA Move Back Error") == 0)
                        MyConfig.Cfg.Model.IOQAStat = "FORWARD";
                    else
                        MyConfig.Cfg.Model.IOQAStat = "ERROR";

                    hw.outputs["Y2_08_I_O_OUEUE_(A)_MOVE_FORWARD"].OFF();
                    hw.outputs["Y2_09_I_O_OUEUE_(A)_MOVE_BACKWARD"].OFF();
                    hw.outputs["Y3_08_LOCK_I_O_OUEUE_(A)_LID"].OFF();
                    break;

                default:
                    break;
            }

            return stat;
        }

        #region waterfill
        public void Backer(string Option)
        {
            switch (Option)
            {
                case "Fill":

                    if (TaskBackerFilling)
                        return;

                    MyConfig.Cfg.Model.BackerStat = "FILLING";
                    //close
                    hw.outputs["Y2_06_BACKER_RINSE_ODR_OPEN"].OFF();
                    hw.outputs["Y2_07_BACKER_RINSE_ODR_CLOSE"].ON();

                    //start fill
                    hw.outputs["Y1_05_BACKER_RINSE_DI_FILL"].ON();
                    //start filing time
                    //tmerstart
                    Task.Run(() =>
                    {
                        TaskBackerFilling = true;
                        WaitTimer(MyConfig.Cfg.FillDelayBacker);
                        hw.outputs["Y1_05_BACKER_RINSE_DI_FILL"].OFF();

                        hw.outputs["Y2_06_BACKER_RINSE_ODR_OPEN"].OFF();
                        hw.outputs["Y2_07_BACKER_RINSE_ODR_CLOSE"].OFF();

                        MyConfig.Cfg.Model.BackerStat = "FILLED";
                        TaskBackerFilling = false;
                    });

                    //dsiabel this button and enable drain
                    break;

                case "Cancel":

                    MyConfig.Cfg.TankFillCancel = true;
                    MyConfig.Cfg.Model.BackerStat = "FILLING CANCEL";
                    hw.outputs["Y1_05_BACKER_RINSE_DI_FILL"].OFF();
                    break;

                case "Drain":

                    if (TaskBackerDrain)
                        return;

                    MyConfig.Cfg.Model.BackerStat = "DRAIN";
                    //off fill
                    hw.outputs["Y1_05_BACKER_RINSE_DI_FILL"].OFF();
                    //open
                    hw.outputs["Y2_06_BACKER_RINSE_ODR_OPEN"].ON();
                    hw.outputs["Y2_07_BACKER_RINSE_ODR_CLOSE"].OFF();

                    //wait until finish drain
                    Task.Run(() =>
                    {
                        TaskBackerDrain = true;
                        WaitTimer(MyConfig.Cfg.DrainDelayBacker);
                        hw.outputs["Y2_06_BACKER_RINSE_ODR_OPEN"].OFF();
                        hw.outputs["Y2_07_BACKER_RINSE_ODR_CLOSE"].OFF();

                        MyConfig.Cfg.Model.BackerStat = "EMPTY";
                        TaskBackerDrain = false;
                        
                    });

                    break;

                case "ReFill":

                    if (TaskBackerRefill)
                        return;

                    TaskBackerRefill = true;
                    MyConfig.Cfg.Model.BackerStat = "DRAIN";
                    //off fill
                    hw.outputs["Y1_05_BACKER_RINSE_DI_FILL"].OFF();
                    //open
                    hw.outputs["Y2_06_BACKER_RINSE_ODR_OPEN"].ON();
                    hw.outputs["Y2_07_BACKER_RINSE_ODR_CLOSE"].OFF();

  
                    WaitTimer(MyConfig.Cfg.DrainDelayBacker);
                    hw.outputs["Y2_06_BACKER_RINSE_ODR_OPEN"].OFF();
                    hw.outputs["Y2_07_BACKER_RINSE_ODR_CLOSE"].OFF();

                    MyConfig.Cfg.Model.BackerStat = "EMPTY";
                    MyConfig.Cfg.Model.BackerStat = "FILLING";
                    //close
                    hw.outputs["Y2_06_BACKER_RINSE_ODR_OPEN"].OFF();
                    hw.outputs["Y2_07_BACKER_RINSE_ODR_CLOSE"].ON();

                    //start fill
                    hw.outputs["Y1_05_BACKER_RINSE_DI_FILL"].ON();
                    //start filing time
                    //tmerstart
                    WaitTimer(MyConfig.Cfg.FillDelayBacker);
                    hw.outputs["Y1_05_BACKER_RINSE_DI_FILL"].OFF();

                    hw.outputs["Y2_06_BACKER_RINSE_ODR_OPEN"].OFF();
                    hw.outputs["Y2_07_BACKER_RINSE_ODR_CLOSE"].OFF();

                    MyConfig.Cfg.Model.BackerStat = "FILLED";
                    TaskBackerRefill = false;
                    break;
            }
        }

        public void Mandrel(string Option)
        {
            switch (Option)
            {
                case "Fill":

                    if (TaskMandrelFilling)
                        return;

                    MyConfig.Cfg.Model.MandrelStat = "FILLING";
                    //close
                    hw.outputs["Y2_04_MANDREL_RINSE_ODR_OPEN"].OFF();
                    hw.outputs["Y2_05_MANDREL_RINSE_ODR_CLOSE"].ON();

                    //start fill
                    hw.outputs["Y1_04_MANDREL_RINSE_DI_FILL"].ON();
                    //start filing time
                    //tmerstart
                    Task.Run(() =>
                    {
                        TaskMandrelFilling = true;
                        WaitTimer(MyConfig.Cfg.FillDelayMandrel);
                        hw.outputs["Y1_04_MANDREL_RINSE_DI_FILL"].OFF();

                        hw.outputs["Y2_04_MANDREL_RINSE_ODR_OPEN"].OFF();
                        hw.outputs["Y2_05_MANDREL_RINSE_ODR_CLOSE"].OFF();

                        MyConfig.Cfg.Model.MandrelStat = "FILLED";
                        TaskMandrelFilling = false;
                    });
                    break;
                //dsiabel this button and enable drain
                case "Cancel":

                    MyConfig.Cfg.TankFillCancel = true;
                    MyConfig.Cfg.Model.MandrelStat = "FILLING CANCEL";
                    hw.outputs["Y1_04_MANDREL_RINSE_DI_FILL"].OFF();
                    break;
                case "Drain":

                    if (TaskMandrelDrain)
                        return;

                    MyConfig.Cfg.Model.MandrelStat = "DRAIN";
                    //off fill
                    hw.outputs["Y1_04_MANDREL_RINSE_DI_FILL"].OFF();
                    //open
                    hw.outputs["Y2_04_MANDREL_RINSE_ODR_OPEN"].ON();
                    hw.outputs["Y2_05_MANDREL_RINSE_ODR_CLOSE"].OFF();

                    //wait until finish drain
                    Task.Run(() =>
                    {
                        TaskMandrelDrain = true;
                        WaitTimer(MyConfig.Cfg.FillDelayMandrel);
                        hw.outputs["Y2_04_MANDREL_RINSE_ODR_OPEN"].OFF();
                        hw.outputs["Y2_05_MANDREL_RINSE_ODR_CLOSE"].OFF();

                        MyConfig.Cfg.Model.MandrelStat = "EMPTY";
                        TaskMandrelDrain = false;

                    });
                    break;

                case "ReFill":

                    if (TaskMandrelRefill)
                        return;

                    TaskMandrelRefill = true;
                    MyConfig.Cfg.Model.MandrelStat = "DRAIN";
                    //off fill
                    hw.outputs["Y1_04_MANDREL_RINSE_DI_FILL"].OFF();
                    //open
                    hw.outputs["Y2_04_MANDREL_RINSE_ODR_OPEN"].ON();
                    hw.outputs["Y2_05_MANDREL_RINSE_ODR_CLOSE"].OFF();

                   
                    WaitTimer(MyConfig.Cfg.DrainDelayMandrel);
                    hw.outputs["Y2_04_MANDREL_RINSE_ODR_OPEN"].OFF();
                    hw.outputs["Y2_05_MANDREL_RINSE_ODR_CLOSE"].OFF();

                    MyConfig.Cfg.Model.MandrelStat = "EMPTY";


                    MyConfig.Cfg.Model.MandrelStat = "FILLING";
                    //close
                    hw.outputs["Y2_04_MANDREL_RINSE_ODR_OPEN"].OFF();
                    hw.outputs["Y2_05_MANDREL_RINSE_ODR_CLOSE"].ON();

                    //start fill
                    hw.outputs["Y1_04_MANDREL_RINSE_DI_FILL"].ON();
                    //start filing time
       
                    WaitTimer(MyConfig.Cfg.FillDelayMandrel);
                    hw.outputs["Y1_04_MANDREL_RINSE_DI_FILL"].OFF();

                    hw.outputs["Y2_04_MANDREL_RINSE_ODR_OPEN"].OFF();
                    hw.outputs["Y2_05_MANDREL_RINSE_ODR_CLOSE"].OFF();

                    MyConfig.Cfg.Model.MandrelStat = "FILLED";
                    TaskMandrelRefill = false;
                    break;
            }
        }

        public void IOQA(string Option)
        {
            switch (Option)
            {
                case "Fill":
                    IOQAFill();
                    //dsiabel this button and enable drain
                    break;

                case "Cancel":

                    MyConfig.Cfg.TankFillCancel = true;
                    MyConfig.Cfg.Model.IOQAStat = "FILLING CANCEL";
                    hw.outputs["Y1_02_IO_OUEUE_(A)_DI_FILL"].OFF();
                    break;

                case "Drain":
                    IOQADrain(fillAfterDrain: false);
                    break;

                case "ReFill":
                    IOQADrain(fillAfterDrain:true);
                    break;
            }
        }

        private void IOQADrain(bool fillAfterDrain)
        {
            if (TaskIOQADrain)
                return;

            MyConfig.Cfg.Model.IOQAStat = "DRAIN";
            //off fill
            hw.outputs["Y1_02_IO_OUEUE_(A)_DI_FILL"].OFF();
            //open
            hw.outputs["Y2_00_I_O_OUEUE_(A)_ODR_OPEN"].ON();
            hw.outputs["Y2_01_I_O_OUEUE_(A)_ODR_CLOSE"].OFF();
         

            //wait until finish drain
            Task.Run(() =>
            {
                TaskIOQADrain = true;
                WaitTimer(MyConfig.Cfg.DrainDelayIOA);
                MyConfig.Cfg.Model.IOQAStat = "EMPTY";
                if (fillAfterDrain && !MyConfig.Cfg.TankFillCancel)
                {
                    IOQAFill();
                }
                TaskIOQADrain = false;
            });
        }

        private void IOQAFill()
        {
            if (TaskIOQAFilling)
                return;
         
            MyConfig.Cfg.Model.IOQAStat = "FILLING";
            //close
            hw.outputs["Y2_00_I_O_OUEUE_(A)_ODR_OPEN"].OFF();
            hw.outputs["Y2_01_I_O_OUEUE_(A)_ODR_CLOSE"].ON();
            //start fill
            hw.outputs["Y1_02_IO_OUEUE_(A)_DI_FILL"].ON();
            //start filing time
            //tmerstart
            Task.Run(() =>
            {
                TaskIOQAFilling = true;
                WaitTimerAlertA(MyConfig.Cfg.FillDelayIOA, "X4_03_I_O_OUEUE_(A)_HIGH_LEVEL", "Y1_02_IO_OUEUE_(A)_DI_FILL", "IOQA tank water not full");
                hw.outputs["Y1_02_IO_OUEUE_(A)_DI_FILL"].OFF();

                //hw.outputs["Y2_00_I_O_OUEUE_(A)_ODR_OPEN"].OFF();
                //hw.outputs["Y2_01_I_O_OUEUE_(A)_ODR_CLOSE"].OFF();

                MyConfig.Cfg.Model.IOQAStat = "FILLED";
                TaskIOQAFilling = false;
            });
        }

        public void IOQB(string Option)
        {
            switch (Option)
            {
                case "Fill":

                    if (TaskIOQBFilling)
                        return;

                    MyConfig.Cfg.Model.IOQBStat = "FILLING";

                    hw.outputs["Y2_02_I_O_OUEUE_(B)_ODR_OPEN"].OFF();
                    hw.outputs["Y2_03_I_O_OUEUE_(B)_ODR_CLOSE"].ON();

                    hw.outputs["Y1_03_IO_OUEUE_(B)_DI_FILL"].ON();
     
                    Task.Run(() =>
                    {
                        TaskIOQBFilling = true;
                        WaitTimerAlertB(MyConfig.Cfg.FillDelayIOB, "X4_04_I_O_OUEUE_(B)_HIGH_LEVEL", "Y1_03_IO_OUEUE_(B)_DI_FILL", "IOQB tank water not full");
                        hw.outputs["Y1_03_IO_OUEUE_(B)_DI_FILL"].OFF();

                        MyConfig.Cfg.Model.IOQBStat = "FILLED";
                        TaskIOQBFilling = false;
                    });

                    break;
                  
                case "Cancel":

                    MyConfig.Cfg.TankFillCancel = true;
                    MyConfig.Cfg.Model.IOQBStat = "Filling Cancel";
                    hw.outputs["Y1_03_IO_OUEUE_(B)_DI_FILL"].OFF();
                    break;

                case "Drain":

                    if (TaskIOQBDrain)
                        return;

                    MyConfig.Cfg.Model.IOQBStat = "DRAIN";

                    hw.outputs["Y1_03_IO_OUEUE_(B)_DI_FILL"].OFF();
                    //open
                    hw.outputs["Y2_02_I_O_OUEUE_(B)_ODR_OPEN"].ON();
                    hw.outputs["Y2_03_I_O_OUEUE_(B)_ODR_CLOSE"].OFF();


                    Task.Run(() =>
                    {
                        TaskIOQBDrain = true;
                        WaitTimer(MyConfig.Cfg.DrainDelayIOB);

                        MyConfig.Cfg.Model.IOQBStat = "EMPTY";
                        TaskIOQBDrain = false;
                    });

                    break;

                case "ReFill":

                    if (TaskIOQBRefill)
                        return;

                    Task.Run(() =>
                    {
                        TaskIOQBRefill = true;
                        MyConfig.Cfg.Model.IOQBStat = "DRAIN";
                        //drain
                        hw.outputs["Y2_02_I_O_OUEUE_(B)_ODR_OPEN"].ON();
                        hw.outputs["Y2_03_I_O_OUEUE_(B)_ODR_CLOSE"].OFF();
                        hw.outputs["Y1_03_IO_OUEUE_(B)_DI_FILL"].OFF();
                        WaitTimer(MyConfig.Cfg.DrainDelayIOB);
                        //start fill
                        hw.outputs["Y1_03_IO_OUEUE_(B)_DI_FILL"].ON();
                        hw.outputs["Y2_02_I_O_OUEUE_(B)_ODR_OPEN"].OFF();
                        hw.outputs["Y2_03_I_O_OUEUE_(B)_ODR_CLOSE"].ON();
                        //start filing time
                        WaitTimerAlertB(MyConfig.Cfg.FillDelayIOB, "X4_04_I_O_OUEUE_(B)_HIGH_LEVEL", "Y1_03_IO_OUEUE_(B)_DI_FILL", "IOQB tank water not full");
                        hw.outputs["Y1_03_IO_OUEUE_(B)_DI_FILL"].OFF();

                        MyConfig.Cfg.Model.IOQBStat = "FILLED";
                        TaskIOQBRefill = false;
                    });

                    break;
            }
        }
        #endregion

        #region LL L H HH


        public void FillToLow()
        {
            if (TankON == true)
                return;

            TankON = true;
            DateTime desired = DateTime.Now.AddSeconds(MyConfig.Cfg.FillToL);
            Task.Run(() =>
            {
                while (TankON)
                {
                    hw.outputs["Y1_00_MAIN_TANK_AUTO_DI_FILL___1"].ON();
                    hw.outputs["Y1_01_MAIN_TANK_AUTO_DI_FILL___2"].ON();

              
                    while (hw.inputs["X3_10_LOW_LEVEL"].Read() != 1)
                    {
                        if (TankON == false)
                            break;

                        if (DateTime.Now > desired)
                        {
                            break;
                        }

                        Thread.Sleep(200);
                    }
                    hw.outputs["Y1_00_MAIN_TANK_AUTO_DI_FILL___1"].OFF();
                    hw.outputs["Y1_01_MAIN_TANK_AUTO_DI_FILL___2"].OFF();

                   
                }
            });
        }


        public void FillToHigh()
        {
            if (TankON == true)
                return;

            TankON = true;
            DateTime desired = DateTime.Now.AddSeconds(MyConfig.Cfg.FillToH);
            Task.Run(() =>
            {
                while (TankON)
                {
                    hw.outputs["Y1_00_MAIN_TANK_AUTO_DI_FILL___1"].ON();
                    hw.outputs["Y1_01_MAIN_TANK_AUTO_DI_FILL___2"].ON();
                    while (hw.inputs["X3_12_HIGH_LEVEL"].Read() != 1)
                    {
                        if (TankON == false)
                            break;

                        if (DateTime.Now > desired)
                        {
                            break;
                        }

                        Thread.Sleep(200);
                    }
                    hw.outputs["Y1_00_MAIN_TANK_AUTO_DI_FILL___1"].OFF();
                    hw.outputs["Y1_01_MAIN_TANK_AUTO_DI_FILL___2"].OFF();
                }


            });

        }

        public void FillToF()
        {
            if (TankON == true)
                return;

            TankON = true;
            DateTime desired = DateTime.Now.AddSeconds(MyConfig.Cfg.FillToF);
            Task.Run(() =>
            {
                while (TankON)
                {
                    hw.outputs["Y1_00_MAIN_TANK_AUTO_DI_FILL___1"].ON();
                    hw.outputs["Y1_01_MAIN_TANK_AUTO_DI_FILL___2"].ON();
                    while (hw.inputs["X3_11_AUTO_FILL_LEVEL"].Read() != 1)
                    {
                        if (TankON == false)
                            break;

                        if (DateTime.Now > desired)
                        {
                            break;
                        }

                        Thread.Sleep(200);
                    }
                    hw.outputs["Y1_00_MAIN_TANK_AUTO_DI_FILL___1"].OFF();
                    hw.outputs["Y1_01_MAIN_TANK_AUTO_DI_FILL___2"].OFF();
                }


            });
        }

        public void FillToHH()
        {
            if (TankON == true)
                return;

            TankON = true;
            DateTime desired = DateTime.Now.AddSeconds(MyConfig.Cfg.FillToHH);
            Task.Run(() =>
            {
                while (TankON)
                {
                    hw.outputs["Y1_00_MAIN_TANK_AUTO_DI_FILL___1"].ON();
                    hw.outputs["Y1_01_MAIN_TANK_AUTO_DI_FILL___2"].ON();
                    while (hw.inputs["X3_13_HIGH_HIGH_LEVEL"].Read() != 1)
                    {
                        if (DateTime.Now > desired)
                        {
                            break;
                        }

                        if (TankON == false)
                            break;
                    }
                }
                hw.outputs["Y1_00_MAIN_TANK_AUTO_DI_FILL___1"].OFF();
                hw.outputs["Y1_01_MAIN_TANK_AUTO_DI_FILL___2"].OFF();

            });
        }

        public void MainStopFill()
        {
            TankON = false;
        }


        #endregion

        public void CycleBRT()
        {
            MyConfig.Cfg.Model.BRTPumpStat = "ON";
            Task.Run(() =>
            {
                for (int i = 0; i < MyConfig.Cfg.BrtStrokeCount; i++)
                {
                    hw.outputs["Y4_01_IWAKI_(BRIGHTNER_)"].ON();
                    Thread.Sleep(225);
                    hw.outputs["Y4_01_IWAKI_(BRIGHTNER_)"].OFF();
                    Thread.Sleep(225);
                }
                MyConfig.Cfg.Model.BRTPumpStat = "OFF";
            });
        }

        private int TimeOut(int sec, string SensorName, string Message, int ioval = 1)
        {
            int stat = 0;
            int count = 0;

            if (MyConfig.Cfg.SimulationMode)
                return stat;

            while (hw.inputs[SensorName].Read() != ioval)
            {
                Thread.Sleep(100);
                count++;
                if (count > (sec*10))
                {
                    MyConfig.Cfg.Model.ErrorCode = "Alert," + Message;
                    stat = 1;
                    break;
                }
            
     
                   
            }
            return stat;
        }

        private void WaitTimer(double second)
        {
            if (second < 1)
                return;

            DateTime desired = DateTime.Now.AddSeconds(second);
            while (DateTime.Now < desired)
            {
                if (MyConfig.Cfg.TankFillCancel)
                {
                    MyConfig.Cfg.TankFillCancel = false;
                    break;
                }

                Thread.Sleep(10);
            }
        }

        private void WaitTimerAlertA(double second, string sensor, string output, string message)
        {
            if (MyConfig.Cfg.SimulationMode)
                return;

            DateTime desired = DateTime.Now.AddSeconds(second);
            while (DateTime.Now < desired)
            {
                if (MyConfig.Cfg.TankFillCancel)
                {
                    MyConfig.Cfg.TankFillCancel = false;
                    break;
                }
                Thread.Sleep(100);
                if (hw.inputs[sensor].Read() == 1)
                {
                    Thread.Sleep(5000);
                    return;
                }
            }

            if (hw.inputs[sensor].Read() == 0)
            {
                IOQAFilling = true;
                IOQBFilling = true;
                MyConfig.Cfg.Model.ErrorCode = "Alert," + message;
                hw.outputs[output].OFF();
                while (MyConfig.Cfg.Model.AlarmStat != "OFF" && !refillIOQA)
                {
                    Thread.Sleep(100);
                }
                refillIOQA = false;
                refillIOQB = false;
                IOQAFilling = false;
                IOQBFilling = false;
                MyConfig.Cfg.Model.ResetAlarm();
                WaitTimerAlertA(second, sensor, output, message);
            }
        }

        private void WaitTimerAlertB(double second, string sensor, string output, string message)
        {
            if (MyConfig.Cfg.SimulationMode)
                return;

            DateTime desired = DateTime.Now.AddSeconds(second);
            while (DateTime.Now < desired)
            {
                if (MyConfig.Cfg.TankFillCancel)
                {
                    MyConfig.Cfg.TankFillCancel = false;
                    break;
                }
                Thread.Sleep(100);
                if (hw.inputs[sensor].Read() == 1)
                {
                    Thread.Sleep(5000);
                    return;
                }
            }

            if (hw.inputs[sensor].Read() == 0)
            {
                IOQAFilling = true;
                IOQBFilling = true;
                MyConfig.Cfg.Model.ErrorCode = "Alert," + message;
                hw.outputs[output].OFF();
                while (MyConfig.Cfg.Model.AlarmStat != "OFF" && !refillIOQB)
                {
                    Thread.Sleep(100);
                }
                refillIOQA = false;
                refillIOQB = false;
                IOQAFilling = false;
                IOQBFilling = false;
                MyConfig.Cfg.Model.ResetAlarm();
                WaitTimerAlertB(second, sensor, output, message);
            }
        }
    }
}

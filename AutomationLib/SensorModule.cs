﻿using HardwareLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AutomationLib
{
    public class SensorModule
    {
        Hardware hw;
        Events ev;
        MainFlow mainFlow;
        public bool StartFill { get; set; } = false;
        int prevEMO = 1;
        int prevRobotEMO = 0;
        int prevEFFORTOR = 0;
        int prevRobotError = 0;
        int prevRobotHandError = 0;
        int prevPlenumPMPStat = 0;
        int prevPlenumSRDStat = 0;
        int prevPlenumRNStat = 0;
        int prevPlenumQAStat = 0;
        int prevPlenumQBStat = 0;
        int prevPlenumDBLStat = 0;
        int prevAlarmSilence = 0;

        bool bathLevelLLAlarmed = false;
        bool bathLevelLAlarmed = false;
        bool bathLevelHAlarmed = false;
        bool bathLevelHHAlarmed = false;
        bool bathLevelLFilling = false;

        public SensorModule(MainFlow mainFlow, Hardware hw, Events ev)
        {
            this.hw = hw;
            this.ev = ev;
            this.mainFlow = mainFlow;
        }

        public void Initialize()
        {
            //ReadFIrstTime
            ReadTankSensor();
           // hw.outputs["Y3_00_MAIN_TANK_PUMP_(RUN)"].ON();
            hw.outputs["Y2_13_MAIN_WATER"].ON();
            hw.outputs["Y1_11_SVON"].ON();
        }

        public void StartSensorReading()
        {
            Task.Run(() =>
            {
                while (true)
                {
                    ReadBathSensor();
                    ReadIOSensor();
                    ReadCellSensor();
                    ReadOtherSensor();
                    ReadPlenumSensor();
                    StartRobotSensorReading();
                    Thread.Sleep(200);
                }
            });

        }


        private void StartRobotSensorReading()
        {
            if (prevRobotEMO != hw.inputs["X6_15_ROBOT_EMERGENCY_STOP"].Read())
            {
                if (MyConfig.Cfg.RobotRequestStop == false)
                {
                    if (MyConfig.Cfg.RobotSoftEMO == true)
                    {
                        MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Robot EMO Triggered By Error");
                    }
                    else
                    {
                        MyConfig.Cfg.Model.ErrorCode = "Alert,ROBOT_EMERGENCY_STOP";
                        MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Robot EMO Pressed");
                    }
                   
                   
                    MyConfig.Cfg.Model.RobotStat = "EMO"; 
                    MyConfig.Cfg.UpdateUI("btnStopRobot", "Disable", "");
                    MyConfig.Cfg.RobotRequestStop = true;
                    //yyy - robot EMO no need gripper off
                    //mainFlow.Robot.TriggerGripper(true);
                    hw.motors["Motor1X"].Stop();
                    //new

                }
                else
                {
                    MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Robot EMO Released");
                }
            }
            prevRobotEMO = hw.inputs["X6_15_ROBOT_EMERGENCY_STOP"].Read();


            if (prevRobotError != hw.inputs["X6_13_ROBOT_ROBOT_ERROR"].Read())
            {
                if (MyConfig.Cfg.RobotRequestStop == false)
                {
                    MyConfig.Cfg.Model.ErrorCode = "Alert, Robot Error";
                    MyConfig.Cfg.Model.RobotStat = "ERROR";
                    MyConfig.Cfg.RobotRequestStop = true;

                    //stop actuator
                    hw.motors["Motor1X"].Stop();
                    //stop root program          
                    hw.outputs["Y4_06_ROBOT_EMO_ON"].ON();
                    mainFlow.Robot.TriggerGripper(true);
                    MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Robot Program Stopped Caused by:Robot Error");
                }

            }
            prevRobotError = hw.inputs["X6_13_ROBOT_ROBOT_ERROR"].Read();


            if (prevRobotHandError != hw.inputs["X6_12_ROBOT_ROBOT_HAND_FULL"].Read())
            {
                if (MyConfig.Cfg.RobotRequestStop == false)
                {
                    MyConfig.Cfg.Model.ErrorCode = "Alert, Robot Error";
                    MyConfig.Cfg.Model.RobotStat = "ERROR";
                    MyConfig.Cfg.RobotRequestStop = true;

                    //stop actuator
                    hw.motors["Motor1X"].Stop();
                    //stop root program          
                    hw.outputs["Y4_06_ROBOT_EMO_ON"].ON();
                    mainFlow.Robot.TriggerGripper(true);
                    MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Robot Program Stopped Caused by:Robot No Plate");
                }

            }
            prevRobotHandError = hw.inputs["X6_12_ROBOT_ROBOT_HAND_FULL"].Read();


            if (prevEFFORTOR != hw.inputs["X4_06_ROBOT_END_EFFECTOR_ERROR"].Read())
            {
                if (MyConfig.Cfg.RobotRequestStop == false)
                {
                    MyConfig.Cfg.Model.ErrorCode = "Alert, ROBOT END_EFFECTOR ERROR";
                    MyConfig.Cfg.Model.RobotStat = "EFFORTOR";
                    MyConfig.Cfg.RobotRequestStop = true;
                    hw.motors["Motor1X"].Stop();
                    hw.outputs["Y4_06_ROBOT_EMO_ON"].ON();
                    mainFlow.Robot.TriggerGripper(true);
                    MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Robot Program Stopped Caused by:Robot END_EFFECTOR");
                }
            }
            prevEFFORTOR = hw.inputs["X4_06_ROBOT_END_EFFECTOR_ERROR"].Read();
        }

        private void ReadTankSensor()
        {
            if (hw.inputs["X4_12_I_O_OUEUE_(A)_REV_POSITION"].Read() == 1)
                MyConfig.Cfg.Model.IOQAStat = "Forward";
            else
                MyConfig.Cfg.Model.IOQAStat = "Reverse";

            if (hw.inputs["X4_14_I_O_OUEUE_(B)_REV_POSITION"].Read() == 1)
                MyConfig.Cfg.Model.IOQBStat = "Forward";
            else
                MyConfig.Cfg.Model.IOQBStat = "Reverse";
        }

        int prevResetValue = 0;
        int prevCurtainSensorValue = 1;

        private void ReadOtherSensor()
        {
            if (MyConfig.Cfg.Model.CurtainSensorCheck == false)
            {

            }
            else if (prevCurtainSensorValue == 1 && hw.inputs["X5_14_CURTAIN_SENSOR"].Read() == 1)
            {
                MyConfig.Cfg.Model.CurtainSensor = "OFF";
            }
            else if (prevCurtainSensorValue == 1 && hw.inputs["X5_14_CURTAIN_SENSOR"].Read() == 0)
            {
                if (MyConfig.Cfg.SimulationMode)
                    return;

                MyConfig.Cfg.Model.CurtainSensor = "ON";
                hw.outputs["Y5_03_ROBOT_PAUSE"].ON();
                MyConfig.Cfg.Model.RobotStat = "EMO";
                MyConfig.Cfg.RobotRequestStop = true;
                MyConfig.Cfg.Model.ErrorCode = "Alert, Curtain Sensor Triggered";
            }
            prevCurtainSensorValue = hw.inputs["X5_14_CURTAIN_SENSOR"].Read();



            if (prevResetValue == 0 && hw.inputs["X5_01_ALARM_RESET_(OWA)"].Read() == 1)
            {
               mainFlow.SetButton("Reset");
                MyConfig.Cfg.Model.ResetAlarm();
            }
            prevResetValue = hw.inputs["X5_01_ALARM_RESET_(OWA)"].Read();


            if (hw.inputs["X5_02_BYPASS_MODE"].Read() == 1)
            {
                MyConfig.Cfg.Model.BypassStat = "ON";
                hw.outputs["Y4_04_DOOR_UNLOCK"].ON();
                MyConfig.Cfg.Model.DoorLockStat = "UNLOCK";
            }
            else
            {
                MyConfig.Cfg.Model.BypassStat = "OFF";
                hw.outputs["Y4_04_DOOR_UNLOCK"].OFF();
                MyConfig.Cfg.Model.DoorLockStat = "LOCK";
            }

            if (hw.outputs["Y2_13_MAIN_WATER"].Read() == 1)
            {
                MyConfig.Cfg.Model.DiWaterStat = "ON";
            }

            else
            {
                MyConfig.Cfg.Model.DiWaterStat = "OFF";
            }

            //infalte
            if (hw.inputs["X0_00_PRESSURE_SENSOR_CELL___1"].Read() == 1)
            {
                MyConfig.Cfg.Model.BladderCell_1 = "INFLATE";
            }

            else
            {
                MyConfig.Cfg.Model.BladderCell_1 = "RETRACT";
            }

            if (hw.inputs["X0_02_PRESSURE_SENSOR_CELL___2"].Read() == 1)
            {
                MyConfig.Cfg.Model.BladderCell_2 = "INFLATE";
            }

            else
            {
                MyConfig.Cfg.Model.BladderCell_2 = "RETRACT";
            }

            if (hw.inputs["X0_04_PRESSURE_SENSOR_CELL___3"].Read() == 1)
            {
                MyConfig.Cfg.Model.BladderCell_3 = "INFLATE";
            }

            else
            {
                MyConfig.Cfg.Model.BladderCell_3 = "RETRACT";
            }


            if (hw.inputs["X0_06_PRESSURE_SENSOR_CELL___4"].Read() == 1)
            {
                MyConfig.Cfg.Model.BladderCell_4 = "INFLATE";
            }

            else
            {
                MyConfig.Cfg.Model.BladderCell_4 = "RETRACT";
            }

            if (hw.inputs["X0_08_PRESSURE_SENSOR_CELL___5"].Read() == 1)
            {
                MyConfig.Cfg.Model.BladderCell_5 = "INFLATE";
            }

            else
            {
                MyConfig.Cfg.Model.BladderCell_5 = "RETRACT";
            }


            if (hw.inputs["X0_10_PRESSURE_SENSOR_CELL___6"].Read() == 1)
            {
                MyConfig.Cfg.Model.BladderCell_6 = "INFLATE";
            }

            else
            {
                MyConfig.Cfg.Model.BladderCell_6 = "RETRACT";
            }

            if (hw.inputs["X0_12_PRESSURE_SENSOR_CELL___7"].Read() == 1)
            {
                MyConfig.Cfg.Model.BladderCell_7 = "INFLATE";
            }

            else
            {
                MyConfig.Cfg.Model.BladderCell_7 = "RETRACT";
            }

            if (hw.inputs["X0_14_PRESSURE_SENSOR_CELL___8"].Read() == 1)
            {
                MyConfig.Cfg.Model.BladderCell_8 = "INFLATE";
            }

            else
            {
                MyConfig.Cfg.Model.BladderCell_8 = "RETRACT";
            }

            if (hw.inputs["X1_00_PRESSURE_SENSOR_CELL___9"].Read() == 1)
            {
                MyConfig.Cfg.Model.BladderCell_9 = "INFLATE";
            }

            else
            {
                MyConfig.Cfg.Model.BladderCell_9 = "RETRACT";
            }

            if (hw.inputs["X1_02_PRESSURE_SENSOR_CELL___10"].Read() == 1)
            {
                MyConfig.Cfg.Model.BladderCell_10 = "INFLATE";
            }

            else
            {
                MyConfig.Cfg.Model.BladderCell_10 = "RETRACT";
            }

            if (hw.inputs["X1_04_PRESSURE_SENSOR_CELL___11"].Read() == 1)
            {
                MyConfig.Cfg.Model.BladderCell_11 = "INFLATE";
            }

            else
            {
                MyConfig.Cfg.Model.BladderCell_11 = "RETRACT";
            }

            if (hw.inputs["X1_06_PRESSURE_SENSOR_CELL___12"].Read() == 1)
            {
                MyConfig.Cfg.Model.BladderCell_12 = "INFLATE";
            }

            else 
            {
                MyConfig.Cfg.Model.BladderCell_12 = "RETRACT";
            }
        }

        private void ReadPlenumSensor()
        {
            if (prevPlenumPMPStat == 0 && hw.inputs["X4_05_PUMP_AREA_PLENUM_HIGH_LEVEL"].Read() == 1)
            {
                hw.outputs["Y2_13_MAIN_WATER"].OFF();
                hw.outputs["Y3_00_MAIN_TANK_PUMP_(RUN)"].OFF();
                
                MyConfig.Cfg.Model.ErrorCode = "Alert,Pump Area High";
                
            }
            if (hw.inputs["X4_05_PUMP_AREA_PLENUM_HIGH_LEVEL"].Read() == 1)
                MyConfig.Cfg.Model.PlenumPMPStat = "High Level";
            else
                MyConfig.Cfg.Model.PlenumPMPStat = "NORMAL";

            if (prevPlenumSRDStat == 0 && hw.inputs["X4_00_SRD_PLENUM_HIGH_LEVEL"].Read() == 1)
            {
                hw.outputs["Y2_13_MAIN_WATER"].OFF();
                MyConfig.Cfg.Model.ErrorCode = "Alert,Srd Plenum High";
                
            }

            if (hw.inputs["X4_00_SRD_PLENUM_HIGH_LEVEL"].Read() == 1)
                MyConfig.Cfg.Model.PlenumSRDStat = "High Level";
            else
                MyConfig.Cfg.Model.PlenumSRDStat = "NORMAL";

            if (prevPlenumRNStat == 0 && hw.inputs["X4_01_RINSE_PLENUM_HIGH_LEVEL"].Read() == 1)
            {
                hw.outputs["Y2_13_MAIN_WATER"].OFF();
                MyConfig.Cfg.Model.ErrorCode = "Alert,Rinse Plenum High";
                
            }
            
            if (hw.inputs["X4_01_RINSE_PLENUM_HIGH_LEVEL"].Read() == 1)
                MyConfig.Cfg.Model.PlenumRNStat = "High Level";
            else
                MyConfig.Cfg.Model.PlenumRNStat = "NORMAL";

            if (prevPlenumQAStat == 0 && hw.inputs["X4_02_I_O_PLENUM_HIGH_LEVEL_(A)"].Read() == 1)
            {
                hw.outputs["Y2_13_MAIN_WATER"].OFF();
                MyConfig.Cfg.Model.ErrorCode = "Alert,IO A Plenum High";
                MyConfig.Cfg.Model.PlenumQAStat = "High Level";
            }

            if (hw.inputs["X4_02_I_O_PLENUM_HIGH_LEVEL_(A)"].Read() == 1)
                MyConfig.Cfg.Model.PlenumQAStat = "High Level";
            else
                MyConfig.Cfg.Model.PlenumQAStat = "NORMAL";

            if (prevPlenumQBStat == 0 && hw.inputs["X4_15_I_O_PLENUM_HIGH_LEVEL_(B)"].Read() == 1)
            {
                hw.outputs["Y2_13_MAIN_WATER"].OFF();
                MyConfig.Cfg.Model.ErrorCode = "Alert,IO B Plenum High";
                MyConfig.Cfg.Model.PlenumQBStat = "High Level";
            }

            if (hw.inputs["X4_15_I_O_PLENUM_HIGH_LEVEL_(B)"].Read() == 1)
                MyConfig.Cfg.Model.PlenumQBStat = "High Level";
            else
                MyConfig.Cfg.Model.PlenumQBStat = "NORMAL";

            if (prevPlenumDBLStat == 0 && hw.inputs["X4_10_DOUBLE_CONTAIN_LEAKING"].Read() == 1)
            {
                hw.outputs["Y2_13_MAIN_WATER"].OFF();
                MyConfig.Cfg.Model.ErrorCode = "Alert,Double Contain Leaking";
                MyConfig.Cfg.Model.PlenumDBLStat = "Leaking";
            } 

            if (hw.inputs["X4_10_DOUBLE_CONTAIN_LEAKING"].Read() == 1)
                MyConfig.Cfg.Model.PlenumDBLStat = "Leaking";
            else
                MyConfig.Cfg.Model.PlenumDBLStat = "NORMAL";

            prevPlenumPMPStat = hw.inputs["X4_05_PUMP_AREA_PLENUM_HIGH_LEVEL"].Read();
            prevPlenumSRDStat = hw.inputs["X4_00_SRD_PLENUM_HIGH_LEVEL"].Read();
            prevPlenumRNStat = hw.inputs["X4_01_RINSE_PLENUM_HIGH_LEVEL"].Read();
            prevPlenumQAStat = hw.inputs["X4_02_I_O_PLENUM_HIGH_LEVEL_(A)"].Read();
            prevPlenumQBStat = hw.inputs["X4_15_I_O_PLENUM_HIGH_LEVEL_(B)"].Read();
            prevPlenumDBLStat = hw.inputs["X4_10_DOUBLE_CONTAIN_LEAKING"].Read();
          
        }

       
        private void ReadBathSensor()
        {
            string BathLevel = "Error";
            

            //LL
            if (hw.inputs["X4_07_ORGANICS_LOW_LEVEL"].Read() == 1)
            {
                MyConfig.Cfg.Model.BathLevelLL = true;
                BathLevel = "Very Low";
                bathLevelLLAlarmed = false;
                //   hw.outputs[]
            }
            else
            {
                MyConfig.Cfg.Model.BathLevelLL = false;
                if (bathLevelLLAlarmed == false)
                {
                    bathLevelLLAlarmed = true;
                    MyConfig.Cfg.Model.BathLevelLabel = "VERY LOW";
                    MyConfig.Cfg.Model.ErrorCode = "Alert,Bath Low Low";
                }
            }

            

            //L
            if (hw.inputs["X3_10_LOW_LEVEL"].Read() == 1)
            {
                MyConfig.Cfg.Model.BathLevelL = true;
                
                BathLevel = "Low";
                bathLevelLAlarmed = false;
            }
            else
            {
                MyConfig.Cfg.Model.BathLevelL = false;
                hw.outputs["Y3_01_HEATER_ENABLE"].OFF();
                hw.outputs["Y3_00_MAIN_TANK_PUMP_(RUN)"].OFF();
                
                if (bathLevelLAlarmed == false)
                {
                    bathLevelLAlarmed = true;
                    MyConfig.Cfg.Model.BathLevelLabel = "LOW";
                    MyConfig.Cfg.Model.ErrorCode = "Alert,Bath Level Low";
                }
            }

            DateTime StartIdleTime = DateTime.Now;
            TimeSpan IdleDuration;
            //F
            if (hw.inputs["X3_11_AUTO_FILL_LEVEL"].Read() == 1)
            {
                MyConfig.Cfg.Model.BathLevelF = true;
                BathLevel = "Fill";
            }
            else
            {

                MyConfig.Cfg.Model.BathLevelF = false;
            }


            if (MyConfig.Cfg.Model.BathLevelL && MyConfig.Cfg.Model.BathLevelLL && !bathLevelLFilling)
            {
              
                if (BathLevelOffForSomeTime())
                {
                    bathLevelLFilling = true;
                    hw.outputs["Y1_00_MAIN_TANK_AUTO_DI_FILL___1"].ON();
                    hw.outputs["Y1_01_MAIN_TANK_AUTO_DI_FILL___2"].ON();

                    DateTime desired = DateTime.Now.AddSeconds(MyConfig.Cfg.FillToF);
                  
                    Task.Run(() =>
                    {
                        while (hw.inputs["X3_11_AUTO_FILL_LEVEL"].Read() == 0)
                        {
                            MyConfig.Cfg.Model.BathLevelLabel = "FILLING";
                            Thread.Sleep(300);
                            if (DateTime.Now > desired)
                            {
                                break;
                            }
                        }
                        hw.outputs["Y1_00_MAIN_TANK_AUTO_DI_FILL___1"].OFF();
                        hw.outputs["Y1_01_MAIN_TANK_AUTO_DI_FILL___2"].OFF();
                        bathLevelLFilling = false;
                        MyConfig.Cfg.Model.BathLevelLabel = "NORMAL";
                    });
                }
                MyConfig.Cfg.Model.BathLevelLabel = "NORMAL";
            }
         
            //H
            if (hw.inputs["X3_12_HIGH_LEVEL"].Read() == 1)
            {
                BathLevel = "High";
                hw.outputs["Y2_13_MAIN_WATER"].OFF();
                
                MyConfig.Cfg.Model.BathLevelH = true;
                if (bathLevelHAlarmed == false)
                {
                    bathLevelHAlarmed = true;
                    MyConfig.Cfg.Model.BathLevelLabel = "HIGH";
                    MyConfig.Cfg.Model.ErrorCode = "Alert,Bath High Level";
                }
            }  
            else
            {
                bathLevelHAlarmed = false;
                MyConfig.Cfg.Model.BathLevelH = false;
            }
                

            //HH
            if (hw.inputs["X3_13_HIGH_HIGH_LEVEL"].Read() == 1)
            {
                BathLevel = "Very High";
  
                MyConfig.Cfg.Model.BathLevelHH = true;
                hw.outputs["Y2_13_MAIN_WATER"].OFF();
                hw.outputs["Y3_01_HEATER_ENABLE"].OFF();
                hw.outputs["Y3_00_MAIN_TANK_PUMP_(RUN)"].OFF();
                // MyConfig.Cfg.EMOStat = true;
                
                if (bathLevelHHAlarmed == false)
                {
                    bathLevelHHAlarmed = true;
                    MyConfig.Cfg.Model.ErrorCode = "Alert,Bath Level HH";
                    MyConfig.Cfg.Model.BathLevelLabel = "VERY HIGH";
                }
            }  
            else
            {
                bathLevelHHAlarmed = false;
                MyConfig.Cfg.Model.BathLevelHH = false;
            }
                
        }

        int fullCount = 0;
        private bool BathLevelOffForSomeTime()
        {
            if (hw.inputs["X3_11_AUTO_FILL_LEVEL"].Read() == 1)
                fullCount = 0;
            else
                fullCount++;

            if (fullCount > 15)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void ReadIOSensor()
        {
            if (hw.inputs["X5_03_E_STOP_LOOP_OK"].Read() == 1)
                MyConfig.Cfg.Model.EMO = "OFF";
            else
                MyConfig.Cfg.Model.EMO = "ON";

            if (prevEMO == 1 && hw.inputs["X5_03_E_STOP_LOOP_OK"].Read() == 0)
            {
                MyConfig.Cfg.Model.ErrorCode = "Alert,EMO triggered";
            }
            prevEMO = hw.inputs["X5_03_E_STOP_LOOP_OK"].Read();

            if (prevAlarmSilence == 0 && hw.inputs["X5_00_ALARM_SILENCE_(OWA)"].Read() == 1)
            {
                MyConfig.Cfg.Model.SilenceAlarm();
            }
            prevAlarmSilence = hw.inputs["X5_00_ALARM_SILENCE_(OWA)"].Read();


            if (hw.inputs["X2_12_CLAMSHELL_PRESENT_CELL_I_O___1"].Read() == 1)
                MyConfig.Cfg.Model.cboxIOA1 = true;
            else
                MyConfig.Cfg.Model.cboxIOA1 = false;

            if (hw.inputs["X2_13_CLAMSHELL_PRESENT_CELL_I_O___2"].Read() == 1)
                MyConfig.Cfg.Model.cboxIOA2 = true;
            else
                MyConfig.Cfg.Model.cboxIOA2 = false;

            if (hw.inputs["X2_14_CLAMSHELL_PRESENT_CELL_I_O___3"].Read() == 1)
                MyConfig.Cfg.Model.cboxIOA3 = true;
            else
                MyConfig.Cfg.Model.cboxIOA3 = false;

            if (hw.inputs["X2_15_CLAMSHELL_PRESENT_CELL_I_O___4"].Read() == 1)
                MyConfig.Cfg.Model.cboxIOA4 = true;
            else
                MyConfig.Cfg.Model.cboxIOA4 = false;

            if (hw.inputs["X3_00_CLAMSHELL_PRESENT_I_O___5"].Read() == 1)
                MyConfig.Cfg.Model.cboxIOA5 = true;
            else
                MyConfig.Cfg.Model.cboxIOA5 = false;

            if (hw.inputs["X3_01_CLAMSHELL_PRESENT_I_O___6"].Read() == 1)
                MyConfig.Cfg.Model.cboxIOA6 = true;
            else
                MyConfig.Cfg.Model.cboxIOA6 = false;

            if (hw.inputs["X3_02_CLAMSHELL_PRESENT_I_O___7"].Read() == 1)
                MyConfig.Cfg.Model.cboxIOA7 = true;
            else
                MyConfig.Cfg.Model.cboxIOA7 = false;

            if (hw.inputs["X3_03_CLAMSHELL_PRESENT_I_O___8"].Read() == 1)
                MyConfig.Cfg.Model.cboxIOB1 = true;
            else
                MyConfig.Cfg.Model.cboxIOB1 = false;

            if (hw.inputs["X3_04_CLAMSHELL_PRESENT_I_O___9"].Read() == 1)
                MyConfig.Cfg.Model.cboxIOB2 = true;
            else
                MyConfig.Cfg.Model.cboxIOB2 = false;

            if (hw.inputs["X3_05_CLAMSHELL_PRESENT_I_O___10"].Read() == 1)
                MyConfig.Cfg.Model.cboxIOB3 = true;
            else
                MyConfig.Cfg.Model.cboxIOB3 = false;

            if (hw.inputs["X3_06_CLAMSHELL_PRESENT_I_O___11"].Read() == 1)
                MyConfig.Cfg.Model.cboxIOB4 = true;
            else
                MyConfig.Cfg.Model.cboxIOB4 = false;

            if (hw.inputs["X3_07_CLAMSHELL_PRESENT_I_O___12"].Read() == 1)
                MyConfig.Cfg.Model.cboxIOB5 = true;
            else
                MyConfig.Cfg.Model.cboxIOB5 = false;

            if (hw.inputs["X3_08_CLAMSHELL_PRESENT_I_O___13"].Read() == 1)
                MyConfig.Cfg.Model.cboxIOB6 = true;
            else
                MyConfig.Cfg.Model.cboxIOB6 = false;

            if (hw.inputs["X3_09_CLAMSHELL_PRESENT_I_O___14"].Read() == 1)
                MyConfig.Cfg.Model.cboxIOB7 = true;
            else
                MyConfig.Cfg.Model.cboxIOB7 = false;


        }

        private void ReadCellSensor()
        {

            if (hw.inputs["X2_00_CLAMSHELL_PRESENT_CELL___1"].Read() == 0)
                MyConfig.Cfg.Model.cboxCellSensor1 = true;
            else
                MyConfig.Cfg.Model.cboxCellSensor1 = false;

            if (hw.inputs["X2_01_CLAMSHELL_PRESENT_CELL___2"].Read() == 0)
                MyConfig.Cfg.Model.cboxCellSensor2 = true;
            else
                MyConfig.Cfg.Model.cboxCellSensor2 = false;

            if (hw.inputs["X2_02_CLAMSHELL_PRESENT_CELL___3"].Read() == 0)
                MyConfig.Cfg.Model.cboxCellSensor3 = true;
            else
                MyConfig.Cfg.Model.cboxCellSensor3 = false;

            if (hw.inputs["X2_03_CLAMSHELL_PRESENT_CELL___4"].Read() == 0)
                MyConfig.Cfg.Model.cboxCellSensor4 = true;
            else
                MyConfig.Cfg.Model.cboxCellSensor4 = false;

            if (hw.inputs["X2_04_CLAMSHELL_PRESENT_CELL___5"].Read() == 0)
                MyConfig.Cfg.Model.cboxCellSensor5 = true;
            else
                MyConfig.Cfg.Model.cboxCellSensor5 = false;

            if (hw.inputs["X2_05_CLAMSHELL_PRESENT_CELL___6"].Read() == 0)
                MyConfig.Cfg.Model.cboxCellSensor6 = true;
            else
                MyConfig.Cfg.Model.cboxCellSensor6 = false;

            if (hw.inputs["X2_06_CLAMSHELL_PRESENT_CELL___7"].Read() == 0)
                MyConfig.Cfg.Model.cboxCellSensor7 = true;
            else
                MyConfig.Cfg.Model.cboxCellSensor7 = false;

            if (hw.inputs["X2_07_CLAMSHELL_PRESENT_CELL___8"].Read() == 0)
                MyConfig.Cfg.Model.cboxCellSensor8 = true;
            else
                MyConfig.Cfg.Model.cboxCellSensor8 = false;

            if (hw.inputs["X2_08_CLAMSHELL_PRESENT_CELL___9"].Read() == 0)
                MyConfig.Cfg.Model.cboxCellSensor9 = true;
            else
                MyConfig.Cfg.Model.cboxCellSensor9 = false;

            if (hw.inputs["X2_09_CLAMSHELL_PRESENT_CELL___10"].Read() == 0)
                MyConfig.Cfg.Model.cboxCellSensor10 = true;
            else
                MyConfig.Cfg.Model.cboxCellSensor10 = false;

            if (hw.inputs["X2_10_CLAMSHELL_PRESENT_CELL___11"].Read() == 0)
                MyConfig.Cfg.Model.cboxCellSensor11 = true;
            else
                MyConfig.Cfg.Model.cboxCellSensor11 = false;

            if (hw.inputs["X2_11_CLAMSHELL_PRESENT_CELL___12"].Read() == 0)
                MyConfig.Cfg.Model.cboxCellSensor12 = true;
            else
                MyConfig.Cfg.Model.cboxCellSensor12 = false;

        }

        private void WaitTimer(double second)
        {
            if (second < 1)
                return;

            DateTime desired = DateTime.Now.AddSeconds(second);
            while (DateTime.Now < desired)
            {
                if (MyConfig.Cfg.MainTankStop)
                {
                    MyConfig.Cfg.MainTankStop = false;
                    break;
                }
                Thread.Sleep(1);
            }
        }

    }
}

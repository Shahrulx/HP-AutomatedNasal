﻿using HardwareLib;
using MachineControl;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AutomationLib
{

    public class RobotControl
    {
        Hardware hw;
        Events ev;
        Dictionary<string, Tuple<string, int>> Bank = new Dictionary<string, Tuple<string, int>>();
        readonly ConcurrentQueue<Tuple<int, string>> UnloadQueA = new ConcurrentQueue<Tuple<int, string>>();
        readonly ConcurrentQueue<Tuple<int, string>> UnloadQueB = new ConcurrentQueue<Tuple<int, string>>();
        Dictionary<string, Tuple<string, string>> SensorList = new Dictionary<string, Tuple<string, string>>();
        static readonly object _object = new object();
        public bool CheckUndip = false;
        public bool CheckUndip2 = false;
        TankModule tankModule;
        public string RobotPrevStat = "";
        public bool waitUndip = false;
        string MotorXPrevPos = "";
        MainFlow mainflow;
        public RobotControl(Hardware hw, Events ev, TankModule tankModule, MainFlow mainflow)
        {
            this.hw = hw;
            this.ev = ev;
            this.tankModule = tankModule;
            MyConfig.Cfg.Model.RobotStat = "Free";
            this.mainflow = mainflow;
            MyConfig.Cfg.UnloadQueA = UnloadQueA;
            MyConfig.Cfg.UnloadQueB = UnloadQueB;
            InitSensors();
            //Task.Run(() => CheckProcessStat());
        }
        public bool FirstTime = true;
        bool RobotLoadError = false;
        bool RobotUnLoadError = false;
        bool RobotDipError = false;
        bool RobotUnDipError = false;

        private void InitSensors()
        {
            int j = 0;
            int cardnum = 0;
            for (int i = 1; i <= 12; i++)
            {
                if (i == 9)
                {
                    j = 0;
                    cardnum = 1;
                }

                string _j = j.ToString().PadLeft(2, '0');
                var Presensor = $"X{cardnum}_{_j}_PRESSURE_SENSOR_CELL___{i}";
                j++;
                _j = j.ToString().PadLeft(2, '0');
                j++;
                var Vacsensor = $"X{cardnum}_{_j}_VACCUM_SENSOR_CELL___{i}";
                var sensor = Tuple.Create(Presensor, Vacsensor);

                SensorList.Add(i.ToString(), sensor);
            }
        }

        public int count = 0;

        public void Run(int BankNumber)
        {
            Task.Run(() =>
            {
                count++;
                //ev.RobotIdle.Reset();

                StartLoadingProcess(BankNumber);
                //mainflow.DoIfEndLot();
                if (MyConfig.Cfg.EMOStat)
                    MyConfig.Cfg.ProcessState = "EMO";
                count--;

                MyConfig.Cfg.StartEndlotTimer = true;
            });

            
            // ev.RobotIdle.Set();

        }

        //task to load input fixture
        public void StartLoadingProcess(int BankNumber)
        {

            if (BankNumber == 1)
                Bank = MyConfig.Cfg.Bank1;
            else
                Bank = MyConfig.Cfg.Bank2;

            MyConfig.Cfg.ProcessState = "Wait Undip Event";
            if (waitUndip)
            {
                ev.UndipDone.WaitOne();
                waitUndip = false;
            }

            //tankModule.MoveIOAB("IOAFront"); //ck 20Mar18
            tankModule.IOQBMove("IOAFront");

            loadToTank(BankNumber);
            Cmd("MoveMotor", "PosTank", "1");

            Thread.Sleep(1000);
            if (MyConfig.Cfg.EMOStat)
            {
                return;
            }

            MyConfig.Cfg.RequestNextSet = true;
            Thread.Sleep(50);
            if (MyConfig.Cfg.EnabledCellinBank2 == 0)
            {
                waitUndip = true;
                StartUndipProcess(BankNumber);
                ev.UndipDone.Set();
                return;
            }
            else if (MyConfig.Cfg.EnabledCellinBank1 == 0)
            {
                waitUndip = true;
                StartUndipProcess(BankNumber);
                ev.UndipDone.Set();
                return;
            }
            else if (FirstTime)
            {
                FirstTime = false;
                return;
            }

            if (BankNumber == 1)
            {
                waitUndip = true;
                StartUndipProcess(2);
                ev.UndipDone.Set();
            }

            else if (BankNumber == 2)
            {
                waitUndip = true;
                StartUndipProcess(1);
                ev.UndipDone.Set();
            }
            //MyConfig.Cfg.CheckUndip = true;
        }

        private void loadToTank(int BankNumber)
        {
            int TotalLoaded = 0;
            int LoadCount = 0;
            MyConfig.Cfg.ProcessState = MyConfig.Var.Loading;
            var count = Bank.ToList().Count();

            foreach (var unit in Bank.ToList())
            {
                //Move to pick and place pos
                RobotLoadError = false;
                Cmd("MoveMotor", "PosTank");
                int UnitPos = unit.Value.Item2;

                Cmd("Load", UnitPos.ToString());
                if (RobotLoadError)
                {
                    //Cmd("Load", UnitPos.ToString());
                }

                //move IOABack if all unit picked
                TotalLoaded++;
                if (TotalLoaded == count)
                {
                    tankModule.IOQA("ReFill");
                    //tankModule.MoveIOAB("IOABack");
                    tankModule.IOQBMove("IOABack");
                }
                
                if (BankNumber == 1)
                    MyConfig.Cfg.NumberOfUnitInsideBank1++;
                else
                    MyConfig.Cfg.NumberOfUnitInsideBank2++;
                
                //Move dip pos
                if (!RobotLoadError)
                {
                    Cmd("MoveMotor", "PosCell", unit.Key);
                    var CellOn = GetModelVal("cboxCellSensor" + unit.Key);

                    while (CellOn == "True" && MyConfig.Cfg.Model.CellSensorCheck)
                    {
                        MyConfig.Cfg.Model.ErrorCode = "Alert,Please Remove Unit Inside Cell:" + unit.Key + "-PRESS RESET AFTER REMOVE";
                        WaitAlarmReset();
                        CellOn = GetModelVal("cboxCellSensor" + unit.Key);
                    }

                    Cmd("Dip", unit.Key);
                    if (RobotDipError == true)
                    {
                        RobotDipError = false;
                    }
                }
                
                if (BankNumber == 1)
                    MyConfig.Cfg.Bank1[unit.Key] = Tuple.Create("Loaded", UnitPos);
                else
                    MyConfig.Cfg.Bank2[unit.Key] = Tuple.Create("Loaded", UnitPos);

                ev.RobotPick.Set();

                //tankModule.MoveIOAB("IOAFront");
                //infalte cell
                //lock
                var valve = int.Parse(unit.Key) - 1;
                var _valve = valve.ToString();

                string outputname = "Y0_" + _valve.PadLeft(2, '0') + "_INFLATE_VALVE_CELL____" + unit.Key;
                hw.outputs[outputname].ON();

                //check lock sensor
                var sensorname = SensorList[unit.Key];
                var stat = TimeOut(50, sensorname.Item1, "_PRESSURE_SENSOR_CELL___" + unit.Key);

                if (stat == 1)
                    UpdateUI($"BladderCell_{unit.Key}", "Error");
                else
                    UpdateUI($"BladderCell_{unit.Key}", "INFLATE");

                Thread.Sleep(100); //wait a while for inflate

                //on power supply
                MyConfig.Cfg.Model.powerSupply.PSCmd(unit.Key, "ON", "AUTO PLATE");

                //start timer countdown
                MyConfig.Cfg.Model.cellModule.StartDip(BankNumber, unit.Key);
                ev.WaitEvent(ev.DippingStart, 100, true, "RobotModule DippingStart");
                
                LoadCount++;
                if (LoadCount == 2)
                {
                    if (TotalLoaded != count)
                    {
                        tankModule.IOQA("Fill");
                    }
                    LoadCount = 0;
                }

                if (MyConfig.Cfg.EMOStat)
                {
                    return;
                }
            }
            //tankModule.MoveIOAB("IOABack");
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Set Loading Complete");
            if (MyConfig.Cfg.EMOStat)
            {
                MyConfig.Cfg.ProcessState = "EMO";
                return;
            }
        }

        public void AddToUnloadQue(int BankNumber, string CellNumber)
        {
            lock (_object)
            {
                if (BankNumber == 1)
                    UnloadQueA.Enqueue(Tuple.Create(BankNumber, CellNumber));
                else
                    UnloadQueB.Enqueue(Tuple.Create(BankNumber, CellNumber));
            }
        }

        public void StartUndipProcess(int BankNumber)
        {
            int NoOfUnit = 0;
            Dictionary<string, Tuple<string, int>> Bank;
            ConcurrentQueue<Tuple<int, string>> UnloadQue;
            int EnabledCell = 0;
            int unDipCount = 0;

            if (BankNumber == 1)
            {
                UnloadQue = UnloadQueA;
                Bank = MyConfig.Cfg.Bank1;
                if (MyConfig.Cfg.Model.CellSensorCheck)
                    EnabledCell = CountFixture(1);
                else
                    EnabledCell = MyConfig.Cfg.EnabledCellinBank1;
            }
            else
            {
                UnloadQue = UnloadQueB;
                Bank = MyConfig.Cfg.Bank2;
                if (MyConfig.Cfg.Model.CellSensorCheck)
                    EnabledCell = CountFixture(2);
                else
                    EnabledCell = MyConfig.Cfg.EnabledCellinBank2;
            }

            var stat = tankModule.IOQBMove("CurrenPosition");
            if (stat == 1)
            {
                MyConfig.Cfg.UpdateUI("btnMoveIOQB", "Enable", "");
                MyConfig.Cfg.Model.ErrorCode = "Info,Please Press Move IOQB After Unloading Unit ";
                while (MyConfig.Cfg.IOQBStatus != true)
                {

                    if (MyConfig.Cfg.EMOStat)
                    {
                        return;
                    }
                    if (MyConfig.Cfg.RobotRequestStop)
                    {
                        return;
                    }
                    Thread.Sleep(250);
                    WaitAlarmReset();
                }
                MyConfig.Cfg.IOQBStatus = false;
            }

            //sort

            MyConfig.Cfg.ProcessState = "Start Undip Process";

            //if(MyConfig.Cfg.UndetectedCellBank1.Count != 0 && BankNumber==1)
            //{
            //    EnabledCell++; ;
            //}
            //else if (MyConfig.Cfg.UndetectedCellBank2.Count != 0 && BankNumber == 2)
            //{
            //    EnabledCell++;
            //}

            if (BankNumber == 1)
                EnabledCell = MyConfig.Cfg.NumberOfUnitInsideBank1;
            else
                EnabledCell = MyConfig.Cfg.NumberOfUnitInsideBank2;

            while (unDipCount != EnabledCell)
            {
                MyConfig.Cfg.Model.TaskUndip = "Undip Check";

                Tuple<int, string> slot;
                while (!UnloadQue.TryDequeue(out slot))
                {
                    if (MyConfig.Cfg.EMOStat)
                    {
                        return;
                    }

                    if (MyConfig.Cfg.JustOneCell)
                    {
                        MyConfig.Cfg.JustOneCell = false;
                        return;
                    }
                    //check sensor timeout
                    Thread.Sleep(200);
                }

                MyConfig.Cfg.Model.TaskUndip = "Undip Runing";
                unDipCount++;
                var cell = Bank.First(x => x.Key == slot.Item2.ToString());

                if (cell.Value.Item1 != "Done")
                {
                    MyConfig.Cfg.ProcessState = MyConfig.Var.Undip;
                    MyConfig.Cfg.StartEndlotTimer = false;

                    if (MyConfig.Cfg.EndLotTask != null)
                    {
                        MyConfig.Cfg.StartEndlotTimer = false;
                        MyConfig.Cfg.EndLotTask = null;
                    }

                    //check ioqb position again
                    stat = tankModule.IOQBMove("CurrenPosition");
                    if (stat == 1)
                    {
                        MyConfig.Cfg.UpdateUI("btnMoveIOQB", "Enable", "");
                        MyConfig.Cfg.Model.ErrorCode = "Info,Please Press Move IOQB After Unloading Unit ";
                        while (MyConfig.Cfg.IOQBStatus != true)
                        {
                            if (MyConfig.Cfg.EMOStat)
                            {
                                return;
                            }
                            if (MyConfig.Cfg.RobotRequestStop)
                            {
                                return;
                            }
                            Thread.Sleep(250);
                            WaitAlarmReset();
                        }
                        MyConfig.Cfg.IOQBStatus = false;
                    }

                    UnloadUnit(slot.Item1, slot.Item2);
                    NoOfUnit++;
                    if (NoOfUnit == 2)
                    {
                        tankModule.IOQB("ReFill");
                        NoOfUnit = 0;
                    }
                }

                if (MyConfig.Cfg.EMOStat)
                {
                    return;
                }

                if (MyConfig.Cfg.EndProcess)
                    break;
            }

            MyConfig.Cfg.Model.TaskUndip = "IDLE";
            //tankModule.MoveIOAB("IOBBack"); //ck 20Mar18
            tankModule.IOQBMove("IOBBack");

            if (BankNumber == 1)
            {
                MyConfig.Cfg.UndetectedCellBank1.Clear();
            }
            if (BankNumber == 2)
            {
                MyConfig.Cfg.UndetectedCellBank2.Clear();
            }

            MyConfig.Cfg.BankUsed = 1;

            //MyConfig.Cfg.StartEndlotTimer = false;
            //MyConfig.Cfg.StartEndlotTimer = true; //CK_10Mar18
            //mainflow.EndLotTimerStart(); //CK_19Mar18

            MyConfig.Cfg.ProcessState = MyConfig.Var.WaitingSet;
        }

        private int CountFixture(int bank)
        {
            int count = 0;
            if (bank == 1)
            {
                if (hw.inputs["X2_00_CLAMSHELL_PRESENT_CELL___1"].Read() == 0)
                    count++;
                if (hw.inputs["X2_01_CLAMSHELL_PRESENT_CELL___2"].Read() == 0)
                    count++;
                if (hw.inputs["X2_02_CLAMSHELL_PRESENT_CELL___3"].Read() == 0)
                    count++;
                if (hw.inputs["X2_03_CLAMSHELL_PRESENT_CELL___4"].Read() == 0)
                    count++;
                if (hw.inputs["X2_04_CLAMSHELL_PRESENT_CELL___5"].Read() == 0)
                    count++;
                if (hw.inputs["X2_05_CLAMSHELL_PRESENT_CELL___6"].Read() == 0)
                    count++;
            }
            else
            {
                if (hw.inputs["X2_06_CLAMSHELL_PRESENT_CELL___7"].Read() == 0)
                    count++;
                if (hw.inputs["X2_07_CLAMSHELL_PRESENT_CELL___8"].Read() == 0)
                    count++;
                if (hw.inputs["X2_08_CLAMSHELL_PRESENT_CELL___9"].Read() == 0)
                    count++;
                if (hw.inputs["X2_09_CLAMSHELL_PRESENT_CELL___10"].Read() == 0)
                    count++;
                if (hw.inputs["X2_10_CLAMSHELL_PRESENT_CELL___11"].Read() == 0)
                    count++;
                if (hw.inputs["X2_11_CLAMSHELL_PRESENT_CELL___12"].Read() == 0)
                    count++;
            }
            return count;
        }

        public void UnloadUnit(int BankNumber, string CellNumber)
        {
            if (BankNumber == 1)
            {
                Bank = MyConfig.Cfg.Bank1;
            }
            else
            {
                Bank = MyConfig.Cfg.Bank2;
            }

            Bank[CellNumber] = Tuple.Create("UnDipping", Bank[CellNumber].Item2);
            
            Cmd("MoveMotor", "PosCell", CellNumber);
                  
            var Slotnum = int.Parse(Bank[CellNumber].Item2.ToString());
            var _Slotnum = (Slotnum - 1).ToString();

            Cmd("UnDip", CellNumber);
            if (RobotUnDipError)
            {
                RobotUnDipError = false;
                //CheckIOQBLoader(CellNumber);
                CheckCellSensor(CellNumber);
            }
            else
            {
                Cmd("MoveMotor", "PosTank");
                //CheckIOQBLoader(CellNumber);
                Cmd("UnLoad", _Slotnum);
                if (RobotUnLoadError)
                {
                    RobotUnLoadError = false;
                    CheckCellSensor(CellNumber);
                }
            }

            UpdateUI("PSStat_" + CellNumber, "IDLE");
            Bank[CellNumber] = Tuple.Create("Done", Bank[CellNumber].Item2);

            int currentbatch = 0;
            if (BankNumber == 1)
            {
                MyConfig.Cfg.NumberOfUnitInsideBank1--;
                if (MyConfig.Cfg.NumberOfUnitInsideBank1 == 0)
                {
                    MyConfig.Cfg.Bank1Stat = "SetEnded";
                    MyConfig.Cfg.UpdateUI("btnStart", "Enable", "");
                    MyConfig.Cfg.UpdateUI("btnRequestEndLot", "Enable", "");
                    currentbatch = int.Parse(MyConfig.Cfg.Model.BatchNo) + 1;
                    MyConfig.Cfg.Model.BatchNo = currentbatch.ToString();
                    //save batchnumber
                    MyConfig.Cfg.SaveINI("SavedValue", "BatchNumber", currentbatch.ToString());

                    MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Undip Done Bank: " + BankNumber);
                    MyConfig.Cfg.Model.StatusBar = "Undip Done Bank: " + BankNumber;
                    MyConfig.Cfg.RequestNextSet = true;
                    MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Batch No:" + currentbatch.ToString());

                    if (MyConfig.Cfg.EndLotTimer != true || MyConfig.Cfg.StartEndlotTimer != true)
                    {
                        if (!MyConfig.Cfg.RequestEndLot)
                        {
                            //tankModule.MoveIOAB("IOBBack"); //ck 20Mar18
                            tankModule.IOQBMove("IOBBack");
                            MyConfig.Cfg.Model.ErrorCode = "Info, Load Next Set";
                        }
                    }

                    MyConfig.Cfg.ProcessState = MyConfig.Var.Idle;

                    if (currentbatch % 3 == 0)
                    {
                        MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Refill Mandrel and Backer");
                        Task.Run(() =>
                        {
                            tankModule.Mandrel("ReFill");

                        }
                        );

                        Task.Run(() =>
                        {
                            tankModule.Backer("ReFill");
                        }
                        );
                    }
                }
            }
            else
            {
                MyConfig.Cfg.NumberOfUnitInsideBank2--;
                if (MyConfig.Cfg.NumberOfUnitInsideBank2 == 0)
                {
                    MyConfig.Cfg.Bank2Stat = "SetEnded";
                    MyConfig.Cfg.UpdateUI("btnStart", "Enable", "");
                    MyConfig.Cfg.UpdateUI("btnRequestEndLot", "Enable", "");
                    currentbatch = int.Parse(MyConfig.Cfg.Model.BatchNo) + 1;
                    MyConfig.Cfg.Model.BatchNo = currentbatch.ToString();
                    //save batchnumber
                    MyConfig.Cfg.SaveINI("SavedValue", "BatchNumber", currentbatch.ToString());

                    MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Undip Done Bank: " + BankNumber);
                    MyConfig.Cfg.Model.StatusBar = "Undip Done Bank: " + BankNumber;
                    MyConfig.Cfg.RequestNextSet = true;
                    MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Batch No:" + currentbatch.ToString());

                    if (MyConfig.Cfg.EndLotTimer != true || MyConfig.Cfg.StartEndlotTimer != true)
                    {
                        if (!MyConfig.Cfg.RequestEndLot)
                        {
                            //tankModule.MoveIOAB("IOBBack"); //ck 20Mar18
                            tankModule.IOQBMove("IOBBack");
                            MyConfig.Cfg.Model.ErrorCode = "Info, Load Next Set";
                        }
                    }

                    MyConfig.Cfg.ProcessState = MyConfig.Var.Idle;

                    if (currentbatch % 3 == 0)
                    {
                        Task.Run(() =>
                        {
                            tankModule.Mandrel("Drain");
                            tankModule.Mandrel("Fill");

                        }
                        );

                        Task.Run(() =>
                        {
                            tankModule.Backer("Drain");
                            tankModule.Backer("Fill");
                        }
                       );

                    }
                }
            }
        }

        private static void CheckIOQBLoader(string CellNumber)
        {
            int slot = int.Parse(CellNumber);

            if (int.Parse(CellNumber) > 7)
                slot = slot - 6;

            MyConfig.Cfg.UpdateUI($"cboxIOB{slot}", "GetValue", "");
            var val = MyConfig.Cfg.UIValue;

            while (val == "False")
            {
                MyConfig.Cfg.Model.ErrorCode = "Alert,Please Manualy Unload Unit to IOQB " + slot.ToString();
                WaitAlarmReset();

                MyConfig.Cfg.UpdateUI($"cboxIOB{slot}", "GetValue", "");
                val = MyConfig.Cfg.UIValue;

                if (MyConfig.Cfg.EMOStat)
                    break;
            }
        }

        private static void WaitAlarmReset()
        {
            while (MyConfig.Cfg.Model.AlarmStat != "OFF")
            {
                Thread.Sleep(200);

                if (MyConfig.Cfg.EMOStat)
                    break;
            }
        }

        private static void CheckCellSensor(string CellNumber)
        {
            int slot = int.Parse(CellNumber);

            //if (int.Parse(CellNumber) > 7)
            //    slot = slot - 6;

            MyConfig.Cfg.UpdateUI($"cboxCellSensor{slot}", "GetValue", "");
            var val = MyConfig.Cfg.UIValue;
            var PrevStat = MyConfig.Cfg.Model.StatusBar;

            while (val == "True")
            {
                MyConfig.Cfg.Model.ErrorCode = "Alert,Please Remove Unit From Cell " + slot.ToString() + "-PRESS RESET AFTER REMOVE";
                MyConfig.Cfg.Model.StatusBar = "Please Remove Unit From Cell " + slot.ToString() + "-PRESS RESET AFTER REMOVE";
                while (MyConfig.Cfg.Model.AlarmStat != "OFF")
                {
                    Thread.Sleep(200);

                    if (MyConfig.Cfg.EMOStat)
                        break;
                }

                MyConfig.Cfg.Model.StatusBar = PrevStat;
                MyConfig.Cfg.UpdateUI($"cboxCellSensor{slot}", "GetValue", "");
                val = MyConfig.Cfg.UIValue;

                if (MyConfig.Cfg.EMOStat)
                    break;
            }
        }

       private int WaitRobot(int sec, string SensorName, string Message)
        {
            int stat = 0;
            int count = 0;
            DateTime desired = DateTime.Now.AddSeconds(sec);
            MyConfig.Cfg.ProcessState = $"WaitRobot:{Message}";
            //if (MyConfig.Cfg.SimulationMode)
            //    return 0;
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, $"WaitRobotStart: {Message}");
            while (hw.inputs[SensorName].Read() != 1)
            {
                Thread.Sleep(200);

                if (DateTime.Now > desired)
                {
                    hw.outputs["Y4_06_ROBOT_EMO_ON"].ON();
                    MyConfig.Cfg.RobotSoftEMO = true;
                    ResetRobotIO(false);
                    MyConfig.Cfg.RobotRequestStop = true;
                    MyConfig.Cfg.Model.RobotStat = "ERROR";
                    MyConfig.Cfg.Model.ErrorCode = "Alert,TimeOut:" + Message;
                }

                //check stop
                if (MyConfig.Cfg.RobotRequestStop == true)
                {
                    switch (MyConfig.Cfg.Model.RobotStat)
                    {
                        case "EFFORTOR":
                        case "ERROR":
                            RobotErrors();
                            return 1;
                            break;

                        case "EMO":
                            RobotEmo();
                            return 1;
                            break;

                        case "STOP":
                            stat = RobotStop(); //for robot stop button flow will stop here
                            //break if machine emo pressed
                            if (stat == 1)
                                return 1;
                            else
                                desired = DateTime.Now.AddSeconds(sec);
                            break;

                        default:
                            break;
                    }
                }

                if (MyConfig.Cfg.SimulationMode)
                {
                    stat = 0;
                    break;
                }
            }
            
            //double check robot sensor
            if (SensorName != "NONE" && !MyConfig.Cfg.SimulationMode)
            {
                MyConfig.Cfg.ProcessState = "Re-Check Sensor";
                while (hw.inputs[SensorName].Read() != 1)
                {
                    if (MyConfig.Cfg.EMOStat)
                        return 1;

                    Thread.Sleep(250);
                }
            }

            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, $"WaitRobotDone:{Message}");
            return stat;
        }

        public void ResetRobotIO(bool peformreset)
        {
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, $"RESET ROBOT IO");

            hw.outputs["Y5_11_ROBOT_TANK_LOCK_READY"].OFF();
            hw.outputs["Y5_12_ROBOT_TANK_UNLOCK_READY"].OFF();
            hw.outputs["Y5_03_ROBOT_PAUSE"].OFF();

            //gripper
            //hw.outputs["Y6_15_SPARE_"].OFF();
            TriggerGripper(true);

            hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].OFF();
            hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].OFF();
            hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].OFF();
            hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].OFF();
            hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].OFF();
            hw.outputs["Y5_00_ROBOT_BASKET_POSITION_BIT_1"].OFF();
            hw.outputs["Y5_01_ROBOT_BASKET_POSITION_BIT_2"].OFF();
            hw.outputs["Y5_02_ROBOT_BASKET_POSITION_BIT_3"].OFF();
            hw.outputs["Y5_04_ROBOT_BASKET_POSITION_BIT_4"].OFF();

            hw.outputs["Y4_06_ROBOT_EMO_ON"].OFF();
            hw.outputs["Y6_07_ROBOT_ACTUATOR_INPUT"].OFF();

            hw.outputs["Y5_07_ROBOT_START"].ON();
            Thread.Sleep(100);
            hw.outputs["Y5_07_ROBOT_START"].OFF();
            //hw.outputs["Y5_14_ROBOT_RESET"].OFF();

            //ofall for homing only
            if (peformreset)
            {
                hw.outputs["Y5_14_ROBOT_RESET"].ON();
                Thread.Sleep(300);
                hw.outputs["Y5_14_ROBOT_RESET"].OFF();
            }
        }

        public void Cmd(string type, string param, string param2 = "", bool UseThread = false)
        {
            //check robot buzy
            //WaitRobotFree();

            if (MyConfig.Cfg.EMOStat)
            {
                return;
            }

            //REMOVE
            //WaitRobot(60, "NONE", "Waiting Robot Free");
            MyConfig.Cfg.ProcessState = $"Start CMD{type},{param}";

            switch (type)
            {
                case "Load":
                    if (UseThread)
                        Task.Run(() => Load(param));
                    else
                        Load(param);
                    break;

                case "UnLoad":
                    if (UseThread)
                        Task.Run(() => Unload(param));
                    else
                        Unload(param);
                    break;

                case "Dip":
                    if (UseThread)
                        Task.Run(() => Dip(param));
                    else
                        Dip(param);
                    break;

                case "UnDip":
                    if (UseThread)
                        Task.Run(() => UnDip(param));
                    else
                        UnDip(param);
                    break;

                case "MoveMotor":

                    Start:

                    int stopstatus = 0;

                    if (hw.inputs["X3_14_ROBOT_Z_AXIS_UP"].Read() == 0)
                    {
                        ActuatorStopZAxis();
                    }

                    else if (MyConfig.Cfg.Model.RobotStat == "EFFORTOR" || MyConfig.Cfg.Model.RobotStat == "EMO" || MyConfig.Cfg.Model.RobotStat == "ERROR" || MyConfig.Cfg.Model.RobotStat == "STOP")
                    {
                        stopstatus = ActuatorStopByRobot();
                        if (stopstatus == 1)
                            return;
                    }
                    ///check if robot z axis up
                    MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Robot Z-Aixs In Position-Actuator Start Move: "+ param);
                    MotorXPrevPos = param;

                    if (param == "PosTank")
                    {
                        hw.motors["Motor1X"].MoveAbsolute(Config.Cfg.PickPos1.Item1, 100);
                        hw.motors["Motor1X"].WaitMotionDone(500);
                    }
                    else if (param == "PosCell")
                    {
                        if (param2 == "1" || param2 == "2" || param2 == "3" || param2 == "7" || param2 == "8" || param2 == "9")
                        {
                            hw.motors["Motor1X"].MoveAbsolute(Config.Cfg.PickPos2.Item1, 100);
                            hw.motors["Motor1X"].WaitMotionDone(500);
                        }
                        else if (param2 == "4" || param2 == "5" || param2 == "6" || param2 == "10" || param2 == "11" || param2 == "12")
                        {
                            hw.motors["Motor1X"].MoveAbsolute(Config.Cfg.PickPos3.Item1, 100);
                            hw.motors["Motor1X"].WaitMotionDone(500);
                        }
                    }

                    if (MyConfig.Cfg.Model.RobotStat == "EFFORTOR" || MyConfig.Cfg.Model.RobotStat == "EMO" || MyConfig.Cfg.Model.RobotStat == "ERROR" || MyConfig.Cfg.Model.RobotStat == "STOP")
                    {
                        stopstatus = ActuatorStopByRobot();
                        if (stopstatus == 1)
                            return;

                        else
                            goto Start;
                    }
                    break;

                case "Stop":
                    hw.outputs["Y5_03_ROBOT_PAUSE"].ON();
                    //RobotPrevStat = MyConfig.Cfg.Model.RobotStat;
                    //MyConfig.Cfg.Model.RobotStat = MyConfig.Var.RobotStop;
                    break;

                case "StopActuator":
                    hw.motors["Motor1X"].Stop();
                    break;

                case "RobotHome":

                    Thread.Sleep(200);
                    hw.outputs["Y5_13_ROBOT_HOME"].ON();
                    Thread.Sleep(200);
                    var stat = TimeOut(120, "X6_14_ROBOT_POSITION_RECEIVED", "Robot Home Error");
                    hw.outputs["Y5_13_ROBOT_HOME"].OFF();
                    if (stat == 1)
                        MyConfig.Cfg.RobotRequestStop = true;
                    else
                        MyConfig.Cfg.RobotRequestStop = false;

                    break;

                case "Start":
                    hw.outputs["Y5_07_ROBOT_START"].ON();
                    break;

                case "Reset":
                    hw.outputs["Y5_14_ROBOT_RESET"].ON();
                    break;

                default:
                    break;
            }
        }

        #region StopFcuntion List
        private int ActuatorStopByRobot()
        {
            MyConfig.Cfg.Model.ErrorCode = "Alert,Actuator Stop Due To Robot:" + MyConfig.Cfg.Model.RobotStat;
            MyConfig.Cfg.Model.StatusBar = $"Actuator Stop Due To Robot:{MyConfig.Cfg.Model.RobotStat}.Please Press RESUME ROBOT To Continue Process";
            MyConfig.Cfg.UpdateUI("btnStopRobot", "SetValueBtn", "RESUME ROBOT");
            MyConfig.Cfg.UpdateUI("btnStopRobot", "Disable", "");

            MyConfig.Cfg.RobotRequestStop = true;
            //wait user reset robot program
            //check stop
            if (MyConfig.Cfg.RobotRequestStop == true)
            {
                int stat = 0; //if stat ==1 machine emo pressed
                switch (MyConfig.Cfg.Model.RobotStat)
                {
                    case "EFFORTOR":
                    case "ERROR":

                        stat = RobotErrors();

                        if (stat == 1)
                            return stat;

                        break;
                    case "EMO":

                        RobotEmo();
                        if (stat == 1)
                            return stat;
                        break;

                    case "STOP":
                        RobotStop();
                        if (stat == 1)
                            return stat;
                        break;


                    default:
                        break;
                }


            }
        
            MyConfig.Cfg.Model.StatusBar = $"Robot Resume";
            return 0;
        }

        private void ActuatorStopZAxis()
        {
            if (hw.inputs["X3_14_ROBOT_Z_AXIS_UP"].Read() == 0)
            {

                MyConfig.Cfg.RobotRequestStop = true;
                MyConfig.Cfg.Model.RobotStat = "ERROR";

                //MyConfig.Cfg.Model.StatusBar ="Actuator Stoped "
                MyConfig.Cfg.Model.ErrorCode = "Alert,Actuator Can't Move Due To Z Axis Not In Top Position";
                MyConfig.Cfg.Model.StatusBar = "Robot Z-Axis Not In Position.Please Move Up And Press RESUME ROBOT";

                //change robot stop button to resume and disable

                MyConfig.Cfg.UpdateUI("btnStopRobot", "SetValueBtn", "RESUME ROBOT");
                //disable robot resume button
                MyConfig.Cfg.UpdateUI("btnStopRobot", "Disable", "");
                while (hw.inputs["X3_14_ROBOT_Z_AXIS_UP"].Read() == 0)
                {
                    if (MyConfig.Cfg.EMOStat)
                        return;

                    //wait until robot z move up
                    Thread.Sleep(250);
                }

                //enable
                MyConfig.Cfg.UpdateUI("btnStopRobot", "Enable", "");
                //wait user to press resume
                while (MyConfig.Cfg.Model.RobotStat != MyConfig.Var.RobotResume)
                {
                    if (MyConfig.Cfg.EMOStat)
                        return;

                    Thread.Sleep(250);
                }
                TriggerGripper(true);
                //close gripper
                //yyy - when actuator moving, no need purpose trigger gripper close
                //hw.outputs["Y6_15_SPARE_"].ON();
                //Thread.Sleep(1000);
                //hw.outputs["Y6_15_SPARE_"].OFF();
                MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Actuator Recovered");
                MyConfig.Cfg.Model.StatusBar = $"Actuator Resume";
            }


        }

        private int RobotEmo()
        {
            MyConfig.Cfg.Model.RobotCurrentStat = "EMO";
            MyConfig.Cfg.ProcessState = "Wait Emo Robot Release";
            ResetRobotIO(false);

            MyConfig.Cfg.Model.ErrorCode = $"Alert,Robot Emo Triggered- Robot Stop";

            hw.outputs["Y5_14_ROBOT_RESET"].ON();
            MyConfig.Cfg.UpdateUI("btnStopRobot", "SetValueBtn", "RESUME ROBOT");
            MyConfig.Cfg.UpdateUI("btnStopRobot", "Disable", "");
            while (hw.inputs["X6_15_ROBOT_EMERGENCY_STOP"].Read() != 0)
            {
                if (MyConfig.Cfg.EMOStat)
                    return 1;

                Thread.Sleep(200);
            }


            MyConfig.Cfg.UpdateUI("btnStopRobot", "Enable", "");
            MyConfig.Cfg.ProcessState = "Wait Robot Resume From Emo";
            while (MyConfig.Cfg.Model.RobotStat != MyConfig.Var.RobotResume)
            {
                if (MyConfig.Cfg.EMOStat)
                    return 1;

                Thread.Sleep(200);
            }

            hw.outputs["Y5_14_ROBOT_RESET"].OFF();
            hw.outputs["Y5_13_ROBOT_HOME"].OFF();
            Thread.Sleep(500);

            hw.outputs["Y5_14_ROBOT_RESET"].ON();
            Thread.Sleep(500);
            MyConfig.Cfg.ProcessState = "Wait Robot Position Recieved From Emo";

            //yyy - close gripper first before user reset robot program
            TriggerGripper(true);
            while (hw.inputs["X6_14_ROBOT_POSITION_RECEIVED"].Read() != 1)
            {
                if (MyConfig.Cfg.EMOStat)
                    return 1;

                if (MyConfig.Cfg.SimulationMode)
                    break;

                Thread.Sleep(200);
            }
            hw.outputs["Y5_14_ROBOT_RESET"].OFF();
            ////gripper
            //hw.outputs["Y6_15_SPARE_"].ON();
            //Thread.Sleep(1000);
            //hw.outputs["Y6_15_SPARE_"].OFF();

            MyConfig.Cfg.RobotRequestStop = false;
            MyConfig.Cfg.Model.RobotCurrentStat = "Resume";
            MyConfig.Cfg.Model.RobotStat = "Free";

            //check Z axis before resume robot from emo
            MyConfig.Cfg.UpdateUI("btnStopRobot", "SetValueBtn", "STOP-ROBOT");

            return 0;

        }

        //enefactor or other error
        private int RobotErrors()
        {
            MyConfig.Cfg.Model.RobotCurrentStat = "STOP";
            MyConfig.Cfg.ProcessState = "Wait Resume from Error:"+ MyConfig.Cfg.Model.RobotStat;

            var PrevStat = MyConfig.Cfg.Model.StatusBar;
            MyConfig.Cfg.Model.ErrorCode = $"Alert,Robot Stoped Caused by {MyConfig.Cfg.Model.RobotStat}";
            MyConfig.Cfg.Model.StatusBar = $"Robot Stoped Please Reset Robot Program";
            ResetRobotIO(false);
  
            Thread.Sleep(100);
            TriggerGripper(true);
            MyConfig.Cfg.UpdateUI("btnStopRobot", "SetValueBtn", "RESUME ROBOT");
            MyConfig.Cfg.UpdateUI("btnStopRobot", "Disable", "");

            hw.outputs["Y5_14_ROBOT_RESET"].ON();
            hw.outputs["Y4_06_ROBOT_EMO_ON"].ON();
            MyConfig.Cfg.RobotSoftEMO = true;
            Thread.Sleep(100);
            hw.outputs["Y4_06_ROBOT_EMO_ON"].OFF();
            while (hw.inputs["X6_15_ROBOT_EMERGENCY_STOP"].Read() != 0)
            {
                if (MyConfig.Cfg.EMOStat)
                    return 1;

                Thread.Sleep(200);
            }


            //wait robot program to start and send posreived signal
            while (hw.inputs["X6_14_ROBOT_POSITION_RECEIVED"].Read() != 1)
            {
                if (MyConfig.Cfg.EMOStat)
                    return 1;

                if (MyConfig.Cfg.SimulationMode)
                    break;

                Thread.Sleep(200);
            }
            //add statusbar
            MyConfig.Cfg.Model.StatusBar = $"Robot Reset Please Press RESUME ROBOT";
            MyConfig.Cfg.UpdateUI("btnStopRobot", "Enable", "");
            //wait user to press robot-resume
            while (MyConfig.Cfg.Model.RobotStat != MyConfig.Var.RobotResume)
            {
                if (MyConfig.Cfg.EMOStat)
                    return 1;

                Thread.Sleep(200);
            }

            hw.outputs["Y5_14_ROBOT_RESET"].OFF();
            hw.outputs["Y5_13_ROBOT_HOME"].OFF();
            //gripper
            //yyy - gripper have 2 outputs. 1 is gripper on, 1 is gripper off. both outputs are always off
            //gripper on - turn on output 1 seconds then off
            //gripper off - turn off output 1 second then off
            //hw.outputs["Y6_15_SPARE_"].ON();
            //Thread.Sleep(1000);
            //hw.outputs["Y6_15_SPARE_"].OFF();
            //Thread.Sleep(500);

            MyConfig.Cfg.RobotRequestStop = false;
            MyConfig.Cfg.Model.RobotCurrentStat = "Resume";
            MyConfig.Cfg.Model.RobotStat = "Free";

            //recheck robot signal
            MyConfig.Cfg.UpdateUI("btnStopRobot", "SetValueBtn", "STOP-ROBOT");
            MyConfig.Cfg.Model.StatusBar = PrevStat;
            return 0;
        }

        /// <summary>
        /// isOn = true, 
        /// </summary>
        /// <param name="isOn"></param>
        public void TriggerGripper(bool CloseGripper)
        {
            if (CloseGripper == true)
            {
                hw.outputs["Y4_07_ROBOT_GRIPPER_UNLOCK"].OFF();
                hw.outputs["Y4_08_ROBOT_GRIPPER_LOCK"].ON();
                Thread.Sleep(1000);
                hw.outputs["Y4_08_ROBOT_GRIPPER_LOCK"].OFF();
            }
            else
            {
                hw.outputs["Y4_08_ROBOT_GRIPPER_LOCK"].OFF();
                hw.outputs["Y4_07_ROBOT_GRIPPER_UNLOCK"].ON();
                Thread.Sleep(1000);
                hw.outputs["Y4_07_ROBOT_GRIPPER_UNLOCK"].OFF();
            }
        }

        //User press stop
        private int RobotStop()
        {
            MyConfig.Cfg.Model.RobotCurrentStat = "Stop";
            hw.outputs["Y5_03_ROBOT_PAUSE"].ON();
            MyConfig.Cfg.Model.ErrorCode = "Alert,Robot Stop";
            var PrevStatus = MyConfig.Cfg.Model.StatusBar;
            MyConfig.Cfg.Model.StatusBar = "Robot Paused.Please Press RESUME ROBOT ";
            Thread.Sleep(100);
            
            MyConfig.Cfg.ProcessState = "Wait Robot Resume From Stop";
            MyConfig.Cfg.UpdateUI("btnStopRobot", "Enable", "");

            while (MyConfig.Cfg.Model.RobotStat != MyConfig.Var.RobotResume)
            {
                if (MyConfig.Cfg.EMOStat)
                    return 1;

                Thread.Sleep(200);
            }

            hw.outputs["Y5_13_ROBOT_HOME"].OFF();
            hw.outputs["Y5_03_ROBOT_PAUSE"].OFF();
            hw.outputs["Y5_07_ROBOT_START"].ON();

            MyConfig.Cfg.ProcessState = "Wait Robot PosRecieved from Stop";
            while (hw.inputs["X6_14_ROBOT_POSITION_RECEIVED"].Read() != 1)
            {
                if (MyConfig.Cfg.EMOStat)
                    return 1;
                //wait robot reset

                if (MyConfig.Cfg.SimulationMode)
                    break;

                Thread.Sleep(100);
            }

            hw.outputs["Y5_03_ROBOT_PAUSE"].OFF();
            Thread.Sleep(100);
            hw.outputs["Y5_07_ROBOT_START"].OFF();
            Thread.Sleep(100);
            //gripper
            //yyy - when user press pause robot on GUI, no need to off gripper
            //hw.outputs["Y6_15_SPARE_"].OFF();
            //Thread.Sleep(100);

            MyConfig.Cfg.RobotRequestStop = false;
            MyConfig.Cfg.Model.RobotStat = "Free";

            //recheck robot signal
            MyConfig.Cfg.UpdateUI("btnStopRobot", "SetValueBtn", "STOP-ROBOT");

            MyConfig.Cfg.Model.RobotCurrentStat = "Resume";
            MyConfig.Cfg.Model.StatusBar = PrevStatus;

            return 0;
          
        }
        #endregion


        private void Unload(string UnitName)
        {
            var statb = tankModule.IOQBMove("CurrenPosition");
            if (statb == 1)
            {
                MyConfig.Cfg.UpdateUI("btnMoveIOQB", "Enable", "");
                MyConfig.Cfg.Model.ErrorCode = "Info,Please Press Move IOQB After Unloading Unit ";
                while (MyConfig.Cfg.IOQBStatus != true)
                {

                    if (MyConfig.Cfg.EMOStat)
                    {
                        return;
                    }
                    if (MyConfig.Cfg.RobotRequestStop)
                    {
                        return;
                    }
                    Thread.Sleep(250);
                    WaitAlarmReset();
                }
                MyConfig.Cfg.IOQBStatus = false;
            }

            MyConfig.Cfg.Model.RobotCurrentStat = "Unloading:" + UnitName;
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Robot Start Unload to Slot: " + UnitName);
            MyConfig.Cfg.Model.StatusBar = "Robot UnLoading To Slot: " + UnitName;



            hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].OFF();
            hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].OFF();
            hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].OFF();
            hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].OFF();
            hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].OFF();
            switch (UnitName)
            {
                //UNLOAD ZERO POS
                case "0":
                    hw.outputs["Y5_00_ROBOT_BASKET_POSITION_BIT_1"].ON();
                    hw.outputs["Y5_01_ROBOT_BASKET_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_02_ROBOT_BASKET_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_04_ROBOT_BASKET_POSITION_BIT_4"].OFF();
                    break;

                case "1":
                    hw.outputs["Y5_00_ROBOT_BASKET_POSITION_BIT_1"].ON();
                    hw.outputs["Y5_01_ROBOT_BASKET_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_02_ROBOT_BASKET_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_04_ROBOT_BASKET_POSITION_BIT_4"].ON();
                    break;

                case "2":
                    hw.outputs["Y5_00_ROBOT_BASKET_POSITION_BIT_1"].ON();
                    hw.outputs["Y5_01_ROBOT_BASKET_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_02_ROBOT_BASKET_POSITION_BIT_3"].ON();
                    hw.outputs["Y5_04_ROBOT_BASKET_POSITION_BIT_4"].OFF();
                    break;

                case "3":
                    hw.outputs["Y5_00_ROBOT_BASKET_POSITION_BIT_1"].ON();
                    hw.outputs["Y5_01_ROBOT_BASKET_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_02_ROBOT_BASKET_POSITION_BIT_3"].ON();
                    hw.outputs["Y5_04_ROBOT_BASKET_POSITION_BIT_4"].ON();
                    break;

                case "4":
                    hw.outputs["Y5_00_ROBOT_BASKET_POSITION_BIT_1"].ON();
                    hw.outputs["Y5_01_ROBOT_BASKET_POSITION_BIT_2"].ON();
                    hw.outputs["Y5_02_ROBOT_BASKET_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_04_ROBOT_BASKET_POSITION_BIT_4"].OFF();
                    break;

                case "5":
                    hw.outputs["Y5_00_ROBOT_BASKET_POSITION_BIT_1"].ON();
                    hw.outputs["Y5_01_ROBOT_BASKET_POSITION_BIT_2"].ON();
                    hw.outputs["Y5_02_ROBOT_BASKET_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_04_ROBOT_BASKET_POSITION_BIT_4"].ON();
                    break;

                case "6":
                    hw.outputs["Y5_00_ROBOT_BASKET_POSITION_BIT_1"].ON();
                    hw.outputs["Y5_01_ROBOT_BASKET_POSITION_BIT_2"].ON();
                    hw.outputs["Y5_02_ROBOT_BASKET_POSITION_BIT_3"].ON();
                    hw.outputs["Y5_04_ROBOT_BASKET_POSITION_BIT_4"].OFF();
                    break;

                case "7":
                    hw.outputs["Y5_00_ROBOT_BASKET_POSITION_BIT_1"].ON();
                    hw.outputs["Y5_01_ROBOT_BASKET_POSITION_BIT_2"].ON();
                    hw.outputs["Y5_02_ROBOT_BASKET_POSITION_BIT_3"].ON();
                    hw.outputs["Y5_04_ROBOT_BASKET_POSITION_BIT_4"].ON();
                    break;


            }

            //chnage power supply stat


            Thread.Sleep(500);
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Robot Start Unload Y6_07_ROBOT_ACTUATOR_INPUT ON");
            hw.outputs["Y6_07_ROBOT_ACTUATOR_INPUT"].ON();
            Thread.Sleep(300);

            var stat = WaitRobot(60, "X6_10_ROBOT_UNLOAD_READY", "Checking Robot UnloadReady Sensor");
            if (stat == 1)
            {
                RobotUnLoadError = true;
                return;
            }
            else
            {
                MyConfig.Cfg.ProcessState = $"WaitRobot:Done";
            }

            //reset signals
            hw.outputs["Y5_00_ROBOT_BASKET_POSITION_BIT_1"].OFF();
            hw.outputs["Y5_01_ROBOT_BASKET_POSITION_BIT_2"].OFF();
            hw.outputs["Y5_02_ROBOT_BASKET_POSITION_BIT_3"].OFF();
            hw.outputs["Y5_04_ROBOT_BASKET_POSITION_BIT_4"].OFF();

            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Robot Start Unload Y6_07_ROBOT_ACTUATOR_INPUT OFF");
            hw.outputs["Y6_07_ROBOT_ACTUATOR_INPUT"].OFF();

            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Robot Done Unload to Slot: " + UnitName);
            MyConfig.Cfg.Model.RobotCurrentStat = "Idle";
        }

        private void Load(string UnitName)
        {
            if (hw.inputs["X4_12_I_O_OUEUE_(A)_REV_POSITION"].Read() == 0)
            {
                MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "IOQA X4_12_I_O_OUEUE_(A)_REV_POSITION not ON");
                MyConfig.Cfg.Model.StatusBar = "IOQA not in forward position, Please move IOQA to foward";
                MyConfig.Cfg.Model.ErrorCode = "Alert, IOQA X4_12_I_O_OUEUE_(A)_REV_POSITION not ON";

                while (hw.inputs["X4_12_I_O_OUEUE_(A)_REV_POSITION"].Read() == 0)
                {
                    Thread.Sleep(200);
                    if (MyConfig.Cfg.EMOStat)
                    {
                        RobotLoadError = true;
                        return;
                    } 
                }
            }
  
            var StatusBarUnitname = (int.Parse(UnitName) - 1).ToString();

            MyConfig.Cfg.Model.RobotCurrentStat = "Loading:" + StatusBarUnitname;
            //MyConfig.Cfg.Model.RobotStat = "Loading";
            MyConfig.Cfg.Model.StatusBar = "Robot Loading From Slot: " + StatusBarUnitname;

            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Robot Start Loading From Slot: " + StatusBarUnitname);
            hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].OFF();
            hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].OFF();
            hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].OFF();
            hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].OFF();
            hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].OFF();
            RobotPrevStat = "Loading";
            switch (UnitName)
            {
                case "0":
                    hw.outputs["Y5_00_ROBOT_BASKET_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_01_ROBOT_BASKET_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_02_ROBOT_BASKET_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_04_ROBOT_BASKET_POSITION_BIT_4"].OFF();
                    break;

                case "1":
                    hw.outputs["Y5_00_ROBOT_BASKET_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_01_ROBOT_BASKET_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_02_ROBOT_BASKET_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_04_ROBOT_BASKET_POSITION_BIT_4"].ON();
                    break;
                case "2":
                    hw.outputs["Y5_00_ROBOT_BASKET_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_01_ROBOT_BASKET_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_02_ROBOT_BASKET_POSITION_BIT_3"].ON();
                    hw.outputs["Y5_04_ROBOT_BASKET_POSITION_BIT_4"].OFF();
                    break;
                case "3":
                    hw.outputs["Y5_00_ROBOT_BASKET_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_01_ROBOT_BASKET_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_02_ROBOT_BASKET_POSITION_BIT_3"].ON();
                    hw.outputs["Y5_04_ROBOT_BASKET_POSITION_BIT_4"].ON();
                    break;
                case "4":
                    hw.outputs["Y5_00_ROBOT_BASKET_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_01_ROBOT_BASKET_POSITION_BIT_2"].ON();
                    hw.outputs["Y5_02_ROBOT_BASKET_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_04_ROBOT_BASKET_POSITION_BIT_4"].OFF();
                    break;

                case "5":
                    hw.outputs["Y5_00_ROBOT_BASKET_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_01_ROBOT_BASKET_POSITION_BIT_2"].ON();
                    hw.outputs["Y5_02_ROBOT_BASKET_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_04_ROBOT_BASKET_POSITION_BIT_4"].ON();
                    break;

                case "6":
                    hw.outputs["Y5_00_ROBOT_BASKET_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_01_ROBOT_BASKET_POSITION_BIT_2"].ON();
                    hw.outputs["Y5_02_ROBOT_BASKET_POSITION_BIT_3"].ON();
                    hw.outputs["Y5_04_ROBOT_BASKET_POSITION_BIT_4"].OFF();
                    break;

                case "7":
                    hw.outputs["Y5_00_ROBOT_BASKET_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_01_ROBOT_BASKET_POSITION_BIT_2"].ON();
                    hw.outputs["Y5_02_ROBOT_BASKET_POSITION_BIT_3"].ON();
                    hw.outputs["Y5_04_ROBOT_BASKET_POSITION_BIT_4"].ON();
                    break;

                default:
                    break;
            }

            Thread.Sleep(500);
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Robot Start Loading Y6_07_ROBOT_ACTUATOR_INPUT ON");
            //comand robot to move
            hw.outputs["Y6_07_ROBOT_ACTUATOR_INPUT"].ON();
            Thread.Sleep(300);

            //check robot status
            var stat = WaitRobot(60, "X6_05_ROBOT_LOAD_READY", "Checking Robot Load Sensor");
            if (stat == 1)
            {
                RobotLoadError = true;
                return;
            }
            else
            {
                MyConfig.Cfg.ProcessState = $"WaitRobot:Done";
            }


            MyConfig.Cfg.RobotRequestStop = false;
            //reset signals
            hw.outputs["Y5_00_ROBOT_BASKET_POSITION_BIT_1"].OFF();
            hw.outputs["Y5_01_ROBOT_BASKET_POSITION_BIT_2"].OFF();
            hw.outputs["Y5_02_ROBOT_BASKET_POSITION_BIT_3"].OFF();
            hw.outputs["Y5_04_ROBOT_BASKET_POSITION_BIT_4"].OFF();

            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Robot Start Loading Y6_07_ROBOT_ACTUATOR_INPUT OFF");
            hw.outputs["Y6_07_ROBOT_ACTUATOR_INPUT"].OFF();
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Robot Done Loading From Slot: " + StatusBarUnitname);
            MyConfig.Cfg.ProcessState = $"Loading Complete";
            MyConfig.Cfg.Model.RobotStat = "Free";
            MyConfig.Cfg.Model.RobotCurrentStat = "Idle";
        }

        private void Dip(string UnitName)
        {

            MyConfig.Cfg.Model.RobotCurrentStat = "Dipping: " + UnitName;
            MyConfig.Cfg.Model.StatusBar = "Robot Dipping to Cell: " + UnitName;
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Robot Start Dipping to Cell: " + UnitName);
            hw.outputs["Y5_00_ROBOT_BASKET_POSITION_BIT_1"].OFF();
            hw.outputs["Y5_01_ROBOT_BASKET_POSITION_BIT_2"].OFF();
            hw.outputs["Y5_02_ROBOT_BASKET_POSITION_BIT_3"].OFF();
            hw.outputs["Y5_04_ROBOT_BASKET_POSITION_BIT_4"].OFF();
            switch (UnitName)
            {

                case "0":
                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].OFF();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].OFF();

                    break;

                case "1":

                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].OFF();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].ON();

                    break;

                case "2":


                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].ON();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].OFF();
                    break;

                case "3":

                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].ON();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].ON();
                    break;

                case "4":

                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].ON();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].OFF();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].OFF();
                    break;

                case "5":

                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].ON();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].OFF();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].ON();
                    break;

                case "6":

                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].ON();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].ON();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].OFF();
                    break;

                case "7":

                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].ON();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].ON();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].ON();
                    break;

                case "8":

                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].ON();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].OFF();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].OFF();
                    break;

                case "9":
                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].ON();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].OFF();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].ON();
                    break;

                case "10":
                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].ON();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].ON();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].OFF();
                    break;

                case "11":
                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].ON();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].ON();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].ON();
                    break;

                case "12":
                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].ON();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].ON();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].OFF();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].OFF();
                    break;

                default:
                    break;
            }

            //start move to cell
            Thread.Sleep(500);
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Robot Start Dipping Y6_07_ROBOT_ACTUATOR_INPUT ON");
            hw.outputs["Y6_07_ROBOT_ACTUATOR_INPUT"].ON();
            Thread.Sleep(200);


            int stat = 0;


            hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].OFF();
            hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].OFF();
            hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].OFF();
            hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].OFF();
            hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].OFF();

            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Robot Start Dipping Y5_11_ROBOT_TANK_LOCK_READY ON");

            Thread.Sleep(250);

           
            stat = WaitRobot(60, "X6_07_ROBOT_DIP_READY", "Wait Robot Dip Ready Sensor");
            if (stat == 1)
            {
                RobotDipError = true;
                //abort

                return;
            }
            else
            {
                MyConfig.Cfg.ProcessState = $"WaitRobot:Done";
            }

            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Robot Start Dipping Y5_11_ROBOT_TANK_LOCK_READY, Y6_07_ROBOT_ACTUATOR_INPUT OFF");

            hw.outputs["Y6_07_ROBOT_ACTUATOR_INPUT"].OFF();
         
         

            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Robot Done Dipping to Cell: " + UnitName);
            MyConfig.Cfg.ProcessState = $"Dipping Complete";
            MyConfig.Cfg.Model.RobotStat = "Free";
            MyConfig.Cfg.Model.RobotCurrentStat = "Idle";
        }

        private void UnDip(string UnitName)
        {
            MyConfig.Cfg.Model.RobotCurrentStat = "Undiping:" + UnitName;
            MyConfig.Cfg.Model.StatusBar = "Robot Undip From Cell: " + UnitName;
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Robot Start Undip From Cell: " + UnitName);

            //retract first
            var valve = int.Parse(UnitName) - 1;
            var _valve = valve.ToString();

            string outputname = "Y0_" + _valve.PadLeft(2, '0') + "_INFLATE_VALVE_CELL____" + UnitName;
            hw.outputs[outputname].OFF();

            //check lock sensor
            var sensorname = SensorList[UnitName];
            int stat = TimeOut(50, sensorname.Item2, "VACCUM_SENSOR_CELL" + UnitName);

            if (stat == 1)
                UpdateUI($"BladderCell_{UnitName}", "ERROR");
            else
                UpdateUI($"BladderCell_{UnitName}", "RETRACT");


            hw.outputs["Y5_00_ROBOT_BASKET_POSITION_BIT_1"].OFF();
            hw.outputs["Y5_01_ROBOT_BASKET_POSITION_BIT_2"].OFF();
            hw.outputs["Y5_02_ROBOT_BASKET_POSITION_BIT_3"].OFF();
            hw.outputs["Y5_04_ROBOT_BASKET_POSITION_BIT_4"].OFF();
            switch (UnitName)
            {

                //intitial
                case "0":

                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].ON();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].ON();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].OFF();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].ON();
                    break;

                case "1":


                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].ON();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].ON();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].ON();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].OFF();
                    break;

                case "2":

                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].ON();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].ON();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].ON();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].ON();
                    break;

                case "3":
                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].ON();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].OFF();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].OFF();
                    break;

                case "4":

                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].ON();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].OFF();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].ON();
                    break;

                case "5":

                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].ON();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].ON();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].OFF();
                    break;

                case "6":

                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].ON();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].ON();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].ON();
                    break;

                case "7":

                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].ON();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].ON();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].OFF();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].OFF();
                    break;

                case "8":

                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].ON();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].ON();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].OFF();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].ON();
                    break;

                case "9":

                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].ON();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].ON();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].ON();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].OFF();
                    break;

                case "10":

                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].ON();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].ON();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].ON();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].ON();
                    break;

                case "11":


                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].ON();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].ON();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].OFF();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].OFF();

                    break;

                case "12":

                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].ON();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].ON();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].OFF();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].ON();

                    break;
            }


            stat = 0;

            if (stat == 1)
                Cmd(MyConfig.Var.RobotStop, "");



            Thread.Sleep(500);
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Robot Start Undip Y6_07_ROBOT_ACTUATOR_INPUT ON");
            hw.outputs["Y6_07_ROBOT_ACTUATOR_INPUT"].ON();
            Thread.Sleep(200);

       

     
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Robot Start Undip Y5_12_ROBOT_TANK_UNLOCK_READY ON");

            Thread.Sleep(200);

            stat = WaitRobot(60, "X6_09_ROBOT_RISE_READY", "Wait Robot RiseReady Sensor");
            if (stat == 1)
            {
                RobotUnDipError = true;
                return;
            }
            else
            {
                MyConfig.Cfg.ProcessState = $"WaitRobot:Done";
            }

            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Robot Start Undip Y5_12_ROBOT_TANK_UNLOCK_READY, Y6_07_ROBOT_ACTUATOR_INPUT OFF");

            //reset signals
            hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].OFF();
            hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].OFF();
            hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].OFF();
            hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].OFF();
            hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].OFF();


            hw.outputs["Y6_07_ROBOT_ACTUATOR_INPUT"].OFF();



            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Robot Undip Done From Cell: " + UnitName);
            MyConfig.Cfg.ProcessState = $"Undip Complete";
            MyConfig.Cfg.Model.RobotStat = "Free";
            MyConfig.Cfg.Model.RobotCurrentStat = "Idle";
        }

        public int TimeOut(int sec, string SensorName, string Message, int ioval = 1)
        {
            MyConfig.Cfg.ProcessState = Message;

            int stat = 0;
            int count = 0;

            if (MyConfig.Cfg.SimulationMode)
                return stat;

            var ss = hw.inputs[SensorName].Read();

            MyConfig.Cfg.ProcessState = $"Wait Sensor Timeout:{SensorName}";

            while (hw.inputs[SensorName].Read() != ioval)
            {
                Thread.Sleep(100);
                count++;
                if (count > sec)
                {
                    MyConfig.Cfg.Model.ErrorCode = "Alert," + Message;
                    stat = 1;
                    break;
                }

                //hw emo pressed
                if (MyConfig.Cfg.EMOStat == true)
                    break;

            }
            MyConfig.Cfg.ProcessState = $"Sensor Detected {SensorName}";
            return stat;

        }

        private void UpdateUI(string UiName, string val)
        {
            var propInfo = MyConfig.Cfg.Model.GetType().GetProperty(UiName);
            if (propInfo != null)
            {
                propInfo.SetValue(MyConfig.Cfg.Model, val, null);
            }
        }

        private string GetModelVal(string UiName)
        {
            string value = "";

            var propInfog = MyConfig.Cfg.Model.GetType().GetProperty(UiName);
            if (propInfog != null)
            {

                var val = propInfog.GetValue(MyConfig.Cfg.Model);
                value = val.ToString();
            }

            return value;
        }

    }
}

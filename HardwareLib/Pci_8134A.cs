
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Runtime.InteropServices;
// ERROR: Not supported in C#: OptionDeclaration
namespace AdLink
{
    static class PCI8134a
    {
        //[DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        [DllImport("8134A.dll",
             EntryPoint = "_8134_initial",
             CallingConvention = CallingConvention.StdCall)]
        public static extern short _8134_initial(ref short existCards);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_close();
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_config_from_file(string fileName);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_get_irq_channel(short CardNo, ref short irq_no);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_get_base_addr(short CardNo, ref short base_addr);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_version_info(short CardNo, ref short HardwareInfo, ref int SoftwareInfo, ref int DriverInfo);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_set_security_key(short CardNo, short old_secu_code, short New_secu_code);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_check_security_key(short CardNo, short secu_code);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_reset_security_key(short CardNo);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        //Pulse Input/Output Configuration Section 7.4
        public static extern short _8134_set_pls_outmode(short AxisNo, short pls_outmode);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_set_pls_iptmode(short AxisNo, short pls_iptmode);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_set_feedback_src(short AxisNo, short src);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_set_move_ratio(short AxisNo, double move_ratio);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        //Continuously Motion Mode Section 7.5
        public static extern short _8134_tv_move(short AxisNo, double StrVel, double MaxVel, double Tacc);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_sv_move(short AxisNo, double StrVel, double MaxVel, double Tacc, double SVacc);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_v_change(short AxisNo, double Vel, double Time);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_sd_stop(short AxisNo, double Tdec);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_emg_stop(short AxisNo);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_set_sd(short AxisNo, short enable, short sd_logic, short sd_latch, short sd_mode);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_fix_speed_range(short AxisNo, double MaxVel);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_unfix_speed_range(short AxisNo);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_get_current_speed(short AxisNo, ref double speed);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern double _8134_verify_speed(double StrVel, double MaxVel, ref double minAccT, ref double maxAccT, double MaxSpeed);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        //Trapezoidal Motion Mode Section 7.6
        public static extern short _8134_start_tr_move(short AxisNo, double Dist, double StrVel, double MaxVel, double Tacc, double Tdec);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_start_ta_move(short AxisNo, double Pos, double StrVel, double MaxVel, double Tacc, double Tdec);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        //S-Curve Profile Motion Section 7.7
        public static extern short _8134_start_sr_move(short AxisNo, double Dist, double StrVel, double MaxVel, double Tacc, double Tdec, double SVacc, double SVdec);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_start_sa_move(short AxisNo, double Pos, double StrVel, double MaxVel, double Tacc, double Tdec, double SVacc, double SVdec);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        //Multiple Axes Point to Point Motion Section 7.8
        public static extern short _8134_start_move_all(short FirstAxisNo);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_stop_move_all(short FirstAxisNo);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_set_tr_move_all(short TotalAx, ref short AxisArray, ref double DistA, ref double StrVelA, ref double MaxVelA, ref double TaccA, ref double TdecA);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_set_ta_move_all(short TotalAx, ref short AxisArray, ref double PosA, ref double StrVelA, ref double MaxVelA, ref double TaccA, ref double TdecA);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_set_sr_move_all(short TotalAx, ref short AxisArray, ref double DistA, ref double StrVelA, ref double MaxVelA, ref double TaccA, ref double TdecA, ref double SVaccA, ref double SVdecA);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_set_sa_move_all(short TotalAx, ref short AxisArray, ref double PosA, ref double StrVelA, ref double MaxVelA, ref double TaccA, ref double TdecA, ref double SVaccA, ref double SVdecA);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        //Multiple Axes simultaneous continuous move
        public static extern short _8134_set_tv_move_all(short TotalAx, ref short AxisArray, ref double StrVelA, ref double MaxVelA, ref double TaccA);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_set_sv_move_all(short TotalAx, ref short AxisArray, ref double StrVelA, ref double MaxVelA, ref double TaccA, ref double SVaccA);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_set_sync_option(short AxisNo, short sync_stop_on, short cstop_output_on);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        //Linear / Circular Interpolated Motion Section 7.9
        public static extern short _8134_start_tr_move_xy(short CardNo, double DistX, double DistY, double StrVel, double MaxVel, double Tacc, double Tdec);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_start_ta_move_xy(short CardNo, double PosX, double PosY, double StrVel, double MaxVel, double Tacc, double Tdec);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_start_sr_move_xy(short CardNo, double DistX, double DistY, double StrVel, double MaxVel, double Tacc, double Tdec, double SVacc, double SVdec);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_start_sa_move_xy(short CardNo, double PosX, double PosY, double StrVel, double MaxVel, double Tacc, double Tdec, double SVacc, double SVdec);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_start_tr_move_zu(short CardNo, double DistX, double DistY, double StrVel, double MaxVel, double Tacc, double Tdec);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_start_ta_move_zu(short CardNo, double PosX, double PosY, double StrVel, double MaxVel, double Tacc, double Tdec);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_start_sr_move_zu(short CardNo, double DistX, double DistY, double StrVel, double MaxVel, double Tacc, double Tdec, double SVacc, double SVdec);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_start_sa_move_zu(short CardNo, double PosX, double PosY, double StrVel, double MaxVel, double Tacc, double Tdec, double SVacc, double SVdec);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        //Home Return Mode Section 7.10
        public static extern short _8134_set_home_config(short AxisNo, short home_mode, short org_logic, short ez_logic, short ez_count, short erc_out);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_home_move(short AxisNo, double StrVel, double MaxVel, double Tacc);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_set_org_offset(short AxisNo, double Offset);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_set_org_logic(short AxisNo, short org_logic);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_set_bounce_filter(short AxisNo, short Value);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_set_fa_speed(short AxisNo, double FA_Speed);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        //Simultaneous home move functions
        public static extern short _8134_set_home_config_all(short totalAxis, ref short AxisNo, ref short home_mode, ref short org_logic, ref short ez_logic, ref short ez_count, ref short erc_out);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_home_move_all(short totalAxis, ref short AxisNo, ref double StrVel, ref double MaxVel, ref double Tacc);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_set_org_latch(short AxisNo, short org_latch);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_reset_when_home_finish(short AxisNo, string Enable);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        //Manual Pulser Motion Section 7.11
        public static extern short _8134_set_pulser_iptmode(short AxisNo, short InputMode, short Indep_Com);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_pulser_vmove(short AxisNo, double SpeedLimit);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_set_pulser_ratio(short AxisNo, short Value);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        //Motion Status Section 7.12
        public static extern short _8134_motion_done(short AxisNo);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        //Servo Driver Interface Section 7.13
        public static extern short _8134_set_alm(short AxisNo, short alm_logic, short alm_mode);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_set_inp(short AxisNo, short inp_enable, short inp_logic);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_set_erc_enable(short AxisNo, short erc_enable);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        //I/O Control and Monitoring Section 7.14
        public static extern short _8134_set_servo(short AxisNo, short on_off);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_get_io_status(short AxisNo, ref short io_sts);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        //Position Counter Control Section 7.15
        public static extern short _8134_get_position(short AxisNo, ref double pos);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_set_position(short AxisNo, double pos);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_get_target_pos(short AxisNo, ref double pos);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_reset_target_pos(short AxisNo, double pos);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_get_command(short AxisNo, ref int cmd);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_set_command(short AxisNo, int cmd);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_get_error_counter(short AxisNo, ref short error_counter);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_reset_error_counter(short AxisNo);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_set_feedback_error_detect(short AxisNo, int max_error);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        //Interrupt Control Section 7.16
        public static extern short _8134_int_enable(short CardNo, int phEvent);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_int_disable(short CardNo);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_int_control(short CardNo, short intFlag);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_set_int_factor(short AxisNo, int int_factor);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_get_int_status(short AxisNo, ref int int_status);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        //Speed Profile Section
        public static extern short _8134_get_tr_move_profile(short AxisNo, double Dist, double StrVel, double MaxVel, double Tacc, double Tdec, ref double pStrVel, ref double pMaxVel, ref double pTacc, ref double pTdec,
        ref double pTconst);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_get_ta_move_profile(short AxisNo, double Pos, double StrVel, double MaxVel, double Tacc, double Tdec, ref double pStrVel, ref double pMaxVel, ref double pTacc, ref double pTdec,
        ref double pTconst);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_get_sr_move_profile(short AxisNo, double Dist, double StrVel, double MaxVel, double Tacc, double Tdec, double SVacc, double SVdec, ref double pStrVel, ref double pMaxVel,
        ref double pTacc, ref double pTdec, ref double pSVacc, ref double pSVdec, ref double pTconst);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_get_sa_move_profile(short AxisNo, double Pos, double StrVel, double MaxVel, double Tacc, double Tdec, double SVacc, double SVdec, ref double pStrVel, ref double pMaxVel,
        ref double pTacc, ref double pTdec, ref double pSVacc, ref double pSVdec, ref double pTconst);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        //New Function
        public static extern short _8134_preset_tr_v_change(short AxisNo, short NumberOfChange, double Distance, double MaxVel, double Tacc, double Tdec, ref double VChange_Offset_Array, ref double VChange_Speed_Array, ref double VChange_Tacc, short Compare_Method);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_preset_v_change_status(short AxisNo, ref short int_status);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_verify_preset_v_change(double StrVel, double MaxVel, double FixSpeedRange, ref double minAccT, ref double maxAccT, ref double Resolution);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_set_axis_option(short AxisNo, short _option);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        //Reserved Functions
        public static extern short _8134_set_sd_stop_mode(short AxisNo, short sd_mode);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_set_step_unit(short AxisNo, short UnitNo);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_set_to_single_mode(short AxisX, short AxisY);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_set_sw_limit(short axisno, double p_limit, double n_limit);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_unset_sw_limit(short axisno);
        [DllImport("8134A.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short _8134_set_rdp_mode(short AxisNo, short Mode);

    }
}

//=======================================================
//Service provided by Telerik (www.telerik.com)
//Conversion powered by NRefactory.
//Twitter: @telerik
//Facebook: facebook.com/telerik
//=======================================================

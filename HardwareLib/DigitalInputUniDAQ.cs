﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UniDAQ_Ns;
using HardwareInterfaces;

namespace HardwareLib
{
    public class DigitalInputUniDAQ : IDigitalInput
    {
        public ushort CardID { get; set; } 
        public bool Disable { get; set; }
        public byte Msb {get; set;}
        public byte Lsb {get; set;}
        public int CurrentValue { get; set; }
        private byte port = 99;
        private byte portMsb;
        private byte portLsb;
        private bool simulationMode;
        private Object thisLock;
        private Dictionary<ushort, Dictionary<byte, byte>> inputValue;
        public string Name { get; set; }

        public DigitalInputUniDAQ(string name, Object thisLock, ushort cardID, 
            Dictionary<ushort, Dictionary<byte, byte>> inputValue, byte msb, byte lsb, bool simulationMode)
        {
            byte portForMsb = 99;
            byte portForLsb = 99;

            this.thisLock = thisLock;
            this.CardID = cardID;
            this.inputValue = inputValue;
            this.Msb = msb;
            this.Lsb = lsb;
            this.simulationMode = simulationMode;
            this.Name = name;
            this.Disable = false;

            var portMsbLsbInfo = Utility.GetMsbLsbPortInfo(msb, lsb);
            portForMsb = portMsbLsbInfo.Item1;
            portForLsb = portMsbLsbInfo.Item2;
            portMsb = portMsbLsbInfo.Item3;
            portLsb = portMsbLsbInfo.Item4;
            if (portForMsb == portForLsb)
            {
                port = portForMsb;
            }

            if (!inputValue.ContainsKey(cardID))
            {
                Dictionary<byte, byte> cardValue = new Dictionary<byte, byte>();
                inputValue[cardID] = cardValue;
            }
        }

        public bool ON
        {
            get
            {
                if (Disable)
                {
                    return true;
                }
                if (simulationMode)
                {
                    return true;
                }
                else
                {
                    return Read() == 1;
                }
            }
        }

        public bool OFF
        {
            get
            {
                if (Disable)
                {
                    return true;
                }
                if (simulationMode)
                {
                    return true;
                }
                else
                {
                    return Read() == 0;
                }
            }
        }


        public int Read()
        {
            int readValue;
            if (Disable)
            {
                return 0;
            }

            if (port != 99)
            {
                readValue = GetInput(port);
                return Utility.GetBits(readValue, portLsb, portMsb);
            }
            else
            {
                readValue = GetInput();
                return Utility.GetBits(readValue, Lsb, Msb);
            }
        }

        public int Get()
        {
            int currentValue;

            if (port != 99)
            {
                currentValue = PeekInput(port);
                return Utility.GetBits(currentValue, portLsb, portMsb);
            }
            else
            {
                currentValue = PeekInput();
                return Utility.GetBits(currentValue, Lsb, Msb);
            }
        }

        public void WaitON(int timeOutInSeconds, int pollTimeMiliSeconds = 50)
        {
            Wait(1, timeOutInSeconds, pollTimeMiliSeconds);
        }

        public void WaitOFF(int timeOutInSeconds, int pollTimeMiliSeconds = 50)
        {
            Wait(0, timeOutInSeconds, pollTimeMiliSeconds);
        }

        public void Wait(int waitForValue, int timeOutInSeconds, int pollTimeMiliSeconds=50)
        {
            DateTime now = DateTime.Now;
            ManualResetEvent ev;
            while (Read() != waitForValue)
            {
                if (Disable)
                {
                    return;
                }
                ev = new ManualResetEvent(false);
                ev.WaitOne(pollTimeMiliSeconds);
                if ((DateTime.Now - now).TotalSeconds > timeOutInSeconds)
                {
                    throw new DigitalInputTimeoutException(Name, 989);
                }
                if (simulationMode)
                {
                    break;
                }
            }
        }

        private short GetInput(byte port)
        {
            ushort status = 0;
            uint value = 0;

            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                }
                else
                {
                    status = UniDAQ.Ixud_ReadDI(CardID, port, ref value);
                }

                if (status >= 0)
                {
                    inputValue[CardID][port] = (byte)value;
                    return (short)value;
                }
                else
                {
                    throw new UniDaqException("Fail to read io input", status);
                }
            }
        }

        private int GetInput()
        {
            byte portNo;
            short val = 0;
            int newValue = 0;

            for (portNo = 0; portNo <= 3; portNo++)
            {
                val = GetInput(portNo);
                newValue = Utility.SetBits(newValue, portNo * 8, (portNo + 1) * 8 - 1, val);
            }
            return newValue;
        }

        private int PeekInput()
        {
            int retValue = 0;
            byte portValue;
            for (byte port = 0; port < 4; port++)
            {
                portValue = PeekInput(port);
                retValue |= portValue << (port * 7);
            }
            return retValue;
        }

        private byte PeekInput(byte port)
        {
            Dictionary<byte, byte> cardValue;

            cardValue = inputValue[CardID];
            if (!cardValue.ContainsKey(port))
            {
                cardValue[port] = 0;
            }

            return cardValue[port];
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TPM;
using System.Threading;
using HardwareInterfaces;

namespace HardwareLib
{

    public class MotionNetMotion : IMotionCard
    {
        public Object thisLock;
        private ushort cardID = 0;
        private string cfgFile = "";
        private ushort axisCount = 0;
        private ushort ringNo = 0;
        private bool simulationMode = false;

        public MotionNetMotion(Object thisLock, ushort ringNo, ushort cardID, string cfgFile, ushort axisCount, bool simulationMode)
        {
            this.thisLock = thisLock;
            this.ringNo = ringNo;
            this.cardID = cardID;
            this.cfgFile = cfgFile;
            this.axisCount = axisCount;
            this.simulationMode = simulationMode;
        }

        public void Setup()
        {
            short status = 0;
            if (simulationMode)
            {
                return;
            }
            MNet.SlaveType slavetype = 0;
            //status = MNet.Basic._mnet_get_slave_type(ringNo, cardID, ref slavetype);
            //if (status != 0) throw new AmonetMotionException("Fail to get slave type", status);

            status = MNet.M1A._mnet_m1a_initial(ringNo, cardID);
            if (status != 0) throw new AmonetMotionException("Fail to initial", status);

            status = MNet.M1A._mnet_m1a_load_motion_file(ringNo, cardID, (cfgFile + "\0").ToCharArray());
            if (status != 0) throw new AmonetMotionException("Fail to load file: " + cfgFile, status);

            //alternate
            //MNet.M1A._mnet_m1a_recovery_from_EEPROM(ringNo, cardID);
        }

        public void TearDown()
        {
        }
    }
}

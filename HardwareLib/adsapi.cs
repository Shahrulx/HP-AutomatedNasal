using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace PCI1730
{
    public partial class Adsapi
    {
        const int MAX_DRIVER_NAME_LEN = 16;

        [StructLayout(LayoutKind.Sequential)]
        public struct AOConfig
        {
            public short chan;
            public short RefSrc;
            public float MaxValue;
            public float MinValue;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct AOBinaryOut
        {
            public short chan;
            public short BinData;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct AOScale
        {
            public short chan;
            public float OutputValue;
            public int BinData;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct PT_DeviceGetFeatures
        {
            public int buffer;
            public short size;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct AOVoltageOut
        {
            public short chan;
            public float OutputValue;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct DEVFEATURES
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)] public byte[] szDriverVer;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = MAX_DRIVER_NAME_LEN)] public byte[] szDriverName;
            public int dwBoardID;
            public ushort usMaxAIDiffChl;
            public ushort usMaxAISiglChl;
            public ushort usMaxAOChl;
            public ushort usMaxDOChl;
            public ushort usMaxDIChl;
            public ushort usDIOPort;
            public ushort usMaxTimerChl;
            public ushort usMaxAlarmChl;
            public ushort usNumADBit;
            public ushort usNumADByte;
            public ushort usNumDABit;
            public ushort usNumDAByte;
            public ushort usNumGain;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)] public GainList[] glGainList;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)] public int[] dwPermutation;
        }

        public struct GainList
        {
            public ushort usGainCde;
            public float fMaxGainVal;
            public float fMinGainVal;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)] public byte[] szGainStr;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct DevList
        {
            public int dwDeviceNum;
            //[MarshalAs(UnmanagedType.ByValArray, SizeConst = 50)] public byte[] szDeviceName;
            public string szDeviceName;
            public short nNumOfSubdevices;
        }

      [DllImport("adsapi32.dll", CharSet = CharSet.Ansi)]
        public static extern int DRV_DeviceGetNumOfList(ref Int16 comm);
        [DllImport("adsapi32.dll", CharSet = CharSet.Ansi)]
        static extern int DRV_DeviceGetList(ref byte d, Int16 MaxEntries, ref Int16 OutEntries);
        [DllImport("adsapi32.dll", CharSet = CharSet.Ansi)]
        static extern int DRV_SelectDevice(IntPtr hCaller, bool GetModule, ref Int32 DeviceNum, ref byte Description);
        [DllImport("adsapi32.dll", CharSet = CharSet.Ansi)]
        public static extern int DRV_DeviceOpen(Int32 DeviceNum, ref Int32 DriverHandle);
        [DllImport("adsapi32.dll", CharSet = CharSet.Ansi)]
        public static extern int DRV_DeviceClose(ref Int32 DriverHandle);
        [DllImport("adsapi32.dll", CharSet = CharSet.Ansi)]
        unsafe static extern int DRV_DeviceGetProperty(Int32 DriverHandle, Int32 nPropertyID, byte* pBuffer, ref Int32 pLength);
        [DllImport("adsapi32.dll", CharSet = CharSet.Ansi)]
        public static extern int DRV_AOConfig(Int32 DriverHandle, ref AOConfig OConfig);
        [DllImport("adsapi32.dll", CharSet = CharSet.Ansi)]
        public static extern int DRV_AOBinaryOut(Int32 DriverHandle, ref AOBinaryOut Out);
        [DllImport("adsapi32.dll", CharSet = CharSet.Ansi)]
        unsafe static extern void DRV_GetErrorMessage(Int32 ErrorCode, ref byte ErrorMsg);
        [DllImport("adsapi32.dll", CharSet = CharSet.Ansi)]
        unsafe static extern int DRV_AOScale(Int32 DriverHandle, ref AOScale Aos);
        [DllImport("adsapi32.dll", CharSet = CharSet.Ansi)]
        unsafe static extern int DRV_DeviceGetFeatures(Int32 DriverHandle, ref PT_DeviceGetFeatures lpDevFeatures);
        [DllImport("adsapi32.dll", CharSet = CharSet.Ansi)]
        static extern int DRV_AOVoltageOut(Int32 DriverHandle, AOVoltageOut Ao);
        [DllImport("adsapi32.dll", CharSet = CharSet.Ansi)]
        static extern int DRV_AOCurrentOut(Int32 DriverHandle, AOVoltageOut Ao);
        [DllImport("adsapi32.dll", CharSet = CharSet.Ansi)]
        static extern int DRV_WriteSyncAO(Int32 DriverHandle);
        [DllImport("adsapi32.dll", CharSet = CharSet.Ansi)]
        static extern int DRV_EnableSyncAO(Int32 DriverHandle, ref Int32 Enable);
        [DllImport("adsapi32.dll", CharSet = CharSet.Ansi)]
        public static extern int AdxDioReadDiPorts(Int32 DriverHandle, UInt32 dwPortStart, UInt32 dwPortCoun, ref byte pBuffer);

        [DllImport("adsapi32.dll", CharSet = CharSet.Ansi)]
        unsafe public static extern int AdxDioWriteDoPorts(Int32 DriverHandle, UInt32 dwPortStart, UInt32 dwPortCoun, ref byte pBuffer);
        [DllImport("adsapi32.dll", CharSet = CharSet.Ansi)]
        unsafe public static extern int AdxDioGetCurrentDoPortsState(Int32 DriverHandle, UInt32 dwPortStart, UInt32 dwPortCoun, ref byte pBuffer);

        /**
        unsafe public static int myDRV_DeviceGetFeatures(Int32 DriverHandle, out PT_DeviceGetFeatures df)
        {
            byte[] buff = new byte[1000];
            DEVFEATURES d = new DEVFEATURES();
            PT_DeviceGetFeatures Gf = new PT_DeviceGetFeatures();

            int r;
            fixed (byte* p = buff)
            {
                Gf.buffer = (IntPtr)p;
                Gf.size = 1000;
                r = DRV_DeviceGetFeatures(DriverHandle, ref Gf);
                DEVFEATURES* K = (DEVFEATURES*)p;
                d = *K;
                df = new DevFeatures(d);
            }
            return r;
        }

        unsafe public static int DRV_AOScale(Int32 DriverHandle, ushort channel, float outvalue, out ushort DataBin)
        {
            AOScale a = new AOScale();
            ushort B;
            ushort* p;
            int r;
            a.chan = channel;
            a.OutputValue = outvalue;
            p = &B;
            a.pt = (IntPtr)p;
            r = DRV_AOScale(DriverHandle, ref a);
            DataBin = B;
            return r;
        }
    */

        public unsafe static string DRV_GetErrorMessage(Int32 ErrorCode)
        {
            byte[] buff = new byte[200];
            string f;
            fixed (byte* p = buff)
            {
                DRV_GetErrorMessage(ErrorCode, ref  buff[0]);
                f = Marshal.PtrToStringAnsi((IntPtr)(p));
            }
            return f;

        }
        unsafe public static int GetDevicePropertyID(Int32 DriverHandle, ref Int32 BoardI, int Property)
        {
            int r;
            int iBoardID = BoardI;
            int l = 4;
            byte* p = (byte*)&iBoardID;
            r = DRV_DeviceGetProperty(DriverHandle, (Int32)(Property), p, ref l);
            BoardI = iBoardID;

            return r;
        }


        unsafe public static int DRV_DeviceGetList(out DevList[] h)
        {
            byte[] buff = new byte[1000];
            DevList[] s;

            Int16 OutLines = 0;
            Int16 Max = 1000;
            int r;
            fixed (byte* p = buff)
            {
                r = DRV_DeviceGetList(ref  buff[0], Max, ref OutLines);
                s = new DevList[OutLines];
                for (int i = 0; i < OutLines; i++)
                {
                    DevList x = new DevList();
                    byte* su = (byte*)p + i * 56;
                    x.dwDeviceNum = Marshal.ReadInt32((IntPtr)(su));
                    su += 4;
                    //x.szDeviceName = Marshal.PtrToStringAnsi((IntPtr)(su));
                    x.szDeviceName = Marshal.PtrToStringAnsi((IntPtr)(su));
                    su += 50;
                    x.nNumOfSubdevices = Marshal.ReadInt16((IntPtr)(su));
                    s[i] = x;
                }

            }
            h = s;
            return r;
        }
        unsafe static public int DRV_SelectDevice(IntPtr hCaller, bool GetModule, ref   Int32 DeviceNum, out string Description)
        {

            byte[] dec = new byte[200];

            int r;
            fixed (byte* p = dec)
            {
                r = Adsapi.DRV_SelectDevice(hCaller, false, ref DeviceNum, ref dec[0]);
                if (r == 0)
                    Description = Marshal.PtrToStringAnsi((IntPtr)(p));
                else
                    Description = null;
            }

            return r;
        }

 

    }

 


}
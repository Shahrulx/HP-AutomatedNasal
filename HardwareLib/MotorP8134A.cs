﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using HardwareInterfaces;
using AdLink;

namespace HardwareLib
{
    //INCOMPLETE!!!!!
    public class MotorP8134A : IMotor
    {
        public string Name { get; set; }
        public MotorType MotorType { get; set; }
        public ISpeedCfg HomeSpeedCfg {get; set;}
        public ISpeedCfg RunSpeedCfg { get; set; }
        public ISpeedCfg XYRunSpeedCfg { get; set; }
        public ISpeedCfg XYZRunSpeedCfg { get; set; }
        public IDigitalOutput ClearAlarm { get; set; }

        private ushort axisCount = 0;
        private ushort ringNo = 0;
        private ushort cardID = 0;
        private bool invertSVON = false;
        private bool simulationMode = false;
        private short axisNo;
        private byte homeDir;
        private Object thisLock;
        private bool requestStop;
        public double PulsePermm { get; set; }
        public bool UnitInmm { get; set; }
        private bool homing;

        public MotorP8134A(string name, Object thisLock, ushort cardID, ushort axisCount,
            ushort axisNo, byte homeDir, bool invertSVON, ISpeedCfg homeSpeedCfg, bool simulationMode)
        {
            this.Name = name;
            this.thisLock = thisLock;
            this.cardID = cardID;
            this.axisCount = axisCount;
            this.axisNo = (short)axisNo;
            this.homeDir = homeDir;
            this.HomeSpeedCfg = homeSpeedCfg;
            this.RunSpeedCfg = homeSpeedCfg;
            this.simulationMode = simulationMode;
            this.invertSVON = invertSVON;
            this.PulsePermm = 1;
            this.UnitInmm = false;
            requestStop = false;
            homing = false;
            throw new NotImplementedException("project incomplete");
        }

        public void Start()
        {
            if (simulationMode)
            {
            }
            else
            {
            }

            SetSVOn(1);
        }

        public void SetSpeed(double StartVelocity, double MaxVelocity, double rampUpTime=0.5, double rampDownTime=0.5)
        {
            short status = 0;
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                }
                else
                {
                }
                var speedCfg = new ISpeedCfg(StartVelocity, MaxVelocity, rampUpTime, rampDownTime);
                this.RunSpeedCfg = speedCfg;
            }
        }

        public void SetSVOn(ushort svOn)
        {
            short status = 0;
            if (invertSVON)
            {
                svOn = (ushort)((svOn == 0) ? 1 : 0);
            }
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                }
                else
                {
                    status = PCI8134a._8134_set_servo(axisNo, (short)svOn);
                    if (status != Pci_8134.ERR_NoError)
                        throw new MotionException("Fail to start sv on", status);
                }
            }
        }

        public void Stop(bool emergency=false)
        {
            short status = 0;
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                }
                else
                {
                    if (emergency)
                        status = PCI8134a._8134_emg_stop(axisNo);
                    else
                        status = PCI8134a._8134_sd_stop(axisNo, RunSpeedCfg.RampDownTime);
                    if (status != Pci_8134.ERR_NoError)
                        throw new MotionException("Fail to stop axis", status);
                }
            }
            // make sure the motor stop then only ask to quit the wait
            requestStop = true;
        }

        public void MoveAbsolute(int Position, int timeOutInSeconds)
        {
            DateTime now = DateTime.Now;
            MoveAbsoluteNoWait(Position);
            WaitMotionDone(timeOutInSeconds);
        }

        public void MoveRelative(int distance, int timeOutInSeconds)
        {
            DateTime now = DateTime.Now;
            MoveRelativeNoWait(distance);
            WaitMotionDone(timeOutInSeconds);
        }

        public void MoveAbsoluteNoWait(int Position)
        {
            short status;
            requestStop = false;
            if (simulationMode)
            {
                status = 0;
            }
            else
            {
                if (!GetMotionDone())
                {
                    throw new AmonetMotionException("Start a motion before the previous motion done", (short)axisNo);
                }
                lock (thisLock)
                {
                    status = Pci_8134.start_a_move(axisNo, Position, RunSpeedCfg.StartVelocity, RunSpeedCfg.MaxVelocity, RunSpeedCfg.RampUpTime);
                    if (status != Pci_8134.ERR_NoError)
                        throw new MotionException("Fail to start a move", status);
                }
            }
        }

        public void MoveAbsoluteXY(int PositionX, int PositionY, int timeOutInSeconds)
        {
            DateTime now = DateTime.Now;
            MoveAbsoluteXYNoWait(PositionX, PositionY);
            WaitMotionDone(timeOutInSeconds);
        }

        public void MoveAbsoluteXYNoWait(int PositionX, int PositionY)
        {
            short status;
            requestStop = false;
            if (simulationMode)
            {
                status = 0;
            }
            else
            {
                if (!GetMotionDone())
                {
                    throw new AmonetMotionException("Start a motion before the previous motion done", (short)axisNo);
                }
                lock (thisLock)
                {
                    status = Pci_8134.start_sa_move_xy((short)cardID, PositionX, PositionY, XYRunSpeedCfg.StartVelocity, XYRunSpeedCfg.MaxVelocity,
                        XYRunSpeedCfg.RampUpTime, XYRunSpeedCfg.RampDownTime);
                    if (status != Pci_8134.ERR_NoError)
                        throw new MotionException("Fail to start sa move xy", status);
                }
            }
        }

        public void MoveAbsoluteXYZ(int PositionX, int PositionY, int PositionZ, int timeOutInSeconds)
        {
            DateTime now = DateTime.Now;
            MoveAbsoluteXYZNoWait(PositionX, PositionY, PositionZ);
            WaitMotionDone(timeOutInSeconds);
        }

        public void MoveAbsoluteXYZNoWait(int PositionX, int PositionY, int PositionZ)
        {
            short status;
            requestStop = false;
            if (simulationMode)
            {
                status = 0;
            }
            else
            {
                if (!GetMotionDone())
                {
                    throw new MotionException("Start a motion before the previous motion done", (short)axisNo);
                }
                lock (thisLock)
                {
                    throw new NotImplementedException("not implemented");
                }
            }
        }

        public void MoveRelativeXY(int PositionX, int PositionY, int timeOutInSeconds)
        {
            DateTime now = DateTime.Now;
            MoveRelativeXYNoWait(PositionX, PositionY);
            WaitMotionDone(timeOutInSeconds);
        }

        public void MoveRelativeXYNoWait(int PositionX, int PositionY)
        {
            short status;
            requestStop = false;
            if (simulationMode)
            {
                status = 0;
            }
            else
            {
                if (!GetMotionDone())
                {
                    throw new AmonetMotionException("Start a motion before the previous motion done", (short)axisNo);
                }
                lock (thisLock)
                {
                    status = Pci_8134.start_sr_move_xy((short)cardID, PositionX, PositionY, XYRunSpeedCfg.StartVelocity,
                        XYRunSpeedCfg.MaxVelocity, XYRunSpeedCfg.RampUpTime, XYRunSpeedCfg.RampDownTime);
                    if (status != Pci_8134.ERR_NoError)
                        throw new MotionException("Fail to start sr move xy", status);
                }
            }
        }

        public void MoveRelativeXYZ(int PositionX, int PositionY, int PositionZ, int timeOutInSeconds)
        {
            DateTime now = DateTime.Now;
            MoveRelativeXYZNoWait(PositionX, PositionY, PositionZ);
            WaitMotionDone(timeOutInSeconds);
        }

        public void MoveRelativeXYZNoWait(int PositionX, int PositionY, int PositionZ)
        {
            short status;
            requestStop = false;
            if (simulationMode)
            {
                status = 0;
            }
            else
            {
                if (!GetMotionDone())
                {
                    throw new MotionException("Start a motion before the previous motion done", (short)axisNo);
                }
                lock (thisLock)
                {
                    throw new NotImplementedException("not implemented");
                }
            }
        }

        public void MoveRelativeNoWait(int distance)
        {
            short status;
            requestStop = false;
            if (simulationMode)
            {
                status = 0;
            }
            else
            {
                if (!GetMotionDone())
                {
                    throw new AmonetMotionException("Start a motion before the previous motion done", (short)axisNo);
                }
                lock (thisLock)
                {
                    status = Pci_8134.start_r_move(axisNo, distance, RunSpeedCfg.StartVelocity, RunSpeedCfg.MaxVelocity, RunSpeedCfg.RampUpTime);
                    if (status != Pci_8134.ERR_NoError)
                        throw new MotionException("Fail to run start_r_move", status);
                }
            }
        }

        public bool GetMotionDone()
        {
            ushort motion = 0;
            short status;

            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                    motion = 0;
                }
                else
                {
                    status = Pci_8134.motion_done(axisNo);
                    motion = (ushort)status;
                    if (status == 5)
                        throw new MotionAlarm("Motion has alarm", 5);
                }

            }
            bool motionDone = (motion != 0);
            if (motionDone && homing)
            {
                homing = false;
                Reset();
            }
            return motionDone;
        }

        public int GetPosition()
        {
            short status;
            double Pos = 0;
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                    Pos = 100;
                }
                else
                {
                    status = Pci_8134.get_position(axisNo, ref Pos);
                    if (status != Pci_8134.ERR_NoError)
                        throw new MotionException("Fail to get position", status);
                }
            }
            return (int)Pos;
        }

        public int GetErrorCounter()
        {
            short status;
            int errorCounter = 0;
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                    errorCounter = 100;
                }
                else
                {
                    errorCounter = 0;
                }
            }
            return errorCounter;
        }

        public void ResetErrorCounter()
        {
            short status;
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                }
                else
                {
                    status = 0;
                }
            }
        }

        public int GetCommand()
        {
            short status;
            double cmdCounter = 0;
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                    cmdCounter = 100;
                }
                else
                {
                    status = Pci_8134.get_command(axisNo, ref cmdCounter);
                    if (status != Pci_8134.ERR_NoError)
                        throw new MotionException("Fail to get command", status);
                }
            }
            return (int)cmdCounter;
        }

        public void Home(int timeOutInSeconds)
        {
            HomeNoWait();
            WaitMotionDone(timeOutInSeconds);
            SetSpeed(RunSpeedCfg.StartVelocity, RunSpeedCfg.MaxVelocity, RunSpeedCfg.RampUpTime, RunSpeedCfg.RampDownTime);
        }

        public void WaitMotionDone(int timeOutInSeconds)
        {
            DateTime now = DateTime.Now;
            ManualResetEvent ev;
            while (!GetMotionDone())
            {
                ev = new ManualResetEvent(false);
                ev.WaitOne(50);
                if (requestStop)
                {
                    requestStop = false;
                    break;
                }
                if ((DateTime.Now - now).TotalSeconds > timeOutInSeconds)
                {
                    throw new MotionTimeout(string.Format("Motion axis {0} timeout on waiting motion done", axisNo));
                }
            }
        }

        public void WaitXYMotionDone(int timeOutInSeconds)
        {
            WaitMotionDone(timeOutInSeconds);
        }

        public void WaitXYZMotionDone(int timeOutInSeconds)
        {
            WaitMotionDone(timeOutInSeconds);
        }

        public void HomeNoWait()
        {
            short status;
            requestStop = false;
            homing = true;
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                }
                else
                {
                    status = Pci_8134.home_move(axisNo, HomeSpeedCfg.StartVelocity, HomeSpeedCfg.MaxVelocity, HomeSpeedCfg.RampUpTime);
                    if (status != Pci_8134.ERR_NoError)
                        throw new MotionException("fail to home move", status);
                }
            }
        }

        public void Reset()
        {
            short status;
            // Start homing
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                }
                else
                {
                    status = Pci_8134.set_command(axisNo, 0);
                    if (status != Pci_8134.ERR_NoError)
                        throw new MotionException("fail to set command 0", status);

                    status = Pci_8134.set_position(axisNo, 0);
                    if (status != Pci_8134.ERR_NoError)
                        throw new MotionException("fail to set position 0", status);
                }
            }

        }

        public void ResetAlarm()
        {
            short status;
            ManualResetEvent ev = new ManualResetEvent(false);
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                }
                else
                {
                    //pass
                }

                ev.WaitOne(100);
                
                if (simulationMode)
                {
                    status = 0;
                }
                else
                {
                    //pass
                }
            }
        }

        public IOStatus GetIOStatus()
        {
            short status;
            short IO_status = 0;
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                }
                else
                {
                    status = Pci_8134.get_io_status(axisNo, ref IO_status);
                    if (status != Pci_8134.ERR_NoError)
                        throw new MotionException("Fail to get io status", status);
                }
            }
            var ioStatus = new IOStatus(0);
            ioStatus.positiveLimit = (byte)((IO_status >> 0) & 1);
            ioStatus.negativeLimit = (byte)((IO_status >> 1) & 1);
            ioStatus.slowDownSignalInput = (byte)((IO_status >> 2) & 1);
            ioStatus.origin = (byte)((IO_status >> 4) & 1);
            ioStatus.indexSignal = (byte)((IO_status >> 5) & 1);
            ioStatus.alarm = ((IO_status >> 6) & 1) == 1;
            ioStatus.servoOn = ((IO_status >> 7) & 1) == 1;
            ioStatus.ready = ((IO_status >> 8) & 1) == 1;
            ioStatus.ERCPinOutput = (byte)((IO_status >> 10) & 1);
            ioStatus.inPosition = ((IO_status >> 11) & 1) == 1;
            if (invertSVON)
            {
                ioStatus.servoOn = (ioStatus.servoOn) ? false : true;
            }
            return ioStatus;
        }

        public bool GetAlarm()
        {
            return GetIOStatus().alarm;
        }

        public bool GetRDY()
        {
            return GetIOStatus().ready;
        }

        public bool GetInPosition()
        {
            return GetIOStatus().inPosition;
        }

        public uint GetErrorStatus()
        {
            uint errorStatus = 0;
            lock (thisLock)
            {
                if (simulationMode)
                {
                    errorStatus = 100;
                }
                else
                {
                    errorStatus = 0;
                }
            }
            return errorStatus;
        }

        public void LimitSearchNoWait(int direction)
        {
            throw new NotImplementedException();
        }

    }
}

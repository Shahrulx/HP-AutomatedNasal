﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HardwareLib
{
    static class Utility
    {
        public static Tuple<byte, byte, byte, byte> GetMsbLsbPortInfo(byte msb, byte lsb)
        {
            byte portForMsb = 99;
            byte portForLsb = 99;
            byte portMsb = 0;
            byte portLsb = 0;
            byte min;
            byte max;

            if (lsb > msb)
            {
                throw new AmonetException("Get input lsb > msb", 999);
            }

            for (byte port = 0; port < 8; port++)
            {
                min = (byte)(port * 8);
                max = (byte)(min + 7);
                if (msb >= min && msb <= max)
                {
                    portForMsb = port;
                    portMsb = (byte)(msb - min);
                }
                if (lsb >= min && lsb <= max)
                {
                    portForLsb = port;
                    portLsb = (byte)(lsb - min);
                }
                if (portForMsb != 99 && portForLsb != 99)
                    break;
            }
            return Tuple.Create<byte, byte, byte, byte>(portForMsb, portForLsb, portMsb, portLsb);
        }

        public static int SetBits(int value, int lowerBit, int upperBit, int writeValue)
        {
            int mask = (1 << (upperBit - lowerBit + 1)) - 1;
            value &= ~(mask << lowerBit);
            value |= (writeValue << lowerBit);
            return value;
        }

        public static int GetBits(int value, int lowerBit, int upperBit)
        {
            int mask = (1 << (upperBit - lowerBit + 1)) - 1;
            return (value >> lowerBit) & mask;
        }

    }
}

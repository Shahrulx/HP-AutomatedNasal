﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Automation.BDaq;

namespace HardwareLib
{
    public class DAQNaviCard
    {
        private bool simulationMode = false;

        public Object thisLock = new Object();
        public Dictionary<ushort, Dictionary<byte, byte>> inputValue = new Dictionary<ushort, Dictionary<byte, byte>>();
        public Dictionary<ushort, Dictionary<byte, byte>> outputValue = new Dictionary<ushort, Dictionary<byte, byte>>();
        public Dictionary<string, DeviceInformation> devs = new Dictionary<string, DeviceInformation>();
        public DeviceInformation devInformation;

        public DAQNaviCard(bool simulationMode)
        {
            this.simulationMode = simulationMode;
        }

        public void AddCard(string cardDesc)
        {
            if (!devs.ContainsKey(cardDesc))
            {
                var dev = new DeviceInformation(cardDesc);
                devs[cardDesc] = dev;
            }
        }

    }
}

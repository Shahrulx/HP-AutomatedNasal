﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using HardwareInterfaces;
using PCI.PS400;

namespace HardwareLib
{

    public class PISOMotion : IMotionCard
    {
        public Object thisLock;
        private ushort cardID = 0;
        private string cfgFile = "";
        private ushort axisCount = 0;
        private ushort ringNo = 0;
        private bool simulationMode = false;
        

        public PISOMotion(ushort cardID, string cfgFile, ushort axisCount, bool simulationMode)
        {
            this.thisLock = new Object();
            this.cardID = cardID;
            this.cfgFile = cfgFile;
            this.axisCount = axisCount;
            this.simulationMode = simulationMode;
        }

        public void Setup()
        {
            short errorCode = 0;
            if (simulationMode)
            {
                return;
            }

            errorCode = Functions.ps400_open_all();
            if (errorCode != 0)
            {
                throw new PISOException("Fail to open all PISO cards", errorCode);
            }

            errorCode = Functions.ps400_load_config(cfgFile);
            if (errorCode != 0)
            {
                throw new PISOException("Fail to load config", errorCode);
            }

        }

        public void TearDown()
        {
            short errorCode = Functions.ps400_close_all();
            if (errorCode != 0)
            {
                throw new PISOException("Fail to close all PISO cards", errorCode);
            }
        }

    }


}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using PCI1730;


namespace HardwareLib
{
    public class PCI1730DaqControl
    {
        private bool simulationMode = false;

        public Object thisLock = new Object();
        private bool m_Initialized = false;
        public Dictionary<ushort, Dictionary<byte, byte>> inputValue = new Dictionary<ushort, Dictionary<byte, byte>>();
        public Dictionary<ushort, Dictionary<byte, byte>> outputValue = new Dictionary<ushort, Dictionary<byte, byte>>();
        public Dictionary<int, int> DriverHandlers = new Dictionary<int, int>();

        public PCI1730DaqControl(bool simulationMode)
        {
            this.simulationMode = simulationMode;
        }

        public void Setup()
        {
            int status;
            short count = 0;
            if (m_Initialized || simulationMode)
            {
                return;
            }

            m_Initialized = true;
            status = Adsapi.DRV_DeviceGetNumOfList(ref count);
            if (status != 0)
                throw new Exception($"Fail to get PCI1730 list with return code: {status}");
            if (count == 0)
                throw new Exception("No PCI1730 card found");


            /*
            byte buffer = 0;
            status = Adsapi.DRV_DeviceOpen(0, ref DriverHandle);
            status = Adsapi.AdxDioReadDiPorts(DriverHandle, 0, 1, ref buffer);
            buffer = 7;
            status = Adsapi.AdxDioWriteDoPorts(DriverHandle, 0, 1, ref buffer);
            
            int DriverHandle2 = 0;
            status = Adsapi.DRV_DeviceOpen(1, ref DriverHandle2);
            status = Adsapi.AdxDioReadDiPorts(DriverHandle2, 0, 1, ref buffer);

            status = Adsapi.AdxDioGetCurrentDoPortsState(DriverHandle, 0, 1, ref buffer);
            */
        }

        private void AddCard(int cardID)
        {
            if (!DriverHandlers.ContainsKey(cardID))
            {
                int status = 0;
                int DriverHandle = 0;
                if (!simulationMode)
                {
                    status = Adsapi.DRV_DeviceOpen(cardID, ref DriverHandle);
                }
                if (status != 0)
                    throw new Exception($"Fail to initialize PCI1730 cardID: {cardID} with return code: {status}");
                DriverHandlers[cardID] = DriverHandle;
            }
        }

        public int GetHandler(int cardID)
        {
            if (!DriverHandlers.ContainsKey(cardID))
                AddCard(cardID);
            return DriverHandlers[cardID];
        }

        public void TearDown()
        {
            int status = 0;
            m_Initialized = false;
            foreach(var pair in DriverHandlers)
            {
                int handle = pair.Value;
                Adsapi.DRV_DeviceClose(ref handle);
                if (status < 0)
                    throw new Exception($"Fail to close PCI1730 cardID: {pair.Key} with return code: {status}");
            }
        }
    }
}

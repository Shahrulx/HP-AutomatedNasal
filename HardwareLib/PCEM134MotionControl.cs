﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HardwareInterfaces;
using TPM2;

namespace HardwareLib
{
    public class PCEM134MotionControl : IMotionCard
    {
        public Object thisLock;
        private uint cardID = 0;
        private uint DeviceID = 0;
        private string cfgFile = "";
        private bool simulationMode = false;

        public PCEM134MotionControl(uint cardID, string cfgFile, bool simulationMode)
        {
            this.thisLock = new Object();
            this.cardID = cardID;
            this.cfgFile = cfgFile;
            this.simulationMode = simulationMode;
        }

        public void Setup()
        {
            ushort totalCards = 2;
            ushort cardNo = 0;
            if (simulationMode)
            {
                return;
            }
            short ret;
            ret = Master.PCE_M134._m134_open(ref totalCards);
            if (ret != (short)Master.PCE_M134.ErrCode.M134ERR_NoError)
            {
                throw new Exception("fail to open M134 motion card");
            }
            if (totalCards == 0)
            {
                throw new Exception("No PCM134 motion device found");
            }
            
            ret = Master.PCE_M134._m134_get_switch_card_num((ushort)this.cardID, ref cardNo);
            if (ret != (short)Master.PCE_M134.ErrCode.M134ERR_NoError)
            {
                throw new Exception("fail to get cardNo");
            }

            char[] filePath = (this.cfgFile + "\0").ToCharArray();
            ret = Master.PCE_M134._m134_config_from_file(cardNo, filePath);
            if (ret != (short)Master.PCE_M134.ErrCode.M134ERR_NoError)
            {
                throw new Exception("fail to configure M134 motion card");
            }

            ret = Master.PCE_M134._m134_initial(cardNo);
            if (ret != (short)Master.PCE_M134.ErrCode.M134ERR_NoError)
            {
                throw new Exception("fail to initialize M134 motion card");
            }

            Master.PCE_M134._m134_set_feedback_src(0, 0, 0);
            Master.PCE_M134._m134_set_feedback_src(0, 1, 0);
            Master.PCE_M134._m134_set_abs_reference(0, 0, 0);
            Master.PCE_M134._m134_set_abs_reference(0, 1, 0);

            Master.PCE_M134._m134_set_pls_outmode(0, 0, 0);
            Master.PCE_M134._m134_set_pls_outmode(0, 1, 0);

            Master.PCE_M134._m134_set_pls_iptmode(0, 0, 2, 1);
            Master.PCE_M134._m134_set_pls_iptmode(0, 1, 2, 0);



        }
    }
}

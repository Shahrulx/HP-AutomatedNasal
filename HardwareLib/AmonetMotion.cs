﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AMONetLib;
using System.Threading;
using HardwareInterfaces;

namespace HardwareLib
{

    public class AmonetMotion : IMotionCard
    {
        public Object thisLock;
        private ushort cardID = 0;
        private string cfgFile = "";
        private ushort axisCount = 0;
        private ushort ringNo = 0;
        private bool simulationMode = false;

        public AmonetMotion(Object thisLock, ushort ringNo, ushort cardID, string cfgFile, ushort axisCount, bool simulationMode)
        {
            this.thisLock = thisLock;
            this.ringNo = ringNo;
            this.cardID = cardID;
            this.cfgFile = cfgFile;
            this.axisCount = axisCount;
            this.simulationMode = simulationMode;
        }

        public void Setup()
        {
            short status = 0;
            if (simulationMode)
            {
                return;
            }

            if (axisCount == 2)
            {
                status = AMONet._mnet_m2_initial(ringNo, cardID);
                if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to initialize motion", status);
                Thread.Sleep(100);
                status = AMONet._mnet_m2_loadconfig(ringNo, cardID, cfgFile);
                if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to load cfgFile", status);
            }
            else if (axisCount == 4)
            {
                status = AMONet._mnet_m4_initial(ringNo, cardID);
                if (status != AMONet.ERR_NoError)
                {
                    //retry again
                    status = AMONet._mnet_m4_initial(ringNo, cardID);
                    if (status != AMONet.ERR_NoError)
                    throw new AmonetMotionException("Fail to initialize motion", status);
                }
                Thread.Sleep(100);
                status = AMONet._mnet_m4_loadconfig(ringNo, cardID, cfgFile);
                if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to load cfgFile", status);
            }
        }

        public void TearDown()
        {
        }

    }


}

// TRANSINFO: Option Strict On
using System.Runtime.InteropServices; 

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;

namespace UniDAQ_Ns
{
    // TRANSMISSINGCOMMENT: Class UniDAQ
    // following class was VB module
    public class UniDAQ
    {
        //ModelNumber
        public const uint PIOD56=		0x800140;
        public const uint PIOD48=		0x800130;
        public const uint PIOD64=		0x800120;
        public const uint PIOD96=		0x800110;
        public const uint PIOD144=		0x800100;
        public const uint PIOD168=		0x800150;
        public const uint PIODA	=	    0x800400;
        public const uint PIO821=		0x800310;
        public const uint PIO827=		0xFF0000;

        public const uint PISOP16R16U=  0x1800FF;
        public const uint PISOC64=		0x800800;
        public const uint PISOP64=		0x800810;
        public const uint PISOA64=		0x800850;
        public const uint PISOP32C32=	0x800820;
        public const uint PISOP32A32=	0x800870;
        public const uint PISOP8R8=	    0x800830;
        public const uint PISO730=		0x800840;
        public const uint PISO730A=	    0x800880;
        public const uint PISO725=		0x8008FF;
        public const uint PISODA2=		0x800B00;

        public const uint PISO813=		0x800A00;

        public const uint PCITMC12=	    0xDF2962;
        public const uint PCIM1024=	    0xDEA074;
        public const uint PCIM512=		0xDE9562;
        public const uint PCIM256=		0xDE92A6;
        public const uint PCIM128=		0xDE9178;
        public const uint PCID64=		0xDE3513;
        public const uint PCI827=		0xDE3828; 

        public const uint PCI100x=		0x341002;
        public const uint PCI1202=		0x345672;
        public const uint PCI1602=		0x345676;
        public const uint PCI180x=		0x345678;
        public const uint PCIP8R8=		0xD6102B;
        public const uint PCIP16R16=	0xD61E39;

                // return code
        public const uint Ixud_NoErr    =                   0;
        public const uint Ixud_OpenDriverErr=               1;
        public const uint Ixud_PnPDriverErr=				 2;
        public const uint Ixud_DriverNoOpen=                3;
        public const uint Ixud_GetDriverVersionErr=         4;
        public const uint Ixud_ExceedBoardNumber=			 5;
        public const uint Ixud_FindBoardErr=				 6;
        public const uint Ixud_BoardMappingErr=			 7;
        public const uint Ixud_DIOModesErr=				 8;
        public const uint Ixud_InvalidAddress=				 9;
        public const uint Ixud_InvalidSize=				10;
        public const uint	Ixud_InvalidPortNumber=			11;
        public const uint Ixud_UnSupportedModel=			12;//未支援此板卡
        public const uint Ixud_UnSupportedFun=				13;//未支援此函式
        public const uint Ixud_InvalidChannelNumber=		14;
        public const uint Ixud_InvalidValue		=		15;
        public const uint Ixud_InvalidMode	=			16;
        public const uint Ixud_GetAIStatusTimeOut	=		17;//取得AI狀態超過時間
        public const uint Ixud_TimeOutErr	=				18;//愈期
        public const uint Ixud_CfgCodeIndexErr	=		19;//找不到適合的ConfigCode表格索引值
        public const uint Ixud_ADCCTLTimeoutErr	=		20;//AD Controller Time Out
        public const uint Ixud_FindPCIIndexErr	=		21;//找不到適合的PCI表格索引值 
        public const uint Ixud_InvalidSetting	=			22;//不合法的設定值
        public const uint Ixud_AllocateMemErr	 =   		23;//分配記憶體空間失敗

        public const uint Ixud_InstallEventErr	=		24;//安裝中斷事件失敗
        public const uint Ixud_InstallIrqErr	=			25;//安裝中斷IRQ失敗
        public const uint Ixud_RemoveIrqErr		=		26;//移除IRQ失敗
        public const uint Ixud_ClearIntCountErr=			27;//清除中斷計數量失敗
        public const uint Ixud_GetSysBufferErr	=		28;//取得系統buffer失敗
        public const uint Ixud_CreateEventErr	=			29;//CreateEvent 失敗
        public const uint Ixud_UnSupportedResolution=		30;//未支援此解析度
        public const uint Ixud_CreateThreadErr	=		31;//建立執行緒失敗
        public const uint Ixud_ThreadTimeOutErr		=	32;//執行緒愈時
        public const uint Ixud_FIFOOverFlowErr	=		33;//FIFO OverFlow
        public const uint Ixud_FIFOTimeOutErr=				34;
        public const uint Ixud_GetIntInstStatus	=		35;//取得中斷安裝狀態錯誤
        public const uint Ixud_GetBufStatus	=			36;//指定SYS的Buffer狀態
        public const uint Ixud_SetBufCountErr	=		    37;//設定Ioctrl buf錯誤
        public const uint Ixud_SetBufInfoErr  =            38;//取得sys內的buf狀態錯誤
        public const uint Ixud_FindCardIDErr	=			39;//取得版卡號碼錯誤(找不到對應的Card ID)
        public const uint Ixud_EventThreadErr	=			40;//EventThread Err
        public const uint Ixud_AutoCreateEventErr	=		41;//
        public const uint Ixud_RegThreadErr	=			42;
        public const uint Ixud_SearchEventErr	=			43;
        public const uint Ixud_FifoResetErr		=		44;//清除FIFO錯誤
        public const uint Ixud_InvalidBlock	=			44;//EEP Block 設定錯誤
        public const uint Ixud_InvalidAddr		=		45;//EEP Address 設定錯誤
        public const uint Ixud_AcqireSpinLock	=			46;//獲取SpinLock失敗
        public const uint Ixud_ReleaseSpinLock		=	47;//釋放SpinLock失敗
        public const uint Ixud_SetControlErr = 48;

        [StructLayout(LayoutKind.Sequential)]
        public struct IXUD_DEVICE_INFO
        {
            public UInt32 dwSize;
            public UInt16 wVendorID;
            public UInt16 wDeviceID;
            public UInt16 wSubVendorID;
            public UInt16 wSubDeviceID;
            // <VBFixedArray(5)> Public dwBAR() As UInt32
            public UInt32 dwBAR0;
            public UInt32 dwBAR1;
            public UInt32 dwBAR2;
            public UInt32 dwBAR3;
            public UInt32 dwBAR4;
            public UInt32 dwBAR5;
            public byte BusNo;
            public byte DevNo;
            public byte IRQ;
            public byte Aux;
            // <VBFixedArray(5)> Public dwReserved1() As UInt32   'Reserver 
            public UInt32 dwReserved10; // Reserver 
            public UInt32 dwReserved11; // Reserver
            public UInt32 dwReserved12; // Reserver 
            public UInt32 dwReserved13; // Reserver 
            public UInt32 dwReserved14; // Reserver 
            public UInt32 dwReserved15; // Reserver 
            // Sub Initialize()
            //    ReDim dwBAR(5)
            //    ReDim dwReserved1(5)
            //  End Sub
        }


        [StructLayout(LayoutKind.Sequential)]
        public struct IXUD_CARD_INFO
        {

            public UInt32 dwSize; // Structure size
            public UInt32 dwModelNo; // Model Number

            // CardID is update when calling the function each time.
            public byte CardID; // for new cards, 0xFF=N/A
            public byte wSingleEnded; // for new cards,1:S.E 2:D.I.F,0xFF=N/A
            public UInt16 wReserved; // Reserver

            public UInt16 wAIChannels; // Number of AI channels(AD)
            public UInt16 wAOChannels; // Number of AO channels(DA)

            public UInt16 wDIPorts; // Number of DI ports
            public UInt16 wDOPorts; // Number of DO ports

            public UInt16 wDIOPorts; // Number of DIO ports
            public UInt16 wDIOPortWidth; // The width is 8/16/32 bit.

            public UInt16 wCounterChannels; // Number of Timers/Counters
            public UInt16 wMomorySize; // PCI-M512==>512, Units in KB.
            public UInt32 dwReserved10; // Reserver 
            public UInt32 dwReserved11; // Reserver
            public UInt32 dwReserved12; // Reserver 
            public UInt32 dwReserved13; // Reserver 
            public UInt32 dwReserved14; // Reserver 
            public UInt32 dwReserved15; // Reserver

            // <VBFixedArray(5)> Public dwReserved1() As UInt32   'Reserver 
            // Sub Initialize()
            //     ReDim dwReserved1(5)
            // End Sub

        }


        public static IXUD_DEVICE_INFO a;
        public static IXUD_CARD_INFO b;


        // User Config Code
        public const UInt16 IXUD_BI_10V = ((ushort)(0)); // //Bipolar +/- 10V
        public const UInt16 IXUD_BI_5V = ((ushort)(1)); // //Bipolar +/-  5V
        public const UInt16 IXUD_BI_2V5 = ((ushort)(2)); // //Bipolar +/-  2.5V
        public const UInt16 IXUD_BI_1V25 = ((ushort)(3)); // //Bipolar +/-  1.25V
        public const UInt16 IXUD_BI_0V625 = ((ushort)(4)); // //Bipolar +/-  0.625V
        public const UInt16 IXUD_BI_0V3125 = ((ushort)(5)); // //Bipolar +/-  0.3125V
        public const UInt16 IXUD_BI_0V5 = ((ushort)(6)); // //Bipolar +/-  0.5V
        public const UInt16 IXUD_BI_0V05 = ((ushort)(7)); // //Bipolar +/-  0.05V
        public const UInt16 IXUD_BI_0V005 = ((ushort)(8)); // //Bipolar +/-  0.005V
        public const UInt16 IXUD_BI_1V = ((ushort)(9)); // //Bipolar +/-  1V
        public const UInt16 IXUD_BI_0V1 = ((ushort)(10)); // //Bipolar +/-  0.1V
        public const UInt16 IXUD_BI_0V01 = ((ushort)(11)); // //Bipolar +/-  0.01V
        public const UInt16 IXUD_BI_0V001 = ((ushort)(12)); // //Bipolar +/-  0.001V
        public const UInt16 IXUD_UNI_20V = ((ushort)(13)); // //Unipolar 0 ~ 20V
        public const UInt16 IXUD_UNI_10V = ((ushort)(14)); // //Unipolar 0 ~ 10V
        public const UInt16 IXUD_UNI_5V = ((ushort)(15)); // //Unipolar 0 ~  5V
        public const UInt16 IXUD_UNI_2V5 = ((ushort)(16)); // //Unipolar 0 ~  2.5V
        public const UInt16 IXUD_UNI_1V25 = ((ushort)(17)); // //Unipolar 0 ~  1.25V
        public const UInt16 IXUD_UNI_0V625 = ((ushort)(18)); // //Unipolar 0 ~  0.625V
        public const UInt16 IXUD_UNI_1V = ((ushort)(19)); // //Unipolar 0 ~  1V
        public const UInt16 IXUD_UNI_0V1 = ((ushort)(20)); // //Unipolar 0 ~  0.1V
        public const UInt16 IXUD_UNI_0V01 = ((ushort)(21)); // //Unipolar 0 ~  0.01V
        public const UInt16 IXUD_UNI_0V001 = ((ushort)(22)); // //Unipolar 0 ~  0.001V

       
        public delegate UInt32 CallBackFun(UInt32 Param);

        // TRANSMISSINGCOMMENT: Method ZeroMemory
        [DllImport("kernel32.dll")]
        public static extern void ZeroMemory(IntPtr addr, int size);

        //  The Driver functions
        [DllImport("UniDAQ.dll")]
        public static extern UInt16 Ixud_GetDllVersion(ref UInt32 dwDLLver);

        // TRANSMISSINGCOMMENT: Method Ixud_DriverInit
        [DllImport("UniDAQ.dll")]
        public static extern UInt16 Ixud_DriverInit(ref UInt16 wTotalBoard);
        // TRANSMISSINGCOMMENT: Method Ixud_DriverClose
        [DllImport("UniDAQ.dll")]
        public static extern UInt16 Ixud_DriverClose();
        // TRANSMISSINGCOMMENT: Method Ixud_GetBoardNoByCardID
        [DllImport("UniDAQ.dll")]
        public static extern UInt16 Ixud_GetBoardNoByCardID(ref UInt16 wBoardNo, UInt32 dwModelNumber, UInt16 wCardID);
        // <DllImport("UniDAQ.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall, SetLastError:=True)> _
        // Public Function Ixud_GetCardInfo(ByVal wBoardNo As UInt16, ByVal a As IXUD_DEVICE_INFO, ByVal b As IXUD_CARD_INFO, ByVal szModelName As String) As UInt16

        // End Function

        [DllImport("UniDAQ.dll")]
        public static extern UInt16 Ixud_GetCardInfo(UInt16 wBoardNo, ref IXUD_DEVICE_INFO a, ref IXUD_CARD_INFO b, [MarshalAs(UnmanagedType.LPArray)]byte[] szModelName);

        // TRANSMISSINGCOMMENT: Method Ixud_ReadPort
        [DllImport("UniDAQ.dll")]
        public static extern UInt16 Ixud_ReadPort(UInt32 dwAddress, UInt16 wsize, ref UInt32 dwVal);

        // TRANSMISSINGCOMMENT: Method Ixud_WritePort
        [DllImport("UniDAQ.dll")]
        public static extern UInt16 Ixud_WritePort(UInt32 dwAddress, UInt16 wsize, UInt32 dwVal);
        // TRANSMISSINGCOMMENT: Method Ixud_SetDIOModes32
        [DllImport("UniDAQ.dll")]
        public static extern UInt16 Ixud_SetDIOModes32(UInt16 wBoardNo, UInt32 dwDirections);

        // TRANSMISSINGCOMMENT: Method Ixud_SetDIOMode
        [DllImport("UniDAQ.dll")]
        public static extern UInt16 Ixud_SetDIOMode(UInt16 wBoardNo, UInt16 wPortNo, UInt16 wDirection);
        // TRANSMISSINGCOMMENT: Method Ixud_ReadDI
        [DllImport("UniDAQ.dll")]
        public static extern UInt16 Ixud_ReadDI(UInt16 wBoardNo, UInt16 wPortNo, ref UInt32 dwDIVal);

        // TRANSMISSINGCOMMENT: Method Ixud_WriteDO
        [DllImport("UniDAQ.dll")]
        public static extern UInt16 Ixud_WriteDO(UInt16 wBoardNo, UInt16 wPortNo, UInt32 dwDOVal);

        // TRANSMISSINGCOMMENT: Method Ixud_ReadCounter
        [DllImport("UniDAQ.dll")]
        public static extern UInt16 Ixud_ReadCounter(UInt16 wBoardNo, UInt16 wChannelNo, ref UInt32 dwVal);
        // TRANSMISSINGCOMMENT: Method Ixud_SetCounter
        [DllImport("UniDAQ.dll")]
        public static extern UInt16 Ixud_SetCounter(UInt16 wBoardNo, UInt16 wChannelNo, UInt16 wMode, UInt32 dwVal);

        // TRANSMISSINGCOMMENT: Method Ixud_DisableCounter
        [DllImport("UniDAQ.dll")]
        public static extern UInt16 Ixud_DisableCounter(UInt16 wBoardNo, UInt16 wChannelNo);

        // TRANSMISSINGCOMMENT: Method Ixud_ReadMemory
        [DllImport("UniDAQ.dll")]
        public static extern UInt16 Ixud_ReadMemory(UInt16 wBoardNo, UInt32 dwOffset, UInt16 Size, ref UInt32 dwValue);
        // TRANSMISSINGCOMMENT: Method Ixud_WriteMemory
        [DllImport("UniDAQ.dll")]
        public static extern UInt16 Ixud_WriteMemory(UInt16 wBoardNo, UInt32 dwOffset, UInt16 Size, ref UInt32 dwValue);

        // TRANSMISSINGCOMMENT: Method Ixud_ReadAI
        [DllImport("UniDAQ.dll")]
        public static extern UInt16 Ixud_ReadAI(UInt16 wBoardNo, UInt16 wChannel, UInt16 wConfig, ref float fValue);
        // TRANSMISSINGCOMMENT: Method Ixud_ReadAIH
        [DllImport("UniDAQ.dll")]
        public static extern UInt16 Ixud_ReadAIH(UInt16 wBoardNo, UInt16 wChannel, UInt16 wConfig, ref UInt32 dwValue);

        // TRANSMISSINGCOMMENT: Method Ixud_ConfigAI
        [DllImport("UniDAQ.dll")]
        public static extern UInt16 Ixud_ConfigAI(UInt16 wBoardNo, UInt16 wFIFOSizeKB, UInt32 BufferSizeKB, UInt16 wCardType, UInt16 wDelaySettlingTime);

        // TRANSMISSINGCOMMENT: Method Ixud_PollingAI
        [DllImport("UniDAQ.dll")]
        public static extern UInt16 Ixud_PollingAI(UInt16 wBoardNo, UInt16 wChannel, UInt16 wConfig, UInt32 dwDataCount, ref float fAIBuf);
        // TRANSMISSINGCOMMENT: Method Ixud_PollingAIH
        [DllImport("UniDAQ.dll")]
        public static extern UInt16 Ixud_PollingAIH(UInt16 wBoardNo, UInt16 wChannel, UInt16 wConfig, UInt32 dwDataCount, ref UInt32 dwAIBuf);
        // TRANSMISSINGCOMMENT: Method Ixud_PollingAIScan
        [DllImport("UniDAQ.dll")]
        public static extern UInt16 Ixud_PollingAIScan(UInt16 wBoardNo, UInt16 wChannel, ref UInt16 wChannelList, ref UInt16 wConfigList, UInt32 dwDataPreChCount, ref float fAIScanBuf);
        // TRANSMISSINGCOMMENT: Method Ixud_PollingAIScanH
        [DllImport("UniDAQ.dll")]
        public static extern UInt16 Ixud_PollingAIScanH(UInt16 wBoardNo, UInt16 wChannel, ref UInt16 wChannelList, ref UInt16 wConfigList, UInt32 dwDataPreChCount, ref UInt32 dwAIScanBuf);

        // TRANSMISSINGCOMMENT: Method Ixud_ClearAIBuffer
        [DllImport("UniDAQ.dll")]
        public static extern UInt16 Ixud_ClearAIBuffer(UInt16 wBoardNo);
        // TRANSMISSINGCOMMENT: Method Ixud_GetBufferStatus
        [DllImport("UniDAQ.dll")]
        public static extern UInt16 Ixud_GetBufferStatus(UInt16 wBoardNo, ref UInt16 wBufferStatus, ref UInt32 dwDataCount);
        // TRANSMISSINGCOMMENT: Method Ixud_StartAI
        [DllImport("UniDAQ.dll")]
        public static extern UInt16 Ixud_StartAI(UInt16 wBoardNo, UInt16 wChannel, UInt16 wConfig, float fSamplingRate, UInt32 dwDataCount);
        // TRANSMISSINGCOMMENT: Method Ixud_StartAIScan
        [DllImport("UniDAQ.dll")]
        public static extern UInt16 Ixud_StartAIScan(UInt16 wBoardNo, UInt16 wTotalChannels, ref UInt16 wChannelList, ref UInt16 wConfigList, float fSamplingRate, UInt32 dwDataCount);
        // TRANSMISSINGCOMMENT: Method Ixud_GetAIBuffer
        [DllImport("UniDAQ.dll")]
        public static extern UInt16 Ixud_GetAIBuffer(UInt16 wBoardNo, UInt32 dwDataCount, ref float fValue);
        // TRANSMISSINGCOMMENT: Method Ixud_GetAIBufferH
        [DllImport("UniDAQ.dll")]
        public static extern UInt16 Ixud_GetAIBufferH(UInt16 wBoardNo, UInt32 dwDataCount, ref UInt32 hValue);
        // TRANSMISSINGCOMMENT: Method Ixud_SetEventCallback
        [DllImport("UniDAQ.dll")]
        public static extern UInt16 Ixud_SetEventCallback(UInt16 wBoardNo, UInt16 wIntSource, UInt32 hEvent, CallBackFun CallbackFunction, UInt32 dwCallbackParameter);
        // TRANSMISSINGCOMMENT: Method Ixud_RemoveEventCallback
        [DllImport("UniDAQ.dll")]
        public static extern UInt16 Ixud_RemoveEventCallback(UInt16 wBoardNo, UInt16 wIntSource);
        // TRANSMISSINGCOMMENT: Method Ixud_InstallIrq
        [DllImport("UniDAQ.dll")]
        public static extern UInt16 Ixud_InstallIrq(UInt16 wBoardNo, UInt32 dwIntMask);
        // TRANSMISSINGCOMMENT: Method Ixud_RemoveIrq
        [DllImport("UniDAQ.dll")]
        public static extern UInt16 Ixud_RemoveIrq(UInt16 wBoardNo);
        // TRANSMISSINGCOMMENT: Method Ixud_StopAI
        [DllImport("UniDAQ.dll")]
        public static extern UInt16 Ixud_StopAI(UInt16 wBoardNo);
        // TRANSMISSINGCOMMENT: Method Ixud_ConfigAO
        [DllImport("UniDAQ.dll")]
        public static extern UInt16 Ixud_ConfigAO(UInt16 wBoardNo, UInt16 wChannel, UInt16 wCfgCode);

        // TRANSMISSINGCOMMENT: Method Ixud_WriteAOVoltage
        [DllImport("UniDAQ.dll")]
        public static extern UInt16 Ixud_WriteAOVoltage(UInt16 wBoardNo, UInt16 wChannel, float fValue);
        // TRANSMISSINGCOMMENT: Method Ixud_WriteAOVoltageH
        [DllImport("UniDAQ.dll")]
        public static extern UInt16 Ixud_WriteAOVoltageH(UInt16 wBoardNo, UInt16 wChannel, UInt16 hValue);

        // TRANSMISSINGCOMMENT: Method Ixud_WriteAOCurrent
        [DllImport("UniDAQ.dll")]
        public static extern UInt16 Ixud_WriteAOCurrent(UInt16 wBoardNo, UInt16 wChannel, float fValue);
        // TRANSMISSINGCOMMENT: Method Ixud_WriteAOCurrentH
        [DllImport("UniDAQ.dll")]
        internal static extern UInt16 Ixud_WriteAOCurrentH(UInt16 wBoardNo, UInt16 wChannel, UInt16 hValue);
        
       
    }



}
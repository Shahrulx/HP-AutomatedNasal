﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using HardwareInterfaces;
using PCI.PS400;

namespace HardwareLib
{
    public class MotorPISO : IMotor
    {
        public string Name { get; set; }
        public MotorType MotorType { get; set; }
        public ISpeedCfg HomeSpeedCfg {get; set;}
        public ISpeedCfg RunSpeedCfg { get; set; }
        public ISpeedCfg XYRunSpeedCfg { get; set; }
        public ISpeedCfg XYZRunSpeedCfg { get; set; }
        public IDigitalOutput ClearAlarm { get; set; }
        private ushort axisCount = 0;
        private byte cardID = 0;
        private bool invertSVON = false;
        private bool simulationMode = false;
        private ushort axisNo;
        private byte homeDir;
        private Object thisLock;
        private bool requestStop;
        private bool useHomeSpeed = false;
        public double PulsePermm { get; set; }
        public bool UnitInmm { get; set; }
        //private ISpeedCfg currentSpeed { get; set; }
        private bool servoOn;

        public MotorPISO(string name, Object thisLock, ushort cardID, ushort axisCount,
            ushort axisNo, byte homeDir, bool invertSVON, ISpeedCfg homeSpeedCfg, bool simulationMode)
        {
            this.Name = name;
            this.thisLock = thisLock;
            this.cardID = (byte)cardID;
            this.axisCount = axisCount;
            this.axisNo = (ushort)(1 << axisNo);
            this.homeDir = homeDir;
            this.HomeSpeedCfg = homeSpeedCfg;
            this.RunSpeedCfg = homeSpeedCfg;
            this.simulationMode = simulationMode;
            this.invertSVON = invertSVON;
            this.PulsePermm = 1;
            this.UnitInmm = false;
            requestStop = false;
            ClearAlarm = null;
            servoOn = false;
        }

        public void Start()
        {
            SetSVOn(1);
            lock (thisLock)
            {
                if (simulationMode)
                {
                }
                else
                {
                    ConfigureHome();
                    short status = Functions.ps400_set_abs_feedback_src(cardID, axisNo,
                                    (byte)Param.FEEDBACK_SRC_LOGIC_COMMAND);
                    if (status != ErrCode.SUCCESS_NO_ERROR)
                    {
                        throw new PISOException("Fail to set feedback src for abs move", status);
                    }
                }                
            }
            
        }

        public void ConfigureHome()
        {
            ushort homeStep2;
            if (homeDir == 0)
            {
                homeStep2 = Param.AUTO_HOME_STEP2_FORWARD;
            }
            else
            {
                homeStep2 = Param.AUTO_HOME_STEP2_REVERSE;
            }
            short status = Functions.ps400_set_home_cfg(cardID, axisNo, Param.HOME_LOGIC_ACTIVE_LOW, Param.NHOME_LOGIC_ACTIVE_HIGH,
                Param.INDEX_LOGIC_ACTIVE_HIGH, 
                (ushort)(Param.AUTO_HOME_STEP1_DISABLE | homeStep2 | Param.AUTO_HOME_STEP3_DISABLE | Param.AUTO_HOME_STEP4_DISABLE), 0);
            if (status != ErrCode.SUCCESS_NO_ERROR)
            {
                throw new PISOException("Fail to configure home", status);
            }
        }

        public void SetSpeed(double StartVelocity, double MaxVelocity, double acceleration=100, double deceleration=100)
        {
            lock (thisLock)
            {
                if (simulationMode)
                {
                }
                this.RunSpeedCfg = new ISpeedCfg(StartVelocity, MaxVelocity, acceleration, deceleration);
                //this.RunSpeedCfg = speedCfg;
            }
        }

        public void SetSVOn(ushort svOn)
        {
            short status = 0;
            servoOn = svOn == 1;
            if (invertSVON)
            {
                svOn = (ushort)((svOn == 0) ? 1 : 0);
            }
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                }
                else
                {
                    status = Functions.ps400_servo_on(cardID, axisNo, svOn, Param.SERVO_MANUAL_OFF);
                    if (status != ErrCode.SUCCESS_NO_ERROR)
                    {
                        throw new PISOException("Fail to servo on", status);
                    }
                }
            }
        }

        public void Stop(bool emergency=false)
        {
            short status = 0;
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                }
                else
                {
                    ushort wStopMove = emergency ? Param.STOP_SUDDEN : Param.STOP_SLOWDOWN;
                    status = Functions.ps400_stop_move(cardID, axisNo, wStopMove);
                    if (status != ErrCode.SUCCESS_NO_ERROR)
                    {
                        throw new PISOException("Fail to stop move", status);
                    }
                }
            }
            // make sure the motor stop then only ask to quit the wait
            requestStop = true;
        }

        public void MoveAbsolute(int Position, int timeOutInSeconds)
        {
            DateTime now = DateTime.Now;
            MoveAbsoluteNoWait(Position);
            WaitMotionDone(timeOutInSeconds);
        }

        public void MoveRelative(int distance, int timeOutInSeconds)
        {
            DateTime now = DateTime.Now;
            MoveRelativeNoWait(distance);
            WaitMotionDone(timeOutInSeconds);
        }

        public void MoveAbsoluteNoWait(int Position)
        {
            short status;
            requestStop = false;
            if (simulationMode)
            {
                GetMotionDone();
                status = 0;
            }
            else
            {
                if (!GetMotionDone())
                {
                    throw new PISOException("Start a motion before the previous motion done", (short)axisNo);
                }
                lock (thisLock)
                {
                    if (RunSpeedCfg.SpeedProfile == ESpeedProfile.Trapezoidal)
                    {
                        status = Functions.ps400_t_abs_move(cardID, axisNo,
                            (uint)RunSpeedCfg.StartVelocity, (uint)RunSpeedCfg.MaxVelocity,
                            (uint)RunSpeedCfg.RampUpTime, (uint)RunSpeedCfg.RampDownTime,
                            Position, 8);
                    }
                    else
                    {
                        status = Functions.ps400_s_abs_move(cardID, axisNo,
                            (uint)RunSpeedCfg.StartVelocity, (uint)RunSpeedCfg.MaxVelocity,
                            (uint)RunSpeedCfg.RampUpTime, (uint)RunSpeedCfg.RampDownTime,
                            Position, 8);
                    }
                    if (status != ErrCode.SUCCESS_NO_ERROR)
                    {
                        throw new PISOException("Fail to move axis abs", status);
                    }
                    
                }
            }
        }

        public void MoveAbsoluteXY(int PositionX, int PositionY, int timeOutInSeconds)
        {
            DateTime now = DateTime.Now;
            MoveAbsoluteXYNoWait(PositionX, PositionY);
            WaitMotionDone(timeOutInSeconds);
        }

        public void MoveAbsoluteXYNoWait(int PositionX, int PositionY)
        {
            short status;
            requestStop = false;
            if (simulationMode)
            {
                GetMotionDone();
                status = 0;
            }
            else
            {
                if (!GetMotionDone())
                {
                    throw new PISOException("Start a motion before the previous motion done", (short)axisNo);
                }
                lock (thisLock)
                {
                    if (XYRunSpeedCfg.SpeedProfile == ESpeedProfile.Trapezoidal)
                    {
                        status = Functions.ps400_t_abs_line2_move(cardID, axisNo, (ushort)(axisNo << 1),
                            (uint)XYRunSpeedCfg.StartVelocity, (uint)XYRunSpeedCfg.MaxVelocity,
                            (uint)XYRunSpeedCfg.RampUpTime, (uint)XYRunSpeedCfg.RampDownTime,
                            PositionX, PositionY, 8, Param.DISABLE_BLOCK_OPEARTION);
                    }
                    else
                    {
                        status = Functions.ps400_s_abs_line2_move(cardID, axisNo, (ushort)(axisNo << 1),
                            (uint)XYRunSpeedCfg.StartVelocity, (uint)XYRunSpeedCfg.MaxVelocity,
                            (uint)XYRunSpeedCfg.RampUpTime, (uint)XYRunSpeedCfg.RampDownTime,
                            PositionX, PositionY, 8, Param.DISABLE_BLOCK_OPEARTION);
                    }
                    if (status != ErrCode.SUCCESS_NO_ERROR)
                    {
                        throw new PISOException("Fail to moveXY-axis abs", status);
                    }
                }
            }
        }

        public void MoveAbsoluteXYZ(int PositionX, int PositionY, int PositionZ, int timeOutInSeconds)
        {
            DateTime now = DateTime.Now;
            MoveAbsoluteXYZNoWait(PositionX, PositionY, PositionZ);
            WaitMotionDone(timeOutInSeconds);
        }

        public void MoveAbsoluteXYZNoWait(int PositionX, int PositionY, int PositionZ)
        {
            short status;
            requestStop = false;
            if (simulationMode)
            {
                GetMotionDone();
                status = 0;
            }
            else
            {
                if (!GetMotionDone())
                {
                    throw new PISOException("Start a motion before the previous motion done", (short)axisNo);
                }
                lock (thisLock)
                {

                    if (XYRunSpeedCfg.SpeedProfile == ESpeedProfile.Trapezoidal)
                    {
                        status = Functions.ps400_t_abs_line3_move(cardID, axisNo,
                            (ushort)(axisNo << 1), (ushort)(axisNo << 2),
                            (uint)XYRunSpeedCfg.StartVelocity, (uint)XYRunSpeedCfg.MaxVelocity,
                            (uint)XYRunSpeedCfg.RampUpTime, (uint)XYRunSpeedCfg.RampDownTime,
                            PositionX, PositionY, PositionZ, 8, Param.DISABLE_BLOCK_OPEARTION);
                    }
                    else
                    {
                        status = Functions.ps400_t_abs_line3_move(cardID, axisNo,
                            (ushort)(axisNo << 1), (ushort)(axisNo << 2),
                            (uint)XYRunSpeedCfg.StartVelocity, (uint)XYRunSpeedCfg.MaxVelocity,
                            (uint)XYRunSpeedCfg.RampUpTime, (uint)XYRunSpeedCfg.RampDownTime,
                            PositionX, PositionY, PositionZ, 8, Param.DISABLE_BLOCK_OPEARTION);
                    }
                    if (status != ErrCode.SUCCESS_NO_ERROR)
                    {
                        throw new PISOException("Fail to moveXYZ-axis abs", status);
                    }
                }
            }
        }

        public void MoveRelativeXY(int PositionX, int PositionY, int timeOutInSeconds)
        {
            DateTime now = DateTime.Now;
            MoveRelativeXYNoWait(PositionX, PositionY);
            WaitMotionDone(timeOutInSeconds);
        }

        public void MoveRelativeXYNoWait(int PositionX, int PositionY)
        {
            short status;
            requestStop = false;
            if (simulationMode)
            {
                GetMotionDone();
                status = 0;
            }
            else
            {
                if (!GetMotionDone())
                {
                    throw new PISOException("Start a motion before the previous motion done", (short)axisNo);
                }
                lock (thisLock)
                {
                    if (XYRunSpeedCfg.SpeedProfile == ESpeedProfile.Trapezoidal)
                    {
                        status = Functions.ps400_t_line2_move(cardID, axisNo, (ushort)(axisNo << 1),
                            (uint)XYRunSpeedCfg.StartVelocity, (uint)XYRunSpeedCfg.MaxVelocity,
                            (uint)XYRunSpeedCfg.RampUpTime, (uint)XYRunSpeedCfg.RampDownTime,
                            PositionX, PositionY, 8, Param.DISABLE_BLOCK_OPEARTION);
                    }
                    else
                    {
                        status = Functions.ps400_s_line2_move(cardID, axisNo, (ushort)(axisNo << 1),
                            (uint)XYRunSpeedCfg.StartVelocity, (uint)XYRunSpeedCfg.MaxVelocity,
                            (uint)XYRunSpeedCfg.RampUpTime, (uint)XYRunSpeedCfg.RampDownTime,
                            PositionX, PositionY, 8, Param.DISABLE_BLOCK_OPEARTION);
                    }
                    if (status != ErrCode.SUCCESS_NO_ERROR)
                    {
                        throw new PISOException("Fail to moveXY-axis rel", status);
                    }
                }
            }
        }

        public void MoveRelativeXYZ(int PositionX, int PositionY, int PositionZ, int timeOutInSeconds)
        {
            DateTime now = DateTime.Now;
            MoveRelativeXYZNoWait(PositionX, PositionY, PositionZ);
            WaitMotionDone(timeOutInSeconds);
        }

        public void MoveRelativeXYZNoWait(int PositionX, int PositionY, int PositionZ)
        {
            short status;
            requestStop = false;
            if (simulationMode)
            {
                status = 0;
            }
            else
            {
                if (!GetMotionDone())
                {
                    throw new PISOException("Start a motion before the previous motion done", (short)axisNo);
                }
                lock (thisLock)
                {
                    if (XYRunSpeedCfg.SpeedProfile == ESpeedProfile.Trapezoidal)
                    {
                        status = Functions.ps400_t_line3_move(cardID, axisNo,
                            (ushort)(axisNo << 1), (ushort)(axisNo << 2),
                            (uint)XYRunSpeedCfg.StartVelocity, (uint)XYRunSpeedCfg.MaxVelocity,
                            (uint)XYRunSpeedCfg.RampUpTime, (uint)XYRunSpeedCfg.RampDownTime,
                            PositionX, PositionY, PositionZ, 8, Param.DISABLE_BLOCK_OPEARTION);
                    }
                    else
                    {
                        status = Functions.ps400_t_line3_move(cardID, axisNo,
                            (ushort)(axisNo << 1), (ushort)(axisNo << 2),
                            (uint)XYRunSpeedCfg.StartVelocity, (uint)XYRunSpeedCfg.MaxVelocity,
                            (uint)XYRunSpeedCfg.RampUpTime, (uint)XYRunSpeedCfg.RampDownTime,
                            PositionX, PositionY, PositionZ, 8, Param.DISABLE_BLOCK_OPEARTION);
                    }
                    if (status != ErrCode.SUCCESS_NO_ERROR)
                    {
                        throw new PISOException("Fail to moveXYZ-axis abs", status);
                    }
                }
            }
        }

        public void MoveRelativeNoWait(int distance)
        {
            short status;
            requestStop = false;
            if (simulationMode)
            {
                GetMotionDone();
                status = 0;
            }
            else
            {
                if (!GetMotionDone())
                {
                    throw new PISOException("Start a motion before the previous motion done", (short)axisNo);
                }
                lock (thisLock)
                {
                    if (RunSpeedCfg.SpeedProfile == ESpeedProfile.Trapezoidal)
                    {
                        status = Functions.ps400_t_move(cardID, axisNo,
                            (uint)RunSpeedCfg.StartVelocity, (uint)RunSpeedCfg.MaxVelocity,
                            (uint)RunSpeedCfg.RampUpTime, (uint)RunSpeedCfg.RampDownTime,
                            distance, 8);
                    }
                    else
                    {
                        status = Functions.ps400_s_move(cardID, axisNo,
                            (uint)RunSpeedCfg.StartVelocity, (uint)RunSpeedCfg.MaxVelocity,
                            (uint)RunSpeedCfg.RampUpTime, (uint)RunSpeedCfg.RampDownTime,
                            distance, 8);
                    }
                    if (status != ErrCode.SUCCESS_NO_ERROR)
                    {
                        throw new PISOException("Fail to move axis abs", status);
                    }
                }
            }
        }

        public bool GetMotionDone()
        {
            int motion = 0;
            short status;

            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                    motion = 0;
                }
                else
                {
                    byte pDone = 0;
                    ushort pStopStatus = 0;
                    if (GetAlarm())
                    {
                        throw new MotionAlarm("Motion has alarm", axisNo);
                    }
                    status = Functions.ps400_motion_done(cardID, axisNo, ref pDone, ref pStopStatus);
                    if (status != ErrCode.SUCCESS_NO_ERROR)
                    {
                        throw new PISOException("Fail to get motion done", status);
                    }
                    motion = pDone == Param.MOTION_DONE ? 0 : 1;
                }

            }
            if (GetAlarm())
            {
                throw new MotionAlarm("Motion has alarm", axisNo);
            }
            if (useHomeSpeed && motion == 0)
            {
                useHomeSpeed = false;
                Reset();
            }
            return (motion == 0);
        }

        public int GetPosition()
        {
            short status;
            int Pos = 0;
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                    Pos = 100;
                }
                else
                {
                    status = Functions.ps400_get_enccounter(cardID, axisCount, ref Pos);
                    if (status != ErrCode.SUCCESS_NO_ERROR)
                    {
                        throw new PISOException("Fail to get position", status);
                    }
                }
            }
            return Pos;
        }

        public int GetErrorCounter()
        {
            short status;
            int errorCounter = 0;
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                    errorCounter = 100;
                }
                else
                {
                    //no error counter for PISO
                    errorCounter = 0;
                }
            }
            return errorCounter;
        }

        public void ResetErrorCounter()
        {
            short status;
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                }
                else
                {
                    //no error counter for PISO
                }
            }
        }

        public int GetCommand()
        {
            short status;
            int cmdCounter = 0;
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                    cmdCounter = 100;
                }
                else
                {
                    status = Functions.ps400_get_cmdcounter(cardID, axisNo, ref cmdCounter);
                    if (status != ErrCode.SUCCESS_NO_ERROR)
                    {
                        throw new PISOException("Fail to get command counter", status);
                    }
                }
            }
            return cmdCounter;
        }

        public void Home(int timeOutInSeconds)
        {
            HomeNoWait();
            WaitMotionDone(timeOutInSeconds);
            //SetSpeed(RunSpeedCfg.StartVelocity, RunSpeedCfg.MaxVelocity, RunSpeedCfg.RampUpTime, RunSpeedCfg.RampDownTime);
        }

        public void WaitMotionDone(int timeOutInSeconds)
        {
            DateTime now = DateTime.Now;
            ManualResetEvent ev;
            while (!GetMotionDone())
            {
                ev = new ManualResetEvent(false);
                ev.WaitOne(50);
                if (requestStop)
                {
                    requestStop = false;
                    break;
                }
                if ((DateTime.Now - now).TotalSeconds > timeOutInSeconds)
                {
                    throw new MotionTimeout(string.Format("Motion axis {0} timeout on waiting motion done", axisNo));
                }
            }
            
        }

        public void WaitXYMotionDone(int timeOutInSeconds)
        {
            WaitMotionDone(timeOutInSeconds);
        }

        public void WaitXYZMotionDone(int timeOutInSeconds)
        {
            WaitMotionDone(timeOutInSeconds);
        }

        public void HomeNoWait()
        {
            short status;
            requestStop = false;
            // Start homing
            useHomeSpeed = true;
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                }
                else
                {
                    status = Functions.ps400_home_start(cardID, axisNo,
                        (uint)HomeSpeedCfg.StartVelocity, (uint)HomeSpeedCfg.RampUpTime,
                        (uint)HomeSpeedCfg.RampDownTime, (uint)HomeSpeedCfg.MaxVelocity,
                        (uint)HomeSpeedCfg.StartVelocity, Param.DISABLE_BLOCK_OPEARTION);
                    if (status != ErrCode.SUCCESS_NO_ERROR)
                    {
                        throw new PISOException("Fail to start home", status);
                    }
                }
            }
        }

        public void Reset()
        {
            short status;
            // Start homing
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                }
                else
                {
                    status = Functions.ps400_set_cmdcounter(cardID, axisNo, 0);
                    if (status != ErrCode.SUCCESS_NO_ERROR)
                    {
                        throw new PISOException("Fail to set cmdcounter", status);
                    }
                    status = Functions.ps400_set_enccounter(cardID, axisNo, 0);
                    if (status != ErrCode.SUCCESS_NO_ERROR)
                    {
                        throw new PISOException("Fail to set enccounter", status);
                    }
                }
            }

        }

        public void ResetAlarm()
        {
            short status;
            ManualResetEvent ev = new ManualResetEvent(false);
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                }
                else
                {
                    if (ClearAlarm != null)
                    {
                        ClearAlarm.ON();
                    }
                }

                ev.WaitOne(100);
                
                if (simulationMode)
                {
                    status = 0;
                }
                else
                {
                    if (ClearAlarm != null)
                    {
                        ClearAlarm.OFF();
                    }
                }
            }
        }

        public IOStatus GetIOStatus()
        {
            short status;
            var ioStatus = new IOStatus(0);
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                }
                else
                {
                    ushort pDIStatus = 0;
                    status = Functions.ps400_get_mdi_status(cardID, axisNo, ref pDIStatus);
                    ioStatus.positiveLimit = (byte)(pDIStatus & Param.DI_STATUS_ACTIVE_LMTP);
                    ioStatus.negativeLimit = (byte)(pDIStatus & Param.DI_STATUS_ACTIVE_LMTM);
                    ioStatus.origin = (byte)(pDIStatus & Param.DI_STATUS_ACTIVE_HOME);
                    ioStatus.inPosition = (pDIStatus & Param.DI_STATUS_ACTIVE_INP) != 0;
                    ioStatus.indexSignal = (byte)(pDIStatus & Param.DI_STATUS_ACTIVE_INDEX);
                    ioStatus.alarm = (pDIStatus & Param.DI_STATUS_ACTIVE_ALARM) != 0;
                    ioStatus.slowDownSignalInput = (byte)(pDIStatus & Param.DI_STATUS_ACTIVE_NEARHOME);
                }
            }
            ioStatus.servoOn = servoOn;
            return ioStatus;
        }

        public bool GetAlarm()
        {
            var sts = GetErrorStatus();
            return ( sts & Param.DRIVE_ERROR_STATUS_ALARM) > 0 || (sts & Param.DRIVE_ERROR_STATUS_EMG) > 0;
        }

        public bool GetRDY()
        {
            return GetIOStatus().ready;
        }

        public bool GetInPosition()
        {
            return GetIOStatus().inPosition;
        }

        public uint GetErrorStatus()
        {
            short status;
            ushort pErrorStatus = 0;
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                    pErrorStatus = 0;
                }
                else
                {
                    status = Functions.ps400_get_error_status(cardID, axisNo, ref pErrorStatus);
                }
            }
            return pErrorStatus;
        }

        public void LimitSearchNoWait(int direction)
        {
            throw new NotImplementedException();
        }
    }
}

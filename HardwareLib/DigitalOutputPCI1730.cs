﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HardwareInterfaces;
using PCI1730;

namespace HardwareLib
{
    public class DigitalOutputPCI1730: IDigitalOutput
    {
        public string Name { get; set; }
        public ushort CardID { get; set; }
        public bool Disable { get; set; }
        public byte Msb { get; set; }
        public byte Lsb { get; set;}
        private byte port = 99;
        private byte portMsb;
        private byte portLsb;
        private bool simulationMode;
        private Object thisLock;
        private Dictionary<ushort, Dictionary<byte, byte>> outputValue;
        private int handle;

        public DigitalOutputPCI1730(string name, Object thisLock, ushort cardID, int handle,
            Dictionary<ushort, Dictionary<byte, byte>> outputValue, byte msb, byte lsb, bool simulationMode)
        {
            byte portForMsb = 99;
            byte portForLsb = 99;

            this.Name = name;
            this.thisLock = thisLock;
            this.CardID = cardID;
            this.outputValue = outputValue;
            this.Msb = msb;
            this.Lsb = lsb;
            this.simulationMode = simulationMode;
            this.Disable = false;
            this.handle = handle;

            var portMsbLsbInfo = Utility.GetMsbLsbPortInfo(msb, lsb);
            portForMsb = portMsbLsbInfo.Item1;
            portForLsb = portMsbLsbInfo.Item2;
            portMsb = portMsbLsbInfo.Item3;
            portLsb = portMsbLsbInfo.Item4;
            if (portForMsb == portForLsb)
            {
                port = portForMsb;
            }

            if (!outputValue.ContainsKey(cardID))
            {
                Dictionary<byte, byte> cardValue = new Dictionary<byte, byte>();
                outputValue[cardID] = cardValue;
            }
            if (!outputValue[cardID].ContainsKey(portForLsb))
            {
                outputValue[cardID][portForLsb] = ReadOutput(portForLsb);
            }
            if (!outputValue[cardID].ContainsKey(portForMsb))
            {
                outputValue[cardID][portForMsb] = ReadOutput(portForMsb);
            }
        }

        public void Write(int value)
        {
            int currentOutput;
            if (Disable)
            {
                return;
            }
            lock (thisLock)
            {
                if (port != 99)
                {
                    currentOutput = GetOutput(port);
                    currentOutput = Utility.SetBits(currentOutput, portLsb, portMsb, value);
                    SetOutput(port, (byte)currentOutput);
                }
                else
                {
                    currentOutput = GetOutput();
                    currentOutput = Utility.SetBits(currentOutput, Lsb, Msb, value);
                    SetOutput(currentOutput);
                }
            }
        }

        public void ON()
        {
            Write(1);
        }

        public void OFF()
        {
            Write(0);
        }

        public int Get()
        {
            int currentOutput;
            lock (thisLock)
            {
                if (port != 99)
                {
                    currentOutput = GetOutput(port);
                    return Utility.GetBits(currentOutput, portLsb, portMsb);
                }
                else
                {
                    currentOutput = GetOutput();
                    return Utility.GetBits(currentOutput, Lsb, Msb);
                }
            }
        }

        public int Read()
        {
            int currentOutput;
            lock (thisLock)
            {
                if (port != 99)
                {
                    currentOutput = ReadOutput(port);
                    return Utility.GetBits(currentOutput, portLsb, portMsb);
                }
                else
                {
                    currentOutput = ReadOutput();
                    return Utility.GetBits(currentOutput, Lsb, Msb);
                }
            }
        }

        private int ReadOutput()
        {
            int retValue = 0;
            byte portValue;
            for (byte port = 0; port < 4; port++)
            {
                portValue = ReadOutput(port);
                retValue |= portValue << (port * 7);
            }
            return retValue;
        }

        private byte ReadOutput(byte port)
        {
            int status = 0;
            byte value = 0;
            if (simulationMode)
            {
                status = 0;
            }
            else
            {
                status = Adsapi.AdxDioGetCurrentDoPortsState(handle, port, 1, ref value);
                if (status != 0)
                    throw new Exception($"Fail to read output PCI1730 cardID: {this.CardID} port: {port} with return code: {status}");
            }
            return (byte)value;
        }

        private int GetOutput()
        {
            int retValue = 0;
            byte portValue;
            for (byte port = 0; port < 4; port++)
            {
                portValue = GetOutput(port);
                retValue |= portValue << (port * 7);
            }
            return retValue;
        }

        private byte GetOutput(byte port)
        {
            return outputValue[CardID][port];
        }

        private void SetOutput(byte port, byte value)
        {
            int status = 0;

            if (simulationMode)
            {
                status = 0;
            }
            else
            {
                status = Adsapi.AdxDioWriteDoPorts(handle, port, 1, ref value);
            }

            if (status == 0)
            {
                outputValue[CardID][port] = value;
            }
            else
            {
                throw new Exception($"Fail to write output PCI1730 cardID: {this.CardID} port: {port} value: {value} with return code: {status}");
            }
        }

        private void SetOutput(int value)
        {
            byte PortNo;
            byte Val = 0;

            for (PortNo = 0; PortNo <= 3; PortNo++)
            {
                Val = (byte)Utility.GetBits(value, PortNo * 8, (PortNo + 1) * 8 - 1);
                SetOutput(PortNo, Val);
            }
        }
    }
}

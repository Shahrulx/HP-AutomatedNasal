﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HardwareLib
{
    public class AmonetMotionException : Exception
    {
        public AmonetMotionException(string msg, short errorCode) : base(string.Format("{0}, code: {1}", msg, errorCode)) { }
    }

    public class MotionException : Exception
    {
        public MotionException(string msg, short errorCode) : base(string.Format("{0}, code: {1}", msg, errorCode)) { }
    }

    public class MotionTimeout: Exception
    {
        public MotionTimeout(string msg) : base(msg) { }
    }

    public class CylinderTimeout: Exception
    {
        public CylinderTimeout(string msg) : base(msg) { }
    }

    public class AmonetException : Exception
    {
        public AmonetException(string msg, short errorCode) : base(string.Format("{0}, code: {1}", msg, errorCode)) { }
    }

    public class DigitalInputTimeoutException : Exception
    {
        public DigitalInputTimeoutException(string inputName, short errorCode) : 
            base(string.Format("{0} timeout, code: {1}", inputName, errorCode)) { }
    }

    public class UniDaqException : Exception
    {
        public UniDaqException(string msg, ushort errorCode) : base(string.Format("{0}, code: {1}", msg, errorCode)) { }
    }
    public class PISOException : Exception
    {
        public PISOException(string msg, short errorCode) : base(string.Format("{0}, code: {1}", msg, errorCode)) { }
    }

}

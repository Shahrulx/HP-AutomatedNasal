﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AMONetLib;
using HardwareInterfaces;

namespace HardwareLib
{
    public class MotorAmonet : IMotor
    {
        public string Name { get; set; }
        public MotorType MotorType { get; set; }
        public ISpeedCfg HomeSpeedCfg {get; set;}
        public ISpeedCfg RunSpeedCfg { get; set; }
        public ISpeedCfg XYRunSpeedCfg { get; set; }
        public ISpeedCfg XYZRunSpeedCfg { get; set; }
        public IDigitalOutput ClearAlarm { get; set; }

        private ushort axisCount = 0;
        private ushort ringNo = 0;
        private ushort cardID = 0;
        private bool invertSVON = false;
        private bool simulationMode = false;
        private ushort axisNo;
        private byte homeDir;
        private Object thisLock;
        private bool requestStop;
        private bool useHomeSpeed = false;
        public double PulsePermm { get; set; }
        public bool UnitInmm { get; set; }

        public MotorAmonet(string name, Object thisLock, ushort ringNo, ushort cardID, ushort axisCount,
            ushort axisNo, byte homeDir, bool invertSVON, ISpeedCfg homeSpeedCfg, bool simulationMode)
        {
            this.Name = name;
            this.thisLock = thisLock;
            this.ringNo = ringNo;
            this.cardID = cardID;
            this.axisCount = axisCount;
            this.axisNo = axisNo;
            this.homeDir = homeDir;
            this.HomeSpeedCfg = homeSpeedCfg;
            this.RunSpeedCfg = homeSpeedCfg;
            this.simulationMode = simulationMode;
            this.invertSVON = invertSVON;
            this.PulsePermm = 1;
            this.UnitInmm = false;
            requestStop = false;
        }

        public void Start()
        {
            short status = 0;
            if (simulationMode)
            {
                status = 0;
            }
            else
            {
                if (axisCount == 2)
                {
                    status = AMONet._mnet_m2_enable_home_reset(ringNo, cardID, axisNo, 1);
                }
                else if (axisCount == 4)
                {
                    status = AMONet._mnet_m4_enable_home_reset(ringNo, cardID, axisNo, 1);
                }
            }
            if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to enable home reset", status);

            SetSVOn(1);
        }

        public void SetSpeed(double StartVelocity, double MaxVelocity, double rampUpTime=0.5, double rampDownTime=0.5)
        {
            short status = 0;
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                }
                else
                {
                    if (axisCount == 2)
                    {
                        status = AMONet._mnet_m2_fix_speed_range(ringNo, cardID, axisNo, MaxVelocity);
                        if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to set fix speed", status);
                        //status = AMONet._mnet_m2_set_tmove_speed(ringNo, cardID, axisNo, StartVelocity, MaxVelocity, rampUpTime, rampDownTime);
                        status = AMONet._mnet_m2_set_smove_speed(ringNo, cardID, axisNo, StartVelocity, MaxVelocity, rampUpTime, rampDownTime,
                            0.25 * (MaxVelocity - StartVelocity), 0.25 * (MaxVelocity - StartVelocity));
                        if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to set tmove speed", status);
                    }
                    else if (axisCount == 4)
                    {
                        status = AMONet._mnet_m4_fix_speed_range(ringNo, cardID, axisNo, MaxVelocity);
                        if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to set fix speed", status);
                        //status = AMONet._mnet_m4_set_tmove_speed(ringNo, cardID, axisNo, StartVelocity, MaxVelocity, rampUpTime, rampDownTime);
                        status = AMONet._mnet_m4_set_smove_speed(ringNo, cardID, axisNo, StartVelocity, MaxVelocity, rampUpTime, rampDownTime,
                            0.25 * (MaxVelocity - StartVelocity), 0.25 * (MaxVelocity - StartVelocity));
                        if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to set tmove speed", status);
                    }
                }
                var speedCfg = new ISpeedCfg(StartVelocity, MaxVelocity, rampUpTime, rampDownTime);
                //this.RunSpeedCfg = speedCfg;
            }
        }

        public void SetSVOn(ushort svOn)
        {
            short status = 0;
            if (invertSVON)
            {
                svOn = (ushort)((svOn == 0) ? 1 : 0);
            }
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                }
                else
                {
                    if (axisCount == 2)
                    {
                        status = AMONet._mnet_m2_set_svon(ringNo, cardID, axisNo, svOn);
                        if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to set svon", status);
                    }
                    else if (axisCount == 4)
                    {
                        status = AMONet._mnet_m4_set_svon(ringNo, cardID, axisNo, svOn);
                        if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to set svon", status);
                    }
                }
            }
        }

        public void Stop(bool emergency=false)
        {
            short status = 0;
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                }
                else
                {
                    if (axisCount == 2)
                    {
                        if (emergency) status = AMONet._mnet_m2_emg_stop(ringNo, cardID, axisNo);
                        else status = AMONet._mnet_m2_sd_stop(ringNo, cardID, axisNo);
                        if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to sd stop", status);
                    }
                    else if (axisCount == 4)
                    {
                        if (emergency) status = AMONet._mnet_m4_emg_stop(ringNo, cardID, axisNo);
                        else status = AMONet._mnet_m4_sd_stop(ringNo, cardID, axisNo);
                        if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to sd stop", status);
                    }
                }
            }
            // make sure the motor stop then only ask to quit the wait
            requestStop = true;
        }

        public void MoveAbsolute(int Position, int timeOutInSeconds)
        {
            DateTime now = DateTime.Now;
            MoveAbsoluteNoWait(Position);
            WaitMotionDone(timeOutInSeconds);
        }

        public void MoveRelative(int distance, int timeOutInSeconds)
        {
            DateTime now = DateTime.Now;
            MoveRelativeNoWait(distance);
            WaitMotionDone(timeOutInSeconds);
        }

        public void MoveAbsoluteNoWait(int Position)
        {
            short status;
            requestStop = false;
            if (useHomeSpeed)
            {
                useHomeSpeed = false;
                SetSpeed(RunSpeedCfg.StartVelocity, RunSpeedCfg.MaxVelocity,
                    RunSpeedCfg.RampUpTime, RunSpeedCfg.RampDownTime);
            }
            if (simulationMode)
            {
                status = 0;
            }
            else
            {
                if (!GetMotionDone())
                {
                    throw new AmonetMotionException("Start a motion before the previous motion done", (short)axisNo);
                }
                lock (thisLock)
                {
                    if (axisCount == 2)
                    {
                        status = AMONet._mnet_m2_start_a_move(ringNo, cardID, axisNo, Position);
                        if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to start_a_move", status);
                    }
                    else if (axisCount == 4)
                    {
                        status = AMONet._mnet_m4_start_a_move(ringNo, cardID, axisNo, Position);
                        if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to start_a_move", status);
                    }
                }
            }
        }

        public void MoveAbsoluteXY(int PositionX, int PositionY, int timeOutInSeconds)
        {
            DateTime now = DateTime.Now;
            MoveAbsoluteXYNoWait(PositionX, PositionY);
            WaitMotionDone(timeOutInSeconds);
        }

        public void MoveAbsoluteXYNoWait(int PositionX, int PositionY)
        {
            short status;
            requestStop = false;
            if (simulationMode)
            {
                status = 0;
            }
            else
            {
                if (!GetMotionDone())
                {
                    throw new AmonetMotionException("Start a motion before the previous motion done", (short)axisNo);
                }
                lock (thisLock)
                {
                    if (axisCount == 2)
                    {
                        status = AMONet._mnet_m2_start_ta_move_xy(ringNo, cardID, PositionX, PositionY,
                            XYRunSpeedCfg.StartVelocity, XYRunSpeedCfg.MaxVelocity, XYRunSpeedCfg.RampUpTime, XYRunSpeedCfg.RampDownTime);
                        if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to start_a_move", status);
                    }
                    else if (axisCount == 4)
                    {
                        SetSpeed(XYRunSpeedCfg.StartVelocity, XYRunSpeedCfg.MaxVelocity, XYRunSpeedCfg.RampUpTime, XYRunSpeedCfg.RampDownTime);
                        status = AMONet._mnet_m4_start_ta_move_xy(ringNo, cardID, PositionX, PositionY,
                            XYRunSpeedCfg.StartVelocity, XYRunSpeedCfg.MaxVelocity, XYRunSpeedCfg.RampUpTime, XYRunSpeedCfg.RampDownTime);
                        
                        //status = AMONet._mnet_m4_start_ta_move_xy(ringNo, cardID, PositionX, PositionY,
                        //    5000, 8000, XYRunSpeedCfg.RampUpTime, XYRunSpeedCfg.RampDownTime);
                        if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to start_a_move", status);
                    }
                }
            }
        }

        public void MoveAbsoluteXYZ(int PositionX, int PositionY, int PositionZ, int timeOutInSeconds)
        {
            DateTime now = DateTime.Now;
            MoveAbsoluteXYZNoWait(PositionX, PositionY, PositionZ);
            WaitMotionDone(timeOutInSeconds);
        }

        public void MoveAbsoluteXYZNoWait(int PositionX, int PositionY, int PositionZ)
        {
            short status;
            requestStop = false;
            if (simulationMode)
            {
                status = 0;
            }
            else
            {
                if (!GetMotionDone())
                {
                    throw new AmonetMotionException("Start a motion before the previous motion done", (short)axisNo);
                }
                lock (thisLock)
                {
                    if (axisCount == 2)
                    {
                        throw new AmonetMotionException("Not support xyz move", -1);
                    }
                    else if (axisCount == 4)
                    {
                        SetSpeed(XYZRunSpeedCfg.StartVelocity, XYZRunSpeedCfg.MaxVelocity, XYZRunSpeedCfg.RampUpTime, XYZRunSpeedCfg.RampDownTime);
                        ushort[] AxisArray = new ushort[3] { 0, 1, 2 };
                        status = AMONet._mnet_m4_start_ta_line3(ringNo, cardID, AxisArray, PositionX, PositionY, PositionZ,
                            XYZRunSpeedCfg.StartVelocity, XYZRunSpeedCfg.MaxVelocity, XYZRunSpeedCfg.RampUpTime, XYZRunSpeedCfg.RampDownTime);
                        
                        if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to start_a_move", status);
                    }
                }
            }
        }

        public void MoveRelativeXY(int PositionX, int PositionY, int timeOutInSeconds)
        {
            DateTime now = DateTime.Now;
            MoveRelativeXYNoWait(PositionX, PositionY);
            WaitMotionDone(timeOutInSeconds);
        }

        public void MoveRelativeXYNoWait(int PositionX, int PositionY)
        {
            short status;
            requestStop = false;
            if (simulationMode)
            {
                status = 0;
            }
            else
            {
                if (!GetMotionDone())
                {
                    throw new AmonetMotionException("Start a motion before the previous motion done", (short)axisNo);
                }
                lock (thisLock)
                {
                    if (axisCount == 2)
                    {
                        status = AMONet._mnet_m2_start_tr_move_xy(ringNo, cardID, PositionX, PositionY,
                            RunSpeedCfg.StartVelocity, RunSpeedCfg.MaxVelocity, RunSpeedCfg.RampUpTime, RunSpeedCfg.RampDownTime);
                        if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to start_tr_move_xy", status);
                    }
                    else if (axisCount == 4)
                    {
                        status = AMONet._mnet_m4_start_tr_move_xy(ringNo, cardID, PositionX, PositionY,
                            RunSpeedCfg.StartVelocity, RunSpeedCfg.MaxVelocity, RunSpeedCfg.RampUpTime, RunSpeedCfg.RampDownTime);
                        if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to start_tr_move_xy", status);
                    }
                }
            }
        }

        public void MoveRelativeXYZ(int PositionX, int PositionY, int PositionZ, int timeOutInSeconds)
        {
            DateTime now = DateTime.Now;
            MoveRelativeXYZNoWait(PositionX, PositionY, PositionZ);
            WaitMotionDone(timeOutInSeconds);
        }

        public void MoveRelativeXYZNoWait(int PositionX, int PositionY, int PositionZ)
        {
            short status;
            requestStop = false;
            if (simulationMode)
            {
                status = 0;
            }
            else
            {
                if (!GetMotionDone())
                {
                    throw new AmonetMotionException("Start a motion before the previous motion done", (short)axisNo);
                }
                lock (thisLock)
                {
                    if (axisCount == 2)
                    {
                        throw new AmonetMotionException("Not support xyz move", -1);
                    }
                    else if (axisCount == 4)
                    {
                        SetSpeed(RunSpeedCfg.StartVelocity, RunSpeedCfg.MaxVelocity, RunSpeedCfg.RampUpTime, RunSpeedCfg.RampDownTime);
                        ushort[] AxisArray = new ushort[3] { 0, 1, 2 };
                        status = AMONet._mnet_m4_start_tr_line3(ringNo, cardID, AxisArray, PositionX, PositionY, PositionZ,
                            RunSpeedCfg.StartVelocity, RunSpeedCfg.MaxVelocity, RunSpeedCfg.RampUpTime, RunSpeedCfg.RampDownTime);
                        
                        if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to start_r_move", status);
                    }
                }
            }
        }

        public void MoveRelativeNoWait(int distance)
        {
            short status;
            requestStop = false;
            if (useHomeSpeed)
            {
                useHomeSpeed = false;
                SetSpeed(RunSpeedCfg.StartVelocity, RunSpeedCfg.MaxVelocity,
                    RunSpeedCfg.RampUpTime, RunSpeedCfg.RampDownTime);
            }
            if (simulationMode)
            {
                status = 0;
            }
            else
            {
                if (!GetMotionDone())
                {
                    throw new AmonetMotionException("Start a motion before the previous motion done", (short)axisNo);
                }
                lock (thisLock)
                {
                    if (axisCount == 2)
                    {
                    
                        status = AMONet._mnet_m2_start_r_move(ringNo, cardID, axisNo, distance);
                        if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to start_r_move", status);
                    }
                    else if (axisCount == 4)
                    {
                        status = AMONet._mnet_m4_start_r_move(ringNo, cardID, axisNo, distance);
                        if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to start_r_move", status);
                    }
                }
            }
        }

        public bool GetMotionDone()
        {
            ushort motion = 0;
            short status;

            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                    motion = 0;
                }
                else
                {
                    if (GetAlarm())
                    {
                        throw new MotionAlarm("Motion has alarm", axisNo);
                    }
                    if (axisCount == 2)
                    {
                        status = AMONet._mnet_m2_motion_done(ringNo, cardID, axisNo, ref motion);
                        if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to get motion done", status);
                    }
                    else if (axisCount == 4)
                    {
                        status = AMONet._mnet_m4_motion_done(ringNo, cardID, axisNo, ref motion);
                        //if (status < 0) throw new AmonetMotionException("Fail to get motion done", status);
                    }
                }

            }
            if (GetAlarm())
            {
                throw new MotionAlarm("Motion has alarm", axisNo);
            }
            return (motion == 0);
        }

        public int GetPosition()
        {
            short status;
            int Pos = 0;
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                    Pos = 100;
                }
                else
                {
                    if (axisCount == 2)
                    {
                        status = AMONet._mnet_m2_get_position(ringNo, cardID, axisNo, ref Pos);
                        if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to get position", status);
                    }
                    else if (axisCount == 4)
                    {
                        status = AMONet._mnet_m4_get_position(ringNo, cardID, axisNo, ref Pos);
                        if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to get position", status);
                    }
                }
            }
            return Pos;
        }

        public int GetErrorCounter()
        {
            short status;
            int errorCounter = 0;
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                    errorCounter = 100;
                }
                else
                {
                    if (axisCount == 2)
                    {
                        status = AMONet._mnet_m2_get_error_counter(ringNo, cardID, axisNo, ref errorCounter);
                        if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to get error counter", status);
                    }
                    else if (axisCount == 4)
                    {
                        status = AMONet._mnet_m4_get_error_counter(ringNo, cardID, axisNo, ref errorCounter);
                        if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to get error counter", status);
                    }
                }
            }
            return errorCounter;
        }

        public void ResetErrorCounter()
        {
            short status;
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                }
                else
                {
                    if (axisCount == 2)
                    {
                        status = AMONet._mnet_m2_reset_error_counter(ringNo, cardID, axisNo);
                        if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to reset error counter", status);
                    }
                    else if (axisCount == 4)
                    {
                        status = AMONet._mnet_m4_reset_error_counter(ringNo, cardID, axisNo);
                        if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to reset error counter", status);
                    }
                }
            }
        }

        public int GetCommand()
        {
            short status;
            int cmdCounter = 0;
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                    cmdCounter = 100;
                }
                else
                {
                    if (axisCount == 2)
                    {
                        status = AMONet._mnet_m2_get_command(ringNo, cardID, axisNo, ref cmdCounter);
                        if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to get error counter", status);
                    }
                    else if (axisCount == 4)
                    {
                        status = AMONet._mnet_m4_get_command(ringNo, cardID, axisNo, ref cmdCounter);
                        if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to get error counter", status);
                    }
                }
            }
            return cmdCounter;
        }

        public void Home(int timeOutInSeconds)
        {
            HomeNoWait();
            WaitMotionDone(timeOutInSeconds);
            SetSpeed(RunSpeedCfg.StartVelocity, RunSpeedCfg.MaxVelocity, RunSpeedCfg.RampUpTime, RunSpeedCfg.RampDownTime);
        }

        public void WaitMotionDone(int timeOutInSeconds)
        {
            DateTime now = DateTime.Now;
            ManualResetEvent ev;
            while (!GetMotionDone())
            {
                ev = new ManualResetEvent(false);
                ev.WaitOne(50);
                if (requestStop)
                {
                    requestStop = false;
                    break;
                }
                if ((DateTime.Now - now).TotalSeconds > timeOutInSeconds)
                {
                    throw new MotionTimeout(string.Format("Motion axis {0} timeout on waiting motion done", axisNo));
                }
            }
            if (useHomeSpeed)
            {
                useHomeSpeed = false;
                SetSpeed(RunSpeedCfg.StartVelocity, RunSpeedCfg.MaxVelocity,
                    RunSpeedCfg.RampUpTime, RunSpeedCfg.RampDownTime);
            }
        }

        public void WaitXYMotionDone(int timeOutInSeconds)
        {
            WaitMotionDone(timeOutInSeconds);
        }

        public void WaitXYZMotionDone(int timeOutInSeconds)
        {
            WaitMotionDone(timeOutInSeconds);
        }

        public void HomeNoWait()
        {
            short status;
            requestStop = false;
            // Start homing
            useHomeSpeed = true;
            SetSpeed(HomeSpeedCfg.StartVelocity, HomeSpeedCfg.MaxVelocity,
                HomeSpeedCfg.RampUpTime, HomeSpeedCfg.RampDownTime);
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                }
                else
                {
                    if (axisCount == 2)
                    {
                        status = AMONet._mnet_m2_start_home_search(ringNo, cardID, axisNo, homeDir, 1);
                        if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to home", status);
                    }
                    else if (axisCount == 4)
                    {
                        status = AMONet._mnet_m4_start_home_search(ringNo, cardID, axisNo, homeDir, 1);
                        if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to home", status);
                    }
                }
            }
        }

        public void Reset()
        {
            short status;
            // Start homing
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                }
                else
                {
                    if (axisCount == 2)
                    {
                        status = AMONet._mnet_m2_reset_position(ringNo, cardID, axisNo);
                        if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to reset position", status);

                        status = AMONet._mnet_m2_reset_error_counter(ringNo, cardID, axisNo);
                        if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to reset error counter", status);

                        status = AMONet._mnet_m2_reset_command(ringNo, cardID, axisNo);
                        if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to reset command", status);
                    }
                    else if (axisCount == 4)
                    {
                        status = AMONet._mnet_m4_reset_position(ringNo, cardID, axisNo);                        
                        if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to reset position", status);
                        
                        status = AMONet._mnet_m4_reset_error_counter(ringNo, cardID, axisNo);
                        if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to reset error counter", status);

                        status = AMONet._mnet_m4_reset_command(ringNo, cardID, axisNo);
                        if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to reset command", status);
                    }
                }
            }

        }

        public void ResetAlarm()
        {
            short status;
            ManualResetEvent ev = new ManualResetEvent(false);
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                }
                else
                {
                    if (axisCount == 2)
                    {
                        status = AMONet._mnet_m2_set_ralm(ringNo, cardID, axisNo, 1);
                        if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to ralm", status);
                    }
                    else if (axisCount == 4)
                    {
                        status = AMONet._mnet_m4_set_ralm(ringNo, cardID, axisNo, 1);
                        if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to ralm", status);
                    }
                }

                ev.WaitOne(100);
                
                if (simulationMode)
                {
                    status = 0;
                }
                else
                {
                    if (axisCount == 2)
                    {
                        status = AMONet._mnet_m2_set_ralm(ringNo, cardID, axisNo, 0);
                        if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to ralm", status);
                    }
                    else if (axisCount == 4)
                    {
                        status = AMONet._mnet_m4_set_ralm(ringNo, cardID, axisNo, 0);
                        if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to ralm", status);
                    }
                }
            }
        }

        public IOStatus GetIOStatus()
        {
            short status;
            uint IO_status = 0;
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                }
                else
                {
                    if (axisCount == 2)
                    {
                        status = AMONet._mnet_m2_get_io_status(ringNo, cardID, axisNo, ref IO_status);
                        if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to get io status", status);
                    }
                    else if (axisCount == 4)
                    {
                        status = AMONet._mnet_m4_get_io_status(ringNo, cardID, axisNo, ref IO_status);
                        if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to get io status", status);
                    }
                }
            }
            var ioStatus = new IOStatus(IO_status);
            if (invertSVON)
            {
                ioStatus.servoOn = (ioStatus.servoOn) ? false : true;
            }
            return ioStatus;
        }

        public bool GetAlarm()
        {
            return GetIOStatus().alarm;
        }

        public bool GetRDY()
        {
            return GetIOStatus().ready;
        }

        public bool GetInPosition()
        {
            return GetIOStatus().inPosition;
        }

        public uint GetErrorStatus()
        {
            short status;
            uint errorStatus = 0;
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                    errorStatus = 100;
                }
                else
                {
                    if (axisCount == 2)
                    {
                        status = AMONet._mnet_m2_error_status(ringNo, cardID, axisNo, ref errorStatus);
                        if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to get error status", status);
                    }
                    else if (axisCount == 4)
                    {
                        status = AMONet._mnet_m4_error_status(ringNo, cardID, axisNo, ref errorStatus);
                        if (status != AMONet.ERR_NoError) throw new AmonetMotionException("Fail to get error status", status);
                    }
                }
            }
            return errorStatus;
        }

        public void LimitSearchNoWait(int direction)
        {
            throw new NotImplementedException();
        }
    }
}

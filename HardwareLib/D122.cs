using System;
using System.Text;
using System.Runtime.InteropServices;

namespace TPM
{
    
        public class D122
        {

            private const string DLLNAME = "D122_x64.dll";
            // Error Table
            public enum ErrCode : short
            {
                D122ERR_NoError = 0,
                D122ERR_InvalidSwitchCardNumber = -1,
                D122ERR_SwitchCardNumberRepeated = -2,
                D122ERR_MapMemoryFailed = -3,
                D122ERR_CardNotExist = -4,
                D122ERR_InvalidBoardID = -5,
                D122ERR_InvalidParameter1 = -6,
                D122ERR_InvalidParameter2 = -7,
                D122ERR_InvalidParameter3 = -8,
                D122ERR_InvalidParameter4 = -9,
                D122ERR_InvalidParameter5 = -10,
                D122ERR_InvalidParameter6 = -11,
                D122ERR_InvalidParameter7 = -12,
                D122ERR_InvalidParameter8 = -13,
                D122ERR_InvalidParameter9 = -14,
                D122ERR_NotSupported = -15,
            }

            public enum CardType : byte
            {
                CARD_UNKNOWN = 0,
                CARD_PCE_D122 = 1,
                CARD_PCE_D132 = 2,
                CARD_PCE_D150 = 3,
                CARD_PCE_D105 = 4,
            }


            [DllImport(DLLNAME)]
            public static extern short _d122_open(ref ushort ExistCards);
            [DllImport(DLLNAME)]
            public static extern short _d122_close();
            [DllImport(DLLNAME)]
            public static extern short _d122_check_switch_card_num(ushort SwitchCardNo, ref byte IsExist);
            [DllImport(DLLNAME)]
            public static extern short _d122_get_switch_card_num(ushort CardIndex, ref ushort SwitchCardNo);
            [DllImport(DLLNAME)]
            public static extern short _d122_get_card_type(ushort SwitchCardNo, ref byte CardType);
            [DllImport(DLLNAME)]
            public static extern short _d122_get_cpld_version(ushort SwitchCardNo, ref ushort CpldVer);

            ////////////////////////////////////////////////////////////////////////////////
            // read / write all
            ////////////////////////////////////////

            // U8 InData[]
            //   PCE-D122: InData[4]
            //   PCE-D132: InData[6]
            //   PCE-D150: InData[10]
            //   PCE-D105: NotSupported
            [DllImport(DLLNAME)]
            public static extern short _d122_read_input(ushort SwitchCardNo, byte[] InData);

            // U8 OutData[]
            //   PCE-D122: OutData[4]
            //   PCE-D132: OutData[4]
            //   PCE-D150: NotSupported
            //   PCE-D105: OutData[10]
            [DllImport(DLLNAME)]
            public static extern short _d122_read_output(ushort SwitchCardNo, byte[] OutData);
            [DllImport(DLLNAME)]
            public static extern short _d122_write_output(ushort SwitchCardNo, byte[] OutData);

            ////////////////////////////////////////////////////////////////////////////////
            // read / write one bit
            ////////////////////////////////////////

            // U8 InBitNo
            //   PCE-D122: 0 ~ 31
            //   PCE-D132: 0 ~ 47
            //   PCE-D150: 0 ~ 79
            //   PCE-D105: NotSupported
            [DllImport(DLLNAME)]
            public static extern short _d122_read_input_bit(ushort SwitchCardNo, byte InBitNo, ref byte OnOff);

            // U8 OutBitNo
            //   PCE-D122: 0 ~ 31
            //   PCE-D132: 0 ~ 31
            //   PCE-D150: NotSupported
            //   PCE-D105: 0 ~ 79
            [DllImport(DLLNAME)]
            public static extern short _d122_read_output_bit(ushort SwitchCardNo, byte OutBitNo, ref byte OnOff);
            [DllImport(DLLNAME)]
            public static extern short _d122_write_output_bit(ushort SwitchCardNo, byte OutBitNo, byte OnOff);
            [DllImport(DLLNAME)]
            public static extern short _d122_toggle_output_bit(ushort SwitchCardNo, byte OutBitNo);

            ////////////////////////////////////////////////////////////////////////////////
            // read / write one byte
            ////////////////////////////////////////

            // U8 PortNo
            //   PCE-D122: 0 ~ 7
            //   PCE-D132: 0 ~ 9
            //   PCE-D150: 0 ~ 9
            //   PCE-D105: 0 ~ 9
            //
            //        D122        D132        D150        D105
            // Port0: DI[ 0.. 7]  DI[ 0.. 7]  DI[ 0.. 7]  DO[ 0.. 7]
            // Port1: DI[ 8..15]  DI[ 8..15]  DI[ 8..15]  DO[ 8..15]
            // Port2: DI[16..23]  DI[16..23]  DI[16..23]  DO[16..23]
            // Port3: DI[24..31]  DI[24..31]  DI[24..31]  DO[24..31]
            // Port4: DO[ 0.. 7]  DI[32..39]  DI[32..39]  DO[32..39]
            // Port5: DO[ 8..15]  DI[40..47]  DI[40..47]  DO[40..47]
            // Port6: DO[16..23]  DO[ 0.. 7]  DI[48..55]  DO[48..55]
            // Port7: DO[24..31]  DO[ 8..15]  DI[56..63]  DO[56..63]
            // Port8:             DO[16..23]  DI[64..71]  DO[64..71]
            // Port9:             DO[24..31]  DI[72..79]  DO[72..79]
            [DllImport(DLLNAME)]
            public static extern short _d122_read_port(ushort SwitchCardNo, byte PortNo, ref byte Value);
            [DllImport(DLLNAME)]
            public static extern short _d122_write_port(ushort SwitchCardNo, byte PortNo, byte Value);
            ////////////////////////////////////////////////////////////////////////////////
        }
    
}

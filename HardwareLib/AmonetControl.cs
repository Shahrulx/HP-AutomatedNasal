﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AMONetLib;
using System.Threading;

namespace HardwareLib
{
    public class AmonetControl
    {
        private bool simulationMode = false;

        public Object thisLock = new Object();
        private ushort expectedCardNum = 0;  //set to 0 for don't care
        private ushort ringNo = 0;
        private ushort baudRate = 0;
        private bool m_Initialized = false;
        public Dictionary<ushort, Dictionary<byte, byte>> inputValue = new Dictionary<ushort, Dictionary<byte, byte>>();
        public Dictionary<ushort, Dictionary<byte, byte>> outputValue = new Dictionary<ushort, Dictionary<byte, byte>>();

        public AmonetControl(ushort ringNo, ushort baudRate, ushort expectedCardNum, bool simulationMode)
        {
            this.ringNo = ringNo;
            this.baudRate = baudRate;
            this.expectedCardNum = expectedCardNum;
            this.simulationMode = simulationMode;
        }

        public void Setup()
        {
            short cardNum;
            short status = 0;

            if (m_Initialized || simulationMode)
            {
                return;
            }

            m_Initialized = true;
            cardNum = AMONet._mnet_initial();

            if ((expectedCardNum != 0 && cardNum != expectedCardNum) || 
                cardNum == 0)
            {
                throw new AmonetException(string.Format("Amonet card not match only found {0} card", cardNum), 44);
            }

            status = AMONet._mnet_set_ring_config(ringNo, baudRate);
            if (status != AMONet.ERR_NoError) throw new AmonetException("Fail set ring config", status);

            status = AMONet._mnet_reset_ring(ringNo);
            if (status != AMONet.ERR_NoError) throw new AmonetException("Fail reset ring", status);

            status = AMONet._mnet_start_ring(ringNo);
            if (status != AMONet.ERR_NoError) throw new AmonetException("Fail start ring", status);
        }

        public void TearDown()
        {
            short status = 0;
            status = AMONet._mnet_close();
            m_Initialized = false;
            if (status < 0)
            {
                throw new AmonetException("Fail to close amonet", status);
            }
        }
    }
}

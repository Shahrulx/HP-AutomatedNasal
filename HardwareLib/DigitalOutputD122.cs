﻿using System;
using System.Collections.Generic;
using HardwareInterfaces;
using TPM;

namespace HardwareLib
{
    public class DigitalOutputD122: IDigitalOutput
    {
        public string Name { get; set; }
        public ushort CardID { get; set; }
        public bool Disable { get; set; }
        public byte Msb { get; set; }
        public byte Lsb { get; set;}
        private byte port = 99;
        private byte portMsb;
        private byte portLsb;
        private bool simulationMode;
        private Object thisLock;
        private Dictionary<ushort, Dictionary<byte, byte>> outputValue;

        public DigitalOutputD122(string name, Object thisLock, ushort cardID,
            Dictionary<ushort, Dictionary<byte, byte>> outputValue, byte msb, byte lsb, bool simulationMode)
        {
            byte portForMsb = 99;
            byte portForLsb = 99;

            this.Name = name;
            this.thisLock = thisLock;
            this.CardID = cardID;
            this.outputValue = outputValue;
            this.Msb = msb;
            this.Lsb = lsb;
            this.simulationMode = simulationMode;
            this.Disable = false;

            var portMsbLsbInfo = Utility.GetMsbLsbPortInfo(msb, lsb);
            portForMsb = portMsbLsbInfo.Item1;
            portForLsb = portMsbLsbInfo.Item2;
            portMsb = portMsbLsbInfo.Item3;
            portLsb = portMsbLsbInfo.Item4;
            if (portForMsb == portForLsb)
            {
                port = portForMsb;
            }

            if (!outputValue.ContainsKey(cardID))
            {
                Dictionary<byte, byte> cardValue = new Dictionary<byte, byte>();
                outputValue[cardID] = cardValue;
            }
            if (!outputValue[cardID].ContainsKey(portForLsb))
            {
                outputValue[cardID][portForLsb] = ReadOutput(portForLsb);
                //outputValue[cardID][portForLsb] = 0;
            }
            if (!outputValue[cardID].ContainsKey(portForMsb))
            {
                outputValue[cardID][portForMsb] = ReadOutput(portForMsb);
                //outputValue[cardID][portForMsb] = 0;
            }
        }

        public void Write(int value)
        {
            int currentOutput;
            if (Disable)
            {
                return;
            }
            lock (thisLock)
            {
                if (port != 99)
                {
                    currentOutput = GetOutput(port);
                    currentOutput = Utility.SetBits(currentOutput, portLsb, portMsb, value);
                    SetOutput(port, (byte)currentOutput);
                }
                else
                {
                    currentOutput = GetOutput();
                    currentOutput = Utility.SetBits(currentOutput, Lsb, Msb, value);
                    SetOutput(currentOutput);
                }
            }
        }

        public void ON()
        {
            Write(1);
        }

        public void OFF()
        {
            Write(0);
        }

        public int Get()
        {
            int currentOutput;
            lock (thisLock)
            {
                if (port != 99)
                {
                    currentOutput = GetOutput(port);
                    return Utility.GetBits(currentOutput, portLsb, portMsb);
                }
                else
                {
                    currentOutput = GetOutput();
                    return Utility.GetBits(currentOutput, Lsb, Msb);
                }
            }
        }

        public int Read()
        {
            int currentOutput;
            lock (thisLock)
            {
                if (port != 99)
                {
                    currentOutput = ReadOutput(port);
                    return Utility.GetBits(currentOutput, portLsb, portMsb);
                }
                else
                {
                    currentOutput = ReadOutput();
                    return Utility.GetBits(currentOutput, Lsb, Msb);
                }
            }
        }

        private int ReadOutput()
        {
            int retValue = 0;
            byte portValue;
            for (byte port = 4; port < 8; port++)
            {
                portValue = ReadOutput(port);
                retValue |= portValue << (port * 7);
            }
            return retValue;
        }

        private byte ReadOutput(byte port)
        {
            short status = 0;
            byte value = 0;
            if (simulationMode)
            {
                status = 0;
            }
            else
            {
                status = D122._d122_read_port(CardID, port, ref value);
            }

            if (status == 0)
            {
                return value;
            }
            else
            {
                throw new UniDaqException("Fail to get io output value", (ushort)status);
            }
        }

        private int GetOutput()
        {
            int retValue = 0;
            byte portValue;
            for (byte port = 4; port < 8; port++)
            {
                portValue = GetOutput(port);
                retValue |= portValue << (port * 7);
            }
            return retValue;
        }

        private byte GetOutput(byte port)
        {
            return outputValue[CardID][port];
        }

        private void SetOutput(byte port, byte value)
        {
            short status = 0;

            if (simulationMode)
            {
                status = 0;
            }
            else
            {
                status = D122._d122_write_port(CardID, port, value);
            }

            if (status == 0)
            {
                outputValue[CardID][port] = value;
            }
            else
            {
                throw new UniDaqException("Fail to write io output", (ushort)status);
            }
        }

        private void SetOutput(int value)
        {
            byte PortNo;
            byte Val = 0;

            for (PortNo = 4; PortNo < 8; PortNo++)
            {
                Val = (byte)Utility.GetBits(value, PortNo * 8, (PortNo + 1) * 8 - 1);
                SetOutput(PortNo, Val);
            }
        }
    }
}

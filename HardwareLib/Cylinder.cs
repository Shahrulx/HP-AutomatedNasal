﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HardwareInterfaces;
using System.Threading;

namespace HardwareLib
{

    struct sNameState
    {
        public string Name;
        public int State;

        public sNameState(string name, int state)
        {
            this.Name = name;
            this.State = state;
        }
    }

    public class Cylinder : ICylinder
    {
        string name;
        string errorMessage;
        string extendName;
        string retractName;
        string defaultState;
        int extendSensorTimeout;
        int retractSensorTimeout;
        int extendWait;
        int retractWait;
        bool resetIO;
        int waitBeforeReset;

        Hardware hw;
        string currentState;
        string currentStatus;
        List<sNameState> extendDOs = new List<sNameState>();
        List<sNameState> retractDOs = new List<sNameState>();
        List<sNameState> extendSensorDIs = new List<sNameState>();
        List<sNameState> retractSensorDIs = new List<sNameState>();

        public Cylinder(Hardware hw, string name, string errorMessage, string extendName,
                        string retractName, string defaultState,
                        int extendSensorTimeout, int retractSensorTimeout,
                        int extendWait, int retractWait,
                        bool resetIO, int waitBeforeReset)
        {
            this.hw = hw;
            this.name = name;
            this.errorMessage = errorMessage;
            this.extendName = extendName;
            this.retractName = retractName;
            this.defaultState = defaultState;
            this.extendSensorTimeout = extendSensorTimeout;
            this.retractSensorTimeout = retractSensorTimeout;
            this.extendWait = extendWait;
            this.retractWait = retractWait;
            this.resetIO = resetIO;
            this.waitBeforeReset = waitBeforeReset;
            this.currentState = "NA";
            this.currentStatus = "NA";
        }

        public void AddExtendDO(string digitalOutName, int state)
        {
            extendDOs.Add(new sNameState(digitalOutName, state));
        }

        public void AddRetractDO(string digitalOutName, int state)
        {
            retractDOs.Add(new sNameState(digitalOutName, state));
        }

        public void AddExtendSensorDI(string digitalInputName, int state)
        {
            extendSensorDIs.Add(new sNameState(digitalInputName, state));
        }

        public void AddRetractSensorDI(string digitalInputName, int state)
        {
            retractSensorDIs.Add(new sNameState(digitalInputName, state));
        }

        public string Name => this.name;

        public string ErrorMessage => this.errorMessage.Replace("{action}", this.CurrentState);

        public string ExtendName => this.extendName;

        public string RetractName => this.retractName;

        public string CurrentStatus => this.currentStatus;

        public string CurrentState => this.currentState;

        public string SensorStatus
        {
            get
            {
                if (compareDI(extendSensorDIs) == true)
                {
                    return ExtendName;
                }
                else if (compareDI(retractSensorDIs) == true)
                {
                    return RetractName;
                }
                else if (extendSensorDIs.Count > 0 && retractSensorDIs.Count > 0)
                {
                    return "Error";
                }
                else
                {
                    return "NoSensor";
                }
            }
        }

        public string SensorInformation
        {
            get
            {
                string info = "";
                string extendInfo = "";
                string retractInfo = "";
                foreach (var o in extendSensorDIs)
                {
                    extendInfo += $"  {o.Name}={o.State}" + Environment.NewLine;
                }
                foreach (var o in retractSensorDIs)
                {
                    retractInfo += $"  {o.Name}={o.State}" + Environment.NewLine;
                }
                info = $"{ExtendName} Sensor:{Environment.NewLine}{extendInfo}" + Environment.NewLine + 
                       $"{RetractName} Sensor:{Environment.NewLine}{retractInfo}";
                return info;
            }
        }

        public string OutputInformation
        {
            get
            {
                string info = "";
                string extendInfo = "";
                string retractInfo = "";
                foreach (var o in extendDOs)
                {
                    extendInfo += $"  {o.Name}={o.State}" + Environment.NewLine;
                }
                foreach (var o in retractDOs)
                {
                    retractInfo += $"  {o.Name}={o.State}" + Environment.NewLine;
                }
                info = $"{ExtendName}:{Environment.NewLine}{extendInfo}" + Environment.NewLine +
                       $"{RetractName}:{Environment.NewLine}{retractInfo}";
                return info;
            }
        }

        public void Do(string name, bool wait = true)
        {
            if (name == ExtendName)
                Extend(wait);
            else if (name == RetractName)
                Retract(wait);
            else
                throw new Exception("Undefine name: " + name);

        }

        public void Extend(bool wait = true)
        {
            setDOs(extendDOs);
            this.currentState = ExtendName;
            Wait(extendWait);
            if (wait)
                WaitSensor();
        }

        public void Retract(bool wait = true)
        {
            setDOs(retractDOs);
            this.currentState = RetractName;
            Wait(retractWait);
            if (wait)
                WaitSensor();
        }

        public void SetDefault(bool wait = true)
        {
            Do(defaultState, wait);
        }

        public void WaitSensor()
        {
            DateTime now = DateTime.Now;
            ManualResetEvent ev;
            //check if there are no sensor to waited
            if (currentState == ExtendName && extendSensorDIs.Count == 0)
                return;
            else if (currentState == RetractName && retractSensorDIs.Count == 0)
                return;

            int timeout = currentState == ExtendName ? extendSensorTimeout : retractSensorTimeout;
    
            while (SensorStatus != currentState)
            {
                ev = new ManualResetEvent(false);
                ev.WaitOne(25);
                if ((DateTime.Now - now).TotalMilliseconds > timeout)
                {
                    throw new CylinderTimeout(ErrorMessage);
                }
                if (hw.SimulationMode)
                    break;
            }
        }

        private void Wait(int time)
        {
            ManualResetEvent ev;
            ev = new ManualResetEvent(false);
            ev.WaitOne(time);
        }

        private bool? compareDI(List<sNameState> DIs)
        {
            foreach(var i in DIs)
            {
                if (hw.inputs[i.Name].Read() != i.State)
                {
                    return false;
                }
            }
            if (DIs.Count > 0)
                return true;
            return null;
        }

        private void setDOs(List<sNameState> DOs)
        {
            foreach (var o in DOs)
                hw.outputs[o.Name].Write(o.State);
        }
    }
}

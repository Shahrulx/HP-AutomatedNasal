﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UniDAQ_Ns;
using System.Threading;

namespace HardwareLib
{
    public class UniDaqControl
    {
        private bool simulationMode = false;

        public Object thisLock = new Object();
        private bool m_Initialized = false;
        public Dictionary<ushort, Dictionary<byte, byte>> inputValue = new Dictionary<ushort, Dictionary<byte, byte>>();
        public Dictionary<ushort, Dictionary<byte, byte>> outputValue = new Dictionary<ushort, Dictionary<byte, byte>>();

        ushort wTotalBoard, wInitialCode, wBoardNo;
        ushort wDIOWidth, wOutputPort, wInputPort;

        UniDAQ.IXUD_CARD_INFO[] sCardInfo = new UniDAQ.IXUD_CARD_INFO[UniDAQ.MAX_BOARD_NUMBER];
        UniDAQ.IXUD_DEVICE_INFO[] sDeviceInfo = new UniDAQ.IXUD_DEVICE_INFO[UniDAQ.MAX_BOARD_NUMBER];

        public UniDaqControl(bool simulationMode)
        {
            this.simulationMode = simulationMode;
        }

        public void Setup()
        {
            byte[] szModeName = new byte[20];
            if (m_Initialized || simulationMode)
            {
                return;
            }

            m_Initialized = true;

            //Driver Initial
            wInitialCode = UniDAQ.Ixud_DriverInit(ref wTotalBoard);
            if (wInitialCode != UniDAQ.Ixud_NoErr)
            {
                throw new UniDaqException("Driver Initial Error!!", wInitialCode);
            }

            for (ushort wBoardIndex = 0; wBoardIndex < wTotalBoard; wBoardIndex++)
            {
                //Get Card Information
                wInitialCode = UniDAQ.Ixud_GetCardInfo(wBoardIndex, ref sDeviceInfo[wBoardIndex], ref sCardInfo[wBoardIndex], szModeName);
            }
            UniDAQ.Ixud_WriteDOBit(0, 1, 1, 1);
            UniDAQ.Ixud_WriteDOBit(0, 1, 2, 1);
            UniDAQ.Ixud_WriteDOBit(0, 1, 3, 1);
            UniDAQ.Ixud_WriteDOBit(0, 1, 4, 1);

        }

        public void TearDown()
        {
            ushort status = 0;
            m_Initialized = false;
            if (status < 0)
            {
                throw new UniDaqException("Fail to close uniDaqIO", status);
            }
        }
    }
}

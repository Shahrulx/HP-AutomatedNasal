﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Advantech.Motion;//Common Motion API

using HardwareInterfaces;
using System.Runtime.InteropServices;

namespace HardwareLib
{
    public class MotorCommonMotion : IMotor
    {
        public string Name { get; set; }
        public MotorType MotorType { get; set; }
        public ISpeedCfg HomeSpeedCfg {get; set;}
        public ISpeedCfg RunSpeedCfg { get; set; }
        public ISpeedCfg XYRunSpeedCfg { get; set; }
        public ISpeedCfg XYZRunSpeedCfg { get; set; }
        public IDigitalOutput ClearAlarm { get; set; }

        private bool simulationMode = false;
        private ushort axisNo;
        private byte homeDir;
        private Object thisLock;
        private bool requestStop;
        private bool useHomeSpeed = false;
        private uint homeMode;
        private CommonMotionControl commonMotionControl;
        public double PulsePermm { get; set; }
        public bool UnitInmm { get; set; }
        private IntPtr deviceHandle
        {
            get
            {
                return commonMotionControl.m_DeviceHandle;
            }
        }
        private IntPtr axisHandle
        {
            get
            {
                return commonMotionControl.m_Axishand[axisNo];
            }
        }
        private IntPtr xyAxisHandle
        {
            get
            {
                return commonMotionControl.xyAxisHandle;
            }
        }

        public MotorCommonMotion(CommonMotionControl commonMotionControl, string name, ushort axisNo, byte homeDir, uint homeMode, ISpeedCfg homeSpeedCfg, bool simulationMode)
        {
            this.commonMotionControl = commonMotionControl;
            this.Name = name;
            this.thisLock = commonMotionControl.thisLock;
            this.axisNo = axisNo;
            this.homeDir = homeDir;
            this.homeMode = homeMode;
            this.HomeSpeedCfg = homeSpeedCfg;
            this.RunSpeedCfg = homeSpeedCfg;
            this.simulationMode = simulationMode;
            //this.deviceHandle = commonMotionControl.m_DeviceHandle;
            //this.axisHandle = commonMotionControl.m_Axishand[axisNo];
            //this.xyAxisHandle = commonMotionControl.xyAxisHandle;
            this.PulsePermm = 1;
            this.UnitInmm = false;
            requestStop = false;
        }

        private void SetAxisProperty(PropertyID Propertyid, double PropertyValue)
        {
            if (simulationMode)
            {
                return;
            }
            UInt32 Result = Motion.mAcm_SetProperty(axisHandle, (uint)Propertyid, ref PropertyValue, (uint)Marshal.SizeOf(typeof(double)));
            if (Result != (uint)ErrorCode.SUCCESS)
            {
                throw new Exception("SetAxisProperty Failed With Error Code[0x" + Convert.ToString(Result, 16) + "]");
            }
        }

        private void SetGpProperty(PropertyID Propertyid, double PropertyValue)
        {
            if (simulationMode)
            {
                return;
            }
            UInt32 Result = Motion.mAcm_SetProperty(xyAxisHandle, (uint)Propertyid, ref PropertyValue, (uint)Marshal.SizeOf(typeof(double)));
            if (Result != (uint)ErrorCode.SUCCESS)
            {
                throw new Exception("SetGpProperty Failed With Error Code[0x" + Convert.ToString(Result, 16) + "]");
            }
        }

        private void SetAxisProperty(PropertyID Propertyid, UInt32 PropertyValue)
        {
            if (simulationMode)
            {
                return;
            }
            UInt32 Result = Motion.mAcm_SetProperty(axisHandle, (uint)Propertyid, ref PropertyValue, (uint)Marshal.SizeOf(typeof(UInt32)));
            if (Result != (uint)ErrorCode.SUCCESS)
            {
                throw new Exception("Set Failed With Error Code[0x" + Convert.ToString(Result, 16) + "]");
            }
        }

        private void SetDO(ushort DoChannel, byte BitData)
        {
            if (simulationMode)
            {
                return;
            }
            UInt32 Result = Motion.mAcm_AxDoSetBit(axisHandle, DoChannel, BitData);
            if (Result != (uint)ErrorCode.SUCCESS)
            {
                throw new Exception("Set DO bit Failed With Error Code[0x" + Convert.ToString(Result, 16) + "]");
            }
        }

        public void Start()
        {
            if (simulationMode)
            {
                // pass
            }
            else
            {
                SetAxisProperty(PropertyID.CFG_AxHomeResetEnable, (UInt32)1);
            }
            SetSVOn(1);
        }

        public void SetSpeed(double StartVelocity, double MaxVelocity, double rampUpTime=0.5, double rampDownTime=0.5)
        {
            lock (thisLock)
            {
                if (simulationMode)
                {
                }
                else
                {
                    SetAxisProperty(PropertyID.PAR_AxVelLow, StartVelocity);
                    SetAxisProperty(PropertyID.PAR_AxVelHigh, MaxVelocity);
                    SetAxisProperty(PropertyID.PAR_AxAcc, rampUpTime);
                    SetAxisProperty(PropertyID.PAR_AxDec, rampDownTime);
                    Thread.Sleep(200);
                }                
                //var speedCfg = new ISpeedCfg(StartVelocity, MaxVelocity, rampUpTime, rampDownTime);
                //this.RunSpeedCfg = speedCfg;
            }
            if (axisNo == 0)
            {
                SetGpSpeed(StartVelocity, MaxVelocity, rampUpTime, rampDownTime);
                Thread.Sleep(200);
            }
            
        }

        private void SetGpSpeed(double StartVelocity, double MaxVelocity, double rampUpTime = 0.5, double rampDownTime = 0.5)
        {
            lock (thisLock)
            {
                if (simulationMode)
                {
                }
                else
                {
                    SetGpProperty(PropertyID.PAR_GpVelLow, StartVelocity);
                    SetGpProperty(PropertyID.PAR_GpVelHigh, MaxVelocity);
                    SetGpProperty(PropertyID.PAR_GpAcc, rampUpTime);
                    SetGpProperty(PropertyID.PAR_GpDec, rampDownTime);
                }
            }
        }

        public void SetSVOn(ushort svOn)
        {
            UInt32 Result;
            lock (thisLock)
            {
                if (simulationMode)
                {
                }
                else
                {
                    Result = Motion.mAcm_AxSetSvOn(axisHandle, svOn);
                    if (Result != (uint)ErrorCode.SUCCESS)
                    {
                        throw new Exception("Servo On Failed With Error Code[0x" + Convert.ToString(Result, 16) + "]");
                    }
                }
            }
        }

        public void Stop(bool emergency=false)
        {
            UInt32 Result;
            lock (thisLock)
            {
                if (simulationMode)
                {
                }
                else
                {
                    if (emergency) Result = Motion.mAcm_AxStopEmg(axisHandle);
                    else Result = Motion.mAcm_AxStopDec(axisHandle);
                    if (Result != (uint)ErrorCode.SUCCESS)
                    {
                        throw new Exception("Stop Failed With Error Code[0x" + Convert.ToString(Result, 16) + "]");
                    }

                    if (axisNo == 0)
                    {
                        if (emergency) Result = Motion.mAcm_GpStopEmg(xyAxisHandle);
                        else Result = Motion.mAcm_GpStopDec(xyAxisHandle);
                    }
                }
            }
            // make sure the motor stop then only ask to quit the wait
            requestStop = true;
        }

        public void MoveAbsolute(int Position, int timeOutInSeconds)
        {
            DateTime now = DateTime.Now;
            MoveAbsoluteNoWait(Position);
            WaitMotionDone(timeOutInSeconds);
        }

        public void MoveRelative(int distance, int timeOutInSeconds)
        {
            DateTime now = DateTime.Now;
            MoveRelativeNoWait(distance);
            WaitMotionDone(timeOutInSeconds);
        }

        public void MoveAbsoluteNoWait(int Position)
        {
            UInt32 Result;
            requestStop = false;
            if (useHomeSpeed)
            {
                useHomeSpeed = false;
                SetSpeed(RunSpeedCfg.StartVelocity, RunSpeedCfg.MaxVelocity,
                    RunSpeedCfg.RampUpTime, RunSpeedCfg.RampDownTime);
            }

            //workaround, set the command with current position first
            //SetCommand(GetPosition());

            if (simulationMode)
            {
            }
            else
            {
                if (!GetMotionDone())
                {
                    throw new AmonetMotionException("Start a motion before the previous motion done" + Name, (short)axisNo);
                }
                lock (thisLock)
                {
                    Result = Motion.mAcm_AxMoveAbs(axisHandle, Convert.ToDouble(Position));
                    Thread.Sleep(100);
                    if (Result != (uint)ErrorCode.SUCCESS)
                    {
                        throw new Exception("MoveAbs Failed With Error Code[0x" + Convert.ToString(Result, 16) + "]");
                    }
                }
            }
        }

        public void MoveAbsoluteXY(int PositionX, int PositionY, int timeOutInSeconds)
        {
            DateTime now = DateTime.Now;
            MoveAbsoluteXYNoWait(PositionX, PositionY);
            WaitXYMotionDone(timeOutInSeconds);
        }

        public void MoveAbsoluteXYNoWait(int PositionX, int PositionY)
        {
            UInt32 Result;
            requestStop = false;
            uint AxisNum = 2;
            if (simulationMode)
            {
            }
            else
            {
                if (!GetXYMotionDone())
                {
                    throw new AmonetMotionException("Start a motion before the previous motion done", (short)axisNo);
                }
                lock (thisLock)
                {
                    Result = Motion.mAcm_GpMoveLinearAbs(xyAxisHandle, new double[] { PositionX, PositionY }, ref AxisNum);
                    Thread.Sleep(100);
                    if (Result != (uint)ErrorCode.SUCCESS)
                    {
                        throw new Exception("MoveAbsXYNoWait Failed With Error Code[0x" + Convert.ToString(Result, 16) + "]");
                    }
                }
            }
        }

        public void MoveRelativeXY(int PositionX, int PositionY, int timeOutInSeconds)
        {
            DateTime now = DateTime.Now;
            MoveRelativeXYNoWait(PositionX, PositionY);
            WaitXYMotionDone(timeOutInSeconds);
        }

        public void MoveRelativeXYNoWait(int PositionX, int PositionY)
        {
            UInt32 Result;
            requestStop = false;
            uint AxisNum = 2;
            if (simulationMode)
            {
            }
            else
            {
                if (!GetXYMotionDone())
                {
                    throw new AmonetMotionException("Start a motion before the previous motion done", (short)axisNo);
                }
                lock (thisLock)
                {
                    Result = Motion.mAcm_GpMoveLinearRel(xyAxisHandle, new double[] { PositionX, PositionY }, ref AxisNum);
                    Thread.Sleep(100);
                    if (Result != (uint)ErrorCode.SUCCESS)
                    {
                        throw new Exception("MoveRelXYNoWait Failed With Error Code[0x" + Convert.ToString(Result, 16) + "]");
                    }
                }
            }
        }

        public void MoveRelativeNoWait(int distance)
        {
            UInt32 Result;
            requestStop = false;
            if (useHomeSpeed)
            {
                useHomeSpeed = false;
                SetSpeed(RunSpeedCfg.StartVelocity, RunSpeedCfg.MaxVelocity,
                    RunSpeedCfg.RampUpTime, RunSpeedCfg.RampDownTime);
            }
            if (simulationMode)
            {
            }
            else
            {
                if (!GetMotionDone())
                {
                    throw new AmonetMotionException("Start a motion before the previous motion done", (short)axisNo);
                }
                lock (thisLock)
                {
                    Result = Motion.mAcm_AxMoveRel(axisHandle, Convert.ToDouble(distance));
                    Thread.Sleep(100);
                    if (Result != (uint)ErrorCode.SUCCESS)
                    {
                        throw new Exception("MoveRel Failed With Error Code[0x" + Convert.ToString(Result, 16) + "]");
                    }
                }
            }
        }

        public bool GetXYMotionDone()
        {

            uint Result;

            ushort motion = 1;
            ushort GpState = 0;
            bool AxisDone;

            lock (thisLock)
            {
                if (simulationMode)
                {
                    motion = 1;
                }
                else
                {
                    if (GetAlarm())
                    {
                        throw new MotionAlarm("Motion has alarm", axisNo);
                    }

                    Result = Motion.mAcm_GpGetState(xyAxisHandle, ref GpState);
                    if (Result == (uint)ErrorCode.SUCCESS)
                    {
                        if (GpState == 1)
                        {
                            motion = 1;
                        }
                        else
                        {
                            motion = 0;
                        }
                    }
                    else
                    {
                        throw new Exception("Get mAcm_GpGetState Failed With Error Code[0x" + Convert.ToString(Result, 16) + "]");
                    }
                }

            }
            if (GetAlarm())
            {
                throw new MotionAlarm("Motion has alarm", axisNo);
            }

            AxisDone = (motion == 1);
            //if (AxisDone)
            //{
            //    int currentPosition = GetPosition();
            //    for (int i = 0; i < 10; i++)
            //    {
            //        Thread.Sleep(200);
            //        if (Math.Abs(GetPosition() - currentPosition) > 3)
            //        {
            //            return false;
            //        }
            //    }
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}
            return AxisDone;
        }

        public bool GetMotionDone()
        {
            uint Result;
            bool AxisDone;

            ushort motion = 1;

            lock (thisLock)
            {
                if (simulationMode)
                {
                    motion = 1;
                }
                else
                {
                    if (GetAlarm())
                    {
                        throw new MotionAlarm("Motion has alarm", axisNo);
                    }
                    ushort AxState = 0;
                    //Result = Motion.mAcm_CheckMotionEvent(deviceHandle, AxEvtStatusArray, GpEvtStatusArray, 4, 1, 10);
                    Result = Motion.mAcm_AxGetState(axisHandle, ref AxState);
                    if (Result == (uint)ErrorCode.SUCCESS)
                    {
                        if (AxState == 1)
                        {
                            motion = 1;
                        }
                        else
                        {
                            motion = 0;
                        }
                    }
                    else
                    {
                        throw new Exception("Get AxGetState Failed With Error Code[0x" + Convert.ToString(Result, 16) + "]");
                    }
                }

            }
            if (GetAlarm())
            {
                throw new MotionAlarm("Motion has alarm", axisNo);
            }
            AxisDone = (motion == 1);
            //if (AxisDone)
            //{
            //    int currentPosition = GetPosition();
            //    for (int i = 0; i < 10; i++)
            //    {
            //        Thread.Sleep(200);
            //        if (Math.Abs(GetPosition() - currentPosition) > 3)
            //        {
            //            return false;
            //        }
            //    }
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}
            return AxisDone;
        }

        public int GetPosition()
        {
            UInt32 Result;
            double Pos = 0;
            lock (thisLock)
            {
                if (simulationMode)
                {
                    Pos = 100;
                }
                else
                {
                    Result = Motion.mAcm_AxGetActualPosition(axisHandle, ref Pos);
                    if (Result != (uint)ErrorCode.SUCCESS)
                    {
                        throw new Exception("Get Position Failed With Error Code[0x" + Convert.ToString(Result, 16) + "]");
                    }
                }
            }
            return (int)Pos;
        }

        public int GetErrorCounter()
        {
            int errorCounter = 0;
            lock (thisLock)
            {
                if (simulationMode)
                {
                    errorCounter = 100;
                }
                else
                {
                    //TODO: add error counter later
                    errorCounter = GetPosition() - GetCommand();
                }
            }
            return errorCounter;
        }

        public void ResetErrorCounter()
        {
            UInt32 Result;
            lock (thisLock)
            {
                if (simulationMode)
                {
                }
                else
                {

                    Result = Motion.mAcm_AxResetError(axisHandle);
                    if (Result != (uint)ErrorCode.SUCCESS)
                    {
                        throw new Exception("Reset Error Failed With Error Code[0x" + Convert.ToString(Result, 16) + "]");
                    }
                }
            }
        }

        public int GetCommand()
        {
            UInt32 Result;
            double cmd = 0;
            lock (thisLock)
            {
                if (simulationMode)
                {
                    cmd = 100;
                }
                else
                {

                    Result = Motion.mAcm_AxGetCmdPosition(axisHandle, ref cmd);
                    if (Result != (uint)ErrorCode.SUCCESS)
                    {
                        throw new Exception("Get Command Failed With Error Code[0x" + Convert.ToString(Result, 16) + "]");
                    }
                }
            }
            return (int)cmd;
        }

        private void SetCommand(double command)
        {
            UInt32 Result;
            lock (thisLock)
            {
                if (simulationMode)
                {
                }
                else
                {

                    Result = Motion.mAcm_AxSetCmdPosition(axisHandle, command);
                    if (Result != (uint)ErrorCode.SUCCESS)
                    {
                        throw new Exception("Set Command Failed With Error Code[0x" + Convert.ToString(Result, 16) + "]");
                    }
                }
            }

        }

        public void Home(int timeOutInSeconds)
        {
            HomeNoWait();
            WaitMotionDone(timeOutInSeconds);
            SetSpeed(RunSpeedCfg.StartVelocity, RunSpeedCfg.MaxVelocity, RunSpeedCfg.RampUpTime, RunSpeedCfg.RampDownTime);
        }

        public void WaitXYMotionDone(int timeOutInSeconds)
        {
            DateTime now = DateTime.Now;
            ManualResetEvent ev;
            while (!GetXYMotionDone())
            {
                ev = new ManualResetEvent(false);
                ev.WaitOne(20);
                if (requestStop)
                {
                    requestStop = false;
                    break;
                }
                if ((DateTime.Now - now).TotalSeconds > timeOutInSeconds)
                {
                    throw new MotionTimeout(string.Format("Motion axis {0} timeout on waiting motion done", axisNo));
                }
            }
            if (useHomeSpeed)
            {
                useHomeSpeed = false;
                SetSpeed(RunSpeedCfg.StartVelocity, RunSpeedCfg.MaxVelocity,
                    RunSpeedCfg.RampUpTime, RunSpeedCfg.RampDownTime);
            }
        }

        public void WaitMotionDone(int timeOutInSeconds)
        {
            DateTime now = DateTime.Now;
            ManualResetEvent ev;
            while (!GetMotionDone())
            {
                ev = new ManualResetEvent(false);
                ev.WaitOne(20);
                if (requestStop)
                {
                    requestStop = false;
                    break;
                }
                if ((DateTime.Now - now).TotalSeconds > timeOutInSeconds)
                {
                    throw new MotionTimeout(string.Format("Motion axis {0} timeout on waiting motion done", axisNo));
                }
            }
            if (useHomeSpeed)
            {
                useHomeSpeed = false;
                SetSpeed(RunSpeedCfg.StartVelocity, RunSpeedCfg.MaxVelocity,
                    RunSpeedCfg.RampUpTime, RunSpeedCfg.RampDownTime);
            }
        }

        public void HomeNoWait()
        {
            UInt32 Result;
            requestStop = false;
            // Start homing
            useHomeSpeed = true;
            SetSpeed(HomeSpeedCfg.StartVelocity, HomeSpeedCfg.MaxVelocity,
                HomeSpeedCfg.RampUpTime, HomeSpeedCfg.RampDownTime);
            lock (thisLock)
            {
                if (simulationMode)
                {
                }
                else
                {
                    Result = Motion.mAcm_AxHome(axisHandle, homeMode, (UInt32)homeDir);
                    if (Result != (uint)ErrorCode.SUCCESS)
                    {
                        throw new Exception("Home Failed With Error Code[0x" + Convert.ToString(Result, 16) + "]");
                    }
                    Thread.Sleep(200);
                }
            }
        }

        public void Reset()
        {
            UInt32 Result;
            // Start homing
            lock (thisLock)
            {
                if (simulationMode)
                {
                }
                else
                {
                    Result = Motion.mAcm_AxSetActualPosition(axisHandle, 0);
                    if (Result != (uint)ErrorCode.SUCCESS)
                    {
                        throw new Exception("Set Position Failed With Error Code[0x" + Convert.ToString(Result, 16) + "]");
                    }
                    Result = Motion.mAcm_AxSetCmdPosition(axisHandle, 0);
                    if (Result != (uint)ErrorCode.SUCCESS)
                    {
                        throw new Exception("Set Cmd Failed With Error Code[0x" + Convert.ToString(Result, 16) + "]");
                    }
                    ResetErrorCounter();
                }
            }

        }

        public void ResetAlarm()
        {
            ManualResetEvent ev = new ManualResetEvent(false);
            lock (thisLock)
            {
                if (simulationMode)
                {
                }
                else
                {
                    SetDO(7, 1);
                    uint Result = Motion.mAcm_AxResetError(axisHandle);
                    if (Result != (uint)ErrorCode.SUCCESS)
                    {
                        throw new Exception("Reset Error Failed With Error Code[0x" + Convert.ToString(Result, 16) + "]");
                    }
                }

                ev.WaitOne(100);
                
                if (simulationMode)
                {
                }
                else
                {
                    SetDO(7, 0);
                }
            }
        }

        public IOStatus GetIOStatus()
        {
            UInt32 Result;
            uint IO_status = 0;
            lock (thisLock)
            {
                if (simulationMode)
                {
                }
                else
                {
                    Result = Motion.mAcm_AxGetMotionIO(axisHandle, ref IO_status);
                    if (Result != (uint)ErrorCode.SUCCESS)
                    {
                        throw new Exception("Get IO Status Failed With Error Code[0x" + Convert.ToString(Result, 16) + "]");
                    }
                }
            }
            var ioStatus = new IOStatus(IO_status);
            return ioStatus;
        }

        public bool GetAlarm()
        {
            if (simulationMode)
            {
                return false;
            }
            return GetIOStatus().alarm;
        }

        public bool GetRDY()
        {
            return GetIOStatus().ready;
        }

        public bool GetInPosition()
        {
            return GetIOStatus().inPosition;
        }

        public uint GetErrorStatus()
        {
            UInt32 Result;
            ushort AxState = 0;
            uint errorStatus = 0;
            lock (thisLock)
            {
                if (simulationMode)
                {
                    errorStatus = 100;
                }
                else
                {
                    Result = Motion.mAcm_AxGetState(axisHandle, ref AxState);
                    if (Result != (uint)ErrorCode.SUCCESS)
                    {
                        throw new Exception("Get State Failed With Error Code[0x" + Convert.ToString(Result, 16) + "]");
                    }
                    errorStatus = AxState;
                }
            }
            return errorStatus;
        }
        public void MoveAbsoluteXYZ(int PositionX, int PositionY, int PositionZ, int timeOutInSeconds)
        {
            throw new NotImplementedException();
        }
        public void MoveAbsoluteXYZNoWait(int PositionX, int PositionY, int PositionZ)
        {
            throw new NotImplementedException();
        }
        public void MoveRelativeXYZ(int PositionX, int PositionY, int PositionZ, int timeOutInSeconds)
        {
            throw new NotImplementedException();
        }
        public void MoveRelativeXYZNoWait(int PositionX, int PositionY, int PositionZ)
        {
            throw new NotImplementedException();
        }
        public void WaitXYZMotionDone(int timeOutInSeconds)
        {
            throw new NotImplementedException();
        }
        public void LimitSearchNoWait(int direction)
        {
            throw new NotImplementedException();
        }
    }
}

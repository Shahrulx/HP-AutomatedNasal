﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Advantech.Motion;//Common Motion API
using HardwareInterfaces;

namespace HardwareLib
{
    public class CommonMotionControl : IMotionCard
    {
        public Object thisLock;
        private uint cardID = 0;
        private uint DeviceID = 0;
        private string cfgFile = "";
        private bool simulationMode = false;
        private DEV_LIST[] devlist = new DEV_LIST[5];
        public IntPtr m_DeviceHandle = IntPtr.Zero;
        public IntPtr[] m_Axishand = new IntPtr[32];
        public IntPtr xyAxisHandle = IntPtr.Zero;

        public CommonMotionControl(uint cardID, string cfgFile, bool simulationMode)
        {
            this.thisLock = new Object();
            this.cardID = cardID;
            this.cfgFile = cfgFile;
            this.simulationMode = simulationMode;
        }

        public void Setup()
        {
            if (simulationMode)
            {
                return;
            }
            int Result;
            uint deviceCount = 0;
            Result = Motion.mAcm_GetAvailableDevs(devlist, Motion.MAX_DEVICES, ref deviceCount);
            if (Result != (int)ErrorCode.SUCCESS)
            {
                throw new Exception("No common motion device found");
            }
            DeviceID = devlist[cardID].DeviceNum;
            OpenBoard();
        }

        private void OpenBoard()
        {
            uint Result;
            uint i = 0;
            uint[] slaveDevs = new uint[16];
            uint AxesPerDev = new uint();
            uint AxisNumber;
            uint buffLen = 0;
            uint[] AxEnableEvtArray = new uint[32];
            uint[] GpEnableEvt = new uint[32];

            Result = Motion.mAcm_DevOpen(DeviceID, ref m_DeviceHandle);
            if (Result != (uint)ErrorCode.SUCCESS)
            {
                new Exception("Can Not Open Device");
            }
            buffLen = 4;
            Result = Motion.mAcm_GetProperty(m_DeviceHandle, (uint)PropertyID.FT_DevAxesCount, ref AxesPerDev, ref buffLen);
            if (Result != (uint)ErrorCode.SUCCESS)
            {
                throw new Exception("Get Property Error");
            }
            AxisNumber = AxesPerDev;
            buffLen = 64;
            Result = Motion.mAcm_GetProperty(m_DeviceHandle, (uint)PropertyID.CFG_DevSlaveDevs, slaveDevs, ref buffLen);
            if (Result == (uint)ErrorCode.SUCCESS)
            {
                i = 0;
                while (slaveDevs[i] != 0)
                {
                    AxisNumber += AxesPerDev;
                    i++;
                }
            }
            for (i = 0; i < AxisNumber; i++)
            {
                //Open every Axis and get the each Axis Handle
                //And Initial property for each Axis 		

                //Open Axis 
                Result = Motion.mAcm_AxOpen(m_DeviceHandle, (UInt16)i, ref m_Axishand[i]);
                if (Result != (uint)ErrorCode.SUCCESS)
                {
                    throw new Exception("Open Axis Failed");
                }

            }

            Result = Motion.mAcm_DevLoadConfig(m_DeviceHandle, cfgFile);
            if (Result != (uint)ErrorCode.SUCCESS)
            {
                if (cfgFile == @"c:\NGInker\Configuration\2d001000.cfg")
                {
                    //bypass for this card first
                }
                else
                {
                    throw new Exception("Load Config Error");
                }
                
            }

            Result = Motion.mAcm_GpAddAxis(ref xyAxisHandle, m_Axishand[0]);
            if (Result != (uint)ErrorCode.SUCCESS)
            {
                throw new Exception("Add group Axis Failed");
            }

            Result = Motion.mAcm_GpAddAxis(ref xyAxisHandle, m_Axishand[1]);
            if (Result != (uint)ErrorCode.SUCCESS)
            {
                throw new Exception("Add group Axis Failed");
            }


            //enable event for motion done
            for (i = 0; i < AxisNumber; i++)
            {
                AxEnableEvtArray[i] = 0x1;
            }
            GpEnableEvt[0] = 0x1;

            Result = Motion.mAcm_EnableMotionEvent(m_DeviceHandle, AxEnableEvtArray, GpEnableEvt, AxisNumber, 1);
            if (Result != (uint)ErrorCode.SUCCESS)
            {
                throw new Exception("EnableMotionEvent Filed With Error Code[0x" + Convert.ToString(Result, 16) + "]");
            }

        }
    }
}

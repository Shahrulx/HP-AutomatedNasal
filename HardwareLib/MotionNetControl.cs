﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TPM;
using System.Threading;

namespace HardwareLib
{
    public class MotionNetControl
    {
        private bool simulationMode = false;

        public Object thisLock = new Object();
        private ushort expectedCardNum = 0;  //set to 0 for don't care
        private ushort ringNo = 0;
        private ushort baudRate = 0;
        private bool m_Initialized = false;
        public Dictionary<ushort, Dictionary<byte, byte>> inputValue = new Dictionary<ushort, Dictionary<byte, byte>>();
        public Dictionary<ushort, Dictionary<byte, byte>> outputValue = new Dictionary<ushort, Dictionary<byte, byte>>();

        public MotionNetControl(ushort ringNo, ushort baudRate, ushort expectedCardNum, bool simulationMode)
        {
            this.ringNo = ringNo;
            this.baudRate = baudRate;
            this.expectedCardNum = expectedCardNum;
            this.simulationMode = simulationMode;
        }

        public void Setup()
        {
            short cardNum;
            short status;
            uint[] lDevTable = new uint[2];
            Master.PCI_M114.ErrCode Err;

            if (m_Initialized || simulationMode)
            {
                return;
            }

            m_Initialized = true;

            Err = Master.PCI_M114._m114_open_mnet(0);
            if (Err != Master.PCI_M114.ErrCode.ERR_NoError)
            {
                throw new AmonetException("Fail to open motionnet", (short)Err);
            }

            Err = Master.PCI_M114._m114_set_ring_config(0, ringNo, (byte)baudRate);
            if (Err != Master.PCI_M114.ErrCode.ERR_NoError) throw new AmonetException("Fail set ring config", (short)Err);

            status = MNet.Basic._mnet_reset_ring(ringNo);
            if (status != 0) throw new AmonetException("Fail reset ring", status);

            status = MNet.Basic._mnet_get_ring_active_table(ringNo, lDevTable);
            if (status == -74) throw new AmonetException("No Device!", status);

            status = MNet.Basic._mnet_start_ring(ringNo);
            if (status != 0) throw new AmonetException("Fail start ring", status);
        }

        public void TearDown()
        {
            short status = 0;
            status = MNet.Basic._mnet_close();
            m_Initialized = false;
            if (status < 0)
            {
                throw new AmonetException("Fail to close amonet", status);
            }
        }
    }
}

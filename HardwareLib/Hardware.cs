﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using JsonConfig;
//using AdvantechLib;
using HardwareInterfaces;
using System.IO;
using System.Collections;
using Automation.BDaq;

namespace HardwareLib
{
    public class Hardware
    {
        public bool SimulationMode { get; set; }
        public Dictionary<string, IMotionCard> motionCards { get; set; }
        public Dictionary<string, IMotor> motors { get; set; }
        public Dictionary<string, IDigitalInput> inputs { get; set; }
        public Dictionary<string, IDigitalOutput> outputs { get; set; }
        public Dictionary<string, ICylinder> cylinders { get; set; }
        public List<ushort> inputCards { get; set; }
        public List<ushort> outputCards { get; set; }
        private AmonetControl amonetControl = null;
        private DAQNaviCard daqNaviCard = null;
        private UniDaqControl uniDaqControl = null;
        private PCED122DAQ pceD122Daq = null;
        private PCEM114MotionControl pceM114MotionControl = null;
        private MotionNetControl motionNetControl = null;
        PCI1730DaqControl pci1730DaqControl = null;

        public Hardware(string configDirectory)
        {
            var hardwareStr = (new StreamReader(configDirectory + @"\hardware.json")).ReadToEnd();
            dynamic hardwareConfig = fastJSON.JSON.ToDynamic(hardwareStr);


            this.SimulationMode = hardwareConfig.simulationMode;

            motionCards = new Dictionary<string, IMotionCard>();
            inputCards = new List<ushort>();
            outputCards = new List<ushort>();
            inputs = new Dictionary<string, IDigitalInput>();
            outputs = new Dictionary<string, IDigitalOutput>();
            motors = new Dictionary<string, IMotor>();
            cylinders = new Dictionary<string, ICylinder>();

            GenerateInputs(configDirectory, hardwareConfig);
            GenerateOutput(configDirectory, hardwareConfig);
            GenerateMotor(configDirectory, hardwareConfig);
            GenerateCylinder(configDirectory);
        }

        private void GenerateMotor(string configDirectory, dynamic hardwareConfig)
        {
            var motorStr = (new StreamReader(configDirectory + @"\motor.json")).ReadToEnd();
            dynamic motorConfig = fastJSON.JSON.ToDynamic(motorStr);
            foreach (var motorName in motorConfig.GetDynamicMemberNames())
            {
                var cfg = motorConfig[motorName];
                if ((string)cfg.cardType == "Amonet")
                {
                    motionCards[(string)motorName] = new AmonetMotion(
                        amonetControl.thisLock, (ushort)cfg.ringNo, (ushort)cfg.cardID,
                        (string)cfg.cfgFile, (ushort)cfg.axisCount, SimulationMode);

                    foreach (var axisName in cfg.axis.GetDynamicMemberNames())
                    {
                        var axisCfg = cfg.axis[axisName];
                        var speedCfg = new ISpeedCfg((double)axisCfg.startVel, (double)axisCfg.maxVel,
                                                      (double)axisCfg.rampUpTime, (double)axisCfg.rampDownTime);
                        motors[(string)axisName] = new MotorAmonet(
                            (string)axisName, amonetControl.thisLock,
                            (ushort)cfg.ringNo, (ushort)cfg.cardID, (ushort)cfg.axisCount,
                            (ushort)axisCfg.axisNo, (byte)axisCfg.homeDir, (bool)axisCfg.invertSVON,
                            speedCfg, (bool)SimulationMode);
                        motors[(string)axisName].PulsePermm = (double)axisCfg.pulsePermm;
                        motors[(string)axisName].MotorType = MotorType.AMONET;

                    }
                }
                else if ((string)cfg.cardType == "MotionNet")
                {
                    motionCards[(string)motorName] = new MotionNetMotion(
                        motionNetControl.thisLock, (ushort)cfg.ringNo, (ushort)cfg.cardID,
                        (string)cfg.cfgFile, (ushort)cfg.axisCount, SimulationMode);

                    foreach (var axisName in cfg.axis.GetDynamicMemberNames())
                    {
                        ESpeedProfile speedProfile;
                        var axisCfg = cfg.axis[axisName];

                        if ((string)axisCfg.speedProfile == "Trapezoidal")
                            speedProfile = ESpeedProfile.Trapezoidal;
                        else
                            speedProfile = ESpeedProfile.SCurve;
                        var speedCfg = new ISpeedCfg((double)axisCfg.startVel, (double)axisCfg.maxVel,
                                                      (double)axisCfg.rampUpTime, (double)axisCfg.rampDownTime,
                                                      speedProfile);
                        motors[(string)axisName] = new MotorMotionNet(
                            (string)axisName, motionNetControl.thisLock,
                            (ushort)cfg.ringNo, (ushort)cfg.cardID, (ushort)cfg.axisCount,
                            (ushort)axisCfg.axisNo, (byte)axisCfg.homeDir, (bool)axisCfg.invertSVON,
                            speedCfg, (bool)SimulationMode);
                        motors[(string)axisName].PulsePermm = (double)axisCfg.pulsePermm;
                        motors[(string)axisName].MotorType = MotorType.AMONET;

                    }
                }
                else if ((string)cfg.cardType == "CommonMotion")
                {
                    CommonMotionControl commonMotionControl = new CommonMotionControl((uint)cfg.cardID, (string)cfg.cfgFile, SimulationMode);
                    motionCards[motorName] = commonMotionControl;
                    foreach (var axisName in cfg.axis.GetDynamicMemberNames())
                    {
                        var axisCfg = cfg.axis[axisName];
                        var speedCfg = new ISpeedCfg((double)axisCfg.startVel, (double)axisCfg.maxVel,
                                                      (double)axisCfg.acc, (double)axisCfg.dcc);
                        motors[(string)axisName] = new MotorCommonMotion(commonMotionControl,
                            (string)axisName, (ushort)axisCfg.axisNo, (byte)axisCfg.homeDir, (uint)axisCfg.homeMode, speedCfg, SimulationMode);
                        motors[(string)axisName].PulsePermm = (double)axisCfg.pulsePermm;
                        motors[(string)axisName].MotorType = MotorType.COMMON_MOTION;
                    }
                }
                else if ((string)cfg.cardType == "PISO")
                {
                    PISOMotion pisoMotion = new PISOMotion((ushort)cfg.cardID,
                        (string)cfg.cfgFile, (ushort)cfg.axisCount, SimulationMode);
                    motionCards[motorName] = pisoMotion;
                    foreach (var axisName in cfg.axis.GetDynamicMemberNames())
                    {
                        var axisCfg = cfg.axis[axisName];
                        var speedCfg = new ISpeedCfg((double)axisCfg.startVel, (double)axisCfg.maxVel,
                                                      (double)axisCfg.acc, (double)axisCfg.dcc);
                        motors[(string)axisName] = new MotorPISO(
                            (string)axisName, pisoMotion.thisLock,
                            (ushort)cfg.cardID, (ushort)cfg.axisCount,
                            (ushort)axisCfg.axisNo, (byte)axisCfg.homeDir, (bool)axisCfg.invertSVON,
                            speedCfg, (bool)SimulationMode);
                        motors[(string)axisName].PulsePermm = (double)axisCfg.pulsePermm;
                        motors[(string)axisName].MotorType = MotorType.COMMON_MOTION;
                    }
                }
                else if ((string)cfg.cardType == "P8134")
                {

                    P8134MotionControl p8134 = new P8134MotionControl((ushort)cfg.cardID,
                        (string)cfg.cfgFile, SimulationMode);
                    motionCards[motorName] = p8134;
                    foreach (var axisName in cfg.axis.GetDynamicMemberNames())
                    {
                        var axisCfg = cfg.axis[axisName];
                        var speedCfg = new ISpeedCfg((double)axisCfg.startVel, (double)axisCfg.maxVel,
                                                      (double)axisCfg.rampUpTime, (double)axisCfg.rampDownTime);
                        motors[(string)axisName] = new MotorP8134(
                            (string)axisName, p8134.thisLock,
                            (ushort)cfg.cardID, (ushort)cfg.axisCount,
                            (ushort)axisCfg.axisNo, (byte)axisCfg.homeDir, (bool)axisCfg.invertSVON,
                            speedCfg, (bool)SimulationMode);
                        motors[(string)axisName].PulsePermm = (double)axisCfg.pulsePermm;
                        motors[(string)axisName].MotorType = MotorType.AMONET;
                    }
                }
                else if ((string)cfg.cardType == "P8134A")
                {

                    P8134AMotionControl p8134 = new P8134AMotionControl((ushort)cfg.cardID,
                        (string)cfg.cfgFile, SimulationMode);
                    motionCards[motorName] = p8134;
                    foreach (var axisName in cfg.axis.GetDynamicMemberNames())
                    {
                        var axisCfg = cfg.axis[axisName];
                        var speedCfg = new ISpeedCfg((double)axisCfg.startVel, (double)axisCfg.maxVel,
                                                      (double)axisCfg.rampUpTime, (double)axisCfg.rampDownTime);
                        motors[(string)axisName] = new MotorP8134A(
                            (string)axisName, p8134.thisLock,
                            (ushort)cfg.cardID, (ushort)cfg.axisCount,
                            (ushort)axisCfg.axisNo, (byte)axisCfg.homeDir, (bool)axisCfg.invertSVON,
                            speedCfg, (bool)SimulationMode);
                        motors[(string)axisName].PulsePermm = (double)axisCfg.pulsePermm;
                        motors[(string)axisName].MotorType = MotorType.AMONET;
                    }
                }
                else if ((string)cfg.cardType == "PCEM134")
                {

                    PCEM134MotionControl m134 = new PCEM134MotionControl((ushort)cfg.cardID,
                        (string)cfg.cfgFile, SimulationMode);
                    motionCards[motorName] = m134;
                    foreach (var axisName in cfg.axis.GetDynamicMemberNames())
                    {
                        var axisCfg = cfg.axis[axisName];
                        var speedCfg = new ISpeedCfg((double)axisCfg.startVel, (double)axisCfg.maxVel,
                                                      (double)axisCfg.rampUpTime, (double)axisCfg.rampDownTime);
                        motors[(string)axisName] = new MotorPCEM134(
                            (string)axisName, m134.thisLock,
                            (ushort)cfg.cardID, (ushort)cfg.axisCount,
                            (ushort)axisCfg.axisNo, (byte)axisCfg.homeDir, (bool)axisCfg.invertSVON,
                            speedCfg, (bool)SimulationMode);
                        motors[(string)axisName].PulsePermm = (double)axisCfg.pulsePermm;
                        motors[(string)axisName].MotorType = MotorType.AMONET;
                    }
                }
                else if ((string)cfg.cardType == "PCEM114")
                {
                    string cfgFile = (string)cfg.cfgFile;
                    if (pceM114MotionControl == null)
                    {
                        pceM114MotionControl = new PCEM114MotionControl((uint)hardwareConfig.cardID, cfgFile, SimulationMode);
                        pceM114MotionControl.Open();
                    }
                    else
                    {
                        pceM114MotionControl.SetCfg(cfgFile);
                    }
                    motionCards[motorName] = pceM114MotionControl;
                    foreach (var axisName in cfg.axis.GetDynamicMemberNames())
                    {
                        ESpeedProfile speedProfile;
                        var axisCfg = cfg.axis[axisName];

                        if ((string)axisCfg.speedProfile == "Trapezoidal")
                            speedProfile = ESpeedProfile.Trapezoidal;
                        else
                            speedProfile = ESpeedProfile.SCurve;

                        var speedCfg = new ISpeedCfg((double)axisCfg.startVel, (double)axisCfg.maxVel,
                                                      (double)axisCfg.rampUpTime, (double)axisCfg.rampDownTime,
                                                      speedProfile);
                        motors[(string)axisName] = new MotorPCEM114(
                            (string)axisName, pceM114MotionControl.thisLock,
                            (ushort)cfg.cardID, (ushort)cfg.axisCount,
                            (ushort)axisCfg.axisNo, (byte)axisCfg.homeDir, (bool)axisCfg.invertSVON,
                            speedCfg, (bool)SimulationMode);
                        motors[(string)axisName].PulsePermm = (double)axisCfg.pulsePermm;
                        motors[(string)axisName].MotorType = MotorType.AMONET;
                    }
                }
            }
        }

        private void GenerateOutput(string configDirectory, dynamic hardwareConfig)
        {
            var outputStr = (new StreamReader(configDirectory + @"\output.json")).ReadToEnd();
            dynamic outputConfig = fastJSON.JSON.ToDynamic(outputStr);
            foreach (var outputCardName in outputConfig.GetDynamicMemberNames())
            {
                var cfg = outputConfig[outputCardName];
                outputCards.Add((ushort)cfg.cardID);

                if ((string)cfg.cardType == "Amonet")
                {
                    if (amonetControl == null)
                    {
                        amonetControl = new AmonetControl((ushort)hardwareConfig.ringNo,
                            (ushort)hardwareConfig.baudRate,
                            (ushort)hardwareConfig.cardNumber, SimulationMode);
                    }

                    foreach (var output in cfg.pin)
                    {
                        var name = output[0];
                        var msb = output[1];
                        var lsb = output[2];
                        outputs[(string)name] = new DigitalOutputAmonet((string)name, amonetControl.thisLock, (ushort)cfg.ringNo, (ushort)cfg.cardID,
                            amonetControl.outputValue, (byte)msb, (byte)lsb, SimulationMode);
                    }
                }
                else if ((string)cfg.cardType == "MotionNet")
                {
                    if (pceM114MotionControl == null)
                    {
                        pceM114MotionControl = new PCEM114MotionControl((uint)hardwareConfig.cardID, "", SimulationMode);
                        pceM114MotionControl.Open();
                    }
                    if (motionNetControl == null)
                    {
                        motionNetControl = new MotionNetControl((ushort)hardwareConfig.ringNo,
                            (ushort)hardwareConfig.baudRate,
                            (ushort)hardwareConfig.cardNumber, SimulationMode);
                        motionNetControl.Setup();
                    }
                    foreach (var output in cfg.pin)
                    {
                        var name = output[0];
                        var msb = output[1];
                        var lsb = output[2];
                        outputs[(string)name] = new DigitalOutputMotionNet((string)name, motionNetControl.thisLock, (ushort)cfg.ringNo, (ushort)cfg.cardID,
                            motionNetControl.outputValue, (byte)msb, (byte)lsb, SimulationMode);
                    }
                }
                else if ((string)cfg.cardType == "DAQNavi")
                {
                    if (daqNaviCard == null)
                    {
                        daqNaviCard = new DAQNaviCard(SimulationMode);
                    }

                    ushort cardID = (ushort)cfg.cardID;
                    string cardDesc = (string)cfg.cardDesc;
                    daqNaviCard.AddCard(cardDesc);
                    var doCtrl = new InstantDoCtrl();
                    if (!SimulationMode) doCtrl.SelectedDevice = daqNaviCard.devs[cardDesc];
                    foreach (var output in cfg.pin)
                    {
                        var name = output[0];
                        var msb = output[1];
                        var lsb = output[2];
                        outputs[(string)name] = new DigitalOutputDAQ((string)name, cardID, daqNaviCard.thisLock, doCtrl,
                            daqNaviCard.outputValue, (byte)msb, (byte)lsb, SimulationMode);
                    }
                }
                else if ((string)cfg.cardType == "UniDAQ")
                {
                    if (uniDaqControl == null)
                    {
                        uniDaqControl = new UniDaqControl(SimulationMode);
                    }

                    foreach (var output in cfg.pin)
                    {
                        var name = output[0];
                        var msb = output[1];
                        var lsb = output[2];
                        outputs[(string)name] = new DigitalOutputUniDAQ((string)name, uniDaqControl.thisLock, (ushort)cfg.cardID,
                            uniDaqControl.outputValue, (byte)msb, (byte)lsb, SimulationMode);
                    }
                }
                else if ((string)cfg.cardType == "PCI1730")
                {
                    if (pci1730DaqControl == null)
                    {
                        pci1730DaqControl = new PCI1730DaqControl(SimulationMode);
                    }

                    int handle = pci1730DaqControl.GetHandler((int)cfg.cardID);
                    foreach (var output in cfg.pin)
                    {
                        var name = output[0];
                        var msb = output[1];
                        var lsb = output[2];
                        outputs[(string)name] = new DigitalOutputPCI1730((string)name, pci1730DaqControl.thisLock, (ushort)cfg.cardID, handle,
                            pci1730DaqControl.outputValue, (byte)msb, (byte)lsb, SimulationMode);
                    }
                }
                else if ((string)cfg.cardType == "PCED122")
                {
                    if (pceD122Daq == null)
                    {
                        pceD122Daq = new PCED122DAQ(SimulationMode);
                    }

                    foreach (var output in cfg.pin)
                    {
                        var name = output[0];
                        var msb = output[1];
                        var lsb = output[2];
                        outputs[(string)name] = new DigitalOutputD122((string)name, pceD122Daq.thisLock, (ushort)cfg.cardID,
                            pceD122Daq.outputValue, (byte)msb, (byte)lsb, SimulationMode);
                    }
                }
            }
        }

        private void GenerateInputs(string configDirectory, dynamic hardwareConfig)
        {
            var inputStr = (new StreamReader(configDirectory + @"\input.json")).ReadToEnd();
            dynamic inputConfig = fastJSON.JSON.ToDynamic(inputStr);
            foreach (var inputCardName in inputConfig.GetDynamicMemberNames())
            {
                var cfg = inputConfig[inputCardName];
                inputCards.Add((ushort)cfg.cardID);
                if ((string)cfg.cardType == "Amonet")
                {
                    if (amonetControl == null)
                    {
                        amonetControl = new AmonetControl((ushort)hardwareConfig.ringNo,
                            (ushort)hardwareConfig.baudRate,
                            (ushort)hardwareConfig.cardNumber, SimulationMode);
                    }

                    foreach (var input in cfg.pin)
                    {
                        var name = input[0];
                        var msb = input[1];
                        var lsb = input[2];
                        inputs[(string)name] = new DigitalInputAmonet((string)name, amonetControl.thisLock, (ushort)cfg.ringNo, (ushort)cfg.cardID,
                            amonetControl.inputValue, (byte)msb, (byte)lsb, SimulationMode,0);
                    }
                }
                else if ((string)cfg.cardType == "MotionNet")
                {
                    if (pceM114MotionControl == null)
                    {
                        pceM114MotionControl = new PCEM114MotionControl((uint)hardwareConfig.cardID, "", SimulationMode);
                        pceM114MotionControl.Open();
                    }
                    if (motionNetControl == null)
                    {
                        motionNetControl = new MotionNetControl((ushort)hardwareConfig.ringNo,
                            (ushort)hardwareConfig.baudRate,
                            (ushort)hardwareConfig.cardNumber, SimulationMode);
                        motionNetControl.Setup();
                    }

                    foreach (var input in cfg.pin)
                    {
                        var name = input[0];
                        var msb = input[1];
                        var lsb = input[2];
                        inputs[(string)name] = new DigitalInputMotionNet((string)name, motionNetControl.thisLock, (ushort)cfg.ringNo, (ushort)cfg.cardID,
                            motionNetControl.inputValue, (byte)msb, (byte)lsb, SimulationMode);
                    }
                }
                else if ((string)cfg.cardType == "DAQNavi")
                {
                    if (daqNaviCard == null)
                    {
                        daqNaviCard = new DAQNaviCard(SimulationMode);
                    }

                    ushort cardID = (ushort)cfg.cardID;
                    string cardDesc = (string)cfg.cardDesc;
                    daqNaviCard.AddCard(cardDesc);
                    var diCtrl = new InstantDiCtrl();
                    if (!SimulationMode) diCtrl.SelectedDevice = daqNaviCard.devs[cardDesc];
                    foreach (var input in cfg.pin)
                    {
                        var name = input[0];
                        var msb = input[1];
                        var lsb = input[2];
                        inputs[(string)name] = new DigitalInputDAQ((string)name, cardID, daqNaviCard.thisLock, diCtrl,
                            daqNaviCard.inputValue, (byte)msb, (byte)lsb, SimulationMode);
                    }
                }
                else if ((string)cfg.cardType == "UniDAQ")
                {
                    if (uniDaqControl == null)
                    {
                        uniDaqControl = new UniDaqControl(SimulationMode);
                    }

                    foreach (var input in cfg.pin)
                    {
                        var name = input[0];
                        var msb = input[1];
                        var lsb = input[2];
                        inputs[(string)name] = new DigitalInputUniDAQ((string)name, uniDaqControl.thisLock, (ushort)cfg.cardID,
                            uniDaqControl.inputValue, (byte)msb, (byte)lsb, SimulationMode);
                    }
                }
                else if ((string)cfg.cardType == "PCI1730")
                {
                    if (pci1730DaqControl == null)
                    {
                        pci1730DaqControl = new PCI1730DaqControl(SimulationMode);
                    }

                    int handle = pci1730DaqControl.GetHandler((int)cfg.cardID);
                    foreach (var input in cfg.pin)
                    {
                        var name = input[0];
                        var msb = input[1];
                        var lsb = input[2];
                        inputs[(string)name] = new DigitalInputPCI1730((string)name, pci1730DaqControl.thisLock, (ushort)cfg.cardID, handle,
                            pci1730DaqControl.inputValue, (byte)msb, (byte)lsb, SimulationMode);
                    }
                }
                else if ((string)cfg.cardType == "PCED122")
                {
                    if (pceD122Daq == null)
                    {
                        pceD122Daq = new PCED122DAQ(SimulationMode);
                    }

                    foreach (var input in cfg.pin)
                    {
                        var name = input[0];
                        var msb = input[1];
                        var lsb = input[2];
                        inputs[(string)name] = new DigitalInputD122((string)name, pceD122Daq.thisLock, (ushort)cfg.cardID,
                            pceD122Daq.inputValue, (byte)msb, (byte)lsb, SimulationMode);
                    }
                }
            }
        }

        public void GenerateCylinder(string configDirectory)
        {
            var cylinderStr = (new StreamReader(configDirectory + @"\cylinder.json")).ReadToEnd();
            dynamic cylinderConfig = fastJSON.JSON.ToDynamic(cylinderStr);
            foreach (var name in cylinderConfig.GetDynamicMemberNames())
            {
                var cfg = cylinderConfig[name];
                Cylinder cylinder = new Cylinder(this, name, cfg.ErrorMessage, cfg.ExtendName, cfg.RetractName,
                    cfg.Default, (int)cfg.ExtendSensorTimeout, (int)cfg.RetractSensorTimeout, (int)cfg.ExtendWait, (int)cfg.RetractWait,
                    cfg.ResetIO, (int)cfg.WaitBeforeReset);


                foreach (var extendDO in cfg.Extend)
                    cylinder.AddExtendDO(extendDO[0], (int)extendDO[1]);

                foreach (var retractDO in cfg.Retract)
                    cylinder.AddRetractDO(retractDO[0], (int)retractDO[1]);

                foreach (var extendDI in cfg.ExtendSensor)
                    cylinder.AddExtendSensorDI(extendDI[0], (int)extendDI[1]);

                foreach (var retractDI in cfg.RetractSensor)
                    cylinder.AddRetractSensorDI(retractDI[0], (int)retractDI[1]);

                cylinders[name] = cylinder;
            }
        }

        public void Setup()
        {
            if (amonetControl != null)
            {
                amonetControl.Setup();
            }
            if (uniDaqControl != null)
            {
                uniDaqControl.Setup();
            }
            if (motionNetControl != null)
            {
                motionNetControl.Setup();
            }
            if (pci1730DaqControl != null)
            {
                pci1730DaqControl.Setup();
            }
            if (pceD122Daq != null)
            {
                pceD122Daq.Setup();
            }
            if (pceM114MotionControl != null)
            {
                pceM114MotionControl.Setup();
            }
            
            foreach (var motion in motionCards.Values)
            {
                motion.Setup();
            }
            foreach (var motor in motors.Values)
            {
                //motor.SetSpeed(motor.HomeSpeedCfg.StartVelocity, motor.HomeSpeedCfg.MaxVelocity,
                //    motor.HomeSpeedCfg.RampUpTime, motor.HomeSpeedCfg.RampDownTime);
                motor.Start();
            }
        }

        public ISpeedCfg GetSpeedCfg(Tuple<double, double, double, double> speedCfg)
        {
            return new ISpeedCfg(speedCfg.Item1, speedCfg.Item2, speedCfg.Item3, speedCfg.Item4);
        }

        public void SetMotorSpeed(string motor, Tuple<double, double, double, double> speedCfg)
        {
            motors[motor].SetSpeed(speedCfg.Item1, speedCfg.Item2, speedCfg.Item3, speedCfg.Item4);
        }

        public void SetMotorSpeed(string motor, HardwareInterfaces.ISpeedCfg speedCfg)
        {
            motors[motor].SetSpeed(speedCfg.StartVelocity, speedCfg.MaxVelocity, speedCfg.RampUpTime, speedCfg.RampDownTime);
        }
    }
}

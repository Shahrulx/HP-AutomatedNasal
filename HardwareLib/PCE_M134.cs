using System;
using System.Text;
using System.Runtime.InteropServices;

namespace TPM2
{
    public partial class Master
    {
        public class PCE_M134
        {


#if __WIN64__
            private const string DLLNAME = "PCE_M134_x64.dll";
#elif __WIN32__
            private const string DLLNAME = "PCE_M134.dll";
#endif
            private const string DLLNAME = "PCE_M134_x64.dll";
            // Error Table
            public enum ErrCode : short
            {
                M134ERR_NoError = 0,
                M134ERR_InvalidSwitchCardNumber = -1,
                M134ERR_SwitchCardNumberRepeated = -2,
                M134ERR_MapMemoryFailed = -3,
                M134ERR_CardNotExist = -4,
                M134ERR_CardNotInitYet = -5,
                M134ERR_InvalidBoardID = -6,
                M134ERR_InvalidAxisNumber = -7,
                M134ERR_InvalidParameter1 = -8,
                M134ERR_InvalidParameter2 = -9,
                M134ERR_InvalidParameter3 = -10,
                M134ERR_InvalidParameter4 = -11,
                M134ERR_InvalidParameter5 = -12,
                M134ERR_InvalidParameter6 = -13,
                M134ERR_InvalidParameter7 = -14,
                M134ERR_InvalidParameter8 = -15,
                M134ERR_InvalidParameter9 = -16,
                M134ERR_InvalidParameter10 = -17,
                M134ERR_InvalidParameter11 = -18,
                M134ERR_InvalidParameter12 = -19,
                M134ERR_NotSupported = -20,
                M134ERR_GetSecureIdFailed = -21,
                M134ERR_GenAesKeyFailed = -22,
                M134ERR_SetAutoCmpFifoFailed = -23,
                M134ERR_LoadFileFailed = -24,
            };

            public enum ECardType:short
            {
                CARD_UNKNOWN = 0,
                CARD_PCE_M134 = 1,
                CARD_PCE_M134_LD = 2,
            };

            // Initialization
            [DllImport(DLLNAME)]
            public static extern short _m134_open(ref ushort ExistCards);
            [DllImport(DLLNAME)]
            public static extern short _m134_close();
            [DllImport(DLLNAME)]
            public static extern short _m134_get_switch_card_num(ushort CardIndex, ref ushort SwitchCardNo);
            [DllImport(DLLNAME)]
            public static extern short _m134_check_switch_card_num(ushort SwitchCardNo, ref byte IsExist);
            [DllImport(DLLNAME)]
            public static extern short _m134_initial(ushort SwitchCardNo);
            [DllImport(DLLNAME)]
            public static extern short _m134_get_cpld_version(ushort SwitchCardNo, ref ushort CpldVer);
            [DllImport(DLLNAME)]
            public static extern short _m134_config_from_file(ushort SwitchCardNo, char[] FilePath);
            [DllImport(DLLNAME)]
            public static extern short _m134_get_card_type(ushort SwitchCardNo, ref byte CardType);

            // Security
            [DllImport(DLLNAME)]
            public static extern short _m134_get_secure_id(ushort SwitchCardNo, byte[] SecureID);
            [DllImport(DLLNAME)]
            public static extern short _m134_gen_aes_key(ushort SwitchCardNo, byte[] SI_Key, byte[] SecureID, byte[] AES_Key);
            [DllImport(DLLNAME)]
            public static extern short _m134_check_aes_key(ushort SwitchCardNo, byte[] SI_Key, byte[] AES_Key, ref byte Validity);

            // Digital I/O (PCE-M134-LD only)
            [DllImport(DLLNAME)]
            public static extern short _m134_get_digital_input(ushort SwitchCardNo, ref ushort Val);
            [DllImport(DLLNAME)]
            public static extern short _m134_get_digital_output(ushort SwitchCardNo, ref ushort Val);
            [DllImport(DLLNAME)]
            public static extern short _m134_set_digital_output(ushort SwitchCardNo, ushort Val);
            [DllImport(DLLNAME)]
            public static extern short _m134_get_digital_input_bit(ushort SwitchCardNo, byte BitNo, ref byte Val);
            [DllImport(DLLNAME)]
            public static extern short _m134_get_digital_output_bit(ushort SwitchCardNo, byte BitNo, ref byte Val);
            [DllImport(DLLNAME)]
            public static extern short _m134_set_digital_output_bit(ushort SwitchCardNo, byte BitNo, byte Val);
            [DllImport(DLLNAME)]
            public static extern short _m134_toggle_digital_output_bit(ushort SwitchCardNo, byte BitNo);

            // Interface I/O Configuration
            [DllImport(DLLNAME)]
            public static extern short _m134_set_ell(ushort SwitchCardNo, ushort AxisNo, short EllLogic, short EllMode);
            [DllImport(DLLNAME)]
            public static extern short _m134_set_sd(ushort SwitchCardNo, ushort AxisNo, short Enable, short SdLogic, short SdLatch, short SdMode);
            [DllImport(DLLNAME)]
            public static extern short _m134_set_alm(ushort SwitchCardNo, ushort AxisNo, short AlmLogic, short AlmMode);
            [DllImport(DLLNAME)]
            public static extern short _m134_set_erc_on(ushort SwitchCardNo, ushort AxisNo, short OnOff);
            [DllImport(DLLNAME)]
            public static extern short _m134_set_servo(ushort SwitchCardNo, ushort AxisNo, short OnOff);
            [DllImport(DLLNAME)]
            public static extern short _m134_set_ralm(ushort SwitchCardNo, ushort AxisNo, short OnOff);

            // Pulse I/O Configuration
            [DllImport(DLLNAME)]
            public static extern short _m134_set_pls_outmode(ushort SwitchCardNo, ushort AxisNo, short PlsOutMode);
            [DllImport(DLLNAME)]
            public static extern short _m134_set_pls_iptmode(ushort SwitchCardNo, ushort AxisNo, short PlsInMode, short PlsInDir);
            [DllImport(DLLNAME)]
            public static extern short _m134_set_feedback_src(ushort SwitchCardNo, ushort AxisNo, short Src);
            [DllImport(DLLNAME)]
            public static extern short _m134_set_abs_reference(ushort SwitchCardNo, ushort AxisNo, short Ref);

            // Status
            [DllImport(DLLNAME)]
            public static extern short _m134_motion_done(ushort SwitchCardNo, ushort AxisNo);
            [DllImport(DLLNAME)]
            public static extern short _m134_get_io_status(ushort SwitchCardNo, ushort AxisNo, ref ushort IoSts);
            [DllImport(DLLNAME)]
            public static extern short _m134_get_current_speed(ushort SwitchCardNo, ushort AxisNo, ref uint Speed);

            // Counter Control
            [DllImport(DLLNAME)]
            public static extern short _m134_get_command(ushort SwitchCardNo, ushort AxisNo, ref int Cmd);
            [DllImport(DLLNAME)]
            public static extern short _m134_set_command(ushort SwitchCardNo, ushort AxisNo, int Cmd);
            [DllImport(DLLNAME)]
            public static extern short _m134_get_position(ushort SwitchCardNo, ushort AxisNo, ref int Pos);
            [DllImport(DLLNAME)]
            public static extern short _m134_set_position(ushort SwitchCardNo, ushort AxisNo, int Pos);

            // Stop
            [DllImport(DLLNAME)]
            public static extern short _m134_sd_stop(ushort SwitchCardNo, ushort AxisNo, float Tdec);
            [DllImport(DLLNAME)]
            public static extern short _m134_imd_stop(ushort SwitchCardNo, ushort AxisNo);
            [DllImport(DLLNAME)]
            public static extern short _m134_emg_stop(ushort SwitchCardNo, ushort AxisNo);

            // Homing
            [DllImport(DLLNAME)]
            public static extern short _m134_home_move(ushort SwitchCardNo, ushort AxisNo, byte Dir, uint StrVel, uint MaxVel, float Tacc);

            // Velocity Move
            [DllImport(DLLNAME)]
            public static extern short _m134_tv_move(ushort SwitchCardNo, ushort AxisNo, byte Dir, uint StrVel, uint MaxVel, float Tacc);
            [DllImport(DLLNAME)]
            public static extern short _m134_sv_move(ushort SwitchCardNo, ushort AxisNo, byte Dir, uint StrVel, uint MaxVel, float Tacc);
            [DllImport(DLLNAME)]
            public static extern short _m134_fix_speed_range(ushort SwitchCardNo, ushort AxisNo, uint MaxVel);
            [DllImport(DLLNAME)]
            public static extern short _m134_unfix_speed_range(ushort SwitchCardNo, ushort AxisNo);
            [DllImport(DLLNAME)]
            public static extern short _m134_v_change(ushort SwitchCardNo, ushort AxisNo, uint NewVel, float Time);

            // Single-Axis Move
            [DllImport(DLLNAME)]
            public static extern short _m134_start_tr_move(ushort SwitchCardNo, ushort AxisNo, int Dist, uint StrVel, uint MaxVel, float Tacc, float Tdec);
            [DllImport(DLLNAME)]
            public static extern short _m134_start_ta_move(ushort SwitchCardNo, ushort AxisNo, int Pos, uint StrVel, uint MaxVel, float Tacc, float Tdec);
            [DllImport(DLLNAME)]
            public static extern short _m134_start_sr_move(ushort SwitchCardNo, ushort AxisNo, int Dist, uint StrVel, uint MaxVel, float Tacc, float Tdec);
            [DllImport(DLLNAME)]
            public static extern short _m134_start_sa_move(ushort SwitchCardNo, ushort AxisNo, int Pos, uint StrVel, uint MaxVel, float Tacc, float Tdec);
            // 2-Axis Linear Move
            [DllImport(DLLNAME)]
            public static extern short _m134_start_tr_line2C(ushort SwitchCardNo, ushort[] AxisArray, int DistX, int DistY, uint StrVel, uint MaxVel, float Tacc, float Tdec);
            [DllImport(DLLNAME)]
            public static extern short _m134_start_ta_line2C(ushort SwitchCardNo, ushort[] AxisArray, int PosX, int PosY, uint StrVel, uint MaxVel, float Tacc, float Tdec);

            // 3-Axis Linear Move
            [DllImport(DLLNAME)]
            public static extern short _m134_start_tr_line3C(ushort SwitchCardNo, ushort[] AxisArray, int DistX, int DistY, int DistZ, uint StrVel, uint MaxVel, float Tacc, float Tdec);
            [DllImport(DLLNAME)]
            public static extern short _m134_start_ta_line3C(ushort SwitchCardNo, ushort[] AxisArray, int PosX, int PosY, int PosZ, uint StrVel, uint MaxVel, float Tacc, float Tdec);

            // 4-Axis Linear Move
            [DllImport(DLLNAME)]
            public static extern short _m134_start_tr_line4C(ushort SwitchCardNo, int DistX, int DistY, int DistZ, int DistU, uint StrVel, uint MaxVel, float Tacc, float Tdec);
            [DllImport(DLLNAME)]
            public static extern short _m134_start_ta_line4C(ushort SwitchCardNo, int PosX, int PosY, int PosZ, int PosU, uint StrVel, uint MaxVel, float Tacc, float Tdec);

            // Auto Compare
            [DllImport(DLLNAME)]
            public static extern short _m134_get_auto_compare_count(ushort SwitchCardNo, ushort AxisNo, ref ushort Count);
            [DllImport(DLLNAME)]
            public static extern short _m134_get_auto_compare_status(ushort SwitchCardNo, ushort AxisNo, ref ushort OnOff);
            [DllImport(DLLNAME)]
            public static extern short _m134_set_auto_compare_source(ushort SwitchCardNo, ushort AxisNo, ushort SrcAxisNo);
            [DllImport(DLLNAME)]
            public static extern short _m134_set_auto_compare_trigger(ushort SwitchCardNo, ushort AxisNo, ushort Level, ushort Width);
            [DllImport(DLLNAME)]
            public static extern short _m134_set_auto_compare_function(ushort SwitchCardNo, ushort AxisNo, byte Dir, int StrPos, int Interval, ushort TrgCnt);
            [DllImport(DLLNAME)]
            public static extern short _m134_set_auto_compare_table(ushort SwitchCardNo, ushort AxisNo, byte Dir, ushort Size, int[] Table);
            [DllImport(DLLNAME)]
            public static extern short _m134_start_auto_compare(ushort SwitchCardNo, ushort AxisNo, ushort OnOff);
            [DllImport(DLLNAME)]
            public static extern short _m134_force_trigger_output(ushort SwitchCardNo, ushort AxisNo);
            [DllImport(DLLNAME)]
            public static extern short _m134_get_auto_compare_encoder(ushort SwitchCardNo, ushort AxisNo, ref int EncPos);
            [DllImport(DLLNAME)]
            public static extern short _m134_set_auto_compare_encoder(ushort SwitchCardNo, ushort AxisNo, int EncPos);
            

            
        }
    }
}

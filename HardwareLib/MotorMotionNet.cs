﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TPM;
using HardwareInterfaces;

namespace HardwareLib
{
    public class MotorMotionNet : IMotor
    {
        public string Name { get; set; }
        public MotorType MotorType { get; set; }
        public ISpeedCfg HomeSpeedCfg {get; set;}
        public ISpeedCfg RunSpeedCfg { get; set; }
        public ISpeedCfg XYRunSpeedCfg { get; set; }
        public ISpeedCfg XYZRunSpeedCfg { get; set; }
        public IDigitalOutput ClearAlarm { get; set; }

        private ushort axisCount = 0;
        private ushort ringNo = 0;
        private ushort cardID = 0;
        private bool invertSVON = false;
        private bool simulationMode = false;
        private ushort axisNo;
        private byte homeDir;
        private Object thisLock;
        private bool requestStop;
        private bool useHomeSpeed = false;
        public double PulsePermm { get; set; }
        public bool UnitInmm { get; set; }
        private bool homing;

        public MotorMotionNet(string name, Object thisLock, ushort ringNo, ushort cardID, ushort axisCount,
            ushort axisNo, byte homeDir, bool invertSVON, ISpeedCfg homeSpeedCfg, bool simulationMode)
        {
            this.Name = name;
            this.thisLock = thisLock;
            this.ringNo = ringNo;
            this.cardID = cardID;
            this.axisCount = axisCount;
            this.axisNo = axisNo;
            this.homeDir = homeDir;
            this.HomeSpeedCfg = homeSpeedCfg;
            this.RunSpeedCfg = homeSpeedCfg;
            this.simulationMode = simulationMode;
            this.invertSVON = invertSVON;
            this.PulsePermm = 1;
            this.UnitInmm = false;
            requestStop = false;
            homing = false;
        }

        public void Start()
        {
            short status = 0;
            if (simulationMode)
            {
                status = 0;
            }
            else
            {
            }
            //SetFeedbackSrc(1);
            SetSVOn(1);
        }

        public void SetSpeed(double StartVelocity, double MaxVelocity, double rampUpTime=0.5, double rampDownTime=0.5)
        {
            short status = 0;
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                }
                else
                {
                    //status = MNet.M1A._mnet_m1a_set_tmove_speed(ringNo, cardID, (uint)StartVelocity, (uint)MaxVelocity, (float)rampUpTime, (float)rampDownTime);
                    status = MNet.M1A._mnet_m1a_set_smove_speed(ringNo, cardID, (uint)StartVelocity, (uint)MaxVelocity, (float)rampUpTime, (float)rampDownTime);
                    if (status != 0) throw new AmonetMotionException("Fail to set fix speed", status);
                }
                var speedCfg = new ISpeedCfg(StartVelocity, MaxVelocity, rampUpTime, rampDownTime);
            }
        }

        public void SetFeedbackSrc(ushort value)
        {
            short status = 0;
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                }
                else
                {
                    status = MNet.M1A._mnet_m1a_set_feedback_src(ringNo, cardID, value);
                    //MNet.M1A._mnet_m1a_set_abs_reference()
                    if (status != 0) throw new AmonetMotionException("Fail to set pulsein mode", status);
                }
            }
        }

        public void SetSVOn(ushort svOn)
        {
            short status = 0;
            if (invertSVON)
            {
                svOn = (ushort)((svOn == 0) ? 1 : 0);
            }
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                }
                else
                {
                    status = MNet.M1A._mnet_m1a_set_svon(ringNo, cardID, svOn);
                    if (status != 0) throw new AmonetMotionException("Fail to set svON", status);
                }
            }
        }

        public void Stop(bool emergency=false)
        {
            short status = 0;
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                }
                else
                {
                    if (!emergency)
                    {
                        if (axisCount == 1)
                            status = MNet.M1A._mnet_m1a_sd_stop(ringNo, cardID);
                        else
                            status = MNet.M1A._mnet_m1a_group_sd_stop(ringNo, 0);
                    }
                    else
                    {
                        if (axisCount == 1)
                            status = MNet.M1A._mnet_m1a_emg_stop(ringNo, cardID);
                        else
                            status = MNet.M1A._mnet_m1a_group_emg_stop(ringNo, 0);
                    }
                    if (status != 0) throw new AmonetMotionException("Fail to stop", status);
                }
            }
            // make sure the motor stop then only ask to quit the wait
            requestStop = true;
        }

        public void MoveAbsolute(int Position, int timeOutInSeconds)
        {
            DateTime now = DateTime.Now;
            MoveAbsoluteNoWait(Position);
            WaitMotionDone(timeOutInSeconds);
        }

        public void MoveRelative(int distance, int timeOutInSeconds)
        {
            DateTime now = DateTime.Now;
            MoveRelativeNoWait(distance);
            WaitMotionDone(timeOutInSeconds);
        }

        public void MoveAbsoluteNoWait(int Position)
        {
            short status;
            requestStop = false;
            SetSpeed(RunSpeedCfg.StartVelocity, RunSpeedCfg.MaxVelocity,
                RunSpeedCfg.RampUpTime, RunSpeedCfg.RampDownTime);
            if (simulationMode)
            {
                status = 0;
            }
            else
            {
                if (!GetMotionDone())
                {
                    throw new AmonetMotionException("Start a motion before the previous motion done", (short)axisNo);
                }
                lock (thisLock)
                {
                    status = MNet.M1A._mnet_m1a_start_a_move(ringNo, cardID, Position);
                    if (status != 0) throw new AmonetMotionException("Fail to start a move", status);
                }
            }
        }

        public void MoveAbsoluteXY(int PositionX, int PositionY, int timeOutInSeconds)
        {
            DateTime now = DateTime.Now;
            MoveAbsoluteXYNoWait(PositionX, PositionY);
            WaitMotionDone(timeOutInSeconds);
        }

        public void MoveAbsoluteXYNoWait(int PositionX, int PositionY)
        {
            short status;
            requestStop = false;
            if (simulationMode)
            {
                status = 0;
            }
            else
            {
                if (!GetMotionDone())
                {
                    throw new AmonetMotionException("Start a motion before the previous motion done", (short)axisNo);
                }
                lock (thisLock)
                {
                    throw new NotImplementedException("Not support xy move");
                }
            }
        }

        public void MoveAbsoluteXYZ(int PositionX, int PositionY, int PositionZ, int timeOutInSeconds)
        {
            DateTime now = DateTime.Now;
            MoveAbsoluteXYZNoWait(PositionX, PositionY, PositionZ);
            WaitMotionDone(timeOutInSeconds);
        }

        public void MoveAbsoluteXYZNoWait(int PositionX, int PositionY, int PositionZ)
        {
            short status;
            requestStop = false;
            if (simulationMode)
            {
                status = 0;
            }
            else
            {
                if (!GetMotionDone())
                {
                    throw new AmonetMotionException("Start a motion before the previous motion done", (short)axisNo);
                }
                lock (thisLock)
                {
                    throw new NotImplementedException("Not support xyz move");
                }
            }
        }

        public void MoveRelativeXY(int PositionX, int PositionY, int timeOutInSeconds)
        {
            DateTime now = DateTime.Now;
            MoveRelativeXYNoWait(PositionX, PositionY);
            WaitMotionDone(timeOutInSeconds);
        }

        public void MoveRelativeXYNoWait(int PositionX, int PositionY)
        {
            short status;
            requestStop = false;
            if (simulationMode)
            {
                status = 0;
            }
            else
            {
                if (!GetMotionDone())
                {
                    throw new AmonetMotionException("Start a motion before the previous motion done", (short)axisNo);
                }
                lock (thisLock)
                {
                    throw new NotImplementedException("Not support xyz move");
                }
            }
        }

        public void MoveRelativeXYZ(int PositionX, int PositionY, int PositionZ, int timeOutInSeconds)
        {
            DateTime now = DateTime.Now;
            MoveRelativeXYZNoWait(PositionX, PositionY, PositionZ);
            WaitMotionDone(timeOutInSeconds);
        }

        public void MoveRelativeXYZNoWait(int PositionX, int PositionY, int PositionZ)
        {
            short status;
            requestStop = false;
            if (simulationMode)
            {
                status = 0;
            }
            else
            {
                if (!GetMotionDone())
                {
                    throw new AmonetMotionException("Start a motion before the previous motion done", (short)axisNo);
                }
                lock (thisLock)
                {
                    throw new NotImplementedException("Not support xyz move");
                }
            }
        }

        public void MoveRelativeNoWait(int distance)
        {
            short status;
            requestStop = false;
            SetSpeed(RunSpeedCfg.StartVelocity, RunSpeedCfg.MaxVelocity,
                RunSpeedCfg.RampUpTime, RunSpeedCfg.RampDownTime);
            if (simulationMode)
            {
                status = 0;
            }
            else
            {
                if (!GetMotionDone())
                {
                    throw new AmonetMotionException("Start a motion before the previous motion done", (short)axisNo);
                }
                lock (thisLock)
                {
                    status = MNet.M1A._mnet_m1a_start_r_move(ringNo, cardID, distance);
                    if (status != 0) throw new AmonetMotionException("Fail to start r move", status);
                }
            }
        }

        public bool GetMotionDone()
        {
            ushort motion = 0;
            short status;

            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                    motion = 0;
                }
                else
                {
                    if (GetAlarm())
                    {
                        throw new MotionAlarm("Motion has alarm", axisNo);
                    }
                    status = MNet.M1A._mnet_m1a_motion_done(ringNo, cardID, ref motion);
                    if (status != 0) throw new AmonetMotionException("Fail to start r move", status);
                }

            }
            bool motionDone = (motion == 0);
            if (motionDone && homing)
            {
                Task.Delay(100).Wait();
                homing = false;
                Reset();
            }
            return motionDone;
        }

        public int GetPosition()
        {
            short status;
            int Pos = 0;
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                    Pos = 100;
                }
                else
                {
                    status = MNet.M1A._mnet_m1a_get_position(ringNo, cardID, ref Pos);
                    if (status != 0) throw new AmonetMotionException("Fail to get position", status);
                }
            }
            return Pos;
        }

        public int GetErrorCounter()
        {
            short status;
            int errorCounter = 0;
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                    errorCounter = 100;
                }
                else
                {
                    status = MNet.M1A._mnet_m1a_get_error_counter(ringNo, cardID, ref errorCounter);
                    if (status != 0) throw new AmonetMotionException("Fail to get error counter", status);
                }
            }
            return errorCounter;
        }

        public void ResetErrorCounter()
        {
            short status;
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                }
                else
                {
                    status = MNet.M1A._mnet_m1a_reset_error_counter(ringNo, cardID);
                    if (status != 0) throw new AmonetMotionException("Fail to reset error counter", status);
                }
            }
        }

        public int GetCommand()
        {
            short status;
            int cmdCounter = 0;
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                    cmdCounter = 100;
                }
                else
                {
                    status = MNet.M1A._mnet_m1a_get_command(ringNo, cardID, ref cmdCounter);
                    if (status != 0) throw new AmonetMotionException("Fail to get cmd counter", status);
                }
            }
            return cmdCounter;
        }

        public void Home(int timeOutInSeconds)
        {
            HomeNoWait();
            WaitMotionDone(timeOutInSeconds);
            SetSpeed(RunSpeedCfg.StartVelocity, RunSpeedCfg.MaxVelocity, RunSpeedCfg.RampUpTime, RunSpeedCfg.RampDownTime);
        }

        public void WaitMotionDone(int timeOutInSeconds)
        {
            DateTime now = DateTime.Now;
            ManualResetEvent ev;
            while (!GetMotionDone())
            {
                ev = new ManualResetEvent(false);
                ev.WaitOne(25);
                if (requestStop)
                {
                    requestStop = false;
                    break;
                }
                if ((DateTime.Now - now).TotalSeconds > timeOutInSeconds)
                {
                    throw new MotionTimeout(string.Format("Motion axis {0} timeout on waiting motion done", axisNo));
                }
            }
        }

        public void WaitXYMotionDone(int timeOutInSeconds)
        {
            WaitMotionDone(timeOutInSeconds);
        }

        public void WaitXYZMotionDone(int timeOutInSeconds)
        {
            WaitMotionDone(timeOutInSeconds);
        }

        public void HomeNoWait()
        {
            short status;
            requestStop = false;
            homing = true;
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                }
                else
                {
                    status = MNet.M1A._mnet_m1a_home_search(ringNo, cardID, homeDir, (uint)HomeSpeedCfg.StartVelocity,
                        (uint)HomeSpeedCfg.MaxVelocity, (float)HomeSpeedCfg.RampUpTime, 100);
                    if (status != 0) throw new AmonetMotionException("Fail to home", status);
                }
            }
        }

        public void Reset()
        {
            short status;
            // Start homing
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                }
                else
                {
                    status = MNet.M1A._mnet_m1a_reset_position(ringNo, cardID);
                    if (status != 0) throw new AmonetMotionException("Fail to reset position", status);

                    status = MNet.M1A._mnet_m1a_reset_error_counter(ringNo, cardID);
                    if (status != 0) throw new AmonetMotionException("Fail to reset error counter", status);

                    status = MNet.M1A._mnet_m1a_reset_command(ringNo, cardID);
                    if (status != 0) throw new AmonetMotionException("Fail to reset command", status);
                }
            }
        }

        public void ResetAlarm()
        {
            short status;
            ManualResetEvent ev = new ManualResetEvent(false);
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                }
                else
                {
                    status = MNet.M1A._mnet_m1a_set_ralm(ringNo, cardID, 1);
                    if (status != 0) throw new AmonetMotionException("Fail to ralm", status);
                }

                ev.WaitOne(100);
                
                if (simulationMode)
                {
                    status = 0;
                }
                else
                {
                    status = MNet.M1A._mnet_m1a_set_ralm(ringNo, cardID, 0);
                    if (status != 0) throw new AmonetMotionException("Fail to ralm", status);
                }
            }
        }

        public IOStatus GetIOStatus()
        {
            short status;
            uint IO_status = 0;
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                }
                else
                {
                    status = MNet.M1A._mnet_m1a_get_io_status(ringNo, cardID, ref IO_status);
                    if (status != 0) throw new AmonetMotionException("Fail to get io status", status);
                }
            }
            var ioStatus = new IOStatus(IO_status);
            if (invertSVON)
            {
                ioStatus.servoOn = (ioStatus.servoOn) ? false : true;
            }
            return ioStatus;
        }

        public bool GetAlarm()
        {
            return GetIOStatus().alarm;
        }

        public bool GetRDY()
        {
            return GetIOStatus().ready;
        }

        public bool GetInPosition()
        {
            return GetIOStatus().inPosition;
        }

        public uint GetErrorStatus()
        {
            short status;
            int errorStatus = 0;
            lock (thisLock)
            {
                if (simulationMode)
                {
                    status = 0;
                    errorStatus = 100;
                }
                else
                {
                    status = MNet.M1A._mnet_m1a_check_error(ringNo, cardID, ref errorStatus);
                    if (status != 0) throw new AmonetMotionException("Fail to get error status", status);
                }
            }
            return (uint)errorStatus;
        }

        public void LimitSearchNoWait(int direction)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HardwareInterfaces;
using AdLink;

namespace HardwareLib
{
    public class P8134MotionControl : IMotionCard
    {
        public Object thisLock;
        private uint cardID = 0;
        private uint DeviceID = 0;
        private string cfgFile = "";
        private bool simulationMode = false;

        public P8134MotionControl(uint cardID, string cfgFile, bool simulationMode)
        {
            this.thisLock = new Object();
            this.cardID = cardID;
            this.cfgFile = cfgFile;
            this.simulationMode = simulationMode;
        }

        public void Setup()
        {
            short totalCards = 2;
            if (simulationMode)
            {
                return;
            }
            short Result;
            Result = Pci_8134.W_8134_InitialA(ref totalCards);
            if (Result != Pci_8134.ERR_NoError)
            {
                throw new Exception("No PCI8134 motion device found");
            }

            Result = Pci_8134.W_8134_Set_Config(@"c:\windows\system32\8134.cfg");
            if (Result != Pci_8134.ERR_NoError)
            {
                throw new Exception("fail to configure 8134");
            }

        }

    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Runtime.InteropServices;
// ERROR: Not supported in C#: OptionDeclaration
namespace AdLink
{
    static class Pci_8134
    {

        //Error Code
        public const short ERR_NoError = 0;
        public const short ERR_BoardNoInit = 1;
        public const short ERR_InvalidBoardNumber = 2;
        public const short ERR_InitializedBoardNumber = 3;
        public const short ERR_BaseAddressError = 4;
        public const short ERR_BaseAddressConflict = 5;
        public const short ERR_DuplicateBoardSetting = 6;
        public const short ERR_DuplicateIrqSetting = 7;
        public const short ERR_PCIBiosNotExist = 8;
        public const short ERR_PCIIrqNotExist = 9;
        public const short ERR_PCICardNotExist = 10;
        public const short ERR_InputAxisError = 11;
        public const short ERR_SetVelError = 12;
        public const short ERR_SetAccError = 13;
        public const short ERR_SetDecError = 14;
        public const short ERR_SetPosError = 15;
        public const short ERR_SetRMDError = 16;
        public const short ERR_ClrPlsError = 17;
        public const short ERR_MoveError = 18;
        public const short ERR_GoHomeError = 19;
        public const short ERR_GetIntError = 20;

        ///* ------------ I/O Status Definition ------------------------------------- */
        public const short pos_limit = 0x1;
        public const short neg_limit = 0x2;
        public const short psd_switch = 0x4;
        public const short nsd_switch = 0x8;
        public const short org_limit = 0x10;
        public const short idx_switch = 0x20;
        public const short alm_switch = 0x40;
        public const short svon_sts = 0x80;
        public const short rdy_sts = 0x100;
        public const short int_sts = 0x200;
        public const short erc_sts = 0x400;
        public const short inp_sts = 0x800;

        ///* ------------ Motion Done Reurn Value Definition ------------------------ */
        public const short BUSY = 0x0;
        public const short DONE = 0x1;
        public const short POS_LIM = 0x2;
        public const short NEG_LIM = 0x3;
        public const short ORG_LIM = 0x4;
        public const short ALARM = 0x5;

       // [DllImport("8134.dll",
        //     EntryPoint = "W_8134_InitialA",
        //     CallingConvention = CallingConvention.StdCall)]
        [DllImport("8134.dll", EntryPoint = "W_8134_InitialA", CallingConvention = CallingConvention.StdCall)]
        public static extern short W_8134_InitialA(ref short TotalCards);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short W_8134_Close(int existCards);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]

        public static extern short W_8134_Set_SVON(int axis, int on_off);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]

        public static extern void W_8134_Get_IRQ_Status(short cardNo, ref short ch1);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern void W_8134_Get_IRQ_Channel(short cardNo, ref short irq_no);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern void W_8134_Get_Base_Addr(short cardNo, ref short base_addr);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern void W_8134_Set_INT_Control(short cardNo, short intFlag);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]

        public static extern short W_8134_Set_Config(string fileName);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]

        public static extern short start_a_move(short axis, double pos, double str_vel, double max_vel, double accel);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short a_move(short axis, double pos, double str_vel, double max_vel, double accel);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short start_r_move(short axis, double distance, double str_vel, double max_vel, double accel);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short r_move(short axis, double distance, double str_vel, double max_vel, double accel);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short start_t_move(short axis, double distance, double str_vel, double max_vel, double accel, double decel);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short t_move(short axis, double distance, double str_vel, double max_vel, double accel, double decel);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short wait_for_done(short axis);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]

        public static extern short set_move_ratio(short axis, double ratio);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short get_position(short axis, ref double pos);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short set_position(short axis, double pos);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short get_command(short axis, ref double pos);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short set_command(short axis, double pos);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short v_move(short axis, double str_vel, double max_vel, double accel);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short sv_move(short axis, double str_vel, double max_vel, double Tlacc, double Tsacc);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short v_stop(short axis, double decel);
        [DllImport("8134.fll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short fix_max_speed(short axis, double max_val);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]

        public static extern short get_io_status(short axis, ref short io_sts);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]

        public static extern short motion_done(short axis);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]

        public static extern short map_axes(short n_axes, short[] map_array);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short set_move_speed(double str_vel, double max_vel);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short set_move_accel(double accel);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short set_arc_division(short axis, double degrees);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short arc_optimization(int optimize);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short move_xy(int cardNo, double x, double y);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short move_zu(int cardNo, double z, double u);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short arc_xy(int cardNo, double x_center, double y_center, double angle);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short arc_zu(int cardNo, double z_center, double u_center, double angle);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short start_move_xy(int cardNo, double x, double y);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short start_move_zu(int cardNo, double z, double u);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short recover_xy(int cardNo);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short recover_zu(int cardNo);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]

        public static extern short set_home_config(int axis, int home_mode, int org_logic, int org_latch, int EZ_logic);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short home_move(int axis, double str_vel, double max_vel, double accel);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]

        public static extern short set_manu_iptmode(int axis, int manu_iptmode, int op_mode);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short manu_move(int axis, double max_vel);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]

        public static extern short set_pls_outmode(int axis, int pls_outmode);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short set_pls_iptmode(int axis, int pls_iptmode);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short set_cnt_src(int axis, int cnt_src);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]

        public static extern short set_alm_logic(int axis, int alm_logic, int alm_mode);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short set_inp_logic(int axis, int inp_logic, int inp_enable);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short set_sd_logic(int axis, int sd_logic, int sd_latch, int sd_enable);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]

        public static extern short set_int_factor(short axis, int int_factor);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short get_int_status(int axis, ref int int_status);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]

        public static extern short W_8134_INT_Enable(int card_number, int phEvent);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short W_8134_INT_Disable(int card_number);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]

        public static extern short start_ta_move(short axis, double pos, double str_vel, double max_vel, double Tacc, double Tdec);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short ta_move(short axis, double pos, double str_vel, double max_vel, double Tacc, double Tdec);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short start_s_move(short axis, double pos, double str_vel, double max_vel, double Tlacc, double Tsacc);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short s_move(short axis, double pos, double str_vel, double max_vel, double Tlacc, double Tsacc);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short start_rs_move(short axis, double distance, double str_vel, double max_vel, double Tlacc, double Tsacc);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short rs_move(short axis, double distance, double str_vel, double max_vel, double Tlacc, double Tsacc);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short start_tas_move(short axis, double pos, double str_vel, double max_vel, double Tlacc, double Tsacc, double Tldec, double Tsdec);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short tas_move(short axis, double pos, double str_vel, double max_vel, double Tlacc, double Tsacc, double Tldec, double Tsdec);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short start_move_all(short length, short[] map_array, double[] pos, double[] str_vel, double[] max_vel, double[] Tacc);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short move_all(short length, short[] map_array, double[] pos, double[] str_vel, double[] max_vel, double[] Tacc);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short wait_for_all(short length, short[] map_array);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]

        public static extern short v_change(short axis, double max_vel, double accel);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short set_move_mode(int mode);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short set_move_pos(double pos);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short set_move_saccel(double Tlacc, double Tsacc);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short start_motion();
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]

        public static extern short special_home_move(int axis, double str_vel, double max_vel, double sliding_vel, double accel);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short start_sa_move_xy(short cardNo, double PosX, double PosY, double StrVel, double MaxVel, double Tacc, double Tdec);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short start_ta_move_xy(short cardNo, double PosX, double PosY, double StrVel, double MaxVel, double Tacc, double Tdec);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short start_sr_move_xy(short cardNo, double DistX, double DistY, double StrVel, double MaxVel, double Tacc, double Tdec);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short start_tr_move_xy(short cardNo, double DistX, double DistY, double StrVel, double MaxVel, double Tacc, double Tdec);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]

        public static extern short start_sa_move_zu(short cardNo, double PosX, double PosY, double StrVel, double MaxVel, double Tacc, double Tdec);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short start_ta_move_zu(short cardNo, double PosX, double PosY, double StrVel, double MaxVel, double Tacc, double Tdec);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short start_sr_move_zu(short cardNo, double DistX, double DistY, double StrVel, double MaxVel, double Tacc, double Tdec);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern short start_tr_move_zu(short cardNo, double DistX, double DistY, double StrVel, double MaxVel, double Tacc, double Tdec);
        [DllImport("8134.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]

        public static extern short start_sa_move_all(short length, short[] map_array, double[] pos, double[] str_vel, double[] max_vel, double[] Tlacc, double[] Tsacc);
    }
}

//=======================================================
//Service provided by Telerik (www.telerik.com)
//Conversion powered by NRefactory.
//Twitter: @telerik
//Facebook: facebook.com/telerik
//=======================================================

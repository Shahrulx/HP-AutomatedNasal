﻿using System;
using System.Collections.Generic;
using TPM;

namespace HardwareLib
{
    public class PCED122DAQ
    {
        private bool simulationMode = false;

        public Object thisLock = new Object();
        private bool m_Initialized = false;
        public Dictionary<ushort, Dictionary<byte, byte>> inputValue = new Dictionary<ushort, Dictionary<byte, byte>>();
        public Dictionary<ushort, Dictionary<byte, byte>> outputValue = new Dictionary<ushort, Dictionary<byte, byte>>();
        public Dictionary<int, int> DriverHandlers = new Dictionary<int, int>();

        public PCED122DAQ(bool simulationMode)
        {
            this.simulationMode = simulationMode;
            Setup();
        }

        public void Setup()
        {
            int status;
            ushort count = 0;
            if (m_Initialized || simulationMode)
            {
                return;
            }

            m_Initialized = true;
            status = D122._d122_open(ref count);
            if (status != 0)
                throw new Exception($"Fail to get PCE D122 card list with return code: {status}");
            if (count == 0)
                throw new Exception("No PCED22 card found");
        }

        public void TearDown()
        {
            int status = 0;
            m_Initialized = false;
            foreach(var pair in DriverHandlers)
            {
                int handle = pair.Value;
                status = D122._d122_close();
                if (status < 0)
                    throw new Exception($"Fail to close PCED122 with return code: {status}");
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HardwareInterfaces;
using TPM;

namespace HardwareLib
{
    public class PCEM114MotionControl : IMotionCard
    {
        public Object thisLock;
        private uint cardID = 0;
        private uint DeviceID = 0;
        private string cfgFile = "";
        private bool simulationMode = false;
        private bool initialized = false;

        public PCEM114MotionControl(uint cardID, string cfgFile, bool simulationMode)
        {
            this.thisLock = new Object();
            this.cardID = cardID;
            this.cfgFile = cfgFile;
            this.simulationMode = simulationMode;
        }

        public void Open()
        {
            ushort totalCards = 0;
            ushort cardNo = 0;
            if (simulationMode)
            {
                return;
            }
            Master.PCI_M114.ErrCode err;
            //avoid re-open card
            err = Master.PCI_M114._m114_close();
            err = Master.PCI_M114._m114_open(ref totalCards);
            if (err != Master.PCI_M114.ErrCode.ERR_NoError)
            {
                throw new Exception("fail to open M114 motion card");
            }
            if (totalCards == 0)
            {
                throw new Exception("No PCM114 motion device found");
            }
            
            err = Master.PCI_M114._m114_get_switch_card_num((ushort)this.cardID, ref cardNo);
            if (err != Master.PCI_M114.ErrCode.ERR_NoError)
            {
                throw new Exception("fail to get cardNo");
            }

            err = Master.PCI_M114._m114_initial(cardNo);
            if (err != (short)Master.PCE_M134.ErrCode.M134ERR_NoError)
            {
                throw new Exception("fail to initialize M114 motion card");
            }
        }

        public void SetCfg(string cfgFile)
        {
            this.cfgFile = cfgFile;
        }

        public void Setup()
        {
            if (initialized)
            {
                return;
            }
            initialized = true;
            ushort cardNo = 0;
            if (simulationMode)
            {
                return;
            }
            Master.PCI_M114.ErrCode err;

            char[] filePath = (cfgFile + "\0").ToCharArray();
            err = Master.PCI_M114._m114_config_from_file(cardNo, filePath);
            if (err != Master.PCI_M114.ErrCode.ERR_NoError)
            {
                throw new Exception("fail to configure M114 motion card from cfg file: Err:" + err.ToString());
            }

            /*
            Master.PCE_M134._m134_set_feedback_src(0, 0, 0);
            Master.PCE_M134._m134_set_feedback_src(0, 1, 0);
            Master.PCE_M134._m134_set_abs_reference(0, 0, 0);
            Master.PCE_M134._m134_set_abs_reference(0, 1, 0);

            Master.PCE_M134._m134_set_pls_outmode(0, 0, 0);
            Master.PCE_M134._m134_set_pls_outmode(0, 1, 0);

            Master.PCE_M134._m134_set_pls_iptmode(0, 0, 2, 1);
            Master.PCE_M134._m134_set_pls_iptmode(0, 1, 2, 0);
            */
        }
    }
}

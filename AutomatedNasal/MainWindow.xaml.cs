﻿using AutomatedNasal.Pages;
using AutomationLib;
using HardwareLib;
using MachineControl;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace AutomatedNasal
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Hardware hw;
        public MainFlow mainFlow;
        ManualMode manualMode;
        PageMotor pageMotor;
        PageInputOutput pageIO = null;
        MainPage mainpage;
        MyConfig myConfig;
        SettingWindow settingWindow;
        PageSetting pageSetting;
        AboutPage pageAbout;


        private static Logger logger = LogManager.GetCurrentClassLogger();
        public static Logger Processlogger = LogManager.GetLogger("processLog");
        public static Logger Alertlogger = LogManager.GetLogger("alertLog"); //CK 21Mar18
        public bool settingwindowOpen = false;
        public bool AlaramStat = false;
        DispatcherTimer timer = new DispatcherTimer();
   
       
        public MainWindow()
        {
            InitializeComponent();

            //MyConfig.Cfg.Bank1 = new Dictionary<string, Tuple<string, int>>();
            //MyConfig.Cfg.Bank1.Add("A",Tuple.Create("TimerDone", 1));
            //var _GotUnit = MyConfig.Cfg.Bank1.Values.FirstOrDefault(x => x.Item1.Contains("TimerDone"));
            
            MyConfig.Cfg.InitializeConfig("Recepi");
            MyConfig.Cfg.LoadUser();
            Binding myBinding = new Binding("StatusBar")
            {
                Source = MyConfig.Cfg.Model
            };
            BindingOperations.SetBinding(lblMachineStatus, Label.ContentProperty, myBinding);

            myBinding = new Binding("TimeNow")
            {
                Source = MyConfig.Cfg.Model
            };
            BindingOperations.SetBinding(lblDateTime, Label.ContentProperty, myBinding);


            myBinding = new Binding("ErrorLabel")
            {
                Source = MyConfig.Cfg.Model
            };
            BindingOperations.SetBinding(lblErrorHandler, Label.ContentProperty, myBinding);


            lblErrorHandler.Visibility = Visibility.Hidden;


            //Disable button
            btnSetting.IsEnabled = false;

            MyConfig.Cfg.Model.StatusBar = "IDLE";
          
            hw = new Hardware(Config.Cfg.path);
            hw.Setup();
            MyConfig.Cfg.SimulationMode = hw.SimulationMode;
            mainpage = new MainPage(this);
            pageSetting = new PageSetting();
            mainFlow = new MainFlow(hw);
            pageIO = new PageInputOutput(this);
            pageMotor = new PageMotor(this);
            pageAbout = new AboutPage();
            btnAbout.Visibility = Visibility.Hidden;
            
            //manualMode = new ManualMode(this);
         
            //load settings
            //MyConfig.Cfg.PlatingCurrent = Config.Cfg.GetSettings("Settings", "PlatingCurrent");

            MyConfig.Cfg.PlatingVoltage = Config.Cfg.GetSettings("Settings", "PlatingVoltage");
            MyConfig.Cfg.TrickleCurrent = Config.Cfg.GetSettings("Settings", "TrickleCurrent");
            MyConfig.Cfg.TrickleVoltage = Config.Cfg.GetSettings("Settings", "TrickleVoltage");
            MyConfig.Cfg.CutoffVoltage = Config.Cfg.GetSettings("Settings", "CutoffVoltage");
            MyConfig.Cfg.LowResistance = Config.Cfg.GetSettings("Settings", "LowResistance");
            MyConfig.Cfg.HighResistance = Config.Cfg.GetSettings("Settings", "HighResistance");
            MyConfig.Cfg.OpenCircuit = Config.Cfg.GetSettings("Settings", "OpenCircuit");
            MyConfig.Cfg.RampSteps = Config.Cfg.GetSettings("Settings", "RampSteps");
            MyConfig.Cfg.RampDuration = Config.Cfg.GetSettings("Settings", "RampDuration");

            MyConfig.Cfg.TimeAgitMoveBk = Config.Cfg.GetSettings("AgitSetting", "TimeAgitMoveBk");
            MyConfig.Cfg.TimeAgitMoveFt = Config.Cfg.GetSettings("AgitSetting", "TimeAgitMoveFt");
            MyConfig.Cfg.TimeSignalFt = Config.Cfg.GetSettings("AgitSetting", "TimeSignalFt");
            MyConfig.Cfg.TimeSignalBk = Config.Cfg.GetSettings("AgitSetting", "TimeSignalBk");

            MyConfig.Cfg.FillDelayIOA = Config.Cfg.GetSettings("TankSetting", "FillDelayIOA");
            MyConfig.Cfg.DrainDelayIOA = Config.Cfg.GetSettings("TankSetting", "DrainDelayIOA");
            MyConfig.Cfg.ReFillDelayIOA = Config.Cfg.GetSettings("TankSetting", "ReFillDelayIOA");

            MyConfig.Cfg.FillDelayIOB = Config.Cfg.GetSettings("TankSetting", "FillDelayIOB");
            MyConfig.Cfg.DrainDelayIOB = Config.Cfg.GetSettings("TankSetting", "DrainDelayIOB");
            MyConfig.Cfg.ReFillDelayIOB = Config.Cfg.GetSettings("TankSetting", "ReFillDelayIOB");

            MyConfig.Cfg.FillDelayMandrel = Config.Cfg.GetSettings("TankSetting", "FillDelayMandrel");
            MyConfig.Cfg.DrainDelayMandrel = Config.Cfg.GetSettings("TankSetting", "DrainDelayMandrel");
            MyConfig.Cfg.FillDelayBacker = Config.Cfg.GetSettings("TankSetting", "FillDelayBacker");
            MyConfig.Cfg.DrainDelayBacker = Config.Cfg.GetSettings("TankSetting", "DrainDelayBacker");


            MyConfig.Cfg.TimeAgitMoveBk = Config.Cfg.GetSettings("AgitSetting", "TimeAgitMoveBk");
            MyConfig.Cfg.TimeAgitMoveFt = Config.Cfg.GetSettings("AgitSetting", "TimeAgitMoveFt");
            MyConfig.Cfg.TimeSignalFt = Config.Cfg.GetSettings("AgitSetting", "TimeSignalFt");
            MyConfig.Cfg.TimeSignalBk = Config.Cfg.GetSettings("AgitSetting", "TimeSignalBk");

            MyConfig.Cfg.CellExpireTime = Config.Cfg.GetSettingsInt("OtherTimer", "CellExpire");
            MyConfig.Cfg.Model.TimerExpireEndLot = Config.Cfg.GetSettingsInt("OtherTimer", "EndLotTimer");
            MyConfig.Cfg.Model.EndLotCountDown = MyConfig.Cfg.Model.TimerExpireEndLot;


            MyConfig.Cfg.CurrentSet = Config.Cfg.GetSettingsStr("SavedValue", "CurrentSet");
            MyConfig.Cfg.Model.NextSet = Config.Cfg.GetSettingsStr("SavedValue", "NextSet");
            MyConfig.Cfg.Model.CurrentSetBank1 = Config.Cfg.GetSettingsStr("SavedValue", "Bank1Set");
            MyConfig.Cfg.Model.CurrentSetBank2 = Config.Cfg.GetSettingsStr("SavedValue", "Bank2Set");
            MyConfig.Cfg.Model.BatchNo = Config.Cfg.GetSettingsStr("SavedValue", "BatchNumber");
           
            MyConfig.Cfg.TemperatureOffset = Config.Cfg.GetSettingsInt("Temeprature", "Offset");


            TimeSpan UpTimeNow = TimeSpan.FromMinutes(MyConfig.Cfg.UpTimeCount);
            MyConfig.Cfg.UpTimeCount = UpTimeNow.TotalMinutes;
            String Uptime = UpTimeNow.Hours + "H " + UpTimeNow.Minutes + "m ";
            MyConfig.Cfg.Model.UPTime = Uptime;

            MainFrame.Content = mainpage;
            MyConfig.Cfg.Model.hw = hw;
            //tetst
            var idx = MyConfig.Cfg.TimerList.FindIndex(x => x.Item1 == "1");
            // mainFlow.cellModule.ChangeTimer("1", 5);


            MyConfig.Cfg.Model.TimeNow = DateTime.Now.ToString();
            timer.Interval = TimeSpan.FromSeconds(1); // 1 second updates
            timer.Tick += timer_Tick;
            timer.Start();

            //LOAD VERSION
            var UpdateDate = System.IO.File.GetLastWriteTime(Assembly.GetExecutingAssembly().Location);

            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            string version = fvi.FileVersion;
            var _UpdateDate = UpdateDate.ToString();
            lblVersion.Content = fvi.FileVersion + $"   Last Update: {_UpdateDate}";

        }

        void timer_Tick(object sender, EventArgs e)
        {
            MyConfig.Cfg.Model.TimeNow = DateTime.Now.ToString();

            //var runtime = DateTime.Now - Process.GetCurrentProcess().StartTime;
            //String Uptime = runtime.Hours + "H " + runtime.Minutes + "m ";
            //MyConfig.Cfg.Model.UPTime = Uptime;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            
        }

        private void btnIO_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Content = pageIO;
           
        }

        private void btnHome_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Content = mainpage;
            
        }

        private void btnManualMode_Click(object sender, RoutedEventArgs e)
        {
            if(settingwindowOpen == false)
            {
                settingwindowOpen = true;
                settingWindow = new SettingWindow(this);
                settingWindow.Show();
            }
            else
            {
                settingWindow.Activate();
                settingWindow.WindowState = WindowState.Normal;
            }
           
           // MainFrame.Content = manualMode;
        }

        private void btnMotorRobot_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Content = pageMotor;
        }

        private void btnAlarmOff_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Content = manualMode; 
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        private void btnSetting_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Content = pageSetting;
        }

        private void btnAbout_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Content = pageAbout;
        }

        internal void DisableUI(bool disable)
        {
            if (disable)
            {
                btnIO.IsEnabled = false;
                btnManualMode.IsEnabled = false;
                btnMotorRobot.IsEnabled = false;
                btnSetting.IsEnabled = false;
            }
            else
            {
                btnIO.IsEnabled = true;
                btnManualMode.IsEnabled = true;
                btnMotorRobot.IsEnabled = true;
                btnSetting.IsEnabled = true;
            }
        }

        public void DisableSpecifiedUi(String UiName, bool Enabled)
        {
            Button mybtn = (Button)this.FindName(UiName);
            mybtn.IsEnabled = Enabled;

        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            if (btnLogin.Content.ToString() == "LOGOUT")
            {
                btnLogin.Content = "LOGIN";
                if(mainFlow.currentState == MainFlow.SequenceState.IDLE)
                {
                    MyConfig.Cfg.UpdateUI("btnManual", "Enable", "");
                }
                else
                {
                    MyConfig.Cfg.UpdateUI("btnManual", "Disable", "");
                }

                MyConfig.Cfg.Username = "";
                MyConfig.Cfg.UserType = "";
                DisableSpecifiedUi("btnSetting", false);
            }
            else
            {
                LoginWindow lg = new LoginWindow();
                lg.ShowDialog();

                if (lg.Stat == -1)
                {

                }
                else
                {
                    btnLogin.Content = "LOGOUT";
                    MyConfig.Cfg.UpdateUI("btnManual", "Enable", "");
                    if (MyConfig.Cfg.UserType != "Admin")
                    {
                        DisableSpecifiedUi("btnSetting", false);
                    }
                    else
                    {
                        DisableSpecifiedUi("btnSetting", true);
                    }
                }
            }
        

         

        }

        private void lblMachineStatus_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void btnMinimize_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void btnUnlock_Click(object sender, RoutedEventArgs e)
        {
            if (this.WindowState == WindowState.Normal)
            {
                this.WindowState = WindowState.Maximized;
                btnUnlock.Content = "UNLOCK";
            }
               
            else
            {
                this.WindowState = WindowState.Normal;
                btnUnlock.Content = "LOCK";
           }
              
        }

 
    }

  
}

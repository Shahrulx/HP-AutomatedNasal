﻿using AutomationLib;
using MachineControl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace AutomatedNasal.Pages
{

    /// <summary>
    /// Interaction logic for ManualMode.xaml
    /// </summary>
    public partial class ManualMode : Page
    {
        private MainWindow mainWindow;
        TemperatureControl tc;
        bool TankON = false;
        bool refillIOQA = false;
        bool refillIOQB = false;

        public ManualMode(MainWindow mainWindow)
        {
            InitializeComponent();
            this.mainWindow = mainWindow;

            StartUITimer();
        }

        private void StartUITimer()
        {
            DispatcherTimer dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
            dispatcherTimer.Start();
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            LoadStatus();
        }
        private void LoadStatus()
        {
            if (mainWindow.hw.outputs["Y3_00_MAIN_TANK_PUMP_(RUN)"].Read() == 1)
            {
                ChangeBtnState("btnMainPump", true);

            }
            else
            {
                ChangeBtnState("btnMainPump", false);
            }

            if (mainWindow.hw.outputs["Y3_01_HEATER_ENABLE"].Read() == 1)
            {
                ChangeBtnState("btnHeater", true);
            }
            else
            {
                ChangeBtnState("btnHeater", false);
            }

            if (mainWindow.hw.outputs["Y4_00_IWAKI_(_CARBON_TREAT_)"].Read() == 0)
            {
                ChangeBtnState("btnCarbonTreat", true);
            }
            else
            {
                ChangeBtnState("btnCarbonTreat", false);
            }
            if (mainWindow.hw.outputs["Y4_01_IWAKI_(BRIGHTNER_)"].Read() == 1)
            {
                ChangeBtnState("btnBrt", true);
            }
            else
            {
                ChangeBtnState("btnBrt", false);
            }

            if (mainWindow.hw.outputs["Y4_04_DOOR_UNLOCK"].Read() == 1)
            {
                ChangeBtnState("btnDoor", false);
            }
            else
            {
                ChangeBtnState("btnDoor", true);
            }

            if (mainWindow.hw.outputs["Y2_13_MAIN_WATER"].Read() == 1)
            {
                ChangeBtnState("btnMainWater", true);
            }
            else
            {
                ChangeBtnState("btnMainWater", false);
            }
            if (MyConfig.Cfg.Model.FTAgitStat == "ON")
            {
                ChangeBtnState("btnFrontAgit", true);
            }
            else
            {
                ChangeBtnState("btnFrontAgit", false);
            }
            if (MyConfig.Cfg.Model.BKAgitStat == "ON")
            {
                ChangeBtnState("btnBackAgit", true);
            }
            else
            {
                ChangeBtnState("btnBackAgit", false);
            }

            if (mainWindow.hw.inputs["X5_10_POWER_SUPPLIES_1_6_F_BACK"].Read() == 1 && mainWindow.hw.inputs["X5_11_POWER_SUPPLIES_7_12_F_BACK"].Read() == 1)
            {
                ChangeBtnState("btnPs", true);
            }
            else
            {
                ChangeBtnState("btnPs", false);
            }

            //inflate retact update
            for (int i = 1; i <= 12; i++)
            {
                var value = GetModelValue("BladderCell_" + i);
                var btnInflate = (Button)this.FindName("btnInflate_" + i);
                var btnRetract = (Button)this.FindName("btnRetract_" + i);
                if (value == "RETRACT")
                {
                    btnRetract.IsEnabled = false;
                    btnInflate.IsEnabled = true;
                }
                else
                {
                    btnInflate.IsEnabled = false;
                    btnRetract.IsEnabled = true;
                }
            }
        }

        private string GetModelValue(String UiName)
        {
            String value = "";

            var propInfog = MyConfig.Cfg.Model.GetType().GetProperty(UiName);
            if (propInfog != null)
            {
                var val = propInfog.GetValue(MyConfig.Cfg.Model);
                value = val.ToString();
            }
            return value;
        }



        private void ButtonON(Button btn, bool isON)
        {
            if (isON)
            {
                btn.Background = Brushes.Green;
            }
            else
            {
                btn.Background = Brushes.LightGray;
            }
        }

        private void ButtonOFF(Button btn, bool isON)
        {
            if (isON)
            {
                btn.Background = Brushes.Yellow;
            }
            else
            {
                btn.Background = Brushes.LightGray;
            }
        }


        private void btnTab1_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;

            tab_Main.SelectedIndex = int.Parse(btn.Tag.ToString());
        }



        private void PScmd(object sender, RoutedEventArgs e)
        {
            // ReadVolt ReadCurrent SetVolt SetCurrent
            var btn = sender as Button;
            int address = 0;
            int.TryParse(tboxAddress.Text, out address);

            if (address == 0)
                return;

            switch (btn.Name)
            {
                case "btnReadVolt":
                    var res = mainWindow.mainFlow.PS.PSManual(address, "ReadVolt");
                    tboxGetVoltVal.Text = res;
                    break;

                case "btnReadAmp":
                    res = mainWindow.mainFlow.PS.PSManual(address, "ReadCurrent");
                    tboxGetAmpVal.Text = res;
                    break;

                case "btnSetVolt":
                    var val = tboxSetVoltVal.Text;
                    mainWindow.mainFlow.PS.PSManual(address, "SetVolt", val);
                    break;

                case "btnSetAmp":
                    val = tboxSetAmpVal.Text;
                    mainWindow.mainFlow.PS.PSManual(address, "SetCurrent", val);
                    break;
                case "btnOpenPS":
                    mainWindow.mainFlow.PS.PSManual(address, "Open");
                    break;
                case "btnClosePS":
                    mainWindow.mainFlow.PS.PSManual(address, "Close");
                    break;

                default:
                    break;
            }
        }

        private void btnInflate_1_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            var _name = btn.Name;
            var name = _name.Split('_');

            if (!btn.IsEnabled)
            {
                return;
            }

            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, btn.Name + "Manual Pressed");

            switch (name[1])
            {
                case "1":
                    if (btn.IsEnabled && btn.Name.Contains("btnInflate"))
                        mainWindow.hw.outputs["Y0_00_INFLATE_VALVE_CELL____1"].ON();
                    else
                        mainWindow.hw.outputs["Y0_00_INFLATE_VALVE_CELL____1"].OFF();
                    break;
                case "2":
                    if (btn.IsEnabled && btn.Name.Contains("btnInflate"))
                        mainWindow.hw.outputs["Y0_01_INFLATE_VALVE_CELL____2"].ON();
                    else
                        mainWindow.hw.outputs["Y0_01_INFLATE_VALVE_CELL____2"].OFF();
                    break;
                case "3":
                    if (btn.IsEnabled && btn.Name.Contains("btnInflate"))
                        mainWindow.hw.outputs["Y0_02_INFLATE_VALVE_CELL____3"].ON();
                    else
                        mainWindow.hw.outputs["Y0_02_INFLATE_VALVE_CELL____3"].OFF();
                    break;
                case "4":
                    if (btn.IsEnabled && btn.Name.Contains("btnInflate"))
                        mainWindow.hw.outputs["Y0_03_INFLATE_VALVE_CELL____4"].ON();
                    else
                        mainWindow.hw.outputs["Y0_03_INFLATE_VALVE_CELL____4"].OFF();
                    break;
                case "5":
                    if (btn.IsEnabled && btn.Name.Contains("btnInflate"))
                        mainWindow.hw.outputs["Y0_04_INFLATE_VALVE_CELL____5"].ON();
                    else
                        mainWindow.hw.outputs["Y0_04_INFLATE_VALVE_CELL____5"].OFF();
                    break;
                case "6":
                    if (btn.IsEnabled && btn.Name.Contains("btnInflate"))
                        mainWindow.hw.outputs["Y0_05_INFLATE_VALVE_CELL____6"].ON();
                    else
                        mainWindow.hw.outputs["Y0_05_INFLATE_VALVE_CELL____6"].OFF();
                    break;
                case "7":
                    if (btn.IsEnabled && btn.Name.Contains("btnInflate"))
                        mainWindow.hw.outputs["Y0_06_INFLATE_VALVE_CELL____7"].ON();
                    else
                        mainWindow.hw.outputs["Y0_06_INFLATE_VALVE_CELL____7"].OFF();
                    break;
                case "8":
                    if (btn.IsEnabled && btn.Name.Contains("btnInflate"))
                        mainWindow.hw.outputs["Y0_07_INFLATE_VALVE_CELL____8"].ON();
                    else
                        mainWindow.hw.outputs["Y0_07_INFLATE_VALVE_CELL____8"].OFF();
                    break;
                case "9":
                    if (btn.IsEnabled && btn.Name.Contains("btnInflate"))
                        mainWindow.hw.outputs["Y0_08_INFLATE_VALVE_CELL____9"].ON();
                    else
                        mainWindow.hw.outputs["Y0_08_INFLATE_VALVE_CELL____9"].OFF();
                    break;
                case "10":
                    if (btn.IsEnabled && btn.Name.Contains("btnInflate"))
                        mainWindow.hw.outputs["Y0_09_INFLATE_VALVE_CELL____10"].ON();
                    else
                        mainWindow.hw.outputs["Y0_09_INFLATE_VALVE_CELL____10"].OFF();
                    break;
                case "11":
                    if (btn.IsEnabled && btn.Name.Contains("btnInflate"))
                        mainWindow.hw.outputs["Y0_10_INFLATE_VALVE_CELL____11"].ON();
                    else
                        mainWindow.hw.outputs["Y0_10_INFLATE_VALVE_CELL____11"].OFF();
                    break;
                case "12":
                    if (btn.IsEnabled && btn.Name.Contains("btnInflate"))
                        mainWindow.hw.outputs["Y0_11_INFLATE_VALVE_CELL____12"].ON();
                    else
                        mainWindow.hw.outputs["Y0_11_INFLATE_VALVE_CELL____12"].OFF();
                    break;
                default:
                    break;
            }

            if (btn.IsEnabled)
                btn.IsEnabled = false;
            else
                btn.IsEnabled = true;

            if (btn.Name.Contains("btnRetract_"))
            {
                string Retractname = "btnInflate_" + name[1];
                var Rbtn = (Button)this.FindName(Retractname);
                Rbtn.IsEnabled = true;
            }
            else
            {
                string Retractname = "btnRetract_" + name[1];
                var Rbtn = (Button)this.FindName(Retractname);
                Rbtn.IsEnabled = true;
            }


        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {


        }

        private void ChangeBtnState(string ButtonName, bool ON)
        {
            Button Rbtn;
            //on
            if (ON == true)
            {
                string offbtn = ButtonName + "_On";
                Rbtn = (Button)this.FindName(offbtn);
                Rbtn.Background = Brushes.Green;

                offbtn = ButtonName + "_Off";
                Rbtn = (Button)this.FindName(offbtn);
                Rbtn.Background = Brushes.LightGray;
            }
            else
            {
                string offbtn = ButtonName + "_On";
                Rbtn = (Button)this.FindName(offbtn);
                Rbtn.Background = Brushes.LightGray;

                offbtn = ButtonName + "_Off";
                Rbtn = (Button)this.FindName(offbtn);
                Rbtn.Background = Brushes.Yellow;
            }


        }

        private void BtnFunc(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            string _name = btn.Name;
            var name2 = btn.Name.Split('_');
            //btn.IsEnabled = false;
            string Stat = "";
            int iocheck = 0;

            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, _name +" Manual Pressed");

            if (_name.Contains("On"))
            {
                var io = mainWindow.hw.outputs[btn.Tag.ToString()].Read();

                if (io == 1)
                {
                    return;
                }

                string offbtn = name2[0] + "_Off";

                if (_name == "btnHeater_On" && mainWindow.hw.inputs["X5_08_NICKEL_PUMP_F_BACK"].Read() == 0)
                {
                    MessageBox.Show("Please turn on main pump before turn on heater", "Not able to turn on heater", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

                if (_name == "btnMainPump_On" && mainWindow.hw.inputs["X3_10_LOW_LEVEL"].Read() == 0)
                {
                    MessageBox.Show("Please fill in water until L level before turn on main pump", "Not able to turn on main pump", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

                if (_name.Contains("btnCarbonTreat"))
                {
                    mainWindow.hw.outputs[btn.Tag.ToString()].OFF();
                }
                else if (_name.Contains("btnBrt"))
                {
                    var hw = mainWindow.hw;
                    var name = btn.Tag.ToString();
                    mainWindow.mainFlow.tankModule.CycleBRT();

                    //Task.Run(() =>
                    //{
                    //    for (int i = 0; i < MyConfig.Cfg.BrtStrokeCount; i++)
                    //    {
                    //        hw.outputs[name].ON();
                    //        Thread.Sleep(225);
                    //        hw.outputs[name].OFF();
                    //        Thread.Sleep(225);
                    //    }
                    //});
                }
                else
                {
                    mainWindow.hw.outputs[btn.Tag.ToString()].ON();
                }


                if (_name.Contains("btnRobotRecover"))
                    return;

                var Rbtn = (Button)this.FindName(offbtn);
                Rbtn.IsEnabled = true;
                Stat = "On";
                btn.Background = Brushes.Green;
                Rbtn.Background = Brushes.LightGray;
            }
            else if (_name.Contains("Off"))
            {
                string onbtn = name2[0] + "_On";
                var Rbtn = (Button)this.FindName(onbtn);
                Rbtn.IsEnabled = true;
                if (_name.Contains("btnCarbonTreat"))
                {
                    mainWindow.hw.outputs[btn.Tag.ToString()].ON();
                }
                else
                {
                    mainWindow.hw.outputs[btn.Tag.ToString()].OFF();
                }
                Stat = "Off";

                btn.Background = Brushes.Yellow;
                Rbtn.Background = Brushes.LightGray;
            }
            Thread.Sleep(10);
            switch (name2[0])
            {

                case "btnMainPump":
                    Thread.Sleep(200);

                    if (Stat == "On")
                    {
                        MainPumpOn();
                    }
                    else
                    {
                        MainPumpOff();
                    }
                    break;

                case "btnHeater":

                    if (Stat == "On")
                    {
                        mainWindow.hw.outputs["Y4_05_HEATER_CIRCUIT_RESET"].ON();
                        Thread.Sleep(200);
                        var status = TimeOut(2, "X5_09_HEATER_CONTACTOR_F_BACK", "Heater Contactor No Feedback");
                        status = TimeOut(2, "X5_15_HEATER_CIRCUIT_F_BACK", "Check Heater Control Circuit");

                        if (status == 0)
                            MyConfig.Cfg.Model.HeaterStat = "OFF";
                        else
                            MyConfig.Cfg.Model.HeaterStat = "ON";

                        mainWindow.hw.outputs["Y4_05_HEATER_CIRCUIT_RESET"].OFF();
                    }
                    else
                    {
                        MyConfig.Cfg.Model.HeaterStat = "OFF";
                    }


                    break;

                case "btnFrontAgit":
                    if (mainWindow.hw.inputs["X5_12_FRONT_AGITATOR_F_BACK"].Read() == 1)
                        MyConfig.Cfg.Model.FTAgitStat = "ON";
                    else if (mainWindow.hw.inputs["X5_12_FRONT_AGITATOR_F_BACK"].Read() == 0)
                        MyConfig.Cfg.Model.FTAgitStat = "OFF";

                    break;

                case "btnBackAgit":
                    if (mainWindow.hw.inputs["X5_13_BACK_AGITATOR_F_BACK"].Read() == 1)
                        MyConfig.Cfg.Model.BKAgitStat = "ON";
                    else if (mainWindow.hw.inputs["X5_13_BACK_AGITATOR_F_BACK"].Read() == 0)
                        MyConfig.Cfg.Model.BKAgitStat = "OFF";

                    break;
                    
                case "btnDoor":
                    if (Stat == "On")
                        MyConfig.Cfg.Model.DoorLockStat = "LOCK";
                    else
                        MyConfig.Cfg.Model.DoorLockStat = "UNLOCK";

                    break;

                case "btnBrt":
                    if (Stat == "On")
                        MyConfig.Cfg.Model.BRTPumpStat = "ON";
                    else
                        MyConfig.Cfg.Model.BRTPumpStat = "OFF";

                    break;

                case "btnCarbonTreat":
                    if (Stat == "On")
                        MyConfig.Cfg.Model.CTPumpStat = "ON";
                    else
                        MyConfig.Cfg.Model.CTPumpStat = "OFF";

                    break;

                default:
                    break;
            }

        }

        private void MainPumpOff()
        {
            if (mainWindow.hw.inputs["X5_08_NICKEL_PUMP_F_BACK"].Read() == 0)
                MyConfig.Cfg.Model.MainPumpStat = "OFF";
            else if (mainWindow.hw.inputs["X5_08_NICKEL_PUMP_F_BACK"].Read() == 1)
            {
                MyConfig.Cfg.Model.MainPumpStat = "ERROR";
                MyConfig.Cfg.Model.ErrorCode = "Alert,Error Main Pump OFF";
            }
        }

        private void MainPumpOn()
        {
            if (mainWindow.hw.inputs["X5_08_NICKEL_PUMP_F_BACK"].Read() == 1)
                MyConfig.Cfg.Model.MainPumpStat = "ON";
            else if (mainWindow.hw.inputs["X5_08_NICKEL_PUMP_F_BACK"].Read() == 0)
            {
                MyConfig.Cfg.Model.MainPumpStat = "ERROR";
                MyConfig.Cfg.Model.ErrorCode = "Alert,Error Main Pump On";
            }
        }

        #region Agitator
        private void btnFrontAgit_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            string _name = btn.Name;
            var name2 = btn.Name.Split('_');
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, btn.Name + " Manual Pressed");

            if (_name.Contains("On"))
            {
                string offbtn = name2[0] + "_Off";
                var Rbtn = (Button)this.FindName(offbtn);
                Rbtn.IsEnabled = true;

                if (MyConfig.Cfg.FrontAgitIsOn)
                    return;

                btn.Background = Brushes.Green;
                Rbtn.Background = Brushes.LightGray;
                mainWindow.mainFlow.StartAgitator();

            }
            else if (_name.Contains("Off"))
            {
                string onbtn = name2[0] + "_On";
                var Rbtn = (Button)this.FindName(onbtn);
                Rbtn.IsEnabled = true;
                mainWindow.mainFlow.StopAgitator();

                btn.Background = Brushes.Yellow;
                Rbtn.Background = Brushes.LightGray;
                ////mainWindow.hw.outputs["Y1_08_HOLD"].ON();
                //mainWindow.hw.outputs["Y1_06_IN0"].OFF();
                //        mainWindow.hw.outputs["Y1_09_DRIVE"].OFF();
                //        //mainWindow.hw.outputs["Y1_08_HOLD"].OFF();
                //        //mainWindow.hw.outputs["Y1_11_SVON"].OFF();
                //        var Rbtn = (Button)this.FindName(onbtn);
                //        MyConfig.Cfg.Model.FTAgitStat = "OFF";
                //        Rbtn.IsEnabled = true;
                //        MyConfig.Cfg.FrontAgitIsOn = false;
                //        MyConfig.Cfg.FrontAgittask = null;
                //       // mainWindow.hw.outputs[btn.Tag.ToString()].OFF();
            }
        }


        private void btnBackAgit_On_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            string _name = btn.Name;
            var name2 = btn.Name.Split('_');
            //btn.IsEnabled = false;
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, btn.Name + " Manual Pressed");

            if (_name.Contains("On"))
            {
                string offbtn = name2[0] + "_Off";
                var Rbtn = (Button)this.FindName(offbtn);
                //Rbtn.IsEnabled = true;
                btn.Background = Brushes.Green;
                Rbtn.Background = Brushes.LightGray;
                if (MyConfig.Cfg.BackAgitIsOn)
                    return;

                MyConfig.Cfg.Model.BKAgitStat = "ON";
                MyConfig.Cfg.BackAgitIsOn = true;
                MyConfig.Cfg.BackAgitTask = Task.Run(() =>
                {
                    mainWindow.hw.outputs["Y6_05_SVON"].ON();
                    Thread.Sleep(100);
                    mainWindow.hw.outputs["Y6_04_RESET"].ON();
                    Thread.Sleep(100);
                    mainWindow.hw.outputs["Y6_04_RESET"].OFF();
                    while (mainWindow.hw.inputs["X6_04_SVRE"].Read() != 1)
                    {
                        if (MyConfig.Cfg.SimulationMode)
                            break;
                    }
                    mainWindow.hw.outputs["Y6_01_SETUP"].ON();
                    while (mainWindow.hw.inputs["X6_00_BUSY"].Read() == 1)
                    {
                        if (MyConfig.Cfg.SimulationMode)
                            break;
                    }
                    while (mainWindow.hw.inputs["X6_01_SETON"].Read() != 1)
                    {
                        if (MyConfig.Cfg.SimulationMode)
                            break;
                    }
                    mainWindow.hw.outputs["Y6_01_SETUP"].OFF();

                    while (MyConfig.Cfg.BackAgitIsOn)
                    {
                        mainWindow.hw.outputs["Y6_03_DRIVE"].ON();
                        Thread.Sleep(50);//wait 2sec
                        while (mainWindow.hw.inputs["X6_00_BUSY"].Read() == 1)
                        {
                            if (MyConfig.Cfg.SimulationMode)
                                break;
                            Thread.Sleep(1);
                        }

                        mainWindow.hw.outputs["Y6_03_DRIVE"].OFF();
                        Thread.Sleep(20);//wait 2sec

                        mainWindow.hw.outputs["Y6_00_IN0"].ON();

                        mainWindow.hw.outputs["Y6_03_DRIVE"].ON();
                        Thread.Sleep(50);
                        while (mainWindow.hw.inputs["X6_00_BUSY"].Read() == 1)
                        {
                            if (MyConfig.Cfg.SimulationMode)
                                break;
                            Thread.Sleep(1);
                        }
                        mainWindow.hw.outputs["Y6_00_IN0"].OFF();
                        mainWindow.hw.outputs["Y6_03_DRIVE"].OFF();
                        Thread.Sleep(20);//wait 2sec
                    }
                });

            }
            else if (_name.Contains("Off"))
            {
                string onbtn = name2[0] + "_On";
                //mainWindow.hw.outputs["Y1_08_HOLD"].ON();
                mainWindow.hw.outputs["Y6_00_IN0"].OFF();
                mainWindow.hw.outputs["Y6_03_DRIVE"].OFF();
                //mainWindow.hw.outputs["Y1_08_HOLD"].OFF();
                //mainWindow.hw.outputs["Y1_11_SVON"].OFF();
                var Rbtn = (Button)this.FindName(onbtn);
                Rbtn.IsEnabled = true;
                MyConfig.Cfg.Model.BKAgitStat = "OFF";
                MyConfig.Cfg.BackAgitIsOn = false;
                MyConfig.Cfg.BackAgitTask = null;
                // mainWindow.hw.outputs[btn.Tag.ToString()].OFF();

                btn.Background = Brushes.Yellow;
                Rbtn.Background = Brushes.LightGray;
            }
        }
        #endregion

        private void PlatingConfigGrid_Loaded(object sender, RoutedEventArgs e)
        {
            sliderPlatingCurrent.Maximum = MyConfig.Cfg.PlatingCurrent;

            MyConfig.Cfg.PlatingVoltage = Config.Cfg.GetSettings("Settings", "PlatingVoltage");
            MyConfig.Cfg.TrickleCurrent = Config.Cfg.GetSettings("Settings", "TrickleCurrent");
            MyConfig.Cfg.TrickleVoltage = Config.Cfg.GetSettings("Settings", "TrickleVoltage");
            MyConfig.Cfg.CutoffVoltage = Config.Cfg.GetSettings("Settings", "CutoffVoltage");
            MyConfig.Cfg.LowResistance = Config.Cfg.GetSettings("Settings", "LowResistance");
            MyConfig.Cfg.HighResistance = Config.Cfg.GetSettings("Settings", "HighResistance");
            MyConfig.Cfg.OpenCircuit = Config.Cfg.GetSettings("Settings", "OpenCircuit");
            MyConfig.Cfg.RampSteps = Config.Cfg.GetSettings("Settings", "RampSteps");
            MyConfig.Cfg.RampDuration = Config.Cfg.GetSettings("Settings", "RampDuration");

            MyConfig.Cfg.TimeAgitMoveBk = Config.Cfg.GetSettings("AgitSetting", "TimeAgitMoveBk");
            MyConfig.Cfg.TimeAgitMoveFt = Config.Cfg.GetSettings("AgitSetting", "TimeAgitMoveFt");
            MyConfig.Cfg.TimeSignalFt = Config.Cfg.GetSettings("AgitSetting", "TimeSignalFt");
            MyConfig.Cfg.TimeSignalBk = Config.Cfg.GetSettings("AgitSetting", "TimeSignalBk");

            tboxPlatingCurrent.Text = MyConfig.Cfg.PlatingCurrent.ToString();
            tboxPlatingVoltage.Text = MyConfig.Cfg.PlatingVoltage.ToString();
            tboxTrickleVoltage.Text = MyConfig.Cfg.TrickleVoltage.ToString();
            tboxTrickleCurrent.Text = MyConfig.Cfg.TrickleCurrent.ToString();
            tboxCutOffVoltage.Text = MyConfig.Cfg.CutoffVoltage.ToString();
            tboxLowResistance.Text = MyConfig.Cfg.LowResistance.ToString();
            tboxHighResistance.Text = MyConfig.Cfg.HighResistance.ToString();
            tboxOpenCircuit.Text = MyConfig.Cfg.OpenCircuit.ToString();
            tboxRampStep.Text = MyConfig.Cfg.RampSteps.ToString();
            tboxRampDuration.Text = MyConfig.Cfg.RampDuration.ToString();
        }

        private void btnSaveSettings_Click(object sender, RoutedEventArgs e)
        {
            Config.Cfg.SaveSetting("Settings", "PlatingCurrent", double.Parse(tboxPlatingCurrent.Text));
            Config.Cfg.SaveSetting("Settings", "PlatingVoltage", double.Parse(tboxPlatingVoltage.Text));
            Config.Cfg.SaveSetting("Settings", "TrickleCurrent", double.Parse(tboxTrickleCurrent.Text));
            Config.Cfg.SaveSetting("Settings", "TrickleVoltage", double.Parse(tboxTrickleVoltage.Text));
            Config.Cfg.SaveSetting("Settings", "CutoffVoltage", double.Parse(tboxCutOffVoltage.Text));
            Config.Cfg.SaveSetting("Settings", "LowResistance", double.Parse(tboxLowResistance.Text));
            Config.Cfg.SaveSetting("Settings", "HighResistance", double.Parse(tboxHighResistance.Text));
            Config.Cfg.SaveSetting("Settings", "OpenCircuit", double.Parse(tboxOpenCircuit.Text));
            Config.Cfg.SaveSetting("Settings", "RampSteps", double.Parse(tboxRampStep.Text));
            Config.Cfg.SaveSetting("Settings", "RampDuration", double.Parse(tboxRampDuration.Text));

            MyConfig.Cfg.PlatingCurrent = double.Parse(tboxPlatingCurrent.Text);
            MyConfig.Cfg.PlatingVoltage = double.Parse(tboxPlatingVoltage.Text);
            MyConfig.Cfg.TrickleVoltage = double.Parse(tboxTrickleVoltage.Text);
            MyConfig.Cfg.TrickleCurrent = double.Parse(tboxTrickleCurrent.Text);
            MyConfig.Cfg.CutoffVoltage = double.Parse(tboxCutOffVoltage.Text);
            MyConfig.Cfg.LowResistance = double.Parse(tboxLowResistance.Text);
            MyConfig.Cfg.HighResistance = double.Parse(tboxHighResistance.Text);
            MyConfig.Cfg.OpenCircuit = double.Parse(tboxOpenCircuit.Text);
            MyConfig.Cfg.RampSteps = double.Parse(tboxRampStep.Text);
            MyConfig.Cfg.RampDuration = double.Parse(tboxRampDuration.Text);

            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Plating Configuration Saved");
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "PlatingCurrent:"+tboxPlatingCurrent.Text);
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "PlatingVoltage:" + tboxPlatingVoltage.Text);

            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "TrickleCurrent:" + tboxTrickleVoltage.Text);
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "TrickleCurrent:" + tboxTrickleCurrent.Text);

            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "CutoffVoltage:" + tboxCutOffVoltage.Text);
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "CutoffCurrent:" + tboxLowResistance.Text);
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "HighResistance:" + tboxHighResistance.Text);
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "OpenCircuit:" + tboxOpenCircuit.Text);
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "RampStep:" + tboxRampStep.Text);
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "RampDuration:" + tboxRampDuration.Text);
            Config.Cfg.Save();
        }


        private void btnPsOn_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            string _name = btn.Name;
            var name2 = btn.Name.Split('_');

            if (_name.Contains("On"))
            {
                var ps1 = mainWindow.hw.outputs["Y3_02_POWER_SUPPLIES_ENABLE_CELL_1_6"].Read();
                var ps2 = mainWindow.hw.outputs["Y3_03_POWER_SUPPLIES_ENABLE_CELL_7_12"].Read();

                if (ps1 == 1 || ps2 == 1)
                {
                    return;
                }

                string offbtn = name2[0] + "_Off";
                var Rbtn = (Button)this.FindName(offbtn);

                mainWindow.hw.outputs["Y3_02_POWER_SUPPLIES_ENABLE_CELL_1_6"].ON();
                mainWindow.hw.outputs["Y3_03_POWER_SUPPLIES_ENABLE_CELL_7_12"].ON();
                // Rbtn.IsEnabled = true;
                btn.Background = Brushes.Green;
                Rbtn.Background = Brushes.LightGray;
                MyConfig.Cfg.Model.PowerSupplyStat = "ON";

                mainWindow.mainFlow.InitPowerSupply(true);

            }
            else if (_name.Contains("Off"))
            {
                string onbtn = name2[0] + "_On";
                var Rbtn = (Button)this.FindName(onbtn);

                mainWindow.mainFlow.InitPowerSupply(false);

                // Rbtn.IsEnabled = true;
                mainWindow.hw.outputs["Y3_02_POWER_SUPPLIES_ENABLE_CELL_1_6"].OFF();
                mainWindow.hw.outputs["Y3_03_POWER_SUPPLIES_ENABLE_CELL_7_12"].OFF();

                btn.Background = Brushes.Yellow;
                Rbtn.Background = Brushes.LightGray;
                MyConfig.Cfg.Model.PowerSupplyStat = "OFF";
            }
        }

        private void WaterFillFunc(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            string _name = btn.Name;
            string tankName = "";
            var name2 = btn.Name.Split('_');
            var tag = btn.Tag.ToString();

            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, tag);
           
            if (_name.Contains("Mandrel"))
            {
                tankName = "Mandrel";
            }
            else if (_name.Contains("Backer"))
            {
                tankName = "Backer";
            }
            else if (_name.Contains("btnIOQA"))
            {
                tankName = "IOQA";
            }
            else if (_name.Contains("btnIOQB"))
            {
                tankName = "IOQB";
            }

            switch (tankName)
            {
                case "Mandrel":

                    if (_name.Contains("Re"))
                    {
                        Task.Run(() =>
                        {
                            mainWindow.mainFlow.tankModule.Mandrel("ReFill");
                        });
                    }

                    else if (_name.Contains("Fill"))
                    {
                        mainWindow.mainFlow.tankModule.Mandrel("Fill");
                        //dsiabel this button and enable drain
                        //btn.IsEnabled = false;
                        string offbtn = name2[0] + "_Drain";
                        var Rbtn = (Button)this.FindName(offbtn);
                        // Rbtn.IsEnabled = true;
                    }
                    else if (_name.Contains("Cancel"))
                    {
                        MyConfig.Cfg.TankFillCancel = true;
                        mainWindow.mainFlow.tankModule.Mandrel("Cancel");
                    }
                    else if (_name.Contains("Drain"))
                    {
                        mainWindow.mainFlow.tankModule.Mandrel("Drain");
                        // btn.IsEnabled = false;
                        string offbtn = name2[0] + "_Fill";
                        var Rbtn = (Button)this.FindName(offbtn);
                        //Rbtn.IsEnabled = true;
                    }
                    break;

                case "Backer":


                    if (_name.Contains("Re"))
                    {
                        Task.Run(() =>
                        {
                            mainWindow.mainFlow.tankModule.Backer("ReFill");
                        });
                    }


                    else if (_name.Contains("Fill"))
                    {
                        mainWindow.mainFlow.tankModule.Backer("Fill");
                        //dsiabel this button and enable drain
                        // btn.IsEnabled = false;
                        string offbtn = name2[0] + "_Drain";
                        var Rbtn = (Button)this.FindName(offbtn);
                        // Rbtn.IsEnabled = true;
                    }
                    else if (_name.Contains("Cancel"))
                    {
                        MyConfig.Cfg.TankFillCancel = true;
                        mainWindow.mainFlow.tankModule.Backer("Cancel");
                    }
                    else if (_name.Contains("Drain"))
                    {
                        mainWindow.mainFlow.tankModule.Backer("Drain");
                        // btn.IsEnabled = false;
                        string offbtn = name2[0] + "_Fill";
                        var Rbtn = (Button)this.FindName(offbtn);
                        //Rbtn.IsEnabled = true;
                    }
                    break;

                case "IOQA":
                    if (_name.Contains("Re"))
                    {
                        if (mainWindow.mainFlow.tankModule.IOQAFilling)
                        {
                            mainWindow.mainFlow.tankModule.refillIOQA = true;
                        }
                        else
                        {
                            mainWindow.mainFlow.tankModule.IOQA("ReFill");
                        }

                        //dsiabel this button and enable drain
                        // btn.IsEnabled = false;
                        string offbtn = name2[0] + "_Drain";
                        var Rbtn = (Button)this.FindName(offbtn);
                        //Rbtn.IsEnabled = true;
                    }

                    else if (_name.Contains("Fill"))
                    {
                        mainWindow.mainFlow.tankModule.IOQA("Fill");
                        //dsiabel this button and enable drain
                        // btn.IsEnabled = false;
                        string offbtn = name2[0] + "_Drain";
                        var Rbtn = (Button)this.FindName(offbtn);
                        //Rbtn.IsEnabled = true;
                    }
                    else if (_name.Contains("Cancel"))
                    {
                        MyConfig.Cfg.TankFillCancel = true;
                        mainWindow.mainFlow.tankModule.IOQA("Cancel");
                    }
                    else if (_name.Contains("Drain"))
                    {
                        mainWindow.mainFlow.tankModule.IOQA("Drain");
                        // btn.IsEnabled = false;
                        string offbtn = name2[0] + "_Fill";
                        var Rbtn = (Button)this.FindName(offbtn);
                        //Rbtn.IsEnabled = true;
                    }
                    break;

                case "IOQB":
                    if (_name.Contains("Re"))
                    {
                        if (mainWindow.mainFlow.tankModule.IOQAFilling)
                        {
                            mainWindow.mainFlow.tankModule.refillIOQB = true;
                        }
                        else
                        {
                            mainWindow.mainFlow.tankModule.IOQB("ReFill");
                        }

                        //dsiabel this button and enable drain
                        // btn.IsEnabled = false;
                        string offbtn = name2[0] + "_Drain";
                        var Rbtn = (Button)this.FindName(offbtn);
                        //Rbtn.IsEnabled = true;
                    }
                    else if (_name.Contains("Fill"))
                    {
                        mainWindow.mainFlow.tankModule.IOQB("Fill");
                        //dsiabel this button and enable drain
                        // btn.IsEnabled = false;
                        string offbtn = name2[0] + "_Drain";
                        var Rbtn = (Button)this.FindName(offbtn);
                        //Rbtn.IsEnabled = true;
                    }
                    else if (_name.Contains("Cancel"))
                    {
                        MyConfig.Cfg.TankFillCancel = true;
                        mainWindow.mainFlow.tankModule.IOQB("Cancel");
                    }
                    else if (_name.Contains("Drain"))
                    {
                        mainWindow.mainFlow.tankModule.IOQB("Drain");
                        //btn.IsEnabled = false;
                        string offbtn = name2[0] + "_Fill";
                        var Rbtn = (Button)this.FindName(offbtn);
                        // Rbtn.IsEnabled = true;
                    }
                    break;
                default:
                    break;
            }

        }


        private void btnIOAFront_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;

            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, btn.Name +" Manual Pressed");

            switch (btn.Name)
            {
                case "btnIOAFront":
                    //mainWindow.mainFlow.tankModule.MoveIOAB("IOAFront"); //ck 20Mar18
                    mainWindow.mainFlow.tankModule.IOQBMove("IOAFront");
                    break;

                case "btnIOBFront":
                    //mainWindow.mainFlow.tankModule.MoveIOAB("IOBFront"); //ck 20Mar18
                    mainWindow.mainFlow.tankModule.IOQBMove("IOBFront");
                    break;

                case "btnIOABack":
                    //mainWindow.mainFlow.tankModule.MoveIOAB("IOABack"); //ck 20Mar18
                    mainWindow.mainFlow.tankModule.IOQBMove("IOABack");
                    break;

                case "btnIOBBack":
                    //mainWindow.mainFlow.tankModule.MoveIOAB("IOBBack"); //ck 20Mar18
                    mainWindow.mainFlow.tankModule.IOQBMove("IOBBack");
                    break;

                default:
                    break;
            }

        }

        private int TimeOut(int sec, string SensorName, string Message)
        {
            int stat = 1;
            int count = 0;
            while (mainWindow.hw.inputs[SensorName].Read() != 1)
            {
                Thread.Sleep(100);
                count++;
                if (count > sec)
                {
                    MyConfig.Cfg.Model.ErrorCode = "Alert," + Message;
                    stat = 0;
                    break;
                }

            }

            return stat;

        }

        private void btnInitAdam_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;

            switch (btn.Name)
            {
                case "btnInitAdam":
                    tc = new TemperatureControl();
                    tc.Init();
                    break;
                case "btnReadAdam":
                    tc.ReadOn();
                    break;
                case "btnStopAdam":
                    tc.ReadOff();
                    break;
                default:
                    break;
            }
        }

        private void btnCycleBRTPump_Click(object sender, RoutedEventArgs e)
        {
            mainWindow.mainFlow.tankModule.CycleBRT();
        }

        #region Tank fill

        private void btnFillToLow_Click(object sender, RoutedEventArgs e)
        {
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "FillToLow" + " Manual Pressed");
            mainWindow.mainFlow.tankModule.FillToLow();

        }

        private void btnFillToHigh_Click(object sender, RoutedEventArgs e)
        {
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "FillToHigh" + " Manual Pressed");
            mainWindow.mainFlow.tankModule.FillToHigh();

        }

        private void btnFillToF_Click(object sender, RoutedEventArgs e)
        {
            mainWindow.mainFlow.tankModule.FillToF();
        }

        private void btnFillToHH_Click(object sender, RoutedEventArgs e)
        {
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "FillToHH" + " Manual Pressed");
            mainWindow.mainFlow.tankModule.FillToHH();
        }

        private void btnStopFill_Click(object sender, RoutedEventArgs e)
        {
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "StopFill" + " Manual Pressed");
            mainWindow.mainFlow.tankModule.TankON = false;
        }



        #endregion

        private void btnPlate_1_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            
            var name = btn.Name.Split('_');
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, $"PowerSupply{name[1]}_ON" + " Manual Pressed");

            mainWindow.mainFlow.PS.PSCmd(name[1], "ON",Message: "MANUAL PLATE");
        }

        private void btnPlateCancel_1_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            var name = btn.Name.Split('_');
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, $"PowerSupply{name[1]}_OFF" + " Manual Pressed");
            mainWindow.mainFlow.PS.PSCmd(name[1], "OFF", "CANCEL");
        }

        private void btnCarbonTreat_On_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            string _name = btn.Name;
            var name2 = btn.Name.Split('_');
            //btn.IsEnabled = false;
 
            if (_name.Contains("On"))
            {
                MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, $"CarbonTreat_ON" + " Manual Pressed");
                mainWindow.hw.outputs[btn.Tag.ToString()].OFF();
                string offbtn = name2[0] + "_Off";
                var Rbtn = (Button)this.FindName(offbtn);
                Rbtn.IsEnabled = true;
                btn.Background = Brushes.Green;
                Rbtn.Background = Brushes.LightGray;
                MyConfig.Cfg.Model.CTPumpStat = "ON";
            }
            else
            {
                MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, $"CarbonTreat_OFF" + " Manual Pressed");
                mainWindow.hw.outputs[btn.Tag.ToString()].ON();
                string onbtn = name2[0] + "_On";
                var Rbtn = (Button)this.FindName(onbtn);
                btn.Background = Brushes.Yellow;
                Rbtn.Background = Brushes.LightGray;
                MyConfig.Cfg.Model.CTPumpStat = "OFF";
            }
        }
    }
}

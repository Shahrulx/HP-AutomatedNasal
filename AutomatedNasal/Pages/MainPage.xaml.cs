﻿using AutomationLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AutomatedNasal.Pages
{
    /// <summary>
    /// Interaction logic for MainPage.xaml
    /// </summary>
    public partial class MainPage : Page
    {
        MainWindow main; 
        static readonly object _object = new object();
        int clearunit = 1;
        bool btnRobotActHomeClicked = false;
        bool btnMoveIOQBClicked = false;
        bool btnInitializeClick = false;

        public MainPage(MainWindow main)
        {
            InitializeComponent();
            CustomMessage1.Visibility = Visibility.Hidden;
            CustomMessage.Visibility = Visibility.Hidden;

            this.main = main;

            #region txtchange
            txtTemp.TextChanged += TxtTemp_TextChanged;

            txtAlarm.Tag = "OFF";
            txtAlarm.TextChanged += TxtAlarm_TextChanged;

            txtEstop.Tag = "OFF";
            txtEstop.TextChanged += TxtAlarm_TextChanged;

            txtByPass.Tag = "OFF";
            txtByPass.TextChanged += TxtWarning_TextChanged;

            txtDoorLock.Tag = "UNLOCK";
            txtDoorLock.TextChanged += TxtWarning_TextChanged;

            txtDiWater.Tag = "ON";
            txtDiWater.TextChanged += TxtAlarm_TextChanged;

            txtPlenumSRD.Tag = "NORMAL";
            txtPlenumSRD.TextChanged += TxtAlarm_TextChanged;

            txtPlenumRNS.Tag = "NORMAL";
            txtPlenumRNS.TextChanged += TxtAlarm_TextChanged;

            txtPlenumPMP.Tag = "NORMAL";
            txtPlenumPMP.TextChanged += TxtAlarm_TextChanged;

            txtPlenumPMP.Tag = "NORMAL";
            txtPlenumPMP.TextChanged += TxtAlarm_TextChanged;

            txtPlenumDBL.Tag = "NORMAL";
            txtPlenumDBL.TextChanged += TxtAlarm_TextChanged;

            txtPlenumQA.Tag = "NORMAL";
            txtPlenumQA.TextChanged += TxtAlarm_TextChanged;

            txtPlenumQB.Tag = "NORMAL";
            txtPlenumQB.TextChanged += TxtAlarm_TextChanged;

            txtHeater.Tag = "ON";
            txtHeater.TextChanged += TxtWarning_TextChanged;

            txtMainPump.Tag = "ON";
            txtMainPump.TextChanged += TxtWarning_TextChanged;

            txtBRTPump.Tag = "IDLE";
            txtBRTPump.TextChanged += TxtWarning_TextChanged;

            txtCTPump.Tag = "ON";
            txtCTPump.TextChanged += TxtWarning_TextChanged;

            txtFTAgit.Tag = "ON";
            txtFTAgit.TextChanged += TxtWarning_TextChanged;

            txtBKAgit.Tag = "ON";
            txtBKAgit.TextChanged += TxtWarning_TextChanged;

            txtPowerSupply.Tag = "ON";
            txtPowerSupply.TextChanged += TxtWarning_TextChanged;

            txtIOQA.Tag = "ERROR";
            txtIOQA.TextChanged += TxtError_TextChanged;

            txtIOQB.Tag = "ERROR";
            txtIOQB.TextChanged += TxtError_TextChanged;

            txtMandrlStat.Tag = "FILLED";
            txtMandrlStat.TextChanged += TxtWarning_TextChanged;

            txtBackerStat.Tag = "FILLED";
            txtBackerStat.TextChanged += TxtWarning_TextChanged;

            txtRobotStat.Tag = "Free";
            txtRobotStat.TextChanged += TxtWarning_TextChanged;

            txtBathLevel.TextChanged += txtBathLevel_TextChanged;
            #endregion

            #region BindGui
            foreach (PropertyInfo prop in typeof(Model).GetProperties())
            {
                Console.WriteLine(prop.Name);
            }

            BindGui("TimeCell1", tboxTimeCell1);
            BindGui("TimeCell2", tboxTimeCell2);
            BindGui("TimeCell3", tboxTimeCell3);
            BindGui("TimeCell4", tboxTimeCell4);
            BindGui("TimeCell5", tboxTimeCell5);
            BindGui("TimeCell6", tboxTimeCell6);
            BindGui("TimeCell7", tboxTimeCell7);
            BindGui("TimeCell8", tboxTimeCell8);
            BindGui("TimeCell9", tboxTimeCell9);
            BindGui("TimeCell10", tboxTimeCell10);
            BindGui("TimeCell11", tboxTimeCell11);
            BindGui("TimeCell12", tboxTimeCell12);


            BindGui("cboxIOA1", cboxIOA1);
            BindGui("cboxIOA2", cboxIOA2);
            BindGui("cboxIOA3", cboxIOA3);
            BindGui("cboxIOA4", cboxIOA4);
            BindGui("cboxIOA5", cboxIOA5);
            BindGui("cboxIOA6", cboxIOA6);
            BindGui("cboxIOA7", cboxIOA7);

            BindGui("cboxIOB1", cboxIOB1);
            BindGui("cboxIOB2", cboxIOB2);
            BindGui("cboxIOB3", cboxIOB3);
            BindGui("cboxIOB4", cboxIOB4);
            BindGui("cboxIOB5", cboxIOB5);
            BindGui("cboxIOB6", cboxIOB6);
            BindGui("cboxIOB7", cboxIOB7);

            //enable

            BindGui("cboxEn_1", cboxEn_1);
            BindGui("cboxEn_2", cboxEn_2);
            BindGui("cboxEn_3", cboxEn_3);
            BindGui("cboxEn_4", cboxEn_4);
            BindGui("cboxEn_5", cboxEn_5);
            BindGui("cboxEn_6", cboxEn_6);
            BindGui("cboxEn_7", cboxEn_7);
            BindGui("cboxEn_8", cboxEn_8);
            BindGui("cboxEn_9", cboxEn_9);
            BindGui("cboxEn_10", cboxEn_10);
            BindGui("cboxEn_11", cboxEn_11);
            BindGui("cboxEn_12", cboxEn_12);

            BindGui("cboxCellSensor1", cboxCellSensor1);
            BindGui("cboxCellSensor2", cboxCellSensor2);
            BindGui("cboxCellSensor3", cboxCellSensor3);
            BindGui("cboxCellSensor4", cboxCellSensor4);
            BindGui("cboxCellSensor5", cboxCellSensor5);
            BindGui("cboxCellSensor6", cboxCellSensor6);
            BindGui("cboxCellSensor7", cboxCellSensor7);
            BindGui("cboxCellSensor8", cboxCellSensor8);
            BindGui("cboxCellSensor9", cboxCellSensor9);
            BindGui("cboxCellSensor10", cboxCellSensor10);
            BindGui("cboxCellSensor11", cboxCellSensor11);
            BindGui("cboxCellSensor12", cboxCellSensor12);

            BindGui("CellVoltage1", tboxVout1);
            BindGui("CellVoltage2", tboxVout2);
            BindGui("CellVoltage3", tboxVout3);
            BindGui("CellVoltage4", tboxVout4);
            BindGui("CellVoltage5", tboxVout5);
            BindGui("CellVoltage6", tboxVout6);
            BindGui("CellVoltage7", tboxVout7);
            BindGui("CellVoltage8", tboxVout8);
            BindGui("CellVoltage9", tboxVout9);
            BindGui("CellVoltage10", tboxVout10);
            BindGui("CellVoltage11", tboxVout11);
            BindGui("CellVoltage12", tboxVout12);


            BindGui("CellCurent1", tboxAmp1);
            BindGui("CellCurent2", tboxAmp2);
            BindGui("CellCurent3", tboxAmp3);
            BindGui("CellCurent4", tboxAmp4);
            BindGui("CellCurent5", tboxAmp5);
            BindGui("CellCurent6", tboxAmp6);
            BindGui("CellCurent7", tboxAmp7);
            BindGui("CellCurent8", tboxAmp8);
            BindGui("CellCurent9", tboxAmp9);
            BindGui("CellCurent10", tboxAmp10);
            BindGui("CellCurent11", tboxAmp11);
            BindGui("CellCurent12", tboxAmp12);

            BindGui("EndLotCountDown", tboxEndLotTimer);
        
            //BindGui("TotalAmpMin", tboxTotalAmpMin);
            tboxTotalAmpMin.Text = MyConfig.Cfg.TotalAmpMin.ToString();

            BindGui("BathLevelLabel", txtBathLevel);
            MyConfig.Cfg.Model.BathLevelLabel = "NORMAL";

            BindGui("BathLevelLL", cboxBathLevel_1);
            BindGui("BathLevelL", cboxBathLevel_2);
            BindGui("BathLevelF", cboxBathLevel_3);
            BindGui("BathLevelH", cboxBathLevel_4);
            BindGui("BathLevelHH", cboxBathLevel_5);

            BindGui("IOQBStat", txtIOQB);
            BindGui("IOQAStat", txtIOQA);
            BindGui("MandrelStat", txtMandrlStat);
            BindGui("BackerStat", txtBackerStat);

            BindGui("PowerSupplyStat", txtPowerSupply);
            MyConfig.Cfg.Model.PowerSupplyStat = "OFF";

            BindGui("BKAgitStat", txtBKAgit);
            MyConfig.Cfg.Model.BKAgitStat = "OFF";

            BindGui("FTAgitStat", txtFTAgit);
            MyConfig.Cfg.Model.FTAgitStat = "OFF";

            BindGui("CTPumpStat", txtCTPump);
            MyConfig.Cfg.Model.CTPumpStat = "ON";

            BindGui("BRTPumpStat", txtBRTPump);
            MyConfig.Cfg.Model.BRTPumpStat = "OFF";

            BindGui("MainPumpStat", txtMainPump);
            MyConfig.Cfg.Model.MainPumpStat = "OFF";

            BindGui("TempStat", txtTemp);
       

            BindGui("HeaterStat", txtHeater);
            MyConfig.Cfg.Model.HeaterStat = "OFF";
            BindGui("PlenumQBStat", txtPlenumQB);
            MyConfig.Cfg.Model.PlenumQBStat = "Normal";
            BindGui("PlenumQAStat", txtPlenumQA);
            MyConfig.Cfg.Model.PlenumQAStat = "Normal";
            BindGui("PlenumDBLStat", txtPlenumDBL);
            MyConfig.Cfg.Model.PlenumDBLStat = "Normal";
            BindGui("PlenumPMPStat", txtPlenumPMP);
            MyConfig.Cfg.Model.PlenumPMPStat = "Normal";
            BindGui("PlenumSRDStat", txtPlenumSRD);
            MyConfig.Cfg.Model.PlenumSRDStat = "Normal";
            BindGui("PlenumRNStat", txtPlenumRNS);
            MyConfig.Cfg.Model.PlenumRNStat = "Normal";
            BindGui("DiWaterStat", txtDiWater);
            MyConfig.Cfg.Model.DiWaterStat = "Normal";
            BindGui("DoorLockStat", txtDoorLock);
            MyConfig.Cfg.Model.DoorLockStat = "Unlock";
            BindGui("BypassStat", txtByPass);
            MyConfig.Cfg.Model.BypassStat = "OFF";

            BindGui("EMO", txtEstop);
            BindGui("AlarmStat", txtAlarm);
            MyConfig.Cfg.Model.AlarmStat = "OFF";

            BindGui("RobotCurrentStat", txtRobotStat);
            MyConfig.Cfg.Model.RobotStat = "Free";

            MyConfig.Cfg.Model.IOQBStat = "";
            MyConfig.Cfg.Model.IOQAStat = "";
            MyConfig.Cfg.Model.MandrelStat = "Empty";
            MyConfig.Cfg.Model.BackerStat = "Empty";

            BindGui("CurrentSetBank1", tboxCurrentSetBank1);
            BindGui("CurrentSetBank2", tboxCurrentSetBank2);
            BindGui("BatchNo", tboxBatchNo);
            BindGui("NextSet", tboxNextSet);

            BindGui("BladderCell_1", tboxPSStat_1);
            BindGui("BladderCell_2", tboxPSStat_2);
            BindGui("BladderCell_3", tboxPSStat_3);
            BindGui("BladderCell_4", tboxPSStat_4);
            BindGui("BladderCell_5", tboxPSStat_5);
            BindGui("BladderCell_6", tboxPSStat_6);
            BindGui("BladderCell_7", tboxPSStat_7);
            BindGui("BladderCell_8", tboxPSStat_8);
            BindGui("BladderCell_9", tboxPSStat_9);
            BindGui("BladderCell_10", tboxPSStat_10);
            BindGui("BladderCell_11", tboxPSStat_11);
            BindGui("BladderCell_12", tboxPSStat_12);

            BindGui("PSStat_1", tboxBladderCell_1);
            BindGui("PSStat_2", tboxBladderCell_2);
            BindGui("PSStat_3", tboxBladderCell_3);
            BindGui("PSStat_4", tboxBladderCell_4);
            BindGui("PSStat_5", tboxBladderCell_5);
            BindGui("PSStat_6", tboxBladderCell_6);
            BindGui("PSStat_7", tboxBladderCell_7);
            BindGui("PSStat_8", tboxBladderCell_8);
            BindGui("PSStat_9", tboxBladderCell_9);
            BindGui("PSStat_10", tboxBladderCell_10);
            BindGui("PSStat_11", tboxBladderCell_11);
            BindGui("PSStat_12", tboxBladderCell_12);


            #endregion

   

            MyConfig.Cfg.UpdateUI = UpdateIU;
            MyConfig.Cfg.ShowCustomDialog = ShowCustomDialog;

            MyConfig.Cfg.Model.PropertyChanged += model_PropertyChanged;
            DataGridTextColumn c1 = new DataGridTextColumn();
            c1.Header = "Date";
            c1.FontSize = 15;
            c1.Binding = new Binding("Date");
            c1.Width = 150;
            dgv_MachineLog.Columns.Add(c1);
            DataGridTextColumn c2 = new DataGridTextColumn();
            c2.Header = "Message";
            c2.FontSize = 15;
            c2.Width = 610;
            c2.Binding = new Binding("Message");
            dgv_MachineLog.Columns.Add(c2);

            
            DataGridTextColumn c3 = new DataGridTextColumn();
            c3.Header = "Date";
            c3.FontSize = 12;
            c3.Binding = new Binding("Date");
            c3.Width = 150;
            dgv_ErrorLog.Columns.Add(c3);
            DataGridTextColumn c4 = new DataGridTextColumn();
            c4.Header = "Message";
            c4.FontSize = 12;
            c4.Width = 610;
            c4.Binding = new Binding("Message");
            dgv_ErrorLog.Columns.Add(c4);

            btnStart.IsEnabled = false;
            btnRequestEndLot.IsEnabled = false;
            btnClear.IsEnabled = false;

            //disable sensors for simulation mode
            if (MyConfig.Cfg.SimulationMode)
            {
                MyConfig.Cfg.Model.PowerSupplyEnable = false;
                MyConfig.Cfg.Model.CellSensorCheck = false;
                MyConfig.Cfg.Model.IOASensorEnable = false;
                MyConfig.Cfg.Model.IOBSensorEnable = false;
                main.hw.inputs["X3_14_ROBOT_Z_AXIS_UP"].CurrentValue = 1;
                main.hw.inputs["X4_12_I_O_OUEUE_(A)_REV_POSITION"].CurrentValue = 1;
            }
               
            //init IO
            main.hw.outputs["Y4_00_IWAKI_(_CARBON_TREAT_)"].OFF();

            //temp
            //MyConfig.Cfg.Model.BypassIOTankSensor = true;
        }

        string tempStatus;
        private void TxtTemp_TextChanged(object sender, TextChangedEventArgs e)
        {
          
            TextBox txt = sender as TextBox;

            if (txt.Text.Contains("'C"))
            {
                var txat = txt.Text.Split(' ');
                txt.Text = txat[0];
            }

            var temperature = Convert.ToDouble(txt.Text);
            if (temperature == 0.0)
            {
                txt.Background = Brushes.Red;
            }
            else if (temperature > 56.0)
            {
                if (tempStatus != "H")
                {
                    MyConfig.Cfg.Model.ErrorCode = "Alert,bath temperature is too high";
                    txt.Background = Brushes.Red;
                    tempStatus = "H";
                }
            }
            else if (temperature >= 52.0 && temperature <= 56.0)
            {
                txt.Background = Brushes.Lime;
                tempStatus = "N";
            }
            else if (temperature < 52.0)
            {
                if (tempStatus != "L")
                {
                    MyConfig.Cfg.Model.ErrorCode = "Alert,bath temperature is too low";
                    txt.Background = Brushes.Yellow;
                    //  txt.Background = new SolidColorBrush(Color.FromArgb(0xFF, 0x30, 0x63, 0XEC));
                    tempStatus = "L";
                }
            }
            //else if (temperature <= 53.0)
            //{
            //    txt.Background = Brushes.Yellow;
            //    tempStatus = "N";
            //}
            else
            {
                txt.Background = Brushes.Red;
            }
        }

        private void TxtAlarm_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox txt = sender as TextBox;
            if (txt.Text.ToString() != txt.Tag.ToString())
            {
                txt.Background = Brushes.Red;
            }
            else
            {
                txt.Background = Brushes.Lime;
            }
        }

        private void TxtError_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox txt = sender as TextBox;
            if (txt.Text.ToString() == txt.Tag.ToString())
            {
                txt.Background = Brushes.Red;
            }
            else
            {
                txt.Background = Brushes.Lime;
            }
        }

        private void TxtWarning_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox txt = sender as TextBox;

            if (txt.Text.ToString() == "ROBOT ERROR")
            {
                btnStopRobot.Content = "STOP - RESUME";
            }

            if (txt.Text.ToString() == "ON")
            {
                txt.Background = Brushes.Lime;
            }
            else if (txt.Text.ToString() == "LOCK")
            {
                txt.Background = Brushes.Lime;
            }
            else
            {
                txt.Background = Brushes.Yellow;
            }

        }

        private void txtBathLevel_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox txt = sender as TextBox;
                
            if (txt.Text.ToString() == "FILLING")
            {
                txt.Background = Brushes.Aqua;
            }
            else if (txt.Text.ToString() == "NORMAL")
            {
                txt.Background = Brushes.Lime;
            }
            else if(txt.Text.ToString() == "VERY LOW")
            {
                txt.Background = Brushes.Red;
            }
            else if (txt.Text.ToString() == "LOW")
            {
                txt.Background = Brushes.Yellow;
            }
            else if(txt.Text.ToString() == "HIGH")
            {
                txt.Background = Brushes.Yellow;
            }
            else if(txt.Text.ToString() == "VERY HIGH")
            {
                txt.Background = Brushes.Red;
            }
         
        }

        private void tboxBladderCell_1_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void PowerSupplyStatTextChange(object sender, TextChangedEventArgs e)
        {
            TextBox txt = sender as TextBox;
            if (txt.Text == "PLATE")
            {
                txt.Background = Brushes.Lime;
            }
            else if (txt.Text == "DISABLE")
            {
                txt.Background = Brushes.LightGray;
            }
            else if(txt.Text == "ERROR")
            {
                txt.Background = Brushes.Red;
            }
            else if (txt.Text == "CANCEL")
            {
                txt.Background = Brushes.DodgerBlue;
            }
            else if (txt.Text == "IDLE")
            {
                txt.Background = Brushes.White;
            }
            else
            {
                txt.Background = Brushes.Yellow;
            }
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            string selectedrecepi = "";

            foreach (var item in MyConfig.Cfg.RecepiList.Cells)
            {
                var myCheckBox = (CheckBox)this.FindName("cboxEn_" + item.Cell);
                myCheckBox.IsChecked = item.Enabled;
            
                if (myCheckBox.IsChecked == false)
                {
                    UpdateIU("PSStat_" + item.Cell, "DISABLE", "");
                }
            }

            //UPDATE AMPMIN BASED ON SET
            var bank1 = MyConfig.Cfg.Model.CurrentSetBank1;
            var bank2 = MyConfig.Cfg.Model.CurrentSetBank2;

            List<double> ampval1 = new List<double>();
            List<double> ampval2 = new List<double>();

            if (bank1 == "A")
                ampval1 = MyConfig.Cfg.RecepiList.SetConfig[0].SetA;
            else
                ampval1 = MyConfig.Cfg.RecepiList.SetConfig[0].SetC;

            if (bank1 == "B")
                ampval2 = MyConfig.Cfg.RecepiList.SetConfig[0].SetB;
            else
                ampval2 = MyConfig.Cfg.RecepiList.SetConfig[0].SetD;

            for (int i = 0; i < ampval1.Count; i++)
            {
                var ampmibox = (TextBox)this.FindName("tboxAmpMin" + (i+1).ToString());
                ampmibox.Text = ampval1[i].ToString();
            }

            for (int i = 0; i < ampval2.Count; i++)
            {
                var ampmibox = (TextBox)this.FindName("tboxAmpMin" + (i + 7).ToString());
                ampmibox.Text = ampval2[i].ToString();
            }

            cboxRecepiList.Items.Clear();
            //set recepi
            foreach (var item in MyConfig.Cfg.RecepiList.Product)
            {
                cboxRecepiList.Items.Add(item.Name);

                if (item.Selected == true)
                {
                    selectedrecepi = item.Name;
                    MyConfig.Cfg.PlatingCurrent = item.Amp;
                    MyConfig.Cfg.SelectedRecepi = item.Name;
                }

            }
            cboxRecepiList.SelectedItem = selectedrecepi;


        }

        #region button func
        private void btnSetRecepi_Click(object sender, RoutedEventArgs e)
        {  //set recepi
            foreach (var item in MyConfig.Cfg.RecepiList.Product)
            {
                item.Selected = false;

            }
            var currentRecepi = cboxRecepiList.SelectedItem;
            var idx = MyConfig.Cfg.RecepiList.Product.FindIndex(x => x.Name == currentRecepi);
            MyConfig.Cfg.RecepiList.Product[idx].Selected = true;
            MyConfig.Cfg.PlatingCurrent = MyConfig.Cfg.RecepiList.Product[idx].Amp;
            MyConfig.Cfg.SaveRecepi();
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now,$"RecepiChanged: {currentRecepi}");
            MessageBox.Show("Recepi Changed");
        }
        private void btnInitialize_Click(object sender, RoutedEventArgs e)
        {
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Initialize Pressed");
            main.mainFlow.InitializePressed = true;
        }
        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Reset Pressed");
            main.mainFlow.SetButton("Reset");
            MyConfig.Cfg.Model.ResetAlarm();
        }
        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
            main.mainFlow.SetButton("Stop");
        }
        private void btnStopRobot_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            if (btn.Content.ToString().Contains("STOP-ROBOT"))
            {
                //STOP ROBOT
                MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Robot Stop Pressed");

                main.mainFlow.hw.motors["Motor1X"].Stop();
                //robot already stop when this button clicked.. but flow still continue
                main.mainFlow.hw.outputs["Y5_03_ROBOT_PAUSE"].ON();
                MyConfig.Cfg.Model.RobotStat = "STOP";
                MyConfig.Cfg.RobotRequestStop = true;
                btn.Content = "STOP-RESUME";
                btn.IsEnabled = false;
            }
            else
            {
                //RESUME ROBOT
                MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Robot Resume Pressed");
                MyConfig.Cfg.Model.RobotStat = MyConfig.Var.RobotResume;
                //main.mainFlow.hw.outputs["Y5_07_ROBOT_START"].ON();
                btn.Content = "STOP-ROBOT";


            }

        }
        private void btnAutoMode_Click(object sender, RoutedEventArgs e)
        {
         
            if (MyConfig.Cfg.SimulationMode)
            {

                lblMode.Content = "AUTO";
                btnStart.IsEnabled = true;

                main.DisableUI(true);
                return;
            }

            if (btnAutoMode.Background != Brushes.LightGray)
            {

                if ((main.mainFlow.hw.outputs["Y3_00_MAIN_TANK_PUMP_(RUN)"].Read() == 0 ||
                    main.mainFlow.hw.outputs["Y3_01_HEATER_ENABLE"].Read() == 0 ||
                    main.mainFlow.hw.outputs["Y3_02_POWER_SUPPLIES_ENABLE_CELL_1_6"].Read() == 0 ||
                    main.mainFlow.hw.outputs["Y3_03_POWER_SUPPLIES_ENABLE_CELL_7_12"].Read() == 0 ||
                    main.mainFlow.hw.outputs["Y2_13_MAIN_WATER"].Read() == 0
                    || MyConfig.Cfg.Model.FTAgitStat == "OFF"
                    || MyConfig.Cfg.Model.BKAgitStat == "OFF" || CheckTemperature()) && MyConfig.Cfg.Model.TemperatureCheck == true)
                {
                    MessageBox.Show("Main Pump, Main Water, Heater, Power Supply, Front and Back Agitator \nrequired to turn before go to Auto Mode", "Can't go to auto mode", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                else
                {
                    //btnAutoMode.Background = Brushes.LightGray;
                    //btnManual.Background = Brushes.Orange;
                    lblMode.Content = "AUTO";
                    btnStart.IsEnabled = true;

                    main.DisableUI(true);
                }


            }
        }

        private void btnRequestEndLot_Click(object sender, RoutedEventArgs e)
        {
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "EndLot Pressed");
            MyConfig.Cfg.EndLotButtonPressed = true;
            MyConfig.Cfg.RequestEndLot = true;
            btnRequestEndLot.IsEnabled = false;
            btnStart.IsEnabled = false;
        }

        private void btnResetBatch_Click(object sender, RoutedEventArgs e)
        {
            MyConfig.Cfg.Model.BatchNo = "0";
            MyConfig.Cfg.SaveINI("SavedValue", "BatchNumber","0");
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Batch Reset");
        }


        private void btnResetSet_Click(object sender, RoutedEventArgs e)
        {
            MyConfig.Cfg.CurrentSet = "A";
            MyConfig.Cfg.Model.NextSet = "A";
            MyConfig.Cfg.Model.CurrentSetBank1 = "A";
            MyConfig.Cfg.Model.CurrentSetBank2 = "B";

            MyConfig.Cfg.SaveINI("SavedValue", "CurrentSet", MyConfig.Cfg.CurrentSet);
            MyConfig.Cfg.SaveINI("SavedValue", "NextSet", MyConfig.Cfg.Model.NextSet);
            MyConfig.Cfg.SaveINI("SavedValue", "Bank1Set", MyConfig.Cfg.Model.CurrentSetBank1);
            MyConfig.Cfg.SaveINI("SavedValue", "Bank2Set", MyConfig.Cfg.Model.CurrentSetBank2);
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Set Reset");
        }

        private void btnManual_Click(object sender, RoutedEventArgs e)
        {
            main.DisableUI(false);
            btnRobotActHome.IsEnabled = true;
            lblMode.Content = "MANUAL";
        }

        private void btnEnterLot_Click(object sender, RoutedEventArgs e)
        {
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "AmpMinList Pressed");
            MultiLot multiLot = new MultiLot(main);
            multiLot.ShowDialog();
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Robot Recover Pressed");
            MyConfig.Cfg.Model.StatusBar = "RECOVER STARTED";

            Task.Run(() =>
            {
                bool stat = main.mainFlow.ClearUnit(clearunit);

                if (!stat)
                {
                    MyConfig.Cfg.UpdateUI("btnClear", "Enable", "");
                    return;
                }
                
                if (clearunit == 1)
                    clearunit = 2;
                else
                    clearunit = 1;

                MyConfig.Cfg.SoftEmoRecover = false;
                
                this.Dispatcher.Invoke(new Action(() =>
                {
                    if (btnClear.Content.ToString() == "RECOVER BANK1")
                    {
                        MyConfig.Cfg.Model.StatusBar = "RECOVER COMPLETE BANK1";
                        btnClear.Content = "RECOVER BANK2";
                        btnClear.IsEnabled = true;
                    }
                    else
                    {
                        MyConfig.Cfg.Model.StatusBar = "RECOVER COMPLETE BANK2";
                        btnClear.Content = "RECOVER BANK1";
                        btnClear.IsEnabled = true;
                    }
                      
                }));
                

            });
            btnClear.IsEnabled = false;

        }

        private void btnMoveIOQB_Click(object sender, RoutedEventArgs e)
        {
            if (btnMoveIOQBClicked)
                return;

            btnMoveIOQBClicked = true;
            var stat = MyConfig.Cfg.Model.tankModule.IOQBMove("IOBFront");
            if (stat == 0)
            {
                Thread.Sleep(500);
                stat = MyConfig.Cfg.Model.tankModule.IOQBMove("IOBSensor");
                if (stat == 1)
                {
                    if (this.main.mainFlow.CheckIOStat("IOBSensor") == 1)
                    {
                        //MyConfig.Cfg.Model.tankModule.MoveIOAB("IOBBack"); //CK20Mar18
                        MyConfig.Cfg.Model.tankModule.IOQBMove("IOBBack");
                        MyConfig.Cfg.Model.ErrorCode = "Alert,Please remove all fixture from IOQB tank";
                        MyConfig.Cfg.IOQBStatus = false;
                        btnMoveIOQBClicked = false;
                        return;
                    }
                   
                }
                MyConfig.Cfg.IOQBStatus = true;
                MyConfig.Cfg.Model.AlarmStat = "OFF";
                main.mainFlow.SetButton("Reset");
                MyConfig.Cfg.Model.ResetAlarm();
            }
            btnMoveIOQBClicked = false;
        }

        #endregion


        private void model_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if ((e.PropertyName == "MessageLogsTuple") && !MyConfig.Cfg.Model.MessageLogsTuple.Item2.Contains("Alert"))
            {
                this.Dispatcher.Invoke(new Action(() =>
                {
                    dgv_MachineLog.Items.Add(new Model.Item() { Date = MyConfig.Cfg.Model.MessageLogsTuple.Item1.ToString("d/M/yyyy HH:mm:ss"), Message = MyConfig.Cfg.Model.MessageLogsTuple.Item2 });
                    if (MyConfig.Cfg.Model.MessageLogsTuple.Item2.Contains("Alert"))
                    {
                        main.mainFlow.AlarmStat = true;
                        lbox_LogMessage.Items.Add(MyConfig.Cfg.Model.MessageLogsTuple.Item2);

                    }
                    else if (MyConfig.Cfg.Model.MessageLogsTuple.Item2.Contains("Info"))
                    {
                        lbox_LogMessage.Items.Add(MyConfig.Cfg.Model.MessageLogsTuple.Item2);

                    }
                }));

            }
            else if ((e.PropertyName == "MessageLogsTuple") && MyConfig.Cfg.Model.MessageLogsTuple.Item2.Contains("Alert"))
            {
                this.Dispatcher.Invoke(new Action(() =>
                {
                    var msg = MyConfig.Cfg.Model.MessageLogsTuple.Item2.Split(',');
                    dgv_ErrorLog.Items.Add(new Model.Item() { Date = MyConfig.Cfg.Model.MessageLogsTuple.Item1.ToString("d/M/yyyy HH:mm:ss"), Message = MyConfig.Cfg.Model.MessageLogsTuple.Item2 });
                    lbox_LogMessage.Items.Add(MyConfig.Cfg.Model.MessageLogsTuple.Item2);
                    // System.Windows.MessageBox.Show(msg[1], "ERROR", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK, System.Windows.MessageBoxOptions.DefaultDesktopOnly);
                }));
            }
        }

        void dataGrid_LoadingRow(object sender, System.Windows.Controls.DataGridRowEventArgs e)
        {
            //dgv_MachineLog.ScrollIntoView(e.Row.Item);
            var item = e.Row.Item as Model.Item;

            if (item.Message.Contains("Alert"))
            {
                e.Row.Background = Brushes.Red;
                e.Row.Foreground = Brushes.White;
          
            }
            else
            {
                e.Row.Background = Brushes.White;
                e.Row.Foreground = Brushes.Black;
            }

        }

        private void dgv_ErrorLog_LoadingRow(object sender, DataGridRowEventArgs e)
        {
           // dgv_ErrorLog.ScrollIntoView(e.Row.Item);
            var item = e.Row.Item as Model.Item;

            if (item.Message.Contains("Alert"))
            {
                e.Row.Background = Brushes.Red;
                e.Row.Foreground = Brushes.White;

            }
            else
            {
                e.Row.Background = Brushes.White;
                e.Row.Foreground = Brushes.Black;
            }
        }

        private void BindGui(string BindingParam, dynamic UI)
        {
            Binding myBinding = new Binding(BindingParam)
            {
                Source = MyConfig.Cfg.Model
            };

            var typ = UI.GetType();
            switch (typ.Name)
            {
                case "TextBox":
                    BindingOperations.SetBinding(UI, TextBox.TextProperty, myBinding);
                    break;

                case "CheckBox":
                    BindingOperations.SetBinding(UI, CheckBox.IsCheckedProperty, myBinding);
                    break;

                case "Label":
                    BindingOperations.SetBinding(UI, Label.ContentProperty, myBinding);
                    break;
                default:
                    break;
            }

        }


        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Start Pressed");
            for(int i = 1; i <= 12; i++)
            {
                var tbox = (TextBox)this.FindName($"tboxBladderCell_{i}");
                if(tbox.Text == "MANUAL PLATE")
                {
                    MyConfig.Cfg.Model.ErrorCode = "Alert,Cannot Start Auto Mode.Please Cancel All Manual plate";
                    return;
                }
            }
          
            var bank1stat = cboxEn_1.IsEnabled;
            var bank2stat = cboxEn_7.IsEnabled;

            int countcell = 6;
            
            if(MyConfig.Cfg.Model.NextSet == "A" || MyConfig.Cfg.Model.NextSet == "C")
            {
                if (bank1stat == false)
                {
                    MyConfig.Cfg.Model.ErrorCode = "Alert,Cannot Start Selected Set " + MyConfig.Cfg.Model.NextSet;
                    return;
                }
                   
                for (int i = 1; i <= 6; i++)
                {
                    bool stat = GetEnableCell($"cboxEn_{i}");
                    if(stat == false)
                    {
                        countcell--;
                    }
                  
                }
            }
            else if (MyConfig.Cfg.Model.NextSet == "B" || MyConfig.Cfg.Model.NextSet == "D")
            {
                if (bank2stat == false)
                {
                    MyConfig.Cfg.Model.ErrorCode = "Alert,Cannot Start Selected Set " + MyConfig.Cfg.Model.NextSet;
                    return;
                }

                for (int i = 1; i <= 6; i++)
                {
                    bool stat = GetEnableCell($"cboxEn_{i+6}");
                    if (stat == false)
                    {
                        countcell--;
                    }
                }
            }

            if (countcell == 0)
            {
                MyConfig.Cfg.Model.ErrorCode = "Alert, No Cell selected for Current Set";
                return;
            }

            btnClear.IsEnabled = false;
            btnManual.IsEnabled = false;
            EnterLot enterLot = new EnterLot(main);
            btnRequestEndLot.IsEnabled = true;
            btnStart.IsEnabled = false;
            enterLot.ShowDialog();
            
            if (enterLot.stat == "success")
            {
                var stat = MyConfig.Cfg.RecepiList.Cells.Count(x => x.Enabled == true);
                if (stat > 0)
                {
                    MyConfig.Cfg.RequestNextSet = false;
                    main.mainFlow.SetButton("Start", "StartLot");
                }
                else
                {
                    MyConfig.Cfg.Model.ErrorCode = "Alert,Error: No Cells Selected";
                    btnStart.IsEnabled = true;
                }
            }
            else
            {
                btnStart.IsEnabled = true;
                btnManual.IsEnabled = true;
            }
        }

        private bool GetEnableCell(string UiName)
        {
            bool val = false;

            var propInfog = MyConfig.Cfg.Model.GetType().GetProperty(UiName);
            if (propInfog != null)
            {

                var _val = propInfog.GetValue(MyConfig.Cfg.Model);
                val = bool.Parse(_val.ToString());
            }

            return val;
        }

        private void cboxEn(object sender, RoutedEventArgs e)
        {
            var _sender = sender as CheckBox;
            var CellName = _sender.Name.Split('_');

            if(_sender.IsChecked == false)
            {
                UpdateIU($"PSStat_{CellName[1]}", "DISABLE", "");
            }
            else
            {
                UpdateIU($"PSStat_{CellName[1]}", "IDLE", "");
            }
          
            var idx = MyConfig.Cfg.RecepiList.Cells.FindIndex(x => x.Cell == CellName[1]);
            MyConfig.Cfg.RecepiList.Cells[idx].Enabled = _sender.IsChecked.Value;
            MyConfig.Cfg.SaveRecepi();
        }

        public void UpdateIU(string UiName, string option, string option2)
        {
            //lock (_object)
            //{

                switch (option)
                {
                    case "Disable":

                        if (option2 == "CheckBox")
                        {
                            this.Dispatcher.Invoke(new Action(() =>
                            {
                                CheckBox mybtn = (CheckBox)this.FindName(UiName);
                                mybtn.IsEnabled = false;
                            }));
                        }
                        else
                        {
                            this.Dispatcher.Invoke(new Action(() =>
                            {
                                Button mybtn = (Button)this.FindName(UiName);
                                mybtn.IsEnabled = false;
                            }));
                        }
                        break;

                    case "Enable":
                        if (option2 == "CheckBox")
                        {
                            this.Dispatcher.Invoke(new Action(() =>
                            {
                                CheckBox mybtn = (CheckBox)this.FindName(UiName);
                                mybtn.IsEnabled = true;
                            }));
                        }
                        else
                        {
                            this.Dispatcher.Invoke(new Action(() =>
                            {
                                Button mybtn = (Button)this.FindName(UiName);
                                mybtn.IsEnabled = true;
                            }));
                        }

                        break;

                    case "GetStatus":

                            this.Dispatcher.Invoke(new Action(() =>
                            {
                                Button mybtn = (Button)this.FindName(UiName);
                                if(mybtn.IsEnabled == true)
                                {
                                    MyConfig.Cfg.UIValue = "True";
                                }
                                else
                                {
                                    MyConfig.Cfg.UIValue = "False";
                                }

                            }));

                    break;

                case "ChangeColor":
                        this.Dispatcher.Invoke(new Action(() =>
                        {
                            var color = (Color)ColorConverter.ConvertFromString(option2);
                            Label mylbl = (Label)this.FindName(UiName);
                            mylbl.Background = new SolidColorBrush(color);
                        }));

                        break;

                    case "SetValueBtn":

                        this.Dispatcher.Invoke(new Action(() =>
                        {
                            Button mylbl = (Button)this.FindName(UiName);
                            mylbl.Content = option2;
                        }));

                        break;

                    case "SetValue":

                        this.Dispatcher.Invoke(new Action(() =>
                        {
                            TextBox mylbl = (TextBox)this.FindName(UiName);
                            mylbl.Text = option2;
                        }));

                        break;

             
                case "ClearMessageLogs":
                        this.Dispatcher.Invoke(new Action(() =>
                        {
                            dgv_MachineLog.Items.Clear();
                   
                        }));
                        break;

                    case "ClearErrorLogs":
                    this.Dispatcher.Invoke(new Action(() =>
                    {
                        dgv_ErrorLog.Items.Clear();

                    }));
                    break;


                    case "GetValue":

                        var propInfog = MyConfig.Cfg.Model.GetType().GetProperty(UiName);
                        if (propInfog != null)
                        {

                            var val = propInfog.GetValue(MyConfig.Cfg.Model);
                            MyConfig.Cfg.UIValue = val.ToString();
                        }

                        break;

                    default:
                        var propInfo = MyConfig.Cfg.Model.GetType().GetProperty(UiName);
                        if (propInfo != null)
                        {
                            propInfo.SetValue(MyConfig.Cfg.Model, option, null);
                        }


                        break;
                }
        }

        private bool CheckTemperature()
        {

            var val = double.Parse(txtTemp.Text);

            if (val < 52 || val > 56)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private async void ShowAlert(string Message)
        {
            //AlertWindow alert = new AlertWindow();
            //alert.Show();
        }

        private void btnend_Click(object sender, RoutedEventArgs e)
        {
            MyConfig.Cfg.EndProcess = true;
            MyConfig.Cfg.RequestEndLot = true;
            //main.mainFlow.cellModule.StopTimer("1");
        }

        private void btnemotest(object sender, RoutedEventArgs e)
        {
            main.mainFlow.hw.inputs["X5_03_E_STOP_LOOP_OK"].CurrentValue = 0;
            main.mainFlow.EmoFunc();
        }

        private void btnend_Copy1_Click(object sender, RoutedEventArgs e)
        {
            main.mainFlow.hw.inputs["X5_03_E_STOP_LOOP_OK"].CurrentValue = 1;
        }

        private void btnrobotemo_Click(object sender, RoutedEventArgs e)
        {
            main.mainFlow.hw.inputs["X6_15_ROBOT_EMERGENCY_STOP"].CurrentValue = 1;
        }

        private void btnrobotemo_Copy_Click(object sender, RoutedEventArgs e)
        {
            main.mainFlow.hw.inputs["X6_15_ROBOT_EMERGENCY_STOP"].CurrentValue = 0;
        }

        private void btnRotateSet_Click(object sender, RoutedEventArgs e)
        {
            var set = MyConfig.Cfg.Model.NextSet;
            int banknumber = 1;
            List<double> ampval1 = new List<double>();

            switch (set)
            {
                case "A":
                    MyConfig.Cfg.Model.NextSet = "B";
                    MyConfig.Cfg.CurrentSet = "A";
                    MyConfig.Cfg.Model.CurrentSetBank1 = "C";
                    MyConfig.Cfg.Model.CurrentSetBank2 = "D";
                    banknumber = 2;
                    ampval1 = MyConfig.Cfg.RecepiList.SetConfig[0].SetB;
                    break;

                case "B":
                    MyConfig.Cfg.Model.NextSet = "C";
                    MyConfig.Cfg.CurrentSet = "B";
                    MyConfig.Cfg.Model.CurrentSetBank1 = "A";
                    MyConfig.Cfg.Model.CurrentSetBank2 = "D";
                    banknumber = 1;
                    ampval1 = MyConfig.Cfg.RecepiList.SetConfig[0].SetC;
                    break;

                case "C":
                    MyConfig.Cfg.Model.NextSet = "D";
                    MyConfig.Cfg.CurrentSet = "C";
                    MyConfig.Cfg.Model.CurrentSetBank1 = "A";
                    MyConfig.Cfg.Model.CurrentSetBank2 = "B";
                    banknumber = 2;
                    ampval1 = MyConfig.Cfg.RecepiList.SetConfig[0].SetD;
                    break;

                case "D":

                    MyConfig.Cfg.Model.NextSet = "A";
                    MyConfig.Cfg.CurrentSet = "D";
                    MyConfig.Cfg.Model.CurrentSetBank1 = "C";
                    MyConfig.Cfg.Model.CurrentSetBank2 = "B";
                    banknumber = 1;
                    ampval1 = MyConfig.Cfg.RecepiList.SetConfig[0].SetA;
                    break;

                default:
                    break;
            }

            if(main.mainFlow.currentState == MainFlow.SequenceState.IDLE || main.mainFlow.currentState == MainFlow.SequenceState.UNINITIALIZE)
            {
                if (banknumber == 1)
                {
                    for (int i = 0; i < ampval1.Count; i++)
                    {
                        var ampmibox = (TextBox)this.FindName("tboxAmpMin" + (i + 1).ToString());
                        ampmibox.Text = ampval1[i].ToString();
                    }
                }
                else
                {
                    for (int i = 0; i < ampval1.Count; i++)
                    {
                        var ampmibox = (TextBox)this.FindName("tboxAmpMin" + (i + 7).ToString());
                        ampmibox.Text = ampval1[i].ToString();
                    }
                }
            }

            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, $"Recepi Rotated: { MyConfig.Cfg.Model.NextSet }");
        }

        private void btnStop_Click_1(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;

            if(btn.Tag.ToString() == "STOP")
            {
                MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Robot Hard Stop Pressed");

                main.mainFlow.hw.motors["Motor1X"].Stop();
                main.mainFlow.hw.outputs["Y4_06_ROBOT_EMO_ON"].ON();
                MyConfig.Cfg.SoftEmoRecover = true;
                MyConfig.Cfg.Model.RobotStat = "EMO";
                MyConfig.Cfg.RobotRequestStop = true;
                MyConfig.Cfg.SoftEmo = true;
                MyConfig.Cfg.EMOStat = true;
                btn.Content = "START";
                MyConfig.Cfg.Model.ErrorCode = "Alert,Machine Stop";
                main.mainFlow.currentState = MainFlow.SequenceState.EMERGENCY;
                btn.Tag = "START";
                btnManual.IsEnabled = true;
                lblMode.Content = "MANUAL";

            }
            else
            {
                MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Start From Robot Hard Stop Pressed");

                main.mainFlow.hw.outputs["Y4_06_ROBOT_EMO_ON"].OFF();

                MyConfig.Cfg.Model.RobotStat = "Free";
                MyConfig.Cfg.RobotRequestStop = false;
                MyConfig.Cfg.EMOStat = false;
                MyConfig.Cfg.SoftEmo = false;
                btn.Content = "ROBOT \n HARD STOP";
                btn.Tag = "STOP";
            }

           
        }

        private void tboxPSStat_1_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox txt = sender as TextBox;
            if (txt.Text == "INFLATE")
            {
                txt.Background = Brushes.Lime;
            }
            else if (txt.Text == "RETRACT")
            {
                txt.Background = Brushes.White;
            }
         
        }

        private void tboxTotalAmpMin_TextChanged(object sender, TextChangedEventArgs e)
        {
            
            TextBox txt = sender as TextBox;
            txt.Text = MyConfig.Cfg.TotalAmpMin.ToString("##.###");
        }

        private void btnAbort_Click(object sender, RoutedEventArgs e)
        {
            var result = MessageBox.Show("Abor?", "Abort all test", MessageBoxButton.YesNo);
            if(result == MessageBoxResult.Yes)
                MyConfig.Cfg.Model.cellModule.SetTimerTo0();
        }

        private void btnRobotActHome_Click(object sender, RoutedEventArgs e)
        {
            MyConfig.Cfg.UpdateUI("btnRobotActHome", "Disable", "");

            if (btnRobotActHomeClicked)
                return;

            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Home Robot And Actuator Pressed");

            Task.Run(() =>
            {
                btnRobotActHomeClicked = true;
                main.hw.outputs["Y5_14_ROBOT_RESET"].ON();
                Thread.Sleep(300);
                main.hw.outputs["Y5_14_ROBOT_RESET"].OFF();

                MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Homing Robot");
                main.hw.outputs["Y5_13_ROBOT_HOME"].ON();
                Thread.Sleep(200);
                var stat = TimeOut(15, "X6_14_ROBOT_POSITION_RECEIVED", "Robot Home Error");
                main.hw.outputs["Y5_13_ROBOT_HOME"].OFF();
                if (stat==1)
                {
                    MyConfig.Cfg.Model.StatusBar = "Error Robot Home";
                    MyConfig.Cfg.UpdateUI("btnRobotActHome", "Enable", "");
                    return;
                }

                MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Homing Done");
                MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Homing Motor");

                if (main.hw.inputs["X3_14_ROBOT_Z_AXIS_UP"].Read() == 0)
                {

                    MyConfig.Cfg.RobotRequestStop = true;
                    MyConfig.Cfg.Model.RobotStat = "ERROR";

                    MyConfig.Cfg.Model.ErrorCode = "Alert,Actuator Can't Move Due To Z Axis Not In Top Position";
                }

                main.hw.motors["Motor1X"].Home(100);
                main.hw.motors["Motor1X"].WaitMotionDone(500);
                MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Homing Motor Done");
                MyConfig.Cfg.Model.StatusBar = "Homing Done";
                btnRobotActHomeClicked = false;
                MyConfig.Cfg.UpdateUI("btnRobotActHome", "Enable", "");
            });
          
        }

        private int TimeOut(int sec, string SensorName, string Message, int ioval = 1)
        {
            MyConfig.Cfg.ProcessState = Message;

            int stat = 0;
            int count = 0;

            if (MyConfig.Cfg.SimulationMode)
                return stat;

            var ss = main.mainFlow.hw.inputs[SensorName].Read();

            MyConfig.Cfg.ProcessState = $"Wait Sensor Timeout:{SensorName}";

            while (main.mainFlow.hw.inputs[SensorName].Read() != ioval)
            {
                Thread.Sleep(100);
                count++;
                if (count > sec)
                {
                    MyConfig.Cfg.Model.ErrorCode = "Alert," + Message;
                    stat = 1;
                    break;
                }

                //hw emo pressed
                if (MyConfig.Cfg.EMOStat == true)
                    break;

            }
            MyConfig.Cfg.ProcessState = $"Sensor Detected {SensorName}";
            return stat;

        }

        private void btnOpenRobotProgram_Click(object sender, RoutedEventArgs e)
        {
            MyConfig.Cfg.OpenRobotProgram();
        }

        private void btnGripperOn_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            string stat = btn.Tag.ToString();

            if (stat == "ON")
            {
                //main.hw.outputs["Y5_12_ROBOT_TANK_UNLOCK_READY"].OFF();
                //main.hw.outputs["Y5_11_ROBOT_TANK_LOCK_READY"].ON();
                MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Robot Gripper On Pressed");
                main.mainFlow.Robot.TriggerGripper(true);
                btn.Tag = "OFF";
                btnGripperOn.Content = "ROBOT \n GRIPPER OFF";

            }
            else
            {
                //main.hw.outputs["Y5_11_ROBOT_TANK_LOCK_READY"].OFF();
                //main.hw.outputs["Y5_12_ROBOT_TANK_UNLOCK_READY"].ON();
                MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Robot Gripper Off Pressed");
                main.mainFlow.Robot.TriggerGripper(false);
                btn.Tag = "ON";
                btnGripperOn.Content = "ROBOT \n GRIPPER ON";
            }
          
        }

        int q = 1;
        private void btntest_Click(object sender, RoutedEventArgs e)
        {
            if (q == 1)
                main.mainFlow.hw.inputs["X2_06_CLAMSHELL_PRESENT_CELL___7"].CurrentValue = 1;
            else
                main.mainFlow.hw.inputs["X2_06_CLAMSHELL_PRESENT_CELL___7"].CurrentValue = 0;

            if (q == 1)
                q = 0;
            else
                q = 1;
        }

        int q1 = 1;
        private void btntest_Copy1_Click(object sender, RoutedEventArgs e)
        {
            if (q1 == 1)
                main.mainFlow.hw.inputs["X2_07_CLAMSHELL_PRESENT_CELL___8"].CurrentValue = 1;
            else
                main.mainFlow.hw.inputs["X2_07_CLAMSHELL_PRESENT_CELL___8"].CurrentValue = 0;

            if (q1 == 1)
                q1 = 0;
            else
                q1 = 1;

        }

        int q2 = 1;
        private void btntest_Copy_Click(object sender, RoutedEventArgs e)
        {
            if (q2 == 1)
                main.mainFlow.hw.inputs["X2_08_CLAMSHELL_PRESENT_CELL___9"].CurrentValue = 1;
            else
                main.mainFlow.hw.inputs["X2_08_CLAMSHELL_PRESENT_CELL___9"].CurrentValue = 0;

            if (q1 == 1)
                q2 = 0;
            else
                q2 = 1;
        }

        private void btntest_Copy2_Click(object sender, RoutedEventArgs e)
        {
            main.mainFlow.hw.inputs["X2_09_CLAMSHELL_PRESENT_CELL___10"].CurrentValue = 1;
            main.mainFlow.hw.inputs["X2_10_CLAMSHELL_PRESENT_CELL___11"].CurrentValue = 1;
            main.mainFlow.hw.inputs["X2_11_CLAMSHELL_PRESENT_CELL___12"].CurrentValue = 1;
           
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            main.mainFlow.hw.inputs["X3_14_ROBOT_Z_AXIS_UP"].CurrentValue = 1;
        }

        private void btn_CloseCustomDialog_Click(object sender, RoutedEventArgs e)
        {
            lbox_LogMessage.Items.Clear();
            CustomMessage1.Visibility = Visibility.Hidden;
            CustomMessage.Visibility = Visibility.Hidden;
           
        }

        private void ShowCustomDialog(string type)
        {
            this.Dispatcher.Invoke(new Action(() =>
            {
                if (CustomMessage.Visibility != Visibility.Visible)
                {
                    CustomMessage1.Visibility = Visibility.Visible;
                    CustomMessage.Visibility = Visibility.Visible;

                }
                if (type == "Alert")
                {
                    var color = (Color)ColorConverter.ConvertFromString("#FFFF0202");
                    CustomMessage.Background = new SolidColorBrush(color);

                }
                else
                {
                    var color = (Color)ColorConverter.ConvertFromString("#FF090F49");
                    CustomMessage.Background = new SolidColorBrush(color);
                }
                lblMessageLogType.Content = type;
            }));

           
            
        }
    }
}

﻿using AutomationLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AutomatedNasal.Pages
{
    /// <summary>
    /// Interaction logic for EnterLot.xaml
    /// </summary>
    public partial class EnterLot : Window
    {
        MainWindow main;
        public string stat { get; set; } = "open";
        List<double> AmpMin { get; set; } = new List<double>() ;
        int bank = 1;

        public EnterLot(MainWindow main)
        {
            InitializeComponent();
            this.main = main;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            lblRecepi.Content = MyConfig.Cfg.SelectedRecepi;
            lblSetName.Content = MyConfig.Cfg.Model.NextSet;

            switch (MyConfig.Cfg.Model.NextSet)
            {
                case "A":
                    AmpMin = MyConfig.Cfg.RecepiList.SetConfig[0].SetA;
                    bank = 1;
                    break;
                case "B":
                    AmpMin = MyConfig.Cfg.RecepiList.SetConfig[0].SetB;
                    bank = 2;
                    break;
                case "C":
                    AmpMin = MyConfig.Cfg.RecepiList.SetConfig[0].SetC;
                    bank = 1;
                    break;
                case "D":
                    AmpMin = MyConfig.Cfg.RecepiList.SetConfig[0].SetD;
                    bank = 2;
                    break;
                default:
                    break;
            }

            tboxAmpMin1.Text = AmpMin[0].ToString();
            tboxAmpMin2.Text = AmpMin[1].ToString();
            tboxAmpMin3.Text = AmpMin[2].ToString();
            tboxAmpMin4.Text = AmpMin[3].ToString();
            tboxAmpMin5.Text = AmpMin[4].ToString();
            tboxAmpMin6.Text = AmpMin[5].ToString();

        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            stat = "close";
            this.Close();
        }

        private void btnAccept_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AmpMin[0] = double.Parse(tboxAmpMin1.Text);
                AmpMin[1] = double.Parse(tboxAmpMin2.Text);
                AmpMin[2] = double.Parse(tboxAmpMin3.Text);
                AmpMin[3] = double.Parse(tboxAmpMin4.Text);
                AmpMin[4] = double.Parse(tboxAmpMin5.Text);
                AmpMin[5] = double.Parse(tboxAmpMin6.Text);

                if(bank == 1)
                {
                    for (int i = 0; i <= 5; i++)
                    {
                        var val = main.mainFlow.cellModule.CalculateAmpMin(AmpMin[i]);
                        MyConfig.Cfg.RecepiList.Cells[i].Timer = val.TotalSeconds;
                        MyConfig.Cfg.RecepiList.Cells[i].AmpMin = AmpMin[i];

                        main.mainFlow.cellModule.ChangeTimer((i + 1).ToString(), (int)val.TotalSeconds);

                        MyConfig.Cfg.UpdateUI($"tboxAmpMin{i + 1}", "SetValue", AmpMin[i].ToString());


                    }
                }

                if (bank == 2)
                {
                    for (int i = 6; i <= 11; i++)
                    {
                        var val = main.mainFlow.cellModule.CalculateAmpMin(AmpMin[i-6]);
                        MyConfig.Cfg.RecepiList.Cells[i-6].Timer = val.TotalSeconds;
                        MyConfig.Cfg.RecepiList.Cells[i-6].AmpMin = AmpMin[i-6];

                        main.mainFlow.cellModule.ChangeTimer((i + 1).ToString(), (int)val.TotalSeconds);

                        MyConfig.Cfg.UpdateUI($"tboxAmpMin{i + 1}", "SetValue", AmpMin[i-6].ToString());
                    }
                }

                MyConfig.Cfg.SaveRecepi();
                stat = "success";

                for (int i = 1; i <= 6; i++)
                {
                    var tbox = GetTextBox($"tboxAmpMin{i}");
                    var value = tbox.Text;
                    MyConfig.Cfg.Model.SetAmpMinLog("Info", $"Autorun Mode AmpMin For Set {lblSetName.Content} Cell {i}-Value:" + value);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR");
                return;
            }   
            
            if (stat == "success")
                this.Close();
        }


        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex(@"[^0-9\-.]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void tboxAmpMin1_KeyDown(object sender, KeyEventArgs e)
        {
            var SenderTbox = sender as TextBox;
            var Name = SenderTbox.Name.Substring(10,1);
            var Name2 = SenderTbox.Name.Substring(11);

            if (e.Key == Key.Return)
            {
                if (Name2 == "")
                    return;

                var num = int.Parse(Name2);
                num = num + 1;

                if (num > 6)
                {
                    
                    num = 1;
                }

                var tbox = GetTextBox($"tboxAmpMin{Name}{num}");
                tbox.Focus();
                tbox.SelectAll();
            }
            else
            {
               
            }
        }

        private TextBox GetTextBox(string TboxName)
        {
            TextBox textBox;

            textBox = (TextBox)(this.FindName(TboxName));

            return textBox;
        }


    }
}

﻿using AutomationLib;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AutomatedNasal.Pages
{
    /// <summary>
    /// Interaction logic for AboutPage.xaml
    /// </summary>
    public partial class AboutPage : Page
    {
        public AboutPage()
        {
            InitializeComponent();
            LoadChangeLog();
            gboxChangeLogConfig.Visibility = Visibility.Hidden;

        }

        private void LoadChangeLog()
        {
            DataTable dtUsers = new DataTable();
            MyConfig.Cfg.Sql.Query("SELECT * FROM ChangeLog", ref dtUsers);

            dgUsers.DataContext = dtUsers.DefaultView;
            var UpdateDate = System.IO.File.GetLastWriteTime(Assembly.GetExecutingAssembly().Location);

            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            string version = fvi.FileVersion;
            tboxName.Content = fvi.ProductName;
            tboxVersion.Content = fvi.FileVersion;
            tboxDate.Content = UpdateDate;


        }

        private void btnSaveChangeLog_Click(object sender, RoutedEventArgs e)
        {
            var changes = tboxChanges.Text;
            var version = tboxChangeVersion.Text;
            var type = tboxChangesType.Text;
            var date = DateTime.Now.ToString();
            MyConfig.Cfg.Sql.Query($"Insert into ChangeLog (Date,Changes,Type,Version) " +
                $"values ('{date}','{changes}','{type}','{version}')");

            MessageBox.Show("Add successful", "ChangeLog", MessageBoxButton.OK, MessageBoxImage.Information);
            LoadChangeLog();
        }

        private void btnRemoveChangeLog_Click(object sender, RoutedEventArgs e)
        {
            DataRowView dataRow = (DataRowView)dgUsers.SelectedItem;
            if (dataRow == null)
                return;

            string id = dataRow.Row.ItemArray[0].ToString();
            MyConfig.Cfg.Sql.Query("DELETE FROM ChangeLog WHERE id = '" + id + "';");
            MessageBox.Show("Delete successful", "ChangeLog", MessageBoxButton.OK, MessageBoxImage.Information);

            LoadChangeLog();

        }

        private void btnUpdateChangeLog_Click(object sender, RoutedEventArgs e)
        {
            DataRowView dataRow = (DataRowView)dgUsers.SelectedItem;
            if (dataRow == null)
                return;

            string id = dataRow.Row.ItemArray[0].ToString();
            string Changes = dataRow.Row.ItemArray[2].ToString();
            string Type = dataRow.Row.ItemArray[3].ToString();
            string Version = dataRow.Row.ItemArray[4].ToString();

            MyConfig.Cfg.Sql.Query($"Update ChangeLog set Changes='{Changes}',Type='{Type}',Version='{Version}' WHERE id = '" + id + "';");
            MessageBox.Show("Update successful", "ChangeLog", MessageBoxButton.OK, MessageBoxImage.Information);

            LoadChangeLog();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            if (MyConfig.Cfg.UserType == "SOPHIC")
            {
                dgUsers.IsReadOnly = false;
                gboxChangeLogConfig.Visibility = Visibility.Visible;
            }
            else
            {
                gboxChangeLogConfig.Visibility = Visibility.Hidden;
                dgUsers.IsReadOnly = true;
            }
        }
    }
}

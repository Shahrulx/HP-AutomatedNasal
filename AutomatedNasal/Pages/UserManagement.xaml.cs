﻿using AutomationLib;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AutomatedNasal.Pages
{
    /// <summary>
    /// Interaction logic for UserManagement.xaml
    /// </summary>
    public partial class UserManagement : Page
    {
        public UserManagement()
        {
            InitializeComponent();
            userLoadTable();
        }

        private void btnNew_Click(object sender, RoutedEventArgs e)
        {
            userNew();
        }

        private void btnSaveNew_Click(object sender, RoutedEventArgs e)
        {
            userSave();
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            userDelete();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            userCancel();
        }

        private void btnUserEdit_Click(object sender, RoutedEventArgs e)
        {
            editSelect();
        }

        private void btnSaveEdit_Click(object sender, RoutedEventArgs e)
        {
            editSave();
        }

        private void userNew()
        {
            txtUsername.IsEnabled = true;
            txtPassword.IsEnabled = true;

            btnUserNew.IsEnabled = false;
            btnUserSave.IsEnabled = true;
            btnUserDelete.IsEnabled = false;
            btnUserCancel.IsEnabled = true;
        }

        private void userCancel()
        {
            txtUsername.IsEnabled = false;
            txtPassword.IsEnabled = false;

            btnUserNew.IsEnabled = true;
            btnUserSave.IsEnabled = false;
            btnUserDelete.IsEnabled = true;
            btnUserCancel.IsEnabled = true;
            btnUserEdit.IsEnabled = true;
            btnSaveEdit.IsEnabled = false;

            txtCurrentPass.IsEnabled = false;
            txtNewPass.IsEnabled = false;
            txtRePass.IsEnabled = false;

            txtUsername.Text = "";
            txtPassword.Password = "";
            txtCurrentPass.Password = "";
            txtNewPass.Password = "";
            txtRePass.Password = "";

            dgUsers.IsEnabled = true;
        }

        private void userSave()
        {
            //Save new Users
            try
            {
                if (txtUsername.Text != "" && txtPassword.Password != "")
                {
                    DataTable dtShare = new DataTable();
                    MyConfig.Cfg.Sql.Query("SELECT * FROM Users WHERE Username='" + txtUsername.Text + "';", ref dtShare);
                    if (dtShare.Rows.Count == 0)
                    {
                        //no duplicate
                        string queryString = String.Format("INSERT INTO Users(Username, Password) VALUES('{0}','{1}');",
                                            txtUsername.Text, txtPassword.Password);
                        MyConfig.Cfg.Sql.Query(queryString);
                        MessageBox.Show("Save successful", "User", MessageBoxButton.OK, MessageBoxImage.Information);
                        userLoadTable();
                    }
                    else
                    {
                        throw new System.ArgumentException("This user ID already been used, please use another ID to register again.");
                    }
                }
                else
                    throw new System.ArgumentException("Username and password cannot left blank, please complete the blank");
                userCancel();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void userDelete()
        {
            //Delete selected Users
            var result = MessageBox.Show("Are you want to delete this user account?", "Affirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                try
                {
                    DataRowView dataRow = (DataRowView)dgUsers.SelectedItem;
                    string username = dataRow.Row.ItemArray[0].ToString();
                    MyConfig.Cfg.Sql.Query("DELETE FROM Users WHERE Username = '" + username + "';");
                    MessageBox.Show("Delete successful", "User", MessageBoxButton.OK, MessageBoxImage.Information);
                    userLoadTable();
                }
                catch
                {
                    MessageBox.Show("Please select a username account to delete.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }

            }
        }

        private void editSelect()
        {
            txtUsername.IsEnabled = false;
            txtPassword.IsEnabled = false;

            btnUserNew.IsEnabled = false;
            btnUserSave.IsEnabled = false;
            btnUserDelete.IsEnabled = false;
            btnUserCancel.IsEnabled = true;
            btnUserEdit.IsEnabled = false;
            btnSaveEdit.IsEnabled = true;

            txtCurrentPass.IsEnabled = true;
            txtNewPass.IsEnabled = true;
            txtRePass.IsEnabled = true;

            dgUsers.IsEnabled = false;
        }

        private void editSave()
        {
            var result = MessageBox.Show("Are you want to edit this account?", "Affirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                try
                {
                    if (txtNewPass.Password != txtRePass.Password)
                        throw new System.ArgumentException("New and Reconfirm Password not match. Please key in again.");
                    if (txtCurrentPass.Password != "" && txtNewPass.Password != "" && txtRePass.Password != "")
                    {
                        DataRowView dataRow = (DataRowView)dgUsers.SelectedItem;
                        string username = dataRow.Row.ItemArray[0].ToString();

                        DataTable dtShare = new DataTable();
                        string queryString = "SELECT * FROM Users WHERE Username='" + username + "' AND Password='" + txtCurrentPass.Password + "';";
                        MyConfig.Cfg.Sql.Query(queryString, ref dtShare);
                        if (dtShare.Rows.Count != 0)
                        {
                            MyConfig.Cfg.Sql.Query("UPDATE Users SET Password = '" + txtNewPass.Password + "' WHERE Username='" + username + "';");
                        }
                        else
                            throw new System.ArgumentException("Incorrect current password. Please key in again.");
                        userCancel();
                        MessageBox.Show("Password updated.", "Admin", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                        throw new System.ArgumentException("Current, New or Reconfirm Password cannot left in blank. Please key in again.");
                }
                catch
                {
                    MessageBox.Show("Please select a username account to edit.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }

            }
        }

        private void userLoadTable()
        {
            //Load Users table
            DataTable dtUsers = new DataTable();
            MyConfig.Cfg.Sql.Query("SELECT Username FROM Users", ref dtUsers);
            dgUsers.DataContext = dtUsers.DefaultView;
        }



    }
}


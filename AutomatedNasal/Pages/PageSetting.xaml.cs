﻿using AutomationLib;
using MachineControl;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AutomatedNasal.Pages
{
    /// <summary>
    /// Interaction logic for PageSetting.xaml
    /// </summary>
    public partial class PageSetting : Page
    {
        public PageSetting()
        {
            InitializeComponent();

            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            string version = fvi.FileVersion;
            lblVersion.Content = "VERSION " + version;

            #region bind gui
            BindGui("IOASensorEnable", cboxSensorIOA);
            BindGui("IOBSensorEnable", cboxSensorIOB);
            BindGui("PowerSupplyEnable", cboxPowerSupply);
            BindGui("TemperatureCheck", cboxTemperatureCheck);
            BindGui("CellSensorCheck", cboxCellSensor);
            BindGui("CurtainSensorCheck", cboxCurtainSensor);
            BindGui("UPTime", tboxUpTime);
            BindGui("TaskMainFlowState", tboxMainFlowStat);
            BindGui("TaskPowerSupplyMonitor", tboxPowerSupplyMonitorTask);
            BindGui("TaskUndip", tboxTaskUndipProcess);
            BindGui("TaskDip", tboxDippingProcessStat);

            for (int i = 1; i <= 12; i++)
            {
                var tb = (Label)this.FindName($"tboxTimerStatCell{i}");
                BindGui($"TimerStatCell{i}", tb);
            }



            #endregion
        }

        private void BindGui(string BindingParam, dynamic UI)
        {
            Binding myBinding = new Binding(BindingParam)
            {
                Source = MyConfig.Cfg.Model
            };

            var typ = UI.GetType();
            switch (typ.Name)
            {
                case "TextBox":
                    BindingOperations.SetBinding(UI, TextBox.TextProperty, myBinding);
                    break;

                case "CheckBox":
                    BindingOperations.SetBinding(UI, CheckBox.IsCheckedProperty, myBinding);
                    break;

                case "Label":
                    BindingOperations.SetBinding(UI, Label.ContentProperty, myBinding);
                    break;
                default:
                    break;
            }

        }

        private void btnSaveSettings_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;

            switch (btn.Name)
            {
                case "btnSaveTank":

                    Config.Cfg.SaveSetting("TankSetting", "FillDelayIOA", double.Parse(tboxTimeFillIOA.Text));
                    Config.Cfg.SaveSetting("TankSetting", "DrainDelayIOA", double.Parse(tboxTimeDrainIOA.Text));
                    Config.Cfg.SaveSetting("TankSetting", "ReFillDelayIOA", double.Parse(tboxTimeReFillIOA.Text));

                    Config.Cfg.SaveSetting("TankSetting", "FillDelayIOB", double.Parse(tboxTimeFillIOB.Text));
                    Config.Cfg.SaveSetting("TankSetting", "DrainDelayIOB", double.Parse(tboxTimeDrainIOB.Text));
                    Config.Cfg.SaveSetting("TankSetting", "ReFillDelayIOB", double.Parse(tboxTimeReFillIOB.Text));

                    Config.Cfg.SaveSetting("TankSetting", "FillDelayMandrel", double.Parse(tboxTimeFillMandrel.Text));
                    Config.Cfg.SaveSetting("TankSetting", "DrainDelayMandrel", double.Parse(tboxTimeDrainMandrel.Text));
                    Config.Cfg.SaveSetting("TankSetting", "FillDelayBacker", double.Parse(tboxTimeFillBacker.Text));
                    Config.Cfg.SaveSetting("TankSetting", "DrainDelayBacker", double.Parse(tboxTimeDrainBacker.Text));

                    Config.Cfg.Save();
                    break;

                case "btnSaveAgit":

                    Config.Cfg.SaveSetting("AgitSetting", "TimeAgitMoveBk", double.Parse(tboxTimeAgitMoveBk.Text));
                    Config.Cfg.SaveSetting("AgitSetting", "TimeAgitMoveFt", double.Parse(tboxTimeAgitMoveFt.Text));
                    Config.Cfg.SaveSetting("AgitSetting", "TimeSignalFt", double.Parse(tboxTimeSignalFt.Text));
                    Config.Cfg.SaveSetting("AgitSetting", "TimeSignalBk", double.Parse(tboxTimeSignalBk.Text));
                    Config.Cfg.Save();
                    break;

                //case "btnSaveTemperature":

                //    Config.Cfg.SaveSetting("Temperature", "TempSetPoint", double.Parse(tboxSetPoint.Text));
                //    Config.Cfg.SaveSetting("Temperature", "TempTolerance", double.Parse(tboxTolerance.Text));
                //    Config.Cfg.Save();
                //    break;

                default:
                    break;
            }

            MyConfig.Cfg.FillDelayIOA = double.Parse(tboxTimeFillIOA.Text);
            MyConfig.Cfg.DrainDelayIOA = double.Parse(tboxTimeDrainIOA.Text);
            MyConfig.Cfg.ReFillDelayIOA = double.Parse(tboxTimeReFillIOA.Text);

            MyConfig.Cfg.ReFillDelayIOB = double.Parse(tboxTimeReFillIOB.Text);
            MyConfig.Cfg.ReFillDelayIOB = double.Parse(tboxTimeDrainIOB.Text);
            MyConfig.Cfg.ReFillDelayIOB = double.Parse(tboxTimeReFillIOB.Text);

            MyConfig.Cfg.DrainDelayMandrel = double.Parse(tboxTimeDrainMandrel.Text);
            MyConfig.Cfg.FillDelayMandrel = double.Parse(tboxTimeFillMandrel.Text);

            MyConfig.Cfg.DrainDelayBacker = double.Parse(tboxTimeDrainBacker.Text);
            MyConfig.Cfg.FillDelayBacker = double.Parse(tboxTimeFillBacker.Text);
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            MyConfig.Cfg.FillDelayIOA = Config.Cfg.GetSettings("TankSetting", "FillDelayIOA");
            MyConfig.Cfg.DrainDelayIOA = Config.Cfg.GetSettings("TankSetting", "DrainDelayIOA");
            MyConfig.Cfg.ReFillDelayIOA = Config.Cfg.GetSettings("TankSetting", "ReFillDelayIOA");


            tboxTimeFillIOA.Text = MyConfig.Cfg.FillDelayIOA.ToString();
            tboxTimeDrainIOA.Text = MyConfig.Cfg.DrainDelayIOA.ToString();
            tboxTimeReFillIOA.Text = MyConfig.Cfg.ReFillDelayIOA.ToString();


            MyConfig.Cfg.FillDelayIOB = Config.Cfg.GetSettings("TankSetting", "FillDelayIOB");
            MyConfig.Cfg.DrainDelayIOB = Config.Cfg.GetSettings("TankSetting", "DrainDelayIOB");
            MyConfig.Cfg.ReFillDelayIOB = Config.Cfg.GetSettings("TankSetting", "ReFillDelayIOB");


            tboxTimeFillIOB.Text = MyConfig.Cfg.FillDelayIOB.ToString();
            tboxTimeDrainIOB.Text = MyConfig.Cfg.DrainDelayIOB.ToString();
            tboxTimeReFillIOB.Text = MyConfig.Cfg.ReFillDelayIOB.ToString();

            MyConfig.Cfg.FillDelayMandrel = Config.Cfg.GetSettings("TankSetting", "FillDelayMandrel");
            MyConfig.Cfg.DrainDelayMandrel = Config.Cfg.GetSettings("TankSetting", "DrainDelayMandrel");
            MyConfig.Cfg.FillDelayBacker = Config.Cfg.GetSettings("TankSetting", "FillDelayBacker");
            MyConfig.Cfg.DrainDelayBacker = Config.Cfg.GetSettings("TankSetting", "DrainDelayBacker");

            tboxTimeFillMandrel.Text = MyConfig.Cfg.FillDelayMandrel.ToString();
            tboxTimeDrainMandrel.Text = MyConfig.Cfg.DrainDelayMandrel.ToString();
            tboxTimeFillBacker.Text = MyConfig.Cfg.FillDelayBacker.ToString();
            tboxTimeDrainBacker.Text = MyConfig.Cfg.DrainDelayBacker.ToString();

            MyConfig.Cfg.TimeAgitMoveBk = Config.Cfg.GetSettings("AgitSetting", "TimeAgitMoveBk");
            MyConfig.Cfg.TimeAgitMoveFt = Config.Cfg.GetSettings("AgitSetting", "TimeAgitMoveFt");
            MyConfig.Cfg.TimeSignalFt = Config.Cfg.GetSettings("AgitSetting", "TimeSignalFt");
            MyConfig.Cfg.TimeSignalBk = Config.Cfg.GetSettings("AgitSetting", "TimeSignalBk");

            MyConfig.Cfg.CellExpireTime = Config.Cfg.GetSettingsInt("OtherTimer", "CellExpire");
            tboxCellExpireTime.Text = MyConfig.Cfg.CellExpireTime.ToString();

            MyConfig.Cfg.Model.TimerExpireEndLot = Config.Cfg.GetSettingsInt("OtherTimer", "EndLotTimer");
            tboxEndLotTimer.Text = MyConfig.Cfg.Model.TimerExpireEndLot.ToString();

            tboxTempOffset.Text = MyConfig.Cfg.TemperatureOffset.ToString();

            tboxBRTCount.Text = MyConfig.Cfg.BrtStrokeCount.ToString();

            tboxFillToL.Text = MyConfig.Cfg.FillToL.ToString();
            tboxFillToF.Text = MyConfig.Cfg.FillToF.ToString();
            tboxFillToH.Text = MyConfig.Cfg.FillToH.ToString();
            tboxFillToHH.Text = MyConfig.Cfg.FillToHH.ToString();


        }

        private void btnSaveBRT_Click(object sender, RoutedEventArgs e)
        {
            //MyConfig.Cfg.BrtStrokeCount = (int)(Config.Cfg.GetSettings("BRT", "Count"));

            Config.Cfg.SaveSetting("BRT", "Count", double.Parse(tboxBRTCount.Text));
            Config.Cfg.Save();
        }

        private void btnSaveWaterLevel_Click(object sender, RoutedEventArgs e)
        {
            Config.Cfg.SaveSetting("Water Level", "FillToL", double.Parse(tboxFillToL.Text));
            Config.Cfg.SaveSetting("Water Level", "FillToF", double.Parse(tboxFillToF.Text));
            Config.Cfg.SaveSetting("Water Level", "FillToH", double.Parse(tboxFillToH.Text));
            Config.Cfg.SaveSetting("Water Level", "FillToHH", double.Parse(tboxFillToHH.Text));

            MyConfig.Cfg.FillToF = int.Parse(tboxFillToF.Text);
            MyConfig.Cfg.FillToH = int.Parse(tboxFillToH.Text);
            MyConfig.Cfg.FillToHH = int.Parse(tboxFillToHH.Text);
            MyConfig.Cfg.FillToL = int.Parse(tboxFillToL.Text);

            Config.Cfg.Save();
        }

        private void btnSaveOther_Click(object sender, RoutedEventArgs e)
        {
            Config.Cfg.SaveSetting("OtherTimer", "CellExpire", int.Parse(tboxCellExpireTime.Text));
            Config.Cfg.SaveSetting("OtherTimer", "EndLotTimer", int.Parse(tboxEndLotTimer.Text));

            MyConfig.Cfg.CellExpireTime = int.Parse(tboxCellExpireTime.Text);
            MyConfig.Cfg.Model.TimerExpireEndLot = int.Parse(tboxEndLotTimer.Text);
            Config.Cfg.Save();
        }

        private void cboxPowerSupply_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as CheckBox;
            var type = btn.Tag.ToString();

            switch (type)
            {
                case "PS":
                    MyConfig.Cfg.Model.PowerSupplyEnable = (bool)btn.IsChecked;
                    break;
                case "IOA":
                    MyConfig.Cfg.Model.IOASensorEnable = (bool)btn.IsChecked;
                    break;
                case "IOB":
                    MyConfig.Cfg.Model.IOBSensorEnable = (bool)btn.IsChecked;
                    break;
                case "TEMP":
                    MyConfig.Cfg.Model.TemperatureCheck = (bool)btn.IsChecked;
                    break;
                case "CELL":
                    MyConfig.Cfg.Model.CellSensorCheck = (bool)btn.IsChecked;
                    break;
                case "CURTAIN":
                    MyConfig.Cfg.Model.CurtainSensorCheck = (bool)btn.IsChecked;
                    break;
                default:
                    break;
            }
        }

        private void btnGetProcess_Click(object sender, RoutedEventArgs e)
        {
            tboxProcessState.Content = MyConfig.Cfg.ProcessState;
        }

        private void btnClearMessageLogs_Click(object sender, RoutedEventArgs e)
        {
            MyConfig.Cfg.UpdateUI("Clear", "ClearMessageLogs", "");
        }

        private void btnClearErrorLogs_Click(object sender, RoutedEventArgs e)
        {
            MyConfig.Cfg.UpdateUI("Clear", "ClearErrorLogs", "");
        }

        private void btnSaveTemp_Click(object sender, RoutedEventArgs e)
        {

            MyConfig.Cfg.TemperatureOffset = double.Parse(tboxTempOffset.Text);
            Config.Cfg.SaveSetting("Temeprature", "Offset", double.Parse(tboxTempOffset.Text));
            Config.Cfg.Save();
        }
    }
}

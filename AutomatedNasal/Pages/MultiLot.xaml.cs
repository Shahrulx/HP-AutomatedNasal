﻿using AutomationLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
namespace AutomatedNasal.Pages
{
    /// <summary>
    /// Interaction logic for MultiLot.xaml
    /// </summary>
    public partial class MultiLot : Window
    {
        MainWindow main;
        public string stat { get; set; } = "open";
        List<double> AmpMin { get; set; } = new List<double>();

        public MultiLot(MainWindow main)
        {
            InitializeComponent();
            this.main = main;
        }

        private void btnAccept_Click(object sender, RoutedEventArgs e)
        {
            if(main.mainFlow.currentState != MainFlow.SequenceState.IDLE)
            {
                MessageBox.Show("AmpMin Could Not Be Saved. Please End Lot In Order To Change AmpMin");
                this.Close();
                return;
            }

            AmpMin = MyConfig.Cfg.RecepiList.SetConfig[0].SetA;
            loadamp(1, "A");
            AmpMin = MyConfig.Cfg.RecepiList.SetConfig[0].SetB;
            loadamp(2, "B");


            AmpMin = MyConfig.Cfg.RecepiList.SetConfig[0].SetC;
            loadamp(1, "C");
            AmpMin = MyConfig.Cfg.RecepiList.SetConfig[0].SetD;
            loadamp(2, "D");


            MyConfig.Cfg.Model.SetAmpMinLog("Info", $"--------Amp Min Master List Change Start----------");
            List<string> Sets = new List<string>() { "A", "B", "C", "D" };
            for (int i = 1; i <= 4; i++)
            {
                for (int j = 1; j <= 6; j++)
                {
                    var tbox = GetTextBox($"tboxAmpMin{Sets[i-1]}{j}");
                    var value = tbox.Text;
                    MyConfig.Cfg.Model.SetAmpMinLog("Info", $"AMP MIN SET {Sets[i-1]} Cell {j}-Value:" + value);
                }
            }
            MyConfig.Cfg.Model.SetAmpMinLog("Info", $"--------Amp Min Master List Change End----------");
            MessageBox.Show("Saved");
            this.Close();
        }

        private void loadamp(int banknumber, string setname)
        {

            for (int i = 1; i <= 6; i++)
            {
                TextBox tbox = (TextBox)this.FindName($"tboxAmpMin{setname}{i}");
                AmpMin[i - 1] = double.Parse(tbox.Text);

            }

            if (banknumber == 1)
            {
                for (int i = 0; i <= 5; i++)
                {
                    var val = main.mainFlow.cellModule.CalculateAmpMin(AmpMin[i]);
                    MyConfig.Cfg.RecepiList.Cells[i].Timer = val.TotalSeconds;
                    MyConfig.Cfg.RecepiList.Cells[i].AmpMin = AmpMin[i];

                    main.mainFlow.cellModule.ChangeTimer((i + 1).ToString(), (int)val.TotalSeconds);

                    if (setname == "A")
                        MyConfig.Cfg.UpdateUI($"tboxAmpMin{i + 1}", "SetValue", AmpMin[i].ToString());

                }
            }

            if (banknumber == 2)
            {
                for (int i = 6; i <= 11; i++)
                {
                    var val = main.mainFlow.cellModule.CalculateAmpMin(AmpMin[i - 6]);
                    MyConfig.Cfg.RecepiList.Cells[i - 6].Timer = val.TotalSeconds;
                    MyConfig.Cfg.RecepiList.Cells[i - 6].AmpMin = AmpMin[i - 6];

                    main.mainFlow.cellModule.ChangeTimer((i + 1).ToString(), (int)val.TotalSeconds);

                    if (setname == "B")
                    {
                        MyConfig.Cfg.UpdateUI($"tboxAmpMin{i + 1}", "SetValue", AmpMin[i - 6].ToString());
                        MyConfig.Cfg.SaveRecepi();
                    }

                }
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            lblRecepi.Content = MyConfig.Cfg.SelectedRecepi;
            //lblSetName.Content = MyConfig.Cfg.Model.NextSet;

            tboxAmpMinA1.Text = MyConfig.Cfg.RecepiList.SetConfig[0].SetA[0].ToString();
            tboxAmpMinA2.Text = MyConfig.Cfg.RecepiList.SetConfig[0].SetA[1].ToString();
            tboxAmpMinA3.Text = MyConfig.Cfg.RecepiList.SetConfig[0].SetA[2].ToString();
            tboxAmpMinA4.Text = MyConfig.Cfg.RecepiList.SetConfig[0].SetA[3].ToString();
            tboxAmpMinA5.Text = MyConfig.Cfg.RecepiList.SetConfig[0].SetA[4].ToString();
            tboxAmpMinA6.Text = MyConfig.Cfg.RecepiList.SetConfig[0].SetA[5].ToString();

            tboxAmpMinB1.Text = MyConfig.Cfg.RecepiList.SetConfig[0].SetB[0].ToString();
            tboxAmpMinB2.Text = MyConfig.Cfg.RecepiList.SetConfig[0].SetB[1].ToString();
            tboxAmpMinB3.Text = MyConfig.Cfg.RecepiList.SetConfig[0].SetB[2].ToString();
            tboxAmpMinB4.Text = MyConfig.Cfg.RecepiList.SetConfig[0].SetB[3].ToString();
            tboxAmpMinB5.Text = MyConfig.Cfg.RecepiList.SetConfig[0].SetB[4].ToString();
            tboxAmpMinB6.Text = MyConfig.Cfg.RecepiList.SetConfig[0].SetB[5].ToString();

            tboxAmpMinC1.Text = MyConfig.Cfg.RecepiList.SetConfig[0].SetC[0].ToString();
            tboxAmpMinC2.Text = MyConfig.Cfg.RecepiList.SetConfig[0].SetC[1].ToString();
            tboxAmpMinC3.Text = MyConfig.Cfg.RecepiList.SetConfig[0].SetC[2].ToString();
            tboxAmpMinC4.Text = MyConfig.Cfg.RecepiList.SetConfig[0].SetC[3].ToString();
            tboxAmpMinC5.Text = MyConfig.Cfg.RecepiList.SetConfig[0].SetC[4].ToString();
            tboxAmpMinC6.Text = MyConfig.Cfg.RecepiList.SetConfig[0].SetC[5].ToString();

            tboxAmpMinD1.Text = MyConfig.Cfg.RecepiList.SetConfig[0].SetD[0].ToString();
            tboxAmpMinD2.Text = MyConfig.Cfg.RecepiList.SetConfig[0].SetD[1].ToString();
            tboxAmpMinD3.Text = MyConfig.Cfg.RecepiList.SetConfig[0].SetD[2].ToString();
            tboxAmpMinD4.Text = MyConfig.Cfg.RecepiList.SetConfig[0].SetD[3].ToString();
            tboxAmpMinD5.Text = MyConfig.Cfg.RecepiList.SetConfig[0].SetD[4].ToString();
            tboxAmpMinD6.Text = MyConfig.Cfg.RecepiList.SetConfig[0].SetD[5].ToString();


        }


        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex(@"[^0-9\-.]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void tboxAmpMinA1_KeyDown(object sender, KeyEventArgs e)
        {
            var SenderTbox = sender as TextBox;
            var Name = SenderTbox.Name.Substring(10,1);
            var Name2 = SenderTbox.Name.Substring(11);

           
            if (e.Key == Key.Return)
            {
                var num = int.Parse(Name2);
                num = num + 1;

                if (num > 6)
                {
                    switch (Name)
                    {
                        case "A":
                            Name = "B";
                            break;
                        case "B":
                            Name = "C";
                            break;
                        case "C":
                            Name = "D";
                            break;
                        case "D":
                            Name = "A";
                            break;

                        default:
                            break;
                    }
                    num = 1;
                }

                var tbox = GetTextBox($"tboxAmpMin{Name}{num}");
                tbox.Focus();
                tbox.SelectAll();
            }
            else
            {
               
            }
        }

        private TextBox GetTextBox(string TboxName)
        {
            TextBox textBox;

            textBox = (TextBox)(this.FindName(TboxName));

            return textBox;
        }
    }
}

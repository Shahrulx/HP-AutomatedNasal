﻿using AutomationLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AutomatedNasal.Pages
{
    /// <summary>
    /// Interaction logic for AmpCalc.xaml
    /// </summary>
    public partial class AmpCalc : Window
    {
        public double AmpMinValue { get; set; } = 0;
        string CellName = "";
        MainWindow main;
        
        public AmpCalc(MainWindow main,string CellName, string CurrentAmpMin)
        {
            InitializeComponent();
            lblSelectedCell.Content = CellName;
            tboxAmpMin.Text = CurrentAmpMin;
            this.CellName = CellName;
            this.main = main;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            AmpMinValue = double.Parse(tboxAmpMin.Text);
            var idx = MyConfig.Cfg.RecepiList.Cells.FindIndex(x => x.Cell == CellName);
            MyConfig.Cfg.RecepiList.Cells[idx].AmpMin = AmpMinValue;
            MyConfig.Cfg.SaveRecepi();

            main.mainFlow.cellModule.ChangeTimer(CellName, int.Parse(tboxAmpMin.Text));
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}

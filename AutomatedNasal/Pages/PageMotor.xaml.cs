﻿using AutomationLib;
using HardwareLib;
using MachineControl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace AutomatedNasal.Pages
{
    /// <summary>
    /// Interaction logic for PageMotor.xaml
    /// </summary>
    public partial class PageMotor : Page
    {
        private MainWindow mainWindow;
        bool MotorStop = false;
        private Hardware hw;
        List<string> SensorList = new List<string>();
        DispatcherTimer timer1 = null;
        bool stop = false;
        int interval = 200;
        double MotorXMax = 1190;
        double MotorXMin = -1.0;
        bool CellDip = true;
        bool robothomeclick = false;

        List<string> DiPos1 = new List<string>() { "1", "2", "3", "7", "8", "9" };
        List<string> DiPos2 = new List<string>() { "4", "5", "6", "10", "11", "12" };

        public PageMotor(MainWindow mainWindow)
        {
            InitializeComponent();
            this.mainWindow = mainWindow;
            hw = mainWindow.hw;
            DataBinding();
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            //hide header
            Head1.Visibility = Visibility.Hidden;
            Head2.Visibility = Visibility.Hidden;
            Head3.Visibility = Visibility.Hidden;
            Head4.Visibility = Visibility.Hidden;

            timer1.IsEnabled = true;
            stop = false;
            Motor1X.Start();
            tab_Main.SelectedIndex = 3;

            var IOStatus = hw.motors["Motor1X"].GetIOStatus();
            this.Dispatcher.Invoke(new Action(() =>
            {
                if (IOStatus.origin == 1)
                {
                    btnMotorPos1.IsEnabled = true;
                    btnMotorPos2.IsEnabled = true;
                    btnMotorPos3.IsEnabled = true;
                    lblStatus.Content = "MOTOR HOME DONE";
                }
                else
                {
                    btnMotorPos1.IsEnabled = false;
                    btnMotorPos2.IsEnabled = false;
                    btnMotorPos3.IsEnabled = false;
                    lblStatus.Content = "Please Home Motor";

                }
            }));


            if (MyConfig.Cfg.UserType == "SuperAdmin")
            {
                btnRobotManualPage.Visibility = Visibility.Visible;
                btnRobotAutoPage.Visibility = Visibility.Visible;
                btnMotorPage.Visibility = Visibility.Visible;
            }
            else if (MyConfig.Cfg.UserType == "Admin")
            {
                btnRobotManualPage.Visibility = Visibility.Hidden;
                btnRobotAutoPage.Visibility = Visibility.Hidden;
                btnMotorPage.Visibility = Visibility.Visible;
            }
            else
            {
                btnRobotManualPage.Visibility = Visibility.Hidden;
                btnRobotAutoPage.Visibility = Visibility.Hidden;
                btnMotorPage.Visibility = Visibility.Hidden;
            }

            var position = Motor1X.GetMotorPosition();
            this.Dispatcher.Invoke(new Action(() =>
            {
                tboxMotorPos.Content = position;
            }));

            lblMaxMotor.Content = "Max:" + MotorXMax.ToString();
            lblMinMotor.Content = "Min:" + MotorXMin.ToString();
        }

        private void DataBinding()
        {
            hw.motors["Motor1X"].UnitInmm = true;
            hw.motors["Motor1X"].RunSpeedCfg = hw.GetSpeedCfg(Config.Cfg.Motor1XSpeed);
            Motor1X.Init(hw.motors["Motor1X"], Config.Cfg, "Motor1XSpeed");
          
            PickPos1.Init("Motor1XPos1", new List<string>(), Config.Cfg, hw.motors["Motor1X"], hw.motors["Motor1X"], false, true);
            PickPos2.Init("Motor1XPos2", new List<string>(), Config.Cfg, hw.motors["Motor1X"], hw.motors["Motor1X"], false, true);
            PickPos3.Init("Motor1XPos3", new List<string>(), Config.Cfg, hw.motors["Motor1X"], hw.motors["Motor1X"], false, true);

            SensorList.Add("X6_05_ROBOT_LOAD_READY");
            SensorList.Add("X6_06_ROBOT_DIP_INPOSITION");
            SensorList.Add("X6_07_ROBOT_DIP_READY");
            SensorList.Add("X6_08_ROBOT_RISE_INPOSITION");
            SensorList.Add("X6_09_ROBOT_RISE_READY");
            SensorList.Add("X6_10_ROBOT_UNLOAD_READY");
            SensorList.Add("X6_11_ROBOT_ROBOT_BUSY");
            SensorList.Add("X6_12_ROBOT_ROBOT_HAND_FULL");
            SensorList.Add("X6_13_ROBOT_ROBOT_ERROR");
            SensorList.Add("X6_14_ROBOT_POSITION_RECEIVED");
            SensorList.Add("X3_14_ROBOT_Z_AXIS_UP");

            UpdateInput();

            timer1 = new DispatcherTimer();
            timer1.Tick += new EventHandler(timer1_Tick);
            stop = false;
            timer1.Interval = new TimeSpan(0, 0, 0, 0, interval);


        }

        private void UpdateInput()
        {
            foreach (var item in SensorList)
            {
                var stat = hw.inputs[item].Read();
                var btn = (Rectangle)this.FindName(item);

                if (stat == 1)
                {
                    btn.Fill = new SolidColorBrush(Colors.LightSeaGreen);
                }
                else
                {
                    btn.Fill = new SolidColorBrush(Colors.White);
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.IsEnabled = false;
            UpdateInput();
            timer1.IsEnabled = true;
            if (stop)
                timer1.IsEnabled = false;
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            Config.Cfg.Save();
            MessageBox.Show("Saved");
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var _btn = sender as Button;
            String Name = _btn.Name.ToString();
            var stat = _btn.Tag;
            hw.outputs[Name].ON();

            if (stat == null || stat == "ON")
            {
                hw.outputs[Name].ON();
                _btn.Background = new SolidColorBrush(Colors.Green);
                _btn.Tag = "OFF";
            }
            else
            {
                hw.outputs[Name].OFF();
                _btn.Background = new SolidColorBrush(Colors.DarkSlateGray);
                _btn.Tag = "ON";
            }
        }

        private void Page_Unloaded(object sender, RoutedEventArgs e)
        {
            stop = true;
        }

        private void OutputFunc(object sender, RoutedEventArgs e)
        {
            var bt = sender as Button;

            if (bt.Tag == "ON")
            {
                hw.outputs[bt.Name].OFF();
                bt.Background = new SolidColorBrush(Colors.White);
                bt.Tag = "OFF";
            }
            else
            {
                hw.outputs[bt.Name].ON();
                bt.Background = new SolidColorBrush(Colors.Green);
                bt.Tag = "ON";
            }

        }

        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            var bt = sender as Button;

            switch (bt.Tag)
            {
                case "0":
                    hw.outputs["Y5_00_ROBOT_BASKET_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_01_ROBOT_BASKET_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_02_ROBOT_BASKET_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_04_ROBOT_BASKET_POSITION_BIT_4"].OFF();
                    break;

                case "1":
                    hw.outputs["Y5_00_ROBOT_BASKET_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_01_ROBOT_BASKET_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_02_ROBOT_BASKET_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_04_ROBOT_BASKET_POSITION_BIT_4"].ON();
                    break;
                case "2":
                    hw.outputs["Y5_00_ROBOT_BASKET_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_01_ROBOT_BASKET_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_02_ROBOT_BASKET_POSITION_BIT_3"].ON();
                    hw.outputs["Y5_04_ROBOT_BASKET_POSITION_BIT_4"].OFF();
                    break;
                case "3":
                    hw.outputs["Y5_00_ROBOT_BASKET_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_01_ROBOT_BASKET_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_02_ROBOT_BASKET_POSITION_BIT_3"].ON();
                    hw.outputs["Y5_04_ROBOT_BASKET_POSITION_BIT_4"].ON();
                    break;
                case "4":
                    hw.outputs["Y5_00_ROBOT_BASKET_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_01_ROBOT_BASKET_POSITION_BIT_2"].ON();
                    hw.outputs["Y5_02_ROBOT_BASKET_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_04_ROBOT_BASKET_POSITION_BIT_4"].OFF();
                    break;

                case "5":
                    hw.outputs["Y5_00_ROBOT_BASKET_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_01_ROBOT_BASKET_POSITION_BIT_2"].ON();
                    hw.outputs["Y5_02_ROBOT_BASKET_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_04_ROBOT_BASKET_POSITION_BIT_4"].ON();
                    break;

                case "6":
                    hw.outputs["Y5_00_ROBOT_BASKET_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_01_ROBOT_BASKET_POSITION_BIT_2"].ON();
                    hw.outputs["Y5_02_ROBOT_BASKET_POSITION_BIT_3"].ON();
                    hw.outputs["Y5_04_ROBOT_BASKET_POSITION_BIT_4"].OFF();
                    break;

                case "7":
                    hw.outputs["Y5_00_ROBOT_BASKET_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_01_ROBOT_BASKET_POSITION_BIT_2"].ON();
                    hw.outputs["Y5_02_ROBOT_BASKET_POSITION_BIT_3"].ON();
                    hw.outputs["Y5_04_ROBOT_BASKET_POSITION_BIT_4"].ON();
                    break;

                //UNLOAD ZERO POS
                case "8":
                    hw.outputs["Y5_00_ROBOT_BASKET_POSITION_BIT_1"].ON();
                    hw.outputs["Y5_01_ROBOT_BASKET_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_02_ROBOT_BASKET_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_04_ROBOT_BASKET_POSITION_BIT_4"].OFF();
                    break;

                case "9":
                    hw.outputs["Y5_00_ROBOT_BASKET_POSITION_BIT_1"].ON();
                    hw.outputs["Y5_01_ROBOT_BASKET_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_02_ROBOT_BASKET_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_04_ROBOT_BASKET_POSITION_BIT_4"].ON();
                    break;

                case "10":
                    hw.outputs["Y5_00_ROBOT_BASKET_POSITION_BIT_1"].ON();
                    hw.outputs["Y5_01_ROBOT_BASKET_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_02_ROBOT_BASKET_POSITION_BIT_3"].ON();
                    hw.outputs["Y5_04_ROBOT_BASKET_POSITION_BIT_4"].OFF();
                    break;

                case "11":
                    hw.outputs["Y5_00_ROBOT_BASKET_POSITION_BIT_1"].ON();
                    hw.outputs["Y5_01_ROBOT_BASKET_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_02_ROBOT_BASKET_POSITION_BIT_3"].ON();
                    hw.outputs["Y5_04_ROBOT_BASKET_POSITION_BIT_4"].ON();
                    break;

                case "12":
                    hw.outputs["Y5_00_ROBOT_BASKET_POSITION_BIT_1"].ON();
                    hw.outputs["Y5_01_ROBOT_BASKET_POSITION_BIT_2"].ON();
                    hw.outputs["Y5_02_ROBOT_BASKET_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_04_ROBOT_BASKET_POSITION_BIT_4"].OFF();
                    break;

                case "13":
                    hw.outputs["Y5_00_ROBOT_BASKET_POSITION_BIT_1"].ON();
                    hw.outputs["Y5_01_ROBOT_BASKET_POSITION_BIT_2"].ON();
                    hw.outputs["Y5_02_ROBOT_BASKET_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_04_ROBOT_BASKET_POSITION_BIT_4"].ON();
                    break;

                case "14":
                    hw.outputs["Y5_00_ROBOT_BASKET_POSITION_BIT_1"].ON();
                    hw.outputs["Y5_01_ROBOT_BASKET_POSITION_BIT_2"].ON();
                    hw.outputs["Y5_02_ROBOT_BASKET_POSITION_BIT_3"].ON();
                    hw.outputs["Y5_04_ROBOT_BASKET_POSITION_BIT_4"].OFF();
                    break;

                case "15":
                    hw.outputs["Y5_00_ROBOT_BASKET_POSITION_BIT_1"].ON();
                    hw.outputs["Y5_01_ROBOT_BASKET_POSITION_BIT_2"].ON();
                    hw.outputs["Y5_02_ROBOT_BASKET_POSITION_BIT_3"].ON();
                    hw.outputs["Y5_04_ROBOT_BASKET_POSITION_BIT_4"].ON();
                    break;



                default:
                    break;

            }


            //hw.outputs["Y5_00_ROBOT_BASKET_POSITION_BIT_1"].OFF();
            //hw.outputs["Y5_01_ROBOT_BASKET_POSITION_BIT_2"].OFF();
            //hw.outputs["Y5_02_ROBOT_BASKET_POSITION_BIT_3"].OFF();
            //hw.outputs["Y5_04_ROBOT_BASKET_POSITION_BIT_4"].OFF();

        }


        private void CellFunc(object sender, RoutedEventArgs e)
        {
            var bt = sender as Button;

            switch (bt.Tag)
            {
                case "100":
                    CellDip = false;
                    tboxDipStat.Content = "Undip";

                    int j = 14;
                    for (int i = 1; i <= 12; i++)
                    {
                        var btn = (Button)this.FindName("c_" + i.ToString());
                
                        btn.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FF007089"));
                        btn.Tag = j.ToString();
                        j++;
                    }
                    break;

                case "101":
                    CellDip = true;
                    tboxDipStat.Content = "Dip";
                    j = 1;

                    for(int i = 1; i <= 12; i++)
                    {
                        var btn = (Button)this.FindName("c_"+i.ToString());
                        btn.Background = new SolidColorBrush(Colors.Green);
                        btn.Tag = j.ToString();
                        j++;
                    }
                    break;

                case "0":
                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].OFF();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].OFF();
                    break;

                case "1":
                
                    if (!CellDip)
                        return;

                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].OFF();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].ON();
                    break;

                case "2":
                   
                    if (!CellDip)
                        return;

                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].ON();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].OFF();
                    break;

                case "3":
                   
                    if (!CellDip)
                        return;
                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].ON();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].ON();
                    break;

                case "4":
                    
                    if (!CellDip)
                        return;
                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].ON();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].OFF();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].OFF();
                    break;

                case "5":
                    
                    if (!CellDip)
                        return;
                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].ON();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].OFF();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].ON();
                    break;

                case "6":
                   
                    if (!CellDip)
                        return;
                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].ON();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].ON();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].OFF();
                    break;

                case "7":
                   
                    if (!CellDip)
                        return;
                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].ON();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].ON();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].ON();
                    break;

                case "8":
                   
                    if (!CellDip)
                        return;
                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].ON();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].OFF();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].OFF();
                    break;

                case "9":
                   
                    if (!CellDip)
                        return;
                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].ON();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].OFF();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].ON();
                    break;

                case "10":
                    
                    if (!CellDip)
                        return;
                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].ON();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].ON();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].OFF();
                    break;

                case "11":
                   
                    if (!CellDip)
                        return;
                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].ON();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].ON();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].ON();
                    break;

                case "12":
                    
                    if (CellDip)
                        return;
                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].ON();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].ON();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].OFF();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].OFF();
                    break;

                //rise position

                //intitial
                case "13":
                    if (CellDip)
                        return;
                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].ON();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].ON();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].OFF();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].ON();
                    break;

                case "14":
                    
                    if (CellDip)
                        return;
                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].ON();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].ON();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].ON();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].OFF();
                    break;

                case "15":
                   
                    if (CellDip)
                        return;
                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].OFF();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].ON();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].ON();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].ON();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].ON();
                    break;

                case "16":
                    
                    if (CellDip)
                        return;
                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].ON();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].OFF();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].OFF();
                    break;

                case "17":
                   
                    if (CellDip)
                        return;
                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].ON();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].OFF();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].ON();
                    break;

                case "18":
                    
                    if (CellDip)
                        return;
                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].ON();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].ON();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].OFF();
                    break;

                case "19":
                   
                    if (CellDip)
                        return;
                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].ON();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].ON();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].ON();
                    break;

                case "20":
                    
                    if (CellDip)
                        return;
                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].ON();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].ON();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].OFF();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].OFF();
                    break;

                case "21":
                    
                    if (CellDip)
                        return;
                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].ON();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].ON();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].OFF();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].ON();
                    break;

                case "22":
                   
                    if (CellDip)
                        return;
                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].ON();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].ON();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].ON();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].OFF();
                    break;

                case "23":
                    
                    if (CellDip)
                        return;
                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].ON();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].OFF();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].ON();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].ON();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].ON();
                    break;

                case "24":
                    
                    if (CellDip)
                        return;
                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].ON();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].ON();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].OFF();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].OFF();
                    break;

                case "25":
                   
                    if (CellDip)
                        return;
                    hw.outputs["Y5_05_ROBOT_TANK_POSITION_BIT_1"].ON();
                    hw.outputs["Y5_06_ROBOT_TANK_POSITION_BIT_2"].ON();
                    hw.outputs["Y5_08_ROBOT_TANK_POSITION_BIT_3"].OFF();
                    hw.outputs["Y5_09_ROBOT_TANK_POSITION_BIT_4"].OFF();
                    hw.outputs["Y5_10_ROBOT_TANK_POSITION_BIT_5"].ON();
                    break;
                default:
                    break;
            }
        }

        private void Grid_Unloaded(object sender, RoutedEventArgs e)
        {

        }

        private void btnTestAutoDip_Click(object sender, RoutedEventArgs e)
        {
            hw.motors["Motor1X"].MoveAbsolute(Config.Cfg.PickPos1.Item1, 100);
            hw.motors["Motor1X"].WaitMotionDone(500);
            mainWindow.mainFlow.Robot.Cmd("Load", "1", "", false);
        }

        private void btnAutoRunLoad_Click(object sender, RoutedEventArgs e)
        {
            Task motortask, robottask;
            var loadingslot = tboxLoadingNo.Text;
            var dipno = tboxDipNo.Text;
            bool checkpos = false;
            int pos = 1;

            if (dipno == "10" || dipno == "11" || dipno == "12")
                pos = 2;
            else
            {
                checkpos = DiPos1.Any(s => dipno.Contains(s));
                if (checkpos)
                    pos = 1;
                else
                    pos = 2;
            } 
           
            if(pos == 1)
            {
                motortask = Task.Run(() =>
                {
                    hw.motors["Motor1X"].MoveAbsolute(Config.Cfg.PickPos1.Item1, 100);
                    hw.motors["Motor1X"].WaitMotionDone(500);
                    mainWindow.mainFlow.Robot.Cmd("Load", loadingslot, "", false);
                    hw.motors["Motor1X"].MoveAbsolute(Config.Cfg.PickPos2.Item1, 100);
                    hw.motors["Motor1X"].WaitMotionDone(500);
                    mainWindow.mainFlow.Robot.Cmd("Dip", dipno.ToString(), "", false);

                });
            }

            else if (pos == 2)
            {
                motortask = Task.Run(() =>
                {
                    hw.motors["Motor1X"].MoveAbsolute(Config.Cfg.PickPos1.Item1, 100);
                    hw.motors["Motor1X"].WaitMotionDone(500);
                    mainWindow.mainFlow.Robot.Cmd("Load", loadingslot, "", false);
                    hw.motors["Motor1X"].MoveAbsolute(Config.Cfg.PickPos3.Item1, 100);
                    hw.motors["Motor1X"].WaitMotionDone(500);
                    mainWindow.mainFlow.Robot.Cmd("Dip", dipno.ToString(), "", false);

                });


            }
        }

        private void btnAutoRunUnload_Click(object sender, RoutedEventArgs e)
        {
            Task motortask, robottask;
            var loadingslot = tboxUnloadingNo.Text;
            var dipno = tboxUndipNo.Text;
            bool checkpos = false;
            int pos = 1;

            if (dipno == "10" || dipno == "11" || dipno == "12")
                pos = 2;
            else
            {
                checkpos = DiPos1.Any(s => dipno.Contains(s));
                if (checkpos)
                    pos = 1;
                else
                    pos = 2;
            }

            if (pos == 1)
            {
                motortask = Task.Run(() =>
                {
                    hw.motors["Motor1X"].MoveAbsolute(Config.Cfg.PickPos2.Item1, 100);
                    hw.motors["Motor1X"].WaitMotionDone(500);
                    mainWindow.mainFlow.Robot.Cmd("UnDip", dipno.ToString(), "", false);
                    hw.motors["Motor1X"].MoveAbsolute(Config.Cfg.PickPos1.Item1, 100);
                    hw.motors["Motor1X"].WaitMotionDone(500);
                    mainWindow.mainFlow.Robot.Cmd("UnLoad", loadingslot, "", false);
   
                });
            }

            else if (pos == 2)
            {
                motortask = Task.Run(() =>
                {
                    hw.motors["Motor1X"].MoveAbsolute(Config.Cfg.PickPos3.Item1, 100);
                    hw.motors["Motor1X"].WaitMotionDone(500);
                    mainWindow.mainFlow.Robot.Cmd("UnDip", dipno.ToString(), "", false);
                    hw.motors["Motor1X"].MoveAbsolute(Config.Cfg.PickPos1.Item1, 100);
                    hw.motors["Motor1X"].WaitMotionDone(500);
                    mainWindow.mainFlow.Robot.Cmd("UnLoad", loadingslot, "", false);

                });
            }
        }

        private void btnStopMotor_Click(object sender, RoutedEventArgs e)
        {
            hw.motors["Motor1X"].Stop();
        }

        private void btnMotorPos_Click(object sender, RoutedEventArgs e)
        {
            lblStatus.Content = "HOMING MOTOR";
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Actuator Manual Home Pressed");
            if (hw.inputs["X3_14_ROBOT_Z_AXIS_UP"].Read() == 0)
            {
                lblStatus.Content = "MOTOR HOME ERROR";
                return;
            }

            Task.Run(() =>
            {
                
                hw.motors["Motor1X"].Home(200);
                hw.motors["Motor1X"].WaitMotionDone(500);

                this.Dispatcher.Invoke(new Action(() =>
                {
                    var IOStatus = hw.motors["Motor1X"].GetIOStatus();

                    if (IOStatus.origin == 1)
                    {
                        btnMotorPos1.IsEnabled = true;
                        btnMotorPos2.IsEnabled = true;
                        btnMotorPos3.IsEnabled = true;
                        lblStatus.Content = "MOTOR HOME DONE";
                        MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Actuator Manual Home Done");
                    }
                    else
                    {
                        btnMotorPos1.IsEnabled = false;
                        btnMotorPos2.IsEnabled = false;
                        btnMotorPos3.IsEnabled = false;
                        lblStatus.Content = "MOTOR HOME ERROR";
                        MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Actuator Manual Home Error");
                    }
                }));
               

            });
        }

        private void btnStopRobot_Click(object sender, RoutedEventArgs e)
        {
            mainWindow.mainFlow.Robot.Cmd("Stop", "");
          
        }

        private void BTNSTOPTEST_Click(object sender, RoutedEventArgs e)
        {
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Actuator Manual Stop Pressed");
            hw.motors["Motor1X"].Stop();
            mainWindow.mainFlow.Robot.Cmd("Stop", "");
        }

        private void btnMotorPos1_Click(object sender, RoutedEventArgs e)
        {
            lblStatus.Content = "MOVING MOTOR POS1";
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Actuator Manual Move Pos1 Pressed");
            if (hw.inputs["X3_14_ROBOT_Z_AXIS_UP"].Read() == 0)
            {
                MyConfig.Cfg.Model.ErrorCode = "Alert,Acutator Manual Move-Robot Z-Axis Not In Pos";
                lblStatus.Content = "MOVE MOTOR ERROR";
                return;
            }

            Task.Run(() =>
            {
                hw.motors["Motor1X"].MoveAbsolute(Config.Cfg.PickPos1.Item1, 100);
                hw.motors["Motor1X"].WaitMotionDone(500);
                this.Dispatcher.Invoke(new Action(() =>
                {
                    lblStatus.Content = "MOTOR POS1";
                    MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Actuator Manual Move Pos1 Done");

                }));
            });
        }

        private void btnMotorPos2_Click(object sender, RoutedEventArgs e)
        {
            lblStatus.Content = "MOVING MOTOR POS2";
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Actuator Manual Move Pos2 Pressed");
            if (hw.inputs["X3_14_ROBOT_Z_AXIS_UP"].Read() == 0)
            {
                MyConfig.Cfg.Model.ErrorCode = "Alert,Acutator Manual Move-Robot Z-Axis Not In Pos";
                lblStatus.Content = "MOVE MOTOR ERROR";
                return;
            }
            Task.Run(() =>
            {
                hw.motors["Motor1X"].MoveAbsolute(Config.Cfg.PickPos2.Item1, 100);
                hw.motors["Motor1X"].WaitMotionDone(500);
                this.Dispatcher.Invoke(new Action(() =>
                {
                    lblStatus.Content = "MOTOR POS2";
                    MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Actuator Manual Move Pos2 Done");

                }));
            });
        }

        private void btnMotorPos3_Click(object sender, RoutedEventArgs e)
        {
            lblStatus.Content = "MOVING MOTOR POS1";
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Actuator Manual Move Pos3 Done");

            if (hw.inputs["X3_14_ROBOT_Z_AXIS_UP"].Read() == 0)
            {
                MyConfig.Cfg.Model.ErrorCode = "Alert,Acutator Manual Move-Robot Z-Axis Not In Pos";
                lblStatus.Content = "MOVE MOTOR ERROR";
                return;
            }
            Task.Run(() =>
            {
                hw.motors["Motor1X"].MoveAbsolute(Config.Cfg.PickPos3.Item1, 100);
                hw.motors["Motor1X"].WaitMotionDone(500);
                this.Dispatcher.Invoke(new Action(() =>
                {
                    lblStatus.Content = "MOTOR POS3";
                    MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Actuator Manual Move Pos3 Done");

                }));
            });
        }

        private void btnHomeRobot_Click(object sender, RoutedEventArgs e)
        {
            if (robothomeclick)
                return;

            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Robot Manual Home Pressed");
            if (hw.inputs["X3_14_ROBOT_Z_AXIS_UP"].Read() == 0)
            {
                MyConfig.Cfg.Model.ErrorCode = "Alert,Acutator Manual Move-Robot Z-Axis Not In Pos";
                lblStatus.Content = "MOVE MOTOR ERROR";
                return;
            }

            Task.Run(() =>
            {
                robothomeclick = true;
                mainWindow.mainFlow.Robot.ResetRobotIO(false);
                hw.outputs["Y5_13_ROBOT_HOME"].ON();
                var stat = MyConfig.Cfg.Model.robotControl.TimeOut(120, "X6_14_ROBOT_POSITION_RECEIVED", "Robot Home Error");
                hw.outputs["Y5_13_ROBOT_HOME"].OFF();
                if (stat == 1)
                {
                    this.Dispatcher.Invoke(new Action(() =>
                    {
                        lblStatus.Content = "Robot Home Error";

                    }));
                }
                robothomeclick = false;
            });
          
        }
        private void bntReserRobotIO(object sender, RoutedEventArgs e)
        {
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Robot Manual IO Reset Pressed");
            mainWindow.mainFlow.Robot.ResetRobotIO(true);
            MyConfig.Cfg.Model.ErrorCode = "Info, Robot IO Manual Reset";
        }


        private void btnRobotMove_Click(object sender, RoutedEventArgs e)
        {

        }


        private void btnRobotPage_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;

            tab_Main.SelectedIndex = int.Parse(btn.Tag.ToString());
        }

        private void btnJogPlus_Click(object sender, RoutedEventArgs e)
        {
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Actuator Manual Jog+ Pressed");
            if (hw.inputs["X3_14_ROBOT_Z_AXIS_UP"].Read() == 0)
            {
                MyConfig.Cfg.Model.ErrorCode = "Alert,Acutator Manual Move-Robot Z-Axis Not In Pos";
                lblStatus.Content = "MOVE MOTOR ERROR";
                return;
            }

            var distance = tboxJogPos.Text;
            var currentdistance = tboxMotorPos.Content;
            //CHECK disatance
            double _distance = double.Parse(distance);
            double Currentdistance = double.Parse(tboxMotorPos.Content.ToString());

            tboxCalculated.Text = (_distance + Currentdistance).ToString();
            if ((_distance + Currentdistance) >= MotorXMax)
            {
                MessageBox.Show("Invalid Jog Value");
                return;
            }

            Task.Run(()=>
            {
                Motor1X.ManualJogPlus(distance);
               
                var position = Motor1X.GetMotorPosition();
                this.Dispatcher.Invoke(new Action(() =>
                {
                    tboxMotorPos.Content = position;
                }));
               
            });
          
    
        }

        private void btnJogNeg_Click(object sender, RoutedEventArgs e)
        {
            MyConfig.Cfg.Model.MessageLogsTuple = Tuple.Create(DateTime.Now, "Actuator Manual Jog- Pressed");
            if (hw.inputs["X3_14_ROBOT_Z_AXIS_UP"].Read() == 0)
            {
                MyConfig.Cfg.Model.ErrorCode = "Alert,Acutator Manual Move-Robot Z-Axis Not In Pos";
                lblStatus.Content = "MOVE MOTOR ERROR";
                return;
            }

            var distance = tboxJogPos.Text;

            //CHECK disatance
            double _distance = double.Parse(distance);
            double Currentdistance = double.Parse(tboxMotorPos.Content.ToString());

            tboxCalculated.Text = (Currentdistance - _distance).ToString();

            if ((Currentdistance-_distance) <= MotorXMin)
            {
                MessageBox.Show("Invalid Jog Value");
                return;
            }

            Task.Run(() =>
            {
                Motor1X.ManualJogNeg(distance);
                var position = Motor1X.GetMotorPosition();
                this.Dispatcher.Invoke(new Action(() =>
                {
                    tboxMotorPos.Content = position;
                }));
            });
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex(@"[^0-9\-.]+");
            e.Handled = regex.IsMatch(e.Text);
        }

     
    }
}

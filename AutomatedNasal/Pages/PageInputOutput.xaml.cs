﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AutomatedNasal.Pages
{
    /// <summary>
    /// Interaction logic for PageInputOutput.xaml
    /// </summary>
    public partial class PageInputOutput : Page
    {
        MainWindow main;
        public PageInputOutput(MainWindow main)
        {
            InitializeComponent();
            this.main = main;

            inputControl.Init(main.hw.inputCards, main.hw.inputs);
            outputControl.Init(main.hw.outputCards, main.hw.outputs);
        }

        public void Start()
        {
            inputControl.Start();
            outputControl.Start();
        }

        public void Stop()
        {
            inputControl.Stop();
            outputControl.Stop();
        }
    }
}

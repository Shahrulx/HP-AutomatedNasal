﻿using AutomationLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AutomatedNasal.Pages
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        public int Stat { set; get; } = -1;

        public LoginWindow()
        {
            InitializeComponent();
        }

     
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Stat = -1;
            this.Close();
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            var username = tboxUserName.Text;
            var passsword = tboxPassword.Password;


            var log = MyConfig.Cfg.userManagement.UserList.Find(x => x.UserName == username && x.Password == passsword);
           
            if(log == null)
            {
               MessageBox.Show("Incorrect Username/Password");
                Stat = -1;
            }
            else
            {
                MyConfig.Cfg.Username = username;
                MessageBox.Show("Login Success");
                MyConfig.Cfg.Username = log.UserName;
                MyConfig.Cfg.UserType = log.Type;
                Stat = 1;
                this.Close();
            }

        }
    }
}

﻿#pragma checksum "..\..\..\Pages\PageMotor - Copy.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "D6913C178B326D4C9CE550B6DB3138F3EA7B495D"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using MachineUserControl;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace AutomatedNasal.Pages {
    
    
    /// <summary>
    /// PageMotor
    /// </summary>
    public partial class PageMotor : System.Windows.Controls.Page, System.Windows.Markup.IComponentConnector {
        
        
        #line 13 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnTab1;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnTab2;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnLoad_Copy;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnStopMotor;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnMotorPos;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnStopRobot;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TabControl tab_Main;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal MachineUserControl.MotorControl Motor1X;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal MachineUserControl.XYPositionTextBoxList PickPos1;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnSave;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal MachineUserControl.XYPositionTextBoxList PickPos2;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal MachineUserControl.XYPositionTextBoxList PickPos3;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid RobotPage;
        
        #line default
        #line hidden
        
        
        #line 54 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnLoad;
        
        #line default
        #line hidden
        
        
        #line 74 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button c_2;
        
        #line default
        #line hidden
        
        
        #line 75 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button c_3;
        
        #line default
        #line hidden
        
        
        #line 76 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button c_4;
        
        #line default
        #line hidden
        
        
        #line 77 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button c_5;
        
        #line default
        #line hidden
        
        
        #line 78 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button c_6;
        
        #line default
        #line hidden
        
        
        #line 79 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button c_1;
        
        #line default
        #line hidden
        
        
        #line 80 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button c_8;
        
        #line default
        #line hidden
        
        
        #line 81 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button c_9;
        
        #line default
        #line hidden
        
        
        #line 82 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button c_10;
        
        #line default
        #line hidden
        
        
        #line 83 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button c_11;
        
        #line default
        #line hidden
        
        
        #line 84 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button c_12;
        
        #line default
        #line hidden
        
        
        #line 85 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button c_7;
        
        #line default
        #line hidden
        
        
        #line 108 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Rectangle X6_05_ROBOT_LOAD_READY;
        
        #line default
        #line hidden
        
        
        #line 112 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Rectangle X6_06_ROBOT_DIP_INPOSITION;
        
        #line default
        #line hidden
        
        
        #line 116 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Rectangle X6_08_ROBOT_RISE_INPOSITION;
        
        #line default
        #line hidden
        
        
        #line 120 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Rectangle X6_09_ROBOT_RISE_READY;
        
        #line default
        #line hidden
        
        
        #line 124 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Rectangle X6_11_ROBOT_ROBOT_BUSY;
        
        #line default
        #line hidden
        
        
        #line 128 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Rectangle X6_13_ROBOT_ROBOT_ERROR;
        
        #line default
        #line hidden
        
        
        #line 132 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Rectangle X6_11_ROBOT_GRIPPER;
        
        #line default
        #line hidden
        
        
        #line 136 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Rectangle X6_13_ROBOT_CYLINDER;
        
        #line default
        #line hidden
        
        
        #line 140 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Rectangle X6_12_ROBOT_ROBOT_HAND_FULL;
        
        #line default
        #line hidden
        
        
        #line 144 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Rectangle X6_14_ROBOT_POSITION_RECEIVED;
        
        #line default
        #line hidden
        
        
        #line 148 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Rectangle X6_10_ROBOT_UNLOAD_READY;
        
        #line default
        #line hidden
        
        
        #line 152 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Rectangle X6_07_ROBOT_DIP_READY;
        
        #line default
        #line hidden
        
        
        #line 166 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Y5_11_ROBOT_TANK_LOCK_READY;
        
        #line default
        #line hidden
        
        
        #line 170 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Y5_12_ROBOT_TANK_UNLOCK_READY;
        
        #line default
        #line hidden
        
        
        #line 174 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Y5_13_ROBOT_HOME;
        
        #line default
        #line hidden
        
        
        #line 186 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Y6_07_ROBOT_ACTUATOR_INPUT;
        
        #line default
        #line hidden
        
        
        #line 210 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Y5_03_ROBOT_STOP;
        
        #line default
        #line hidden
        
        
        #line 214 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Y5_07_ROBOT_START;
        
        #line default
        #line hidden
        
        
        #line 221 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label tboxDipStat;
        
        #line default
        #line hidden
        
        
        #line 226 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tboxLoadingNo;
        
        #line default
        #line hidden
        
        
        #line 227 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnAutoRunLoad;
        
        #line default
        #line hidden
        
        
        #line 228 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tboxDipNo;
        
        #line default
        #line hidden
        
        
        #line 231 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tboxUnloadingNo;
        
        #line default
        #line hidden
        
        
        #line 232 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tboxUndipNo;
        
        #line default
        #line hidden
        
        
        #line 235 "..\..\..\Pages\PageMotor - Copy.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnAutoRunUnload;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/AutomatedNasal;component/pages/pagemotor%20-%20copy.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Pages\PageMotor - Copy.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 9 "..\..\..\Pages\PageMotor - Copy.xaml"
            ((AutomatedNasal.Pages.PageMotor)(target)).Unloaded += new System.Windows.RoutedEventHandler(this.Page_Unloaded);
            
            #line default
            #line hidden
            return;
            case 2:
            
            #line 10 "..\..\..\Pages\PageMotor - Copy.xaml"
            ((System.Windows.Controls.Grid)(target)).Loaded += new System.Windows.RoutedEventHandler(this.Grid_Loaded);
            
            #line default
            #line hidden
            
            #line 10 "..\..\..\Pages\PageMotor - Copy.xaml"
            ((System.Windows.Controls.Grid)(target)).Unloaded += new System.Windows.RoutedEventHandler(this.Grid_Unloaded);
            
            #line default
            #line hidden
            return;
            case 3:
            this.btnTab1 = ((System.Windows.Controls.Button)(target));
            return;
            case 4:
            this.btnTab2 = ((System.Windows.Controls.Button)(target));
            return;
            case 5:
            this.btnLoad_Copy = ((System.Windows.Controls.Button)(target));
            
            #line 15 "..\..\..\Pages\PageMotor - Copy.xaml"
            this.btnLoad_Copy.Click += new System.Windows.RoutedEventHandler(this.btnLoad_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            
            #line 16 "..\..\..\Pages\PageMotor - Copy.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.CellFunc);
            
            #line default
            #line hidden
            return;
            case 7:
            this.btnStopMotor = ((System.Windows.Controls.Button)(target));
            
            #line 17 "..\..\..\Pages\PageMotor - Copy.xaml"
            this.btnStopMotor.Click += new System.Windows.RoutedEventHandler(this.btnStopMotor_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.btnMotorPos = ((System.Windows.Controls.Button)(target));
            
            #line 18 "..\..\..\Pages\PageMotor - Copy.xaml"
            this.btnMotorPos.Click += new System.Windows.RoutedEventHandler(this.btnMotorPos_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.btnStopRobot = ((System.Windows.Controls.Button)(target));
            
            #line 19 "..\..\..\Pages\PageMotor - Copy.xaml"
            this.btnStopRobot.Click += new System.Windows.RoutedEventHandler(this.btnStopRobot_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            this.tab_Main = ((System.Windows.Controls.TabControl)(target));
            return;
            case 11:
            this.Motor1X = ((MachineUserControl.MotorControl)(target));
            return;
            case 12:
            this.PickPos1 = ((MachineUserControl.XYPositionTextBoxList)(target));
            return;
            case 13:
            this.btnSave = ((System.Windows.Controls.Button)(target));
            
            #line 39 "..\..\..\Pages\PageMotor - Copy.xaml"
            this.btnSave.Click += new System.Windows.RoutedEventHandler(this.btnSave_Click);
            
            #line default
            #line hidden
            return;
            case 14:
            this.PickPos2 = ((MachineUserControl.XYPositionTextBoxList)(target));
            return;
            case 15:
            this.PickPos3 = ((MachineUserControl.XYPositionTextBoxList)(target));
            return;
            case 16:
            this.RobotPage = ((System.Windows.Controls.Grid)(target));
            return;
            case 17:
            this.btnLoad = ((System.Windows.Controls.Button)(target));
            
            #line 54 "..\..\..\Pages\PageMotor - Copy.xaml"
            this.btnLoad.Click += new System.Windows.RoutedEventHandler(this.btnLoad_Click);
            
            #line default
            #line hidden
            return;
            case 18:
            
            #line 55 "..\..\..\Pages\PageMotor - Copy.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnLoad_Click);
            
            #line default
            #line hidden
            return;
            case 19:
            
            #line 56 "..\..\..\Pages\PageMotor - Copy.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnLoad_Click);
            
            #line default
            #line hidden
            return;
            case 20:
            
            #line 57 "..\..\..\Pages\PageMotor - Copy.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnLoad_Click);
            
            #line default
            #line hidden
            return;
            case 21:
            
            #line 58 "..\..\..\Pages\PageMotor - Copy.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnLoad_Click);
            
            #line default
            #line hidden
            return;
            case 22:
            
            #line 59 "..\..\..\Pages\PageMotor - Copy.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnLoad_Click);
            
            #line default
            #line hidden
            return;
            case 23:
            
            #line 60 "..\..\..\Pages\PageMotor - Copy.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnLoad_Click);
            
            #line default
            #line hidden
            return;
            case 24:
            this.c_2 = ((System.Windows.Controls.Button)(target));
            
            #line 74 "..\..\..\Pages\PageMotor - Copy.xaml"
            this.c_2.Click += new System.Windows.RoutedEventHandler(this.CellFunc);
            
            #line default
            #line hidden
            return;
            case 25:
            this.c_3 = ((System.Windows.Controls.Button)(target));
            
            #line 75 "..\..\..\Pages\PageMotor - Copy.xaml"
            this.c_3.Click += new System.Windows.RoutedEventHandler(this.CellFunc);
            
            #line default
            #line hidden
            return;
            case 26:
            this.c_4 = ((System.Windows.Controls.Button)(target));
            
            #line 76 "..\..\..\Pages\PageMotor - Copy.xaml"
            this.c_4.Click += new System.Windows.RoutedEventHandler(this.CellFunc);
            
            #line default
            #line hidden
            return;
            case 27:
            this.c_5 = ((System.Windows.Controls.Button)(target));
            
            #line 77 "..\..\..\Pages\PageMotor - Copy.xaml"
            this.c_5.Click += new System.Windows.RoutedEventHandler(this.CellFunc);
            
            #line default
            #line hidden
            return;
            case 28:
            this.c_6 = ((System.Windows.Controls.Button)(target));
            
            #line 78 "..\..\..\Pages\PageMotor - Copy.xaml"
            this.c_6.Click += new System.Windows.RoutedEventHandler(this.CellFunc);
            
            #line default
            #line hidden
            return;
            case 29:
            this.c_1 = ((System.Windows.Controls.Button)(target));
            
            #line 79 "..\..\..\Pages\PageMotor - Copy.xaml"
            this.c_1.Click += new System.Windows.RoutedEventHandler(this.CellFunc);
            
            #line default
            #line hidden
            return;
            case 30:
            this.c_8 = ((System.Windows.Controls.Button)(target));
            
            #line 80 "..\..\..\Pages\PageMotor - Copy.xaml"
            this.c_8.Click += new System.Windows.RoutedEventHandler(this.CellFunc);
            
            #line default
            #line hidden
            return;
            case 31:
            this.c_9 = ((System.Windows.Controls.Button)(target));
            
            #line 81 "..\..\..\Pages\PageMotor - Copy.xaml"
            this.c_9.Click += new System.Windows.RoutedEventHandler(this.CellFunc);
            
            #line default
            #line hidden
            return;
            case 32:
            this.c_10 = ((System.Windows.Controls.Button)(target));
            
            #line 82 "..\..\..\Pages\PageMotor - Copy.xaml"
            this.c_10.Click += new System.Windows.RoutedEventHandler(this.CellFunc);
            
            #line default
            #line hidden
            return;
            case 33:
            this.c_11 = ((System.Windows.Controls.Button)(target));
            
            #line 83 "..\..\..\Pages\PageMotor - Copy.xaml"
            this.c_11.Click += new System.Windows.RoutedEventHandler(this.CellFunc);
            
            #line default
            #line hidden
            return;
            case 34:
            this.c_12 = ((System.Windows.Controls.Button)(target));
            
            #line 84 "..\..\..\Pages\PageMotor - Copy.xaml"
            this.c_12.Click += new System.Windows.RoutedEventHandler(this.CellFunc);
            
            #line default
            #line hidden
            return;
            case 35:
            this.c_7 = ((System.Windows.Controls.Button)(target));
            
            #line 85 "..\..\..\Pages\PageMotor - Copy.xaml"
            this.c_7.Click += new System.Windows.RoutedEventHandler(this.CellFunc);
            
            #line default
            #line hidden
            return;
            case 36:
            
            #line 90 "..\..\..\Pages\PageMotor - Copy.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnLoad_Click);
            
            #line default
            #line hidden
            return;
            case 37:
            
            #line 91 "..\..\..\Pages\PageMotor - Copy.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnLoad_Click);
            
            #line default
            #line hidden
            return;
            case 38:
            
            #line 92 "..\..\..\Pages\PageMotor - Copy.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnLoad_Click);
            
            #line default
            #line hidden
            return;
            case 39:
            
            #line 93 "..\..\..\Pages\PageMotor - Copy.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnLoad_Click);
            
            #line default
            #line hidden
            return;
            case 40:
            
            #line 94 "..\..\..\Pages\PageMotor - Copy.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnLoad_Click);
            
            #line default
            #line hidden
            return;
            case 41:
            
            #line 95 "..\..\..\Pages\PageMotor - Copy.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnLoad_Click);
            
            #line default
            #line hidden
            return;
            case 42:
            
            #line 96 "..\..\..\Pages\PageMotor - Copy.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnLoad_Click);
            
            #line default
            #line hidden
            return;
            case 43:
            this.X6_05_ROBOT_LOAD_READY = ((System.Windows.Shapes.Rectangle)(target));
            return;
            case 44:
            this.X6_06_ROBOT_DIP_INPOSITION = ((System.Windows.Shapes.Rectangle)(target));
            return;
            case 45:
            this.X6_08_ROBOT_RISE_INPOSITION = ((System.Windows.Shapes.Rectangle)(target));
            return;
            case 46:
            this.X6_09_ROBOT_RISE_READY = ((System.Windows.Shapes.Rectangle)(target));
            return;
            case 47:
            this.X6_11_ROBOT_ROBOT_BUSY = ((System.Windows.Shapes.Rectangle)(target));
            return;
            case 48:
            this.X6_13_ROBOT_ROBOT_ERROR = ((System.Windows.Shapes.Rectangle)(target));
            return;
            case 49:
            this.X6_11_ROBOT_GRIPPER = ((System.Windows.Shapes.Rectangle)(target));
            return;
            case 50:
            this.X6_13_ROBOT_CYLINDER = ((System.Windows.Shapes.Rectangle)(target));
            return;
            case 51:
            this.X6_12_ROBOT_ROBOT_HAND_FULL = ((System.Windows.Shapes.Rectangle)(target));
            return;
            case 52:
            this.X6_14_ROBOT_POSITION_RECEIVED = ((System.Windows.Shapes.Rectangle)(target));
            return;
            case 53:
            this.X6_10_ROBOT_UNLOAD_READY = ((System.Windows.Shapes.Rectangle)(target));
            return;
            case 54:
            this.X6_07_ROBOT_DIP_READY = ((System.Windows.Shapes.Rectangle)(target));
            return;
            case 55:
            this.Y5_11_ROBOT_TANK_LOCK_READY = ((System.Windows.Controls.Button)(target));
            
            #line 166 "..\..\..\Pages\PageMotor - Copy.xaml"
            this.Y5_11_ROBOT_TANK_LOCK_READY.Click += new System.Windows.RoutedEventHandler(this.OutputFunc);
            
            #line default
            #line hidden
            return;
            case 56:
            this.Y5_12_ROBOT_TANK_UNLOCK_READY = ((System.Windows.Controls.Button)(target));
            
            #line 170 "..\..\..\Pages\PageMotor - Copy.xaml"
            this.Y5_12_ROBOT_TANK_UNLOCK_READY.Click += new System.Windows.RoutedEventHandler(this.OutputFunc);
            
            #line default
            #line hidden
            return;
            case 57:
            this.Y5_13_ROBOT_HOME = ((System.Windows.Controls.Button)(target));
            
            #line 174 "..\..\..\Pages\PageMotor - Copy.xaml"
            this.Y5_13_ROBOT_HOME.Click += new System.Windows.RoutedEventHandler(this.OutputFunc);
            
            #line default
            #line hidden
            return;
            case 58:
            
            #line 178 "..\..\..\Pages\PageMotor - Copy.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.OutputFunc);
            
            #line default
            #line hidden
            return;
            case 59:
            
            #line 182 "..\..\..\Pages\PageMotor - Copy.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.OutputFunc);
            
            #line default
            #line hidden
            return;
            case 60:
            this.Y6_07_ROBOT_ACTUATOR_INPUT = ((System.Windows.Controls.Button)(target));
            
            #line 186 "..\..\..\Pages\PageMotor - Copy.xaml"
            this.Y6_07_ROBOT_ACTUATOR_INPUT.Click += new System.Windows.RoutedEventHandler(this.OutputFunc);
            
            #line default
            #line hidden
            return;
            case 61:
            
            #line 190 "..\..\..\Pages\PageMotor - Copy.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.OutputFunc);
            
            #line default
            #line hidden
            return;
            case 62:
            
            #line 194 "..\..\..\Pages\PageMotor - Copy.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.OutputFunc);
            
            #line default
            #line hidden
            return;
            case 63:
            
            #line 198 "..\..\..\Pages\PageMotor - Copy.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.OutputFunc);
            
            #line default
            #line hidden
            return;
            case 64:
            
            #line 202 "..\..\..\Pages\PageMotor - Copy.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.OutputFunc);
            
            #line default
            #line hidden
            return;
            case 65:
            
            #line 206 "..\..\..\Pages\PageMotor - Copy.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.OutputFunc);
            
            #line default
            #line hidden
            return;
            case 66:
            this.Y5_03_ROBOT_STOP = ((System.Windows.Controls.Button)(target));
            
            #line 210 "..\..\..\Pages\PageMotor - Copy.xaml"
            this.Y5_03_ROBOT_STOP.Click += new System.Windows.RoutedEventHandler(this.OutputFunc);
            
            #line default
            #line hidden
            return;
            case 67:
            this.Y5_07_ROBOT_START = ((System.Windows.Controls.Button)(target));
            
            #line 214 "..\..\..\Pages\PageMotor - Copy.xaml"
            this.Y5_07_ROBOT_START.Click += new System.Windows.RoutedEventHandler(this.OutputFunc);
            
            #line default
            #line hidden
            return;
            case 68:
            
            #line 219 "..\..\..\Pages\PageMotor - Copy.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.CellFunc);
            
            #line default
            #line hidden
            return;
            case 69:
            
            #line 220 "..\..\..\Pages\PageMotor - Copy.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.CellFunc);
            
            #line default
            #line hidden
            return;
            case 70:
            this.tboxDipStat = ((System.Windows.Controls.Label)(target));
            return;
            case 71:
            this.tboxLoadingNo = ((System.Windows.Controls.TextBox)(target));
            return;
            case 72:
            this.btnAutoRunLoad = ((System.Windows.Controls.Button)(target));
            
            #line 227 "..\..\..\Pages\PageMotor - Copy.xaml"
            this.btnAutoRunLoad.Click += new System.Windows.RoutedEventHandler(this.btnAutoRunLoad_Click);
            
            #line default
            #line hidden
            return;
            case 73:
            this.tboxDipNo = ((System.Windows.Controls.TextBox)(target));
            return;
            case 74:
            this.tboxUnloadingNo = ((System.Windows.Controls.TextBox)(target));
            return;
            case 75:
            this.tboxUndipNo = ((System.Windows.Controls.TextBox)(target));
            return;
            case 76:
            this.btnAutoRunUnload = ((System.Windows.Controls.Button)(target));
            
            #line 235 "..\..\..\Pages\PageMotor - Copy.xaml"
            this.btnAutoRunUnload.Click += new System.Windows.RoutedEventHandler(this.btnAutoRunUnload_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

